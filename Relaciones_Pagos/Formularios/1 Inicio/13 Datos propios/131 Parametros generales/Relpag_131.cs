﻿using ClaseIntsa.Clases;
using ClaseIntsa.Controles;
using ClaseIntsa.Formularios;
using ClaseIntsa.Funciones;
using ClaseIntsa.Propiedades;
using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Relaciones_Pagos
{
    public partial class Relpag_131 : frmFormularioSimple
    {
        #region Propiedades

        /// <summary>
        /// Acción ejecutada actual
        /// </summary>
        public sbyte Accion_Actual { get; set; } = -1;

        #endregion Propiedades

        #region Carga del formulario

        /// <summary>
        ///  Inicialización de componentes y asignación cadena conexión
        /// </summary>
        public Relpag_131()
        {
            InitializeComponent();
            this.CadenaConexion = Variables_Globales.CadenaConexionAplicativo;
        }

        #endregion Carga del formulario

        #region Inicialización del formulario

        /// <summary>
        /// Carga formulario, inicialización de variables y preparación formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Relpag_131_Load(object sender, EventArgs e)
        {
            Combo4.Valor = "2020,EJERCICIO 2020";

            for (short i = 2021; i <= DateTime.Now.Year; i++)
                Combo4.Valor += $",{i},EJERCICIO {i}";

            Combo4.CargarCombo();
            Combo4.SelectedValue = Propiedades_Aplicativo.Ejercicio_Trabajo;

            Presenta_Datos();
            Accion_Actual = 0;
            Refresca_Panel();
            Boton1_1.Focus();
        }

        #endregion Inicialización del formulario

        #region Funciones

        /// <summary>
        /// Presenta los datos en el formulario
        /// </summary>
        private void Presenta_Datos()
        {
            AppConfig Configuracion = new AppConfig(Application.ExecutablePath);
            Tuple<int, string> Forma_Pago;

            Text1.Text = Configuracion.Leer("Relaciones_Pagos", "Directorio_Pdf").Trim();
            Text2.Text = Configuracion.Leer("Relaciones_Pagos", "Directorio_Modelo_997").Trim();
            Text3.Text = Configuracion.Leer("Relaciones_Pagos", "Directorio_Sepa").Trim();

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_8000 Temporal = new Relpag_8000(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);
                    Cadena_Conexion.Open();

                    Temporal.ide_8000 = 1;
                    Temporal.Cargar();

                    if (Temporal.ide_8000 == 0)
                        PresentaMensaje("PARÁMETROS GENERALES. NO DEFINIDOS", 2, Color.Red);
                    else
                    {
                        Forma_Pago = Funciones_Aplicativo.Identificador_Relpag_1005(CadenaConexion, Temporal.ide_1005);

                        Text5.Text = Forma_Pago.Item1.ToString();
                        Text5_1.Text = Forma_Pago.Item2.ToString();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Actualiza los datos del archivo de configuracion de la aplicación
        /// </summary>
        private void Actualizar_Parametros()
        {
            AppConfig Configuracion = new AppConfig(Application.ExecutablePath);

            Configuracion.Grabar("Relaciones_Pagos", "Directorio_Pdf", Text1.Text.Trim());
            Configuracion.Grabar("Relaciones_Pagos", "Directorio_Modelo_997", Text2.Text.Trim());
            Configuracion.Grabar("Relaciones_Pagos", "Directorio_Sepa", Text3.Text.Trim());

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_8000 Temporal = new Relpag_8000(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);
                    Cadena_Conexion.Open();

                    Temporal.ide_8000 = 1;
                    Temporal.eje_8000 = Convert.ToInt16(Combo4.SelectedValue);
                    Temporal.ide_1005 = Convert.ToInt32(Text5.Text);

                    Temporal.Actualizar();
                    Propiedades_Aplicativo.Ejercicio_Trabajo = Temporal.eje_8000;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion Funciones

        #region Funciones de manipulación de base de datos

        #endregion

        #region Botonera

        /// <- Refresca el panel superior de botones ->
        /// Parámetros
        private void Refresca_Panel()
        {
            Control[] Boton;
            btnBotonGeneral Boton_Panel;

            for (byte i = 0; i < Panel.Controls.Count; i++)
            {
                Boton = Panel.Controls.Find("Boton_" + Convert.ToString(i + 1), false);

                if (Boton.Length != 0)
                {
                    Boton_Panel = (btnBotonGeneral)Boton[0];
                    Boton_Panel.Refrescar();
                }
            }
        }

        /// <- Refresco botón -1 ->
        /// <- Boton_1_Refresco ->
        /// Parámetros
        /// <1 sender>
        /// <2 e>

        /// <summary>
        ///  Refresco botón de actualización datos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_1_Refresco(object sender, EventArgs e)
        {
            Boton_1.Enabled = (Accion_Actual == 0);
        }

        /// <summary>
        /// Ejecuta los procesos de actualización
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_1_Click(object sender, EventArgs e)
        {
            Accion_Actual = 2;
            Refresca_Panel();

            if (ValidarControles())
            {
                // DESACTIVAR CONTROLES
                Funciones_Genericas.Activar_Etiquetas("1", this, Color.DarkGray);
                Funciones_Genericas.Activar_Controles("1", this, false);

                Accion_Actual = 1;
                Actualizar_Parametros();
                Refresca_Panel();
                imgIntsa.Focus();
            }
            else
            {
                Accion_Actual = 0;
                Refresca_Panel();
                ControlFoco.Focus();
            }
        }

        #endregion Botonera

        #region Campos formulario

        /// <summary>
        /// Selección directorio de salida archivos 'PDF'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton1_1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog Examinar = new FolderBrowserDialog
            {
                Description = "Seleccionar directorio archivos 'PDF'"
            };

            if (Text1.Text.Trim() == string.Empty)
                Examinar.RootFolder = Environment.SpecialFolder.MyComputer;

            DialogResult Resultado = Examinar.ShowDialog();

            if (Resultado == DialogResult.OK)
            {
                Text1.Text = Examinar.SelectedPath;
            }
            else
            {
                if (Text1.Text != string.Empty)
                    Text1.Text = "";
            }
        }

        /// <summary>
        /// Refresco el boton de selección de directorio  'PDF'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton1_1_Refresco(object sender, EventArgs e)
        {
            Boton1_1.Enabled = (Accion_Actual == 0);
        }

        /// <summary>
        /// Validación directorio de salida archivos 'PDF'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text1_Validating(object sender, CancelEventArgs e)
        {
            if (Text1.Text.Trim() == string.Empty)
            {
                PresentaMensaje("DIRECTORIO DE  ARCHIVOS 'PDF'. OBLIGATORIO", 2, Color.Red);
                ControlFoco = Text1;
                e.Cancel = true;
            }
            else
            {
                if (!Directory.Exists(Text1.Text.Trim()))
                {
                    PresentaMensaje("DIRECTORIO DE  ARCHIVOS 'PDF'. INEXISTENTE", 2, Color.Red);
                    ControlFoco = Text1;
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Selección  del directorio archivos 'MODELO-997'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton1_2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog Examinar = new FolderBrowserDialog
            {
                Description = "Seleccionar directorio de archivos 'MODELO-997'"
            };

            if (Text2.Text.Trim() == string.Empty)
                Examinar.RootFolder = Environment.SpecialFolder.MyComputer;

            DialogResult Resultado = Examinar.ShowDialog();

            if (Resultado == DialogResult.OK)
            {
                Text2.Text = Examinar.SelectedPath;
            }
            else
            {
                if (Text2.Text != string.Empty)
                    Text2.Text = "";
            }
        }

        /// <summary>
        /// Refresco el boton de selección del directorio archivos 'MODELO-997'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton1_2_Refresco(object sender, EventArgs e)
        {
            Boton1_3.Enabled = (Accion_Actual == 0);
        }

        /// <summary>
        /// Validación directorio de archivos 'MODELO-997'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text2_Validating(object sender, CancelEventArgs e)
        {
            if (Text2.Text.Trim() == string.Empty)
            {
                PresentaMensaje("DIRECTORIO DE ARCHIVOS MODELO-997. OBLIGATORIO", 2, Color.Red);
                ControlFoco = Text2;
                e.Cancel = true;
            }
            else
            {
                if (!Directory.Exists(Text2.Text.Trim()))
                {
                    PresentaMensaje("DIRECTORIO DE ARCHIVOS MODELO-997. INEXISTENTE", 2, Color.Red);
                    ControlFoco = Text2;
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Selección  del directoriode archivos 'SEPA'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton1_3_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog Examinar = new FolderBrowserDialog
            {
                Description = "Seleccionar directorio de archivos 'SEPA'"
            };

            if (Text3.Text.Trim() == string.Empty)
                Examinar.RootFolder = Environment.SpecialFolder.MyComputer;

            DialogResult Resultado = Examinar.ShowDialog();

            if (Resultado == DialogResult.OK)
            {
                Text3.Text = Examinar.SelectedPath;
            }
            else
            {
                if (Text3.Text != string.Empty)
                    Text3.Text = "";
            }
        }

        /// <summary>
        /// Refresco el boton de selección del directorio archivos SEPA
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton1_3_Refresco(object sender, EventArgs e)
        {
            Boton1_3.Enabled = (Accion_Actual == 0);
        }

        /// <summary>
        /// Validación directorio archivos SEPA
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text3_Validating(object sender, CancelEventArgs e)
        {
            if (Text3.Text.Trim() == string.Empty)
            {
                PresentaMensaje("DIRECTORIO DE ARCHIVOS 'SEPA'. OBLIGATORIO", 2, Color.Red);
                ControlFoco = Text3;
                e.Cancel = true;
            }
            else
            {
                if (!Directory.Exists(Text3.Text.Trim()))
                {
                    PresentaMensaje("DIRECTORIO DE ARCHIVOS 'SEPA'. INEXISTENTE", 2, Color.Red);
                    ControlFoco = Text3;
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Validación de la forma de pago (Transferencia)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text5_Validating(object sender, CancelEventArgs e)
        {
            Tuple<int, string> Forma_Pago;

            if (Text5.Text == string.Empty)
            {
                PresentaMensaje("FORMA DE PAGO. TRANSFERENCIA. OBLIGATORIO", 2, Color.DarkGreen);
                Text5_1.Text = "";
            }
            else
            {
                if (Funciones_Genericas.Solo_Numeros(Text5.Text))
                {
                    Forma_Pago = Funciones_Aplicativo.Identificador_Relpag_1005(CadenaConexion, Convert.ToInt32(Text5.Text));

                    if (Forma_Pago.Item1 == 0)
                    {
                        PresentaMensaje("FORMA DE PAGO. TRANSFERENCIA [" + Text5.Text + "]. INEXISTENTE", 2, Color.Red);
                        ControlFoco = Text5;
                        Text5_1.Text = "";
                        e.Cancel = true;
                    }
                    else
                    {
                        if (Text5.Cambio_Valor)
                            Text5_1.Text = Forma_Pago.Item2.Trim();
                    }
                }
                else
                {
                    PresentaMensaje("FORMA DE PAGO. TRANSFERENCIA. FORMATO ERRÓNEO (0 a 9).", 2, Color.DarkGreen);
                    ControlFoco = Text5;
                    Text5_1.Text = "";
                    e.Cancel = true;
                }

            }
        }

        #endregion Campos formulario

        #region Cierre formulario

        #endregion

        //
    }
    //
}
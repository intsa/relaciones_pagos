﻿namespace Relaciones_Pagos
{
    partial class Relpag_131
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Relpag_131));
            this.Panel = new System.Windows.Forms.Panel();
            this.Boton_1 = new ClaseIntsa.Controles.btnBotonGeneral(this.components);
            this.Text1 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Label1 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Boton1_1 = new ClaseIntsa.Controles.btnBotonGeneral(this.components);
            this.Text2 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Label2 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Boton1_2 = new ClaseIntsa.Controles.btnBotonGeneral(this.components);
            this.Text3 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Label3 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Boton1_3 = new ClaseIntsa.Controles.btnBotonGeneral(this.components);
            this.Text5 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Text5_1 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Label5 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label4 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Combo4 = new ClaseIntsa.Controles.cmbComboGeneral(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.imgIntsa)).BeginInit();
            this.Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Text1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text5_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            this.SuspendLayout();
            // 
            // imgIntsa
            // 
            this.imgIntsa.Location = new System.Drawing.Point(620, 30);
            this.imgIntsa.Margin = new System.Windows.Forms.Padding(4);
            this.imgIntsa.Size = new System.Drawing.Size(170, 50);
            // 
            // cmbVista
            // 
            this.cmbVista.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbVista.BackColor = System.Drawing.Color.White;
            this.cmbVista.DisplayMember = "DESCRIPCION";
            this.cmbVista.Location = new System.Drawing.Point(-130, -20);
            this.cmbVista.Size = new System.Drawing.Size(130, 23);
            this.cmbVista.TabIndex = 2;
            this.cmbVista.ValueMember = "ID";
            this.cmbVista.Visible = false;
            // 
            // lblMensajes
            // 
            this.lblMensajes.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.lblMensajes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMensajes.Size = new System.Drawing.Size(800, 20);
            // 
            // BarraProgreso
            // 
            this.BarraProgreso.Location = new System.Drawing.Point(640, 0);
            this.BarraProgreso.Margin = new System.Windows.Forms.Padding(4);
            this.BarraProgreso.Size = new System.Drawing.Size(160, 20);
            // 
            // Panel
            // 
            this.Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Panel.BackColor = System.Drawing.Color.White;
            this.Panel.Controls.Add(this.Boton_1);
            this.Panel.Font = new System.Drawing.Font("Calibri", 10F);
            this.Panel.ForeColor = System.Drawing.Color.Transparent;
            this.Panel.Location = new System.Drawing.Point(210, 30);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(400, 50);
            this.Panel.TabIndex = 9;
            this.Panel.TabStop = true;
            // 
            // Boton_1
            // 
            this.Boton_1.FlatAppearance.BorderSize = 0;
            this.Boton_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton_1.Font = new System.Drawing.Font("Calibri", 10F);
            this.Boton_1.Image = ((System.Drawing.Image)(resources.GetObject("Boton_1.Image")));
            this.Boton_1.ImagenDeshabilitado = ((System.Drawing.Image)(resources.GetObject("Boton_1.ImagenDeshabilitado")));
            this.Boton_1.ImagenFoco = ((System.Drawing.Image)(resources.GetObject("Boton_1.ImagenFoco")));
            this.Boton_1.ImagenNormal = ((System.Drawing.Image)(resources.GetObject("Boton_1.ImagenNormal")));
            this.Boton_1.Location = new System.Drawing.Point(0, 0);
            this.Boton_1.Name = "Boton_1";
            this.Boton_1.Size = new System.Drawing.Size(50, 50);
            this.Boton_1.TabIndex = 0;
            this.ttpFormulario.SetToolTip(this.Boton_1, "Actualizar registro");
            this.Boton_1.UseVisualStyleBackColor = false;
            this.Boton_1.Refresco += new System.EventHandler(this.Boton_1_Refresco);
            this.Boton_1.Click += new System.EventHandler(this.Boton_1_Click);
            // 
            // Text1
            // 
            this.Text1._TabStop = false;
            this.Text1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Text1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Text1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Text1.BackColor = System.Drawing.Color.White;
            this.Text1.BusquedaF8 = false;
            this.Text1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Text1.Datos_Auxiliares = "1";
            this.Text1.Depth = 0;
            this.Text1.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text1.GridNumero = ((byte)(0));
            this.Text1.Location = new System.Drawing.Point(65, 115);
            this.Text1.MaxLength = 1000;
            this.Text1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text1.Multiline = false;
            this.Text1.Name = "Text1";
            this.Text1.PasswordChar = '\0';
            this.Text1.ReadOnly = false;
            this.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Text1.SelectedText = "";
            this.Text1.SelectionLength = 0;
            this.Text1.SelectionStart = 0;
            this.Text1.Size = new System.Drawing.Size(730, 23);
            this.Text1.TabIndex = 1;
            this.Text1.TabStop = false;
            this.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Text1.TextoMayusculas = false;
            this.ttpFormulario.SetToolTip(this.Text1, "Descripción ubicación");
            this.Text1.UseSystemPasswordChar = false;
            this.Text1.Validating += new System.ComponentModel.CancelEventHandler(this.Text1_Validating);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.BackColor = System.Drawing.Color.White;
            this.Label1.Depth = 0;
            this.Label1.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label1.FormatoCorporativo = false;
            this.Label1.Location = new System.Drawing.Point(65, 90);
            this.Label1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(160, 15);
            this.Label1.TabIndex = 466;
            this.Label1.Text = "Directorio de archivos \'PDF\'";
            this.ttpFormulario.SetToolTip(this.Label1, "Directorio de salida archivos \'PDF\' de notificaciones y licencias");
            // 
            // Boton1_1
            // 
            this.Boton1_1.Datos_Auxiliares = "1";
            this.Boton1_1.FlatAppearance.BorderSize = 0;
            this.Boton1_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton1_1.Font = new System.Drawing.Font("Calibri", 10F);
            this.Boton1_1.Image = ((System.Drawing.Image)(resources.GetObject("Boton1_1.Image")));
            this.Boton1_1.ImagenDeshabilitado = ((System.Drawing.Image)(resources.GetObject("Boton1_1.ImagenDeshabilitado")));
            this.Boton1_1.ImagenFoco = ((System.Drawing.Image)(resources.GetObject("Boton1_1.ImagenFoco")));
            this.Boton1_1.ImagenNormal = ((System.Drawing.Image)(resources.GetObject("Boton1_1.ImagenNormal")));
            this.Boton1_1.Location = new System.Drawing.Point(5, 90);
            this.Boton1_1.Name = "Boton1_1";
            this.Boton1_1.Size = new System.Drawing.Size(50, 50);
            this.Boton1_1.TabIndex = 0;
            this.ttpFormulario.SetToolTip(this.Boton1_1, "Seleccionar directorio");
            this.Boton1_1.UseVisualStyleBackColor = false;
            this.Boton1_1.Refresco += new System.EventHandler(this.Boton1_1_Refresco);
            this.Boton1_1.Click += new System.EventHandler(this.Boton1_1_Click);
            // 
            // Text2
            // 
            this.Text2._TabStop = false;
            this.Text2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Text2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Text2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Text2.BackColor = System.Drawing.Color.White;
            this.Text2.BusquedaF8 = false;
            this.Text2.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Text2.Datos_Auxiliares = "1";
            this.Text2.Depth = 0;
            this.Text2.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text2.GridNumero = ((byte)(0));
            this.Text2.Location = new System.Drawing.Point(65, 175);
            this.Text2.MaxLength = 1000;
            this.Text2.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text2.Multiline = false;
            this.Text2.Name = "Text2";
            this.Text2.PasswordChar = '\0';
            this.Text2.ReadOnly = false;
            this.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Text2.SelectedText = "";
            this.Text2.SelectionLength = 0;
            this.Text2.SelectionStart = 0;
            this.Text2.Size = new System.Drawing.Size(730, 23);
            this.Text2.TabIndex = 3;
            this.Text2.TabStop = false;
            this.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Text2.TextoMayusculas = false;
            this.ttpFormulario.SetToolTip(this.Text2, "Descripción ubicación");
            this.Text2.UseSystemPasswordChar = false;
            this.Text2.Validating += new System.ComponentModel.CancelEventHandler(this.Text2_Validating);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.BackColor = System.Drawing.Color.White;
            this.Label2.Depth = 0;
            this.Label2.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label2.FormatoCorporativo = false;
            this.Label2.Location = new System.Drawing.Point(65, 150);
            this.Label2.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(206, 15);
            this.Label2.TabIndex = 472;
            this.Label2.Text = "Directorio de archivos \'Modelo-997\'\r\n";
            this.ttpFormulario.SetToolTip(this.Label2, "Directorio de salida archivos Modelo-997");
            // 
            // Boton1_2
            // 
            this.Boton1_2.Datos_Auxiliares = "1";
            this.Boton1_2.FlatAppearance.BorderSize = 0;
            this.Boton1_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton1_2.Font = new System.Drawing.Font("Calibri", 10F);
            this.Boton1_2.Image = ((System.Drawing.Image)(resources.GetObject("Boton1_2.Image")));
            this.Boton1_2.ImagenDeshabilitado = ((System.Drawing.Image)(resources.GetObject("Boton1_2.ImagenDeshabilitado")));
            this.Boton1_2.ImagenFoco = ((System.Drawing.Image)(resources.GetObject("Boton1_2.ImagenFoco")));
            this.Boton1_2.ImagenNormal = ((System.Drawing.Image)(resources.GetObject("Boton1_2.ImagenNormal")));
            this.Boton1_2.Location = new System.Drawing.Point(5, 150);
            this.Boton1_2.Name = "Boton1_2";
            this.Boton1_2.Size = new System.Drawing.Size(50, 50);
            this.Boton1_2.TabIndex = 2;
            this.ttpFormulario.SetToolTip(this.Boton1_2, "Seleccionar directorio");
            this.Boton1_2.UseVisualStyleBackColor = false;
            this.Boton1_2.Refresco += new System.EventHandler(this.Boton1_2_Refresco);
            this.Boton1_2.Click += new System.EventHandler(this.Boton1_2_Click);
            // 
            // Text3
            // 
            this.Text3._TabStop = false;
            this.Text3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Text3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Text3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Text3.BackColor = System.Drawing.Color.White;
            this.Text3.BusquedaF8 = false;
            this.Text3.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Text3.Datos_Auxiliares = "1";
            this.Text3.Depth = 0;
            this.Text3.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text3.GridNumero = ((byte)(0));
            this.Text3.Location = new System.Drawing.Point(65, 235);
            this.Text3.MaxLength = 1000;
            this.Text3.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text3.Multiline = false;
            this.Text3.Name = "Text3";
            this.Text3.PasswordChar = '\0';
            this.Text3.ReadOnly = false;
            this.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Text3.SelectedText = "";
            this.Text3.SelectionLength = 0;
            this.Text3.SelectionStart = 0;
            this.Text3.Size = new System.Drawing.Size(730, 23);
            this.Text3.TabIndex = 5;
            this.Text3.TabStop = false;
            this.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Text3.TextoMayusculas = false;
            this.ttpFormulario.SetToolTip(this.Text3, "Descripción ubicación");
            this.Text3.UseSystemPasswordChar = false;
            this.Text3.Validating += new System.ComponentModel.CancelEventHandler(this.Text3_Validating);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.BackColor = System.Drawing.Color.White;
            this.Label3.Depth = 0;
            this.Label3.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label3.FormatoCorporativo = false;
            this.Label3.Location = new System.Drawing.Point(65, 210);
            this.Label3.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(164, 15);
            this.Label3.TabIndex = 475;
            this.Label3.Text = "Directorio de archivos \'SEPA\'\r\n";
            this.ttpFormulario.SetToolTip(this.Label3, "Directorio de salida archivos Modelo-997");
            // 
            // Boton1_3
            // 
            this.Boton1_3.Datos_Auxiliares = "1";
            this.Boton1_3.FlatAppearance.BorderSize = 0;
            this.Boton1_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton1_3.Font = new System.Drawing.Font("Calibri", 10F);
            this.Boton1_3.Image = ((System.Drawing.Image)(resources.GetObject("Boton1_3.Image")));
            this.Boton1_3.ImagenDeshabilitado = ((System.Drawing.Image)(resources.GetObject("Boton1_3.ImagenDeshabilitado")));
            this.Boton1_3.ImagenFoco = ((System.Drawing.Image)(resources.GetObject("Boton1_3.ImagenFoco")));
            this.Boton1_3.ImagenNormal = ((System.Drawing.Image)(resources.GetObject("Boton1_3.ImagenNormal")));
            this.Boton1_3.Location = new System.Drawing.Point(5, 210);
            this.Boton1_3.Name = "Boton1_3";
            this.Boton1_3.Size = new System.Drawing.Size(50, 50);
            this.Boton1_3.TabIndex = 4;
            this.ttpFormulario.SetToolTip(this.Boton1_3, "Seleccionar directorio");
            this.Boton1_3.UseVisualStyleBackColor = false;
            this.Boton1_3.Refresco += new System.EventHandler(this.Boton1_3_Refresco);
            this.Boton1_3.Click += new System.EventHandler(this.Boton1_3_Click);
            // 
            // Text5
            // 
            this.Text5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Text5.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Text5.BackColor = System.Drawing.Color.White;
            this.Text5.BusquedaF8 = true;
            this.Text5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Text5.Datos_Auxiliares = "1";
            this.Text5.Depth = 0;
            this.Text5.F8_AplicarFormato = true;
            this.Text5.F8_SQLInicial = "SELECT ide_1005[Código], den_1005 [Denominación] FROM dbo.Relpag_1005";
            this.Text5.F8_Titulo = "Selección formas de pago (Transferencias)";
            this.Text5.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text5.ForeColor = System.Drawing.Color.Green;
            this.Text5.GridNumero = ((byte)(0));
            this.Text5.Location = new System.Drawing.Point(151, 290);
            this.Text5.MaxLength = 10;
            this.Text5.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text5.Multiline = false;
            this.Text5.Name = "Text5";
            this.Text5.PasswordChar = '\0';
            this.Text5.ReadOnly = false;
            this.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Text5.SelectedText = "";
            this.Text5.SelectionLength = 0;
            this.Text5.SelectionStart = 0;
            this.Text5.Size = new System.Drawing.Size(75, 23);
            this.Text5.TabIndex = 7;
            this.Text5.TabStop = false;
            this.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ttpFormulario.SetToolTip(this.Text5, "Concepto retributivo (Seguridad social)");
            this.Text5.UseSystemPasswordChar = false;
            this.Text5.Validating += new System.ComponentModel.CancelEventHandler(this.Text5_Validating);
            // 
            // Text5_1
            // 
            this.Text5_1._TabStop = false;
            this.Text5_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Text5_1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Text5_1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Text5_1.BackColor = System.Drawing.Color.White;
            this.Text5_1.BusquedaF8 = false;
            this.Text5_1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Text5_1.Datos_Auxiliares = "1";
            this.Text5_1.Depth = 0;
            this.Text5_1.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text5_1.ForeColor = System.Drawing.Color.Green;
            this.Text5_1.GridNumero = ((byte)(0));
            this.Text5_1.Location = new System.Drawing.Point(236, 290);
            this.Text5_1.MaxLength = 200;
            this.Text5_1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text5_1.Multiline = false;
            this.Text5_1.Name = "Text5_1";
            this.Text5_1.PasswordChar = '\0';
            this.Text5_1.ReadOnly = true;
            this.Text5_1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Text5_1.SelectedText = "";
            this.Text5_1.SelectionLength = 0;
            this.Text5_1.SelectionStart = 0;
            this.Text5_1.Size = new System.Drawing.Size(550, 23);
            this.Text5_1.TabIndex = 8;
            this.Text5_1.TabStop = false;
            this.Text5_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Text5_1.TextoMayusculas = false;
            this.ttpFormulario.SetToolTip(this.Text5_1, "Denominación concepto retributivo (Seguridad social)");
            this.Text5_1.UseSystemPasswordChar = false;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.BackColor = System.Drawing.Color.White;
            this.Label5.Depth = 0;
            this.Label5.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label5.FormatoCorporativo = false;
            this.Label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label5.Location = new System.Drawing.Point(151, 265);
            this.Label5.Margin = new System.Windows.Forms.Padding(0);
            this.Label5.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(173, 15);
            this.Label5.TabIndex = 480;
            this.Label5.Text = "Forma de pago-Transferencias";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.BackColor = System.Drawing.Color.Transparent;
            this.Label4.CausesValidation = false;
            this.Label4.Depth = 0;
            this.Label4.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label4.FormatoCorporativo = false;
            this.Label4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label4.Location = new System.Drawing.Point(8, 265);
            this.Label4.Margin = new System.Windows.Forms.Padding(0);
            this.Label4.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(115, 15);
            this.Label4.TabIndex = 637;
            this.Label4.Tag = "";
            this.Label4.Text = "Ejercicio de trabajo";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Combo4
            // 
            this.Combo4.BackColor = System.Drawing.Color.White;
            this.Combo4.Datos_Auxiliares = "1";
            this.Combo4.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.Combo4.DropDownHeight = 105;
            this.Combo4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Combo4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Combo4.Font = new System.Drawing.Font("Calibri", 11F);
            this.Combo4.ForeColor = System.Drawing.Color.Transparent;
            this.Combo4.FormattingEnabled = true;
            this.Combo4.GridNumero = ((byte)(0));
            this.Combo4.IntegralHeight = false;
            this.Combo4.ItemHeight = 17;
            this.Combo4.Location = new System.Drawing.Point(3, 290);
            this.Combo4.MaxDropDownItems = 10;
            this.Combo4.Name = "Combo4";
            this.Combo4.Size = new System.Drawing.Size(140, 23);
            this.Combo4.TabIndex = 6;
            this.ttpFormulario.SetToolTip(this.Combo4, "Tipo de titularidad");
            this.Combo4.ValorInicial = "1";
            // 
            // Relpag_131
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(800, 340);
            this.Controls.Add(this.Combo4);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Text5);
            this.Controls.Add(this.Text5_1);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Text3);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Boton1_3);
            this.Controls.Add(this.Text2);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Boton1_2);
            this.Controls.Add(this.Text1);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Boton1_1);
            this.Controls.Add(this.Panel);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Relpag_131";
            this.Text = "Parámetros generales";
            this.Load += new System.EventHandler(this.Relpag_131_Load);
            this.Controls.SetChildIndex(this.Panel, 0);
            this.Controls.SetChildIndex(this.cmbVista, 0);
            this.Controls.SetChildIndex(this.imgIntsa, 0);
            this.Controls.SetChildIndex(this.Boton1_1, 0);
            this.Controls.SetChildIndex(this.Label1, 0);
            this.Controls.SetChildIndex(this.Text1, 0);
            this.Controls.SetChildIndex(this.Boton1_2, 0);
            this.Controls.SetChildIndex(this.Label2, 0);
            this.Controls.SetChildIndex(this.Text2, 0);
            this.Controls.SetChildIndex(this.Boton1_3, 0);
            this.Controls.SetChildIndex(this.Label3, 0);
            this.Controls.SetChildIndex(this.Text3, 0);
            this.Controls.SetChildIndex(this.Label5, 0);
            this.Controls.SetChildIndex(this.Text5_1, 0);
            this.Controls.SetChildIndex(this.Text5, 0);
            this.Controls.SetChildIndex(this.Label4, 0);
            this.Controls.SetChildIndex(this.Combo4, 0);
            ((System.ComponentModel.ISupportInitialize)(this.imgIntsa)).EndInit();
            this.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Text1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text5_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel Panel;
        private ClaseIntsa.Controles.btnBotonGeneral Boton_1;
        private ClaseIntsa.Controles.txtTextoGeneral Text1;
        private ClaseIntsa.Controles.lblLabelGeneral Label1;
        private ClaseIntsa.Controles.btnBotonGeneral Boton1_1;
        private ClaseIntsa.Controles.txtTextoGeneral Text2;
        private ClaseIntsa.Controles.lblLabelGeneral Label2;
        private ClaseIntsa.Controles.btnBotonGeneral Boton1_2;
        private ClaseIntsa.Controles.txtTextoGeneral Text3;
        private ClaseIntsa.Controles.lblLabelGeneral Label3;
        private ClaseIntsa.Controles.btnBotonGeneral Boton1_3;
        private ClaseIntsa.Controles.txtTextoGeneral Text5;
        private ClaseIntsa.Controles.txtTextoGeneral Text5_1;
        private ClaseIntsa.Controles.lblLabelGeneral Label5;
        private ClaseIntsa.Controles.lblLabelGeneral Label4;
        private ClaseIntsa.Controles.cmbComboGeneral Combo4;
    }
}
﻿using ClaseIntsa.Clases;
using ClaseIntsa.Controles;
using ClaseIntsa.Formularios;
using ClaseIntsa.Funciones;
using ClaseIntsa.Interfaces;
using ClaseIntsa.Propiedades;
using Microsoft.Reporting.WinForms;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Relaciones_Pagos
{
    public partial class Relpag_221 : frmFormularioSimple, itf_Busquedas, ITf_Relpag
    {
        #region Interface

        /// <summary>
        /// Devolución del formulario general (ITf_Relpag)
        /// </summary>
        /// <param name="Cuadricula">Cuadrículas del formulario</param>
        /// <param name="Identificador">Identificadores de registro de las cuadrículas</param>
        /// <param name="Cuadricula_Activa">Cuadrícula que va a recibir el foco</param>
        public void Retorno_Formulario(grdGridConsulta[] Cuadricula, int?[] Identificador, byte Cuadricula_Activa = 0)
        {
            for (short i = 0; i < Cuadricula.Length; i++)
            {
                if (Identificador[i] < 0)
                {
                    Id_Cuadricula[i] = 0;
                    Cuadricula[i].CargarGrid((int)Identificador[i]);
                }
                else
                {
                    if (Identificador[i] != 0)
                    {
                        Id_Cuadricula[i] = 0;
                        Cuadricula[i].CargarGrid((int)Identificador[i]);
                    }
                }
            }

            Cuadricula[Cuadricula_Activa].Select();
        }

        /// <summary>
        /// Devolución del formulario búsquedas (itf_Busquedas)
        /// </summary>
        /// <param name="Tabla_Busquedas">Tabla de búsquedas formulario búsquedas</param>
        /// <param name="Busquedas_Sql">Matriz de cadenas SQL de cada cuadrícula</param>
        public void _Devolucion_Formulario(DataTable Tabla_Busquedas, string[] Busquedas_Sql)
        {
            if (Busquedas_Sql[0] != string.Empty)
            {
                for (byte i = 0; i < Id_Cuadricula.Length; i++)
                    Id_Cuadricula[i] = 0;

                for (byte i = 0; i < Busquedas_Sql.Length; i++)
                    _Sql_Busquedas[i] = Busquedas_Sql[i];

            }

            GrdGridConsulta1.SQLInicial = Sql_Inicial_Cuadricula_1(_Sql_Busquedas[0]);
            GrdGridConsulta1.CargarGrid(0);
            Funciones_Genericas._Numerador_Registros(GrdGridConsulta1, Label_1_2);

            if (GrdGridConsulta1.Rows.Count == 0)
            {
                GrdGridConsulta2.SQLInicial = Sql_Inicial_Cuadricula_2(0, _Sql_Busquedas[1]);
                GrdGridConsulta2.CargarGrid(0);
                Funciones_Genericas._Numerador_Registros(GrdGridConsulta2, Label_2_2);
            }

            Refresca_Panel();
        }

        #endregion

        #region Propiedades

        /// <summary>
        /// Matriz de tablas de criterios de búsqueda
        /// </summary>
        private DataTable _Tabla_Busquedas = new DataTable();

        /// <summary>
        /// Matriz de cadenas SQL criterios de búsqueda
        /// </summary>
        private string[] _Sql_Busquedas = new string[2];

        /// <summary>
        /// Matriz de identificadores actuales de cada cuadrícula
        /// </summary>
        private int[] Id_Cuadricula = new int[2];

        /// <summary>
        /// Matriz de etiquetas representativas de cada cuadrícula
        /// </summary>
        private lblLabelGeneral[] Etiquetas = new lblLabelGeneral[2];

        /// <summary>
        /// Semaforo archivo generado 
        /// </summary>
        public byte M997_Generado { get; set; } = 0;

        /// <summary>
        /// Acción actual
        /// </summary>
        public sbyte Accion_Actual { get; set; } = -1;

        /// <summary>
        /// Cuadrícula actual
        /// </summary>
        public byte Cuadricula_Actual { get; set; } = 1;

        #endregion Propiedades

        /// <summary>
        /// Inicialización del formulario
        /// </summary>
        public Relpag_221()
        {
            InitializeComponent();
            this.CadenaConexion = Variables_Globales.CadenaConexionAplicativo;
        }

        /// <summary>
        /// Carga formulario, cambio alineación cabeceras de cuadrículas y carga inicial
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Relpag_221_Load(object sender, EventArgs e)
        {
            Accion_Actual = -1;
            Refresca_Panel();
            Boton_Impresion = Boton_6;

            Etiquetas[0] = Label_1_1;
            Etiquetas[1] = Label_2_1;

            _Tabla_Busquedas = Funciones_Busqueda._Crear_Tabla_Busqueda();

            for (byte i = 0; i < _Sql_Busquedas.Length; i++)
            {
                Id_Cuadricula[i] = 0;
                _Sql_Busquedas[i] = "";
            }

            GrdGridConsulta1.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GrdGridConsulta1.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GrdGridConsulta1.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GrdGridConsulta1.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GrdGridConsulta1.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GrdGridConsulta2.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GrdGridConsulta2.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GrdGridConsulta2.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GrdGridConsulta1.SQLInicial = Sql_Inicial_Cuadricula_1();
            GrdGridConsulta1.CargarGrid(0);
            Funciones_Genericas._Numerador_Registros(GrdGridConsulta1, Label_1_2);


            Cuadricula_Actual = 1;
            Accion_Actual = 0;
            Refresca_Panel();
            ActualizaEtiquetas(Cuadricula_Actual);
            GrdGridConsulta1.Select();
        }

        #region Funciones

        /// <summary>
        /// Genera la SQL inicial de la cuadrícula 'RELACIONES PAGOS'
        /// </summary>
        /// <param name="Sql_Busquedas">Cadena de búsquedas</param>
        /// <returns>Cadena 'SQL'</returns>
        private string Sql_Inicial_Cuadricula_1(string Sql_Busquedas = "")
        {
            string Cadena_Sql;
            string Cadena_Select;
            string Cadena_From;
            string Restriccion_Ejercicio = $"eje_0010 = {Propiedades_Aplicativo.Ejercicio_Trabajo}";
            string Cadena_Where = Restriccion_Ejercicio;

            Cadena_Select = "SELECT ide_0010, eje_0010, rel_0010, des_0010, fec_0010, fcp_0010, tot_0010, nre_0010, fgf_0010, fei_0010, sre_0010";
            Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.Relpag_0010";

            if (Sql_Busquedas != string.Empty)
                Cadena_Where += $" AND ide_0010 IN ({Sql_Busquedas})";

            Cadena_Sql = Cadena_Select + Cadena_From + " WHERE " + Cadena_Where;

            return Cadena_Sql;
        }

        /// <summary>
        /// Genera la SQL inicial de la cuadrícula. 'RELACIONES PAGOS. DETALLE'
        /// </summary>
        /// <param name="Id_0010">Identificador registro 'RELACIONES PAGOS'</param>
        /// <param name="Sql_Busquedas">Cadena de búsquedas</param>
        /// <returns>Cadena 'SQL'</returns>
        private string Sql_Inicial_Cuadricula_2(int Id_0010, string Sql_Busquedas = "")
        {
            string Cadena_Sql;
            string Cadena_Select;
            string Cadena_From;
            string Cadena_Where;
            string Restriccion_Identificador = $"relpag_v_0010_1.ide_0010 = {Id_0010}";

            Cadena_Select = "SELECT ide_0010_1, eje_0001 + '-' + ord_0001 ejeord, Funciones_Generales.dbo.Normaliza_Iban (iba_0001) ibater, nfa_0001, razsoc, fee_0001, liq_0001, ide_0001, ide_0010 iderel, sdo_0010_1";
            Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.relpag_v_0010_1";

            if (Sql_Busquedas != string.Empty)
                Cadena_Where = Restriccion_Identificador + $" AND ide_0010_1 IN ({Sql_Busquedas})";
            else
                Cadena_Where = Restriccion_Identificador;

            Cadena_Sql = Cadena_Select + Cadena_From + " WHERE " + Cadena_Where;

            return Cadena_Sql;
        }

        /// <summary>
        /// Actualiza el color de las etiquetas de las cuadrículas
        /// </summary>
        /// <param name="Cuadricula">Cuadricula a activar</param>
        private void ActualizaEtiquetas(short Cuadricula)
        {
            for (byte i = 0; i < Etiquetas.Length; i++)
            {
                Etiquetas[i].ForeColor = Color.FromArgb(0, 100, 255);
            }

            Etiquetas[Cuadricula - 1].ForeColor = Color.DarkGoldenrod;
        }

        #endregion Funciones

        #region Funciones de impresión

        /// <summary>
        /// Genera la SQL para el informe
        /// </summary>
        /// <param name="Sql_Busquedas"> Genera la SQL para el informe</param>
        /// <returns>Cadena 'SQL'</returns>
        private string Seleccion_Datos(string[] Sql_Busquedas)
        {
            string Cadena_Sql;
            string Cadena_Select;
            string Cadena_From;
            string Cadena_Where = "";

            Cadena_Select = "SELECT 1 tipreg, relpag_0010.ide_0010 idereg, CAST (eje_0010 as varchar (4)) +'-' + CAST(rel_0010 as varchar (6)) anorel, des_0010 desrel, fec_0010 fecrel,";
            Cadena_Select += " fcp_0010 feccpa,  tot_0010 totrel, ide_0010_1 idedoc, eje_0001 +'-' + ord_0001 ejeord, nre_0001 numren, nfa_0001 numfac , nif_0001 nifter,";
            Cadena_Select += " Funciones_Generales.dbo.Normaliza_Razon_Social(ape_0001, nom_0001) razter, iba_0001 ibater, bic_0001 bicter, res_0001 expseg,";
            Cadena_Select += " Funciones_Generales.dbo.Normaliza_Aplicacion_3(eco_0001, exp_0001, doc_0001) expcon, con_0001 condoc, liq_0001 impliq, sdr_0001 semgen";
            Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.relpag_0001 RIGHT OUTER JOIN { Variables_Globales.NombreBaseDatosAplicativo}.dbo.relpag_0010_1";
            Cadena_From += $" ON relpag_0001.ide_0001 = relpag_0010_1.ide_0001 RIGHT OUTER JOIN {Variables_Globales.NombreBaseDatosAplicativo}.dbo.relpag_0010";
            Cadena_From += " ON relpag_0010_1.ide_0010 = relpag_0010.ide_0010";

            if (Sql_Busquedas[0] != string.Empty)
                Cadena_Where = $"Relpag_0010.ide_0010 IN ({Sql_Busquedas[0]}) AND Relpag_0010_1.ide_0010_1 IN ({Sql_Busquedas[1]})";

            Cadena_Sql = Cadena_Select + Cadena_From;

            if (Cadena_Where != string.Empty)
                Cadena_Sql += $" WHERE {Cadena_Where}";

            return Cadena_Sql;
        }

        /// <summary>
        /// Impresión informes 'RESUMEN. POR CAPITULOS'
        /// </summary>
        /// <param name="Sql_Busquedas">Cadena SQL de búsquedas</param>
        private void Impresion_Informe(string[] Sql_Busquedas)
        {
            AppConfig Configuracion = new AppConfig(Application.ExecutablePath);

            string Cadena_Sql_Cabecera;
            string Cadena_Sql;
            string Titulo_Informe;
            string Fecha_Informe = DateTime.Now.ToString("dddd, d' de 'MMMM' de 'yyyy");
            string Periodo_Informe = $"EJERCICIO {Propiedades_Aplicativo.Ejercicio_Trabajo}";
            string Titulo_Cabecera = "";
            string Fecha_Real = DateTime.Now.ToString("dddd, d' de 'MMMM' de 'yyyy");
            string Informe_General = InformeGeneralNamespace;

            InformeGeneralEnsamblado = Application.ExecutablePath;
            RutaPDFS = PathExtensions.PathAddBackSlash(Configuracion.Leer("Relaciones_Pagos", "Directorio_Pdf").Trim());

            Titulo_Informe = "RELACIONES DE PAGOS. MODELO-997";
            NombreFichero = "Relaciones_Pagos_Modelo_997_" + DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss");
            InformeGeneralNamespace = "Relaciones_Pagos.Informes.Relaciones_Pagos.I_Relpag_221.rdlc";

            ReportViewer Informe = new ReportViewer();
            SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion);
            SqlDataAdapter Data_Adapter;
            ReportDataSource Origen_Datos_Informe;
            DataSet Data_Set;

            Informe.LocalReport.ReportEmbeddedResource = InformeGeneralNamespace;
            Informe.LocalReport.EnableExternalImages = true;

            // CREACIÓN Y LLENADO DEL DATA TABLE DE IMAGEN DE CABECERA
            Data_Set = new DataSet("Imagen_Cabecera");
            Cadena_Sql_Cabecera = Funciones_Genericas._Cabecera_Informes(Propiedades_Aplicativo.Ejercicio_Trabajo.ToString(), CadenaConexion, Titulo_Informe, Fecha_Informe, Periodo_Informe, Titulo_Cabecera);

            Data_Adapter = new SqlDataAdapter(Cadena_Sql_Cabecera, Cadena_Conexion);
            Data_Adapter.Fill(Data_Set);
            Origen_Datos_Informe = new ReportDataSource("Imagen_Cabecera", Data_Set.Tables[0]);
            Informe.LocalReport.DataSources.Add(Origen_Datos_Informe);

            // CREACIÓN Y LLENADO DEL DATA TABLE DEL INFORME
            Data_Set = new DataSet("Informe");
            Cadena_Sql = Seleccion_Datos(Sql_Busquedas);
            Data_Adapter = new SqlDataAdapter(Cadena_Sql, Cadena_Conexion);
            Data_Adapter.Fill(Data_Set);
            Origen_Datos_Informe = new ReportDataSource("Informe", Data_Set.Tables[0]);
            Informe.LocalReport.DataSources.Add(Origen_Datos_Informe);

            Informe.ProcessingMode = ProcessingMode.Local;
            Cadena_Conexion.Close();

            Impresion(Informe);
            InformeGeneralNamespace = Informe_General;
        }

        #endregion Funciones de impresión

        #region Funciones manipulación datos

        #endregion Funciones manipulación datos

        #region Panel de botones

        /// <summary>
        /// Refresco del panel de botones
        /// </summary>
        private void Refresca_Panel()
        {
            Control[] Boton;
            btnBotonGeneral Boton_Panel;

            for (byte i = 0; i < Panel.Controls.Count; i++)
            {
                Boton = Panel.Controls.Find("Boton_" + Convert.ToString(i + 1), false);

                if (Boton.Length != 0)
                {
                    Boton_Panel = (btnBotonGeneral)Boton[0];
                    Boton_Panel.Refrescar();
                }
            }
        }

        /// <summary>
        /// Altas. Refresco del botón
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_1_Refresco(object sender, EventArgs e)
        {
            bool Semaforo_Activo;

            if (Accion_Actual == 0
                 &&
                 Cuadricula_Actual == 1
                 &&
                 _Tabla_Busquedas.Rows.Count == 0
                 &&
                 Permisos[1])
                Semaforo_Activo = true;
            else
                Semaforo_Activo = false;

            Boton_1.Enabled = Semaforo_Activo;
        }

        /// <summary>
        ///  Modificaciones. Refresco del botón
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_2_Refresco(object sender, EventArgs e)
        {
            bool Semaforo_Activo;
            int Numero_Registros;

            Numero_Registros = GrdGridConsulta1.Rows.Count;

            if (Accion_Actual == 0
                &&
                Cuadricula_Actual == 1
                &&
                M997_Generado == 0
                &&
                Numero_Registros != 0
                &&
                Permisos[3])
                Semaforo_Activo = true;
            else
                Semaforo_Activo = false;

            Boton_2.Enabled = Semaforo_Activo;
        }

        /// <summary>
        /// Carga de datos. Refresco del botón
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_3_Refresco(object sender, EventArgs e)
        {
            Boton_3.Enabled = (Accion_Actual == 0);
        }

        /// <summary>
        /// Búsquedas. Refresco del botón
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_4_Refresco(object sender, EventArgs e)
        {
            Boton_4.Enabled = (Accion_Actual == 0);
        }

        /// <summary>
        /// Vaciar búsquedas y recargar datos originales. Refresco del botón
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_5_Refresco(object sender, EventArgs e)
        {
            bool Semaforo_Activo;

            if (Accion_Actual == 0
                 &&
                 _Tabla_Busquedas.Rows.Count != 0)
                Semaforo_Activo = true;
            else
                Semaforo_Activo = false;

            Boton_5.Enabled = Semaforo_Activo;
        }

        /// <summary>
        /// Impresión. Refresco del botón
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_6_Refresco(object sender, EventArgs e)
        {
            bool Semaforo_Activo;

            if (Accion_Actual == 0
                &&
                GrdGridConsulta1.Rows.Count != 0
               &&
               Permisos[4])
                Semaforo_Activo = true;
            else
                Semaforo_Activo = false;

            Boton_6.Enabled = Semaforo_Activo;

        }

        /// <summary>
        /// Altas de registros 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_1_Click(object sender, EventArgs e)
        {
            Relpag_221_11 Form_Altas = new Relpag_221_11(2);
            Form_Altas._Cuadricula[0] = GrdGridConsulta1;
            Form_Altas._Cuadricula[1] = GrdGridConsulta2;
            Form_Altas._Identificador[0] = 0;
            Form_Altas._Identificador[1] = 0;

            Form_Altas.Form_Padre = this;
            Funciones_Globales.AbrirFormulario(Form_Altas, true, false);
        }

        /// <summary>
        /// Modificaciones de registros
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_2_Click(object sender, EventArgs e)
        {
            Relpag_221_21 Form_Modificaciones = new Relpag_221_21(2);
            Form_Modificaciones._Cuadricula[0] = GrdGridConsulta1;
            Form_Modificaciones._Cuadricula[1] = GrdGridConsulta2;
            Form_Modificaciones._Identificador[0] = Id_Cuadricula[0];
            Form_Modificaciones._Identificador[1] = 0;

            Form_Modificaciones.Form_Padre = this;
            Funciones_Globales.AbrirFormulario(Form_Modificaciones, true, false);
        }

        /// <summary>
        /// Carga de datos. Recarga datos en cuadrícula
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_3_Click(object sender, EventArgs e)
        {
            switch (Cuadricula_Actual)
            {
                case 1:                // PUESTOS DE TRABAJO
                    {
                        GrdGridConsulta1.CargarGrid(Id_Cuadricula[0]);

                        break;
                    }

                case 2:                // PUESTOS DE TRABAJO - CLASIFICACIÓN EMPLEADOS PÚBLICOS
                    {
                        GrdGridConsulta1.CargarGrid(Id_Cuadricula[1]);

                        break;
                    }
            }
        }

        /// <summary>
        /// Búsquedas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_4_Click(object sender, EventArgs e)
        {
            string Cadena_From = " FROM relpag_0010 LEFT OUTER JOIN relpag_v_0010_1 ON relpag_0010.ide_0010 = relpag_v_0010_1.ide_0010";

            Busquedas Form_Busqueda = new Busquedas(2, 2, Cuadricula_Actual);
            Form_Busqueda.Form_Padre = this;

            Form_Busqueda._Nombres_Bdd[0] = "";
            Form_Busqueda._Nombres_Bdd[1] = "";

            Form_Busqueda._Sql_Parcial[0] = "SELECT relpag_0010.ide_0010" + Cadena_From;
            Form_Busqueda._Sql_Parcial[1] = "SELECT relpag_v_0010_1.ide_0010_1" + Cadena_From;

            Form_Busqueda._Tablas_Bdd[0] = "relpag_0010";
            Form_Busqueda._Tablas_Bdd[1] = "relpag_v_0010_1";

            Form_Busqueda._Nombres_Tablas[0] = "RELACIONES DE PAGOS. MODELO-997";
            Form_Busqueda._Nombres_Tablas[1] = "RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS";

            Form_Busqueda._Columnas[0] = "'uar_0010', 'far_0010', 'dip_0010', 'uua_0010', 'fua_0010'";
            Form_Busqueda._Columnas[1] = "'uar_0010_1', 'far_0010_1', 'dip_0010_1', 'uua_0010_1', 'fua_0010_1'";

            Form_Busqueda._Sql_Busquedas[0] = _Sql_Busquedas[0];
            Form_Busqueda._Sql_Busquedas[1] = _Sql_Busquedas[1];

            Form_Busqueda._Tabla_Busquedas = _Tabla_Busquedas;

            Funciones_Globales.AbrirFormulario(Form_Busqueda, true, false);
        }

        /// <summary>
        /// Nueva búsqueda. Vacia la tabla de búsquedas y recarga los datos originales
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_5_Click(object sender, EventArgs e)
        {
            _Tabla_Busquedas.Clear();

            for (byte i = 0; i < _Sql_Busquedas.Length; i++)
                _Sql_Busquedas[i] = "";

            Id_Cuadricula[0] = 0;

            GrdGridConsulta1.SQLInicial = Sql_Inicial_Cuadricula_1(_Sql_Busquedas[0]);
            GrdGridConsulta1.CargarGrid(0);
            Funciones_Genericas._Numerador_Registros(GrdGridConsulta1, Label_1_2);

            if (GrdGridConsulta1.Rows.Count == 0)
            {
                GrdGridConsulta2.SQLInicial = Sql_Inicial_Cuadricula_2(0, _Sql_Busquedas[1]);
                GrdGridConsulta2.CargarGrid(0);
                Funciones_Genericas._Numerador_Registros(GrdGridConsulta2, Label_2_2);
            }

            Refresca_Panel();
        }

        /// <summary>
        /// Impresión de datos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_6_Click(object sender, EventArgs e)
        {
            imgIntsa.Focus();
            Accion_Actual = 2;
            Refresca_Panel();

            Impresion_Informe(_Sql_Busquedas);

            Accion_Actual = 0;
            Refresca_Panel();
        }

        #endregion

        #region Etiquetas de selección

        #endregion Etiquetas de selección

        #region Campos formulario

        /// <summary>
        /// Cambio de registro 'RELACIONES DE PAGOS. MODELO-997'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_SelectionChanged(object sender, EventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            int Identificador;

            if (Cuadricula.SelectedRows.Count > 0)
            {
                Identificador = Convert.ToInt32(Cuadricula.SelectedRows[0].Cells[Cuadricula.ColumnaClaveIdentidad].Value);

                if (Identificador != Id_Cuadricula[0]
                  &&
                      Identificador != 0)
                {
                    Id_Cuadricula[0] = Identificador;
                    Funciones_Genericas._Numerador_Registros(Cuadricula, Label_1_2);

                    M997_Generado = Convert.ToByte(Cuadricula.SelectedRows[0].Cells["sre_0010"].Value);

                    switch (M997_Generado)
                    {
                        case 0:                 // PENDIENTE
                            {
                                Label_Info_1.Text = "PENDIENTE";
                                Label_Info_1.ForeColor = Color.Black;

                                break;
                            }

                        case 1:                 // MODELO - 997 GENERADO
                            {
                                Label_Info_1.Text = "MODELO - 997 GENERADO";
                                Label_Info_1.ForeColor = Color.MediumSeaGreen;

                                break;
                            }

                        case 2:                 // MODELO - 997 ENVIADO
                            {
                                Label_Info_1.Text = "MODELO - 997 ENVIADO";
                                Label_Info_1.ForeColor = Color.DarkGreen;

                                break;
                            }

                        case 3:                 //  VERIFICADA
                            {
                                Label_Info_1.Text = "VERIFICADA";
                                Label_Info_1.ForeColor = Color.FromArgb(0, 100, 255);

                                break;
                            }

                    }

                    GrdGridConsulta2.SQLInicial = Sql_Inicial_Cuadricula_2(Identificador, _Sql_Busquedas[1]);
                    GrdGridConsulta2.CargarGrid(0);
                    Funciones_Genericas._Numerador_Registros(GrdGridConsulta2, Label_2_2);

                    Refresca_Panel();
                }
            }
        }

        /// <summary>
        /// Refresco etiquetas cuadrícula 'RELACIONES DE PAGOS. MODELO-997'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_Enter(object sender, EventArgs e)
        {
            Cuadricula_Actual = 1;
            ActualizaEtiquetas(Cuadricula_Actual);
            Refresca_Panel();
        }

        /// <summary>
        /// Formatea la cuadrícula 'RELACIONES DE PAGOS. MODELO-997'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            byte Situacion = Convert.ToByte(Cuadricula.Rows[e.RowIndex].Cells["sre_0010"].Value);
            Color Color_Letra = new Color();

            switch (Situacion)
            {
                case 0:                 // PENDIENTE
                    {
                        Color_Letra = Color.Black;

                        break;
                    }

                case 1:                 // MODELO - 997 GENERADO
                    {
                        Color_Letra = Color.MediumSeaGreen;

                        break;
                    }

                case 2:                 // MODELO - 997 ENVIADO
                    {
                        Color_Letra = Color.DarkGreen;

                        break;
                    }

                case 3:                 //  VERIFICADA
                    {
                        Color_Letra = Color.FromArgb(0, 100, 255);

                        break;
                    }

            }

            e.CellStyle.ForeColor = Color_Letra;
            e.CellStyle.SelectionForeColor = Color_Letra;
        }

        /// <summary>
        /// Cambio de registro 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta2_SelectionChanged(object sender, EventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta2;
            int Identificador;
            byte Situacion;
            
            if (Cuadricula.SelectedRows.Count > 0)
            {
                Identificador = Convert.ToInt32(Cuadricula.SelectedRows[0].Cells[Cuadricula.ColumnaClaveIdentidad].Value);

                if (Identificador != Id_Cuadricula[1]
                  &&
                      Identificador != 0)
                {
                    Id_Cuadricula[1] = Identificador;
                    Funciones_Genericas._Numerador_Registros(Cuadricula, Label_2_2);

                    Situacion = Convert.ToByte(Cuadricula.SelectedRows[0].Cells["sdo_0010_1"].Value);

                    switch (Situacion)
                    {
                        case 0:                 // PENDIENTE VERIFICACIÓN
                            {
                                Label_Info_2.Text = "PENDIENTE VERIFICACIÓN";
                                Label_Info_2.ForeColor = Color.Black;

                                break;
                            }

                        case 1:                 // CORRECTO
                            {
                                Label_Info_2.Text = "CORRECTO";
                                Label_Info_2.ForeColor = Color.FromArgb(0, 100, 255);

                                break;
                            }

                        case 2:                 // SITUACIÓN -1-
                            {
                                Label_Info_2.Text = "SITUACIÓN -1-";
                                Label_Info_2.ForeColor = Color.MediumSeaGreen;

                                break;
                            }

                        case 3:                // SITUACIÓN -2-
                            {
                                Label_Info_2.Text = "SITUACIÓN -2-";
                                Label_Info_2.ForeColor = Color.Tomato;

                                break;
                            }

                        case 4:                // SITUACIÓN -3-
                            {
                                Label_Info_2.Text = "SITUACIÓN -3-";
                                Label_Info_2.ForeColor = Color.BlueViolet;

                                break;
                            }
                    }

                    Refresca_Panel();
                }
            }
        }

        /// <summary>
        /// Refresco etiquetas cuadrícula 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta2_Enter(object sender, EventArgs e)
        {
            Cuadricula_Actual = 2;
            ActualizaEtiquetas(Cuadricula_Actual);
            Refresca_Panel();
        }

        /// <summary>
        /// Formatea la cuadrícula 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta2;
            byte Situacion = Convert.ToByte(Cuadricula.Rows[e.RowIndex].Cells["sdo_0010_1"].Value);
            Color Color_Letra = new Color();

            switch (Situacion)
            {
                case 0:                 // PENDIENTE VERIFICACIÓN
                    {
                        Color_Letra = Color.Black;

                        break;
                    }

                case 1:                 // CORRECTO
                    {
                        Color_Letra = Color.FromArgb(0, 100, 255);

                        break;
                    }

                case 2:                 // SITUACIÓN -1-
                    {
                        Color_Letra = Color.MediumSeaGreen;

                        break;
                    }

                case 3:                // SITUACIÓN -2-
                    {
                        Color_Letra = Color.Tomato;

                        break;
                    }

                case 4:                // SITUACIÓN -3-
                    {
                        Color_Letra = Color.BlueViolet;

                        break;
                    }

            }

            e.CellStyle.ForeColor = Color_Letra;
            e.CellStyle.SelectionForeColor = Color_Letra;
        }

        #endregion

        #region Menú contextual cuadrícula 'PUESTOS DE TRABAJO'

        /// <summary>
        /// Crea menú contextual de acciones cuadrícula 'PUESTOS DE TRABAJO'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_MouseClick(object sender, MouseEventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            int Numero_Relaciones = 0;
            int Id_0010;

            if (e.Button == MouseButtons.Right)
            {
                Cuadricula.Focus();

                if (Cuadricula.Rows.Count != 0)
                {
                    var Fila_Columna = Cuadricula.HitTest(e.X, e.Y);
                    MenuItem Item_Menu;

                    if (Fila_Columna.RowIndex >= 0)
                    {
                        ContextMenu Menu_Contextual = new ContextMenu();
                        Cuadricula.ClearSelection();
                        Cuadricula.Rows[Fila_Columna.RowIndex].Selected = true;

                        #region Consultar

                        Item_Menu = new MenuItem("Consultar");
                        Item_Menu.Click += Consultar_Puesto_Click;
                        Menu_Contextual.MenuItems.Add(Item_Menu);

                        #endregion  Consultar

                        #region Generar Modelo-997

                        if (M997_Generado == 0)
                        {
                            Item_Menu = new MenuItem("Generar M-997 y archivo");
                            Item_Menu.Click += Generar_M997_Click;
                            Menu_Contextual.MenuItems.Add(Item_Menu);
                        }
                        else
                        {
                            Item_Menu = new MenuItem("Generar archivo M-997");
                            Item_Menu.Click += Generar_Archivo_M997_Click;
                            Menu_Contextual.MenuItems.Add(Item_Menu);
                        }
                        #endregion Generar Modelo-997

                        #region Eliminar

                        Id_0010 = Convert.ToInt32(Cuadricula.SelectedRows[0].Cells["ide_0010"].Value);
                        //                        Numero_Relaciones = Funciones_Aplicativo.Relaciones_Adlogp_1004(CadenaConexion, Id_Puesto);

                        if (Permisos[2]
                            &&
                            M997_Generado == 0
                            &&
                            Numero_Relaciones == 0)
                        {
                            Item_Menu = new MenuItem();
                            Item_Menu.Text = "Eliminar registro";
                            Item_Menu.Click += Eliminar_Relacion_Click;
                            Menu_Contextual.MenuItems.Add(Item_Menu);
                        }

                        #endregion Eliminar

                        Menu_Contextual.Show(Cuadricula, new Point(e.X, e.Y));
                    }
                }
            }
        }

        /// <summary>
        /// Abre el formulario de consulta 'PUESTOS DE TRABAJO'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Consultar_Puesto_Click(Object sender, EventArgs e)
        {
            //Relpag_221_31 Form_Menu_1 = new Relpag_221_31(1);
            //Form_Menu_1._Cuadricula[0] = GrdGridConsulta1;
            //Form_Menu_1._Identificador[0] = Id_Cuadricula[0];

            //Form_Menu_1.Form_Padre = this;
            //Funciones_Globales.AbrirFormulario(Form_Menu_1, true, false);
        }

        /// <summary>
        /// Genera el modelo-997
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Generar_M997_Click(Object sender, EventArgs e)
        {
            AppConfig Configuracion = new AppConfig(Application.ExecutablePath);

            PresentaMensaje("GENERANDO MODELO-997", 2, Color.Blue);

            grdGridConsulta Cuadricula = GrdGridConsulta1;
            int Id_0010 = Convert.ToInt32(Cuadricula.SelectedRows[0].Cells["ide_0010"].Value);
            string Fecha_Extraccion = Convert.ToDateTime(Cuadricula.SelectedRows[0].Cells["fei_0010"].Value).ToString("yyyyMMdd");
            string Fecha_Caducidad = Convert.ToDateTime(Cuadricula.SelectedRows[0].Cells["fcp_0010"].Value).ToString("yyyyMMdd");
            DateTime Fecha_Generacion = DateTime.Now;
            string Modelo_997 = Funciones_Aplicativo.Generar_Modelo_997(CadenaConexion, Id_0010, Fecha_Extraccion, Fecha_Generacion.ToString("yyyyMMdd"), Fecha_Caducidad);
            string Directorio = Configuracion.Leer("Relaciones_Pagos", "Directorio_Modelo_997").Trim();
            string Archivo_997 = $"M_997_{Id_0010}.txt";
            File.WriteAllText(Directorio + @"\" + Archivo_997, Modelo_997);

            Desencadenantes.Actualizar_Relpag_0010(CadenaConexion, Id_0010, Modelo_997, Fecha_Generacion.ToString("dd/MM/yyyy hh:mm:ss"), 1);

            Cuadricula.ClearSelection();
            Id_Cuadricula[0] = 0;
            Cuadricula.CargarGrid(Id_0010);
//            Funciones_Genericas._Numerador_Registros(Cuadricula, Label_1_2);
            PresentaMensaje($"MODELO-997 {Archivo_997}. GENERADO EN {Directorio}", 3, Color.Blue);

        }

        /// <summary>
        /// Genera el modelo-997
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Generar_Archivo_M997_Click(Object sender, EventArgs e)
        {
            AppConfig Configuracion = new AppConfig(Application.ExecutablePath);

            PresentaMensaje("GENERANDO ARCHIVO MODELO-997", 2, Color.Blue);

            grdGridConsulta Cuadricula = GrdGridConsulta1;
            int Id_0010 = Convert.ToInt32(Cuadricula.SelectedRows[0].Cells["ide_0010"].Value);
            string Directorio = Configuracion.Leer("Relaciones_Pagos", "Directorio_Modelo_997").Trim();
            string Archivo_997 = $"M_997_{Id_0010}.txt";
            string Modelo_997 = Funciones_Aplicativo.M_997_Relpag_0010(CadenaConexion, Id_0010);
            File.WriteAllText(Directorio + @"\" + Archivo_997, Modelo_997);

            PresentaMensaje($"ARCHIVO MODELO-997 {Archivo_997}. GENERADO EN {Directorio}", 3, Color.Blue);
        }

        /// <summary>
        /// Elimina un registro 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Eliminar_Relacion_Click(Object sender, EventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            int Identificador = Convert.ToInt32(Cuadricula.SelectedRows[0].Cells["ide_1004"].Value);

            if (frmMessageBox.Mostrar($"¿ ELIMINAR EL REGISTRO SELECCIONADO ?{Environment.NewLine}[{Convert.ToString(Cuadricula.SelectedRows[0].Cells["den_1004"].Value)}]", "ELIMINAR REGISTRO", frmMessageBox.TipoBotones.SiNo, frmMessageBox.Iconos.Interrogacion, frmMessageBox.Animacion.FadeIn) == frmMessageBox.Boton.Si)
            {
                //Desencadenantes.Eliminar_Adlogp_1004_1_Extras(CadenaConexion, Identificador);
                //Desencadenantes.Eliminar_Adlogp_1004(CadenaConexion, Identificador);
                Cuadricula.CargarGrid(0);
            }
        }

        #endregion Menú contextual cuadrícula 'PUESTOS DE TRABAJO'

        #region Menú contextual cuadrícula 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'

        /// <summary>
        /// Crea menú contextual de acciones cuadrícula 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta2_MouseClick(object sender, MouseEventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta2;

            if (e.Button == MouseButtons.Right)
            {
                Cuadricula.Focus();

                if (Cuadricula.Rows.Count != 0)
                {
                    var Fila_Columna = Cuadricula.HitTest(e.X, e.Y);
                    MenuItem Item_Menu;

                    if (Fila_Columna.RowIndex >= 0)
                    {
                        ContextMenu Menu_Contextual = new ContextMenu();
                        Cuadricula.ClearSelection();
                        Cuadricula.Rows[Fila_Columna.RowIndex].Selected = true;

                        #region Eliminar

                        if (Permisos[2]
                            &&
                            M997_Generado == 0
                            &&
                            Cuadricula.Rows.Count != 0)
                        {
                            Item_Menu = new MenuItem
                            {
                                Text = "Eliminar registro"
                            };

                            Item_Menu.Click += Eliminar_Documento_Click;
                            Menu_Contextual.MenuItems.Add(Item_Menu);
                        }

                        #endregion

                        Menu_Contextual.Show(Cuadricula, new Point(e.X, e.Y));
                    }
                }
            }
        }

        /// <summary>
        /// Elimina un registro 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Eliminar_Documento_Click(Object sender, EventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta2;
            int Id_0010_1 = Convert.ToInt32(Cuadricula.SelectedRows[0].Cells["ide_0010_1"].Value);
            int Id_0001 = Convert.ToInt32(Cuadricula.SelectedRows[0].Cells["ide_0001"].Value);
            int Id_0010 = Convert.ToInt32(Cuadricula.SelectedRows[0].Cells["iderel"].Value);
            string Ejercicio_Orden = Convert.ToString(Cuadricula.SelectedRows[0].Cells["ejeord"].Value);
            decimal Importe;

            if (frmMessageBox.Mostrar($"¿ ELIMINAR EL REGISTRO SELECCIONADO ?{Environment.NewLine}{Ejercicio_Orden} [{Convert.ToString(Cuadricula.SelectedRows[0].Cells["razsoc"].Value)}]", "ELIMINAR REGISTRO", frmMessageBox.TipoBotones.SiNo, frmMessageBox.Iconos.Interrogacion, frmMessageBox.Animacion.FadeIn) == frmMessageBox.Boton.Si)
            {
                Importe = Convert.ToDecimal(Cuadricula.SelectedRows[0].Cells["liq_0001"].Value);
                Desencadenantes.Eliminar_Relpag_0010_1(CadenaConexion, Id_0010_1);
                Desencadenantes.Actualizar_Relpag_0010(CadenaConexion, Id_0010, Importe, -1);
                Desencadenantes.Actualizar_Relpag_0001(CadenaConexion, Id_0001, 0);
                GrdGridConsulta1.CargarGrid(Id_Cuadricula[0]);
                Cuadricula.CargarGrid(0);
            }
        }

        #endregion Menú contextual cuadrícula 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'

        #region Cierre del formulario

        private void Relpag_221_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        #endregion

        //
    }
    //
}
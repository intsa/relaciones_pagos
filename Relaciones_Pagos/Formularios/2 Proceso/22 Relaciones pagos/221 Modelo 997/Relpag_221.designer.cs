﻿namespace Relaciones_Pagos
{
    partial class Relpag_221
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Relpag_221));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Panel = new System.Windows.Forms.Panel();
            this.Boton_5 = new ClaseIntsa.Controles.btnBotonGeneral(this.components);
            this.Boton_6 = new ClaseIntsa.Controles.btnBotonGeneral(this.components);
            this.Boton_4 = new ClaseIntsa.Controles.btnBotonGeneral(this.components);
            this.Boton_3 = new ClaseIntsa.Controles.btnBotonGeneral(this.components);
            this.Boton_2 = new ClaseIntsa.Controles.btnBotonGeneral(this.components);
            this.Boton_1 = new ClaseIntsa.Controles.btnBotonGeneral(this.components);
            this.Label_2_1 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label_2_2 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.GrdGridConsulta2 = new ClaseIntsa.Controles.grdGridConsulta(this.components);
            this.Label_1_1 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label_1_2 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.GrdGridConsulta1 = new ClaseIntsa.Controles.grdGridConsulta(this.components);
            this.ide_0010 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eje_0010 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rel_0010 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.des_0010 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fec_0010 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcp_0010 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fgf_0010 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tot_0010 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nre_0010 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fei_0010 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sre_0010 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Label_Info_1 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label_Info_2 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.ide_0010_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ejeord = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ibater = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nfa_0001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.razsoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fee_0001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.liq_0001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ide_0001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iderel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sdo_0010_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.imgIntsa)).BeginInit();
            this.Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label_2_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdGridConsulta2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdGridConsulta1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_Info_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_Info_2)).BeginInit();
            this.SuspendLayout();
            // 
            // imgIntsa
            // 
            this.imgIntsa.Location = new System.Drawing.Point(820, 30);
            this.imgIntsa.Size = new System.Drawing.Size(170, 50);
            // 
            // cmbVista
            // 
            this.cmbVista.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbVista.DisplayMember = "DESCRIPCION";
            this.cmbVista.Location = new System.Drawing.Point(0, 20);
            this.cmbVista.Size = new System.Drawing.Size(130, 23);
            this.cmbVista.ValueMember = "ID";
            // 
            // lblMensajes
            // 
            this.lblMensajes.Size = new System.Drawing.Size(1000, 20);
            // 
            // BarraProgreso
            // 
            this.BarraProgreso.Location = new System.Drawing.Point(880, 0);
            // 
            // Panel
            // 
            this.Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Panel.Controls.Add(this.Boton_5);
            this.Panel.Controls.Add(this.Boton_6);
            this.Panel.Controls.Add(this.Boton_4);
            this.Panel.Controls.Add(this.Boton_3);
            this.Panel.Controls.Add(this.Boton_2);
            this.Panel.Controls.Add(this.Boton_1);
            this.Panel.Font = new System.Drawing.Font("Calibri", 10F);
            this.Panel.ForeColor = System.Drawing.Color.Transparent;
            this.Panel.Location = new System.Drawing.Point(230, 30);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(580, 50);
            this.Panel.TabIndex = 3;
            this.Panel.TabStop = true;
            // 
            // Boton_5
            // 
            this.Boton_5.FlatAppearance.BorderSize = 0;
            this.Boton_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton_5.Font = new System.Drawing.Font("Calibri", 10F);
            this.Boton_5.Image = ((System.Drawing.Image)(resources.GetObject("Boton_5.Image")));
            this.Boton_5.ImagenDeshabilitado = ((System.Drawing.Image)(resources.GetObject("Boton_5.ImagenDeshabilitado")));
            this.Boton_5.ImagenFoco = ((System.Drawing.Image)(resources.GetObject("Boton_5.ImagenFoco")));
            this.Boton_5.ImagenNormal = ((System.Drawing.Image)(resources.GetObject("Boton_5.ImagenNormal")));
            this.Boton_5.Location = new System.Drawing.Point(400, 0);
            this.Boton_5.Name = "Boton_5";
            this.Boton_5.Size = new System.Drawing.Size(50, 50);
            this.Boton_5.TabIndex = 4;
            this.ttpFormulario.SetToolTip(this.Boton_5, "Eliminar búsqueda");
            this.Boton_5.UseVisualStyleBackColor = false;
            this.Boton_5.Refresco += new System.EventHandler(this.Boton_5_Refresco);
            this.Boton_5.Click += new System.EventHandler(this.Boton_5_Click);
            // 
            // Boton_6
            // 
            this.Boton_6.ContextMenuStrip = this.Menu_Impresoras;
            this.Boton_6.FlatAppearance.BorderSize = 0;
            this.Boton_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton_6.Font = new System.Drawing.Font("Calibri", 10F);
            this.Boton_6.Image = ((System.Drawing.Image)(resources.GetObject("Boton_6.Image")));
            this.Boton_6.ImagenDeshabilitado = ((System.Drawing.Image)(resources.GetObject("Boton_6.ImagenDeshabilitado")));
            this.Boton_6.ImagenFoco = ((System.Drawing.Image)(resources.GetObject("Boton_6.ImagenFoco")));
            this.Boton_6.ImagenNormal = ((System.Drawing.Image)(resources.GetObject("Boton_6.ImagenNormal")));
            this.Boton_6.Location = new System.Drawing.Point(500, 0);
            this.Boton_6.Name = "Boton_6";
            this.Boton_6.Size = new System.Drawing.Size(50, 50);
            this.Boton_6.TabIndex = 5;
            this.ttpFormulario.SetToolTip(this.Boton_6, "Imprimir. Botón derecho para seleccionar ");
            this.Boton_6.UseVisualStyleBackColor = false;
            this.Boton_6.Refresco += new System.EventHandler(this.Boton_6_Refresco);
            this.Boton_6.Click += new System.EventHandler(this.Boton_6_Click);
            // 
            // Boton_4
            // 
            this.Boton_4.FlatAppearance.BorderSize = 0;
            this.Boton_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton_4.Font = new System.Drawing.Font("Calibri", 10F);
            this.Boton_4.Image = ((System.Drawing.Image)(resources.GetObject("Boton_4.Image")));
            this.Boton_4.ImagenDeshabilitado = ((System.Drawing.Image)(resources.GetObject("Boton_4.ImagenDeshabilitado")));
            this.Boton_4.ImagenFoco = ((System.Drawing.Image)(resources.GetObject("Boton_4.ImagenFoco")));
            this.Boton_4.ImagenNormal = ((System.Drawing.Image)(resources.GetObject("Boton_4.ImagenNormal")));
            this.Boton_4.Location = new System.Drawing.Point(300, 0);
            this.Boton_4.Name = "Boton_4";
            this.Boton_4.Size = new System.Drawing.Size(50, 50);
            this.Boton_4.TabIndex = 3;
            this.ttpFormulario.SetToolTip(this.Boton_4, "Búsquedas");
            this.Boton_4.UseVisualStyleBackColor = false;
            this.Boton_4.Refresco += new System.EventHandler(this.Boton_4_Refresco);
            this.Boton_4.Click += new System.EventHandler(this.Boton_4_Click);
            // 
            // Boton_3
            // 
            this.Boton_3.FlatAppearance.BorderSize = 0;
            this.Boton_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton_3.Font = new System.Drawing.Font("Calibri", 10F);
            this.Boton_3.Image = ((System.Drawing.Image)(resources.GetObject("Boton_3.Image")));
            this.Boton_3.ImagenDeshabilitado = ((System.Drawing.Image)(resources.GetObject("Boton_3.ImagenDeshabilitado")));
            this.Boton_3.ImagenFoco = ((System.Drawing.Image)(resources.GetObject("Boton_3.ImagenFoco")));
            this.Boton_3.ImagenNormal = ((System.Drawing.Image)(resources.GetObject("Boton_3.ImagenNormal")));
            this.Boton_3.Location = new System.Drawing.Point(200, 0);
            this.Boton_3.Name = "Boton_3";
            this.Boton_3.Size = new System.Drawing.Size(50, 50);
            this.Boton_3.TabIndex = 2;
            this.ttpFormulario.SetToolTip(this.Boton_3, "Recargar datos");
            this.Boton_3.UseVisualStyleBackColor = false;
            this.Boton_3.Refresco += new System.EventHandler(this.Boton_3_Refresco);
            this.Boton_3.Click += new System.EventHandler(this.Boton_3_Click);
            // 
            // Boton_2
            // 
            this.Boton_2.FlatAppearance.BorderSize = 0;
            this.Boton_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton_2.Font = new System.Drawing.Font("Calibri", 10F);
            this.Boton_2.Image = ((System.Drawing.Image)(resources.GetObject("Boton_2.Image")));
            this.Boton_2.ImagenDeshabilitado = ((System.Drawing.Image)(resources.GetObject("Boton_2.ImagenDeshabilitado")));
            this.Boton_2.ImagenFoco = ((System.Drawing.Image)(resources.GetObject("Boton_2.ImagenFoco")));
            this.Boton_2.ImagenNormal = ((System.Drawing.Image)(resources.GetObject("Boton_2.ImagenNormal")));
            this.Boton_2.Location = new System.Drawing.Point(100, 0);
            this.Boton_2.Name = "Boton_2";
            this.Boton_2.Size = new System.Drawing.Size(50, 50);
            this.Boton_2.TabIndex = 1;
            this.ttpFormulario.SetToolTip(this.Boton_2, "Modificar registro");
            this.Boton_2.UseVisualStyleBackColor = false;
            this.Boton_2.Refresco += new System.EventHandler(this.Boton_2_Refresco);
            this.Boton_2.Click += new System.EventHandler(this.Boton_2_Click);
            // 
            // Boton_1
            // 
            this.Boton_1.FlatAppearance.BorderSize = 0;
            this.Boton_1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Boton_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton_1.Font = new System.Drawing.Font("Calibri", 10F);
            this.Boton_1.Image = ((System.Drawing.Image)(resources.GetObject("Boton_1.Image")));
            this.Boton_1.ImagenDeshabilitado = ((System.Drawing.Image)(resources.GetObject("Boton_1.ImagenDeshabilitado")));
            this.Boton_1.ImagenFoco = ((System.Drawing.Image)(resources.GetObject("Boton_1.ImagenFoco")));
            this.Boton_1.ImagenNormal = ((System.Drawing.Image)(resources.GetObject("Boton_1.ImagenNormal")));
            this.Boton_1.Location = new System.Drawing.Point(0, 0);
            this.Boton_1.Name = "Boton_1";
            this.Boton_1.Size = new System.Drawing.Size(50, 50);
            this.Boton_1.TabIndex = 0;
            this.ttpFormulario.SetToolTip(this.Boton_1, "Añadir registro");
            this.Boton_1.UseVisualStyleBackColor = false;
            this.Boton_1.Refresco += new System.EventHandler(this.Boton_1_Refresco);
            this.Boton_1.Click += new System.EventHandler(this.Boton_1_Click);
            // 
            // Label_2_1
            // 
            this.Label_2_1.AutoSize = true;
            this.Label_2_1.BackColor = System.Drawing.Color.Transparent;
            this.Label_2_1.CausesValidation = false;
            this.Label_2_1.Depth = 0;
            this.Label_2_1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_2_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_2_1.FormatoCorporativo = false;
            this.Label_2_1.Location = new System.Drawing.Point(5, 320);
            this.Label_2_1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_2_1.Name = "Label_2_1";
            this.Label_2_1.Size = new System.Drawing.Size(159, 15);
            this.Label_2_1.TabIndex = 432;
            this.Label_2_1.Text = "RELACIONES. DOCUMENTOS";
            this.Label_2_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label_2_2
            // 
            this.Label_2_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_2_2.BackColor = System.Drawing.Color.Transparent;
            this.Label_2_2.CausesValidation = false;
            this.Label_2_2.Depth = 0;
            this.Label_2_2.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label_2_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_2_2.FormatoCorporativo = false;
            this.Label_2_2.Location = new System.Drawing.Point(905, 320);
            this.Label_2_2.Margin = new System.Windows.Forms.Padding(0);
            this.Label_2_2.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_2_2.Name = "Label_2_2";
            this.Label_2_2.Size = new System.Drawing.Size(90, 15);
            this.Label_2_2.TabIndex = 431;
            this.Label_2_2.Text = "-";
            this.Label_2_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // GrdGridConsulta2
            // 
            this.GrdGridConsulta2.AllowUserToAddRows = false;
            this.GrdGridConsulta2.AllowUserToDeleteRows = false;
            this.GrdGridConsulta2.AllowUserToResizeColumns = false;
            this.GrdGridConsulta2.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            this.GrdGridConsulta2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.GrdGridConsulta2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GrdGridConsulta2.BackgroundColor = System.Drawing.Color.White;
            this.GrdGridConsulta2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GrdGridConsulta2.CampoOrdenInicial = "ejeord";
            this.GrdGridConsulta2.CausesValidation = false;
            this.GrdGridConsulta2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.GrdGridConsulta2.ColumnaClaveIdentidad = "ide_0010_1";
            this.GrdGridConsulta2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GrdGridConsulta2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.GrdGridConsulta2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrdGridConsulta2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ide_0010_1,
            this.ejeord,
            this.ibater,
            this.nfa_0001,
            this.razsoc,
            this.fee_0001,
            this.liq_0001,
            this.ide_0001,
            this.iderel,
            this.sdo_0010_1});
            this.GrdGridConsulta2.EnableHeadersVisualStyles = false;
            this.GrdGridConsulta2.Expandible = false;
            this.GrdGridConsulta2.GridColor = System.Drawing.Color.LightGray;
            this.GrdGridConsulta2.Location = new System.Drawing.Point(5, 345);
            this.GrdGridConsulta2.LongitudMinima = 220;
            this.GrdGridConsulta2.Margin = new System.Windows.Forms.Padding(5);
            this.GrdGridConsulta2.MostrarRegSeleccionados = false;
            this.GrdGridConsulta2.MultiSelect = false;
            this.GrdGridConsulta2.Name = "GrdGridConsulta2";
            this.GrdGridConsulta2.ReadOnly = true;
            this.GrdGridConsulta2.RowHeadersVisible = false;
            this.GrdGridConsulta2.RowHeadersWidth = 20;
            this.GrdGridConsulta2.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.GrdGridConsulta2.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Calibri", 10F);
            this.GrdGridConsulta2.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(133)))), ((int)(((byte)(255)))));
            this.GrdGridConsulta2.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            this.GrdGridConsulta2.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.GrdGridConsulta2.RowTemplate.Height = 20;
            this.GrdGridConsulta2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.GrdGridConsulta2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GrdGridConsulta2.Size = new System.Drawing.Size(990, 220);
            this.GrdGridConsulta2.StandardTab = true;
            this.GrdGridConsulta2.TabIndex = 430;
            this.GrdGridConsulta2.TabStop = false;
            this.ttpFormulario.SetToolTip(this.GrdGridConsulta2, "Botón derecho del ratón para más opciones");
            this.GrdGridConsulta2.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.GrdGridConsulta2_CellFormatting);
            this.GrdGridConsulta2.SelectionChanged += new System.EventHandler(this.GrdGridConsulta2_SelectionChanged);
            this.GrdGridConsulta2.Enter += new System.EventHandler(this.GrdGridConsulta2_Enter);
            this.GrdGridConsulta2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GrdGridConsulta2_MouseClick);
            // 
            // Label_1_1
            // 
            this.Label_1_1.AutoSize = true;
            this.Label_1_1.BackColor = System.Drawing.Color.Transparent;
            this.Label_1_1.CausesValidation = false;
            this.Label_1_1.Depth = 0;
            this.Label_1_1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_1_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_1_1.FormatoCorporativo = false;
            this.Label_1_1.Location = new System.Drawing.Point(5, 90);
            this.Label_1_1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_1_1.Name = "Label_1_1";
            this.Label_1_1.Size = new System.Drawing.Size(74, 15);
            this.Label_1_1.TabIndex = 429;
            this.Label_1_1.Text = "RELACIONES";
            this.Label_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label_1_2
            // 
            this.Label_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_1_2.BackColor = System.Drawing.Color.Transparent;
            this.Label_1_2.CausesValidation = false;
            this.Label_1_2.Depth = 0;
            this.Label_1_2.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label_1_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_1_2.FormatoCorporativo = false;
            this.Label_1_2.Location = new System.Drawing.Point(905, 90);
            this.Label_1_2.Margin = new System.Windows.Forms.Padding(0);
            this.Label_1_2.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_1_2.Name = "Label_1_2";
            this.Label_1_2.Size = new System.Drawing.Size(90, 15);
            this.Label_1_2.TabIndex = 428;
            this.Label_1_2.Text = "-";
            this.Label_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // GrdGridConsulta1
            // 
            this.GrdGridConsulta1.AllowUserToAddRows = false;
            this.GrdGridConsulta1.AllowUserToDeleteRows = false;
            this.GrdGridConsulta1.AllowUserToResizeColumns = false;
            this.GrdGridConsulta1.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Calibri", 10F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            this.GrdGridConsulta1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.GrdGridConsulta1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GrdGridConsulta1.BackgroundColor = System.Drawing.Color.White;
            this.GrdGridConsulta1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GrdGridConsulta1.CampoOrdenInicial = "eje_0010, rel_0010";
            this.GrdGridConsulta1.CausesValidation = false;
            this.GrdGridConsulta1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.GrdGridConsulta1.ColumnaClaveIdentidad = "ide_0010";
            this.GrdGridConsulta1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GrdGridConsulta1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.GrdGridConsulta1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrdGridConsulta1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ide_0010,
            this.eje_0010,
            this.rel_0010,
            this.des_0010,
            this.fec_0010,
            this.fcp_0010,
            this.fgf_0010,
            this.tot_0010,
            this.nre_0010,
            this.fei_0010,
            this.sre_0010});
            this.GrdGridConsulta1.EnableHeadersVisualStyles = false;
            this.GrdGridConsulta1.Expandible = false;
            this.GrdGridConsulta1.GridColor = System.Drawing.Color.LightGray;
            this.GrdGridConsulta1.Location = new System.Drawing.Point(5, 115);
            this.GrdGridConsulta1.LongitudMinima = 200;
            this.GrdGridConsulta1.Margin = new System.Windows.Forms.Padding(5);
            this.GrdGridConsulta1.MostrarRegSeleccionados = false;
            this.GrdGridConsulta1.MultiSelect = false;
            this.GrdGridConsulta1.Name = "GrdGridConsulta1";
            this.GrdGridConsulta1.ReadOnly = true;
            this.GrdGridConsulta1.RowHeadersVisible = false;
            this.GrdGridConsulta1.RowHeadersWidth = 20;
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Calibri", 10F);
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(133)))), ((int)(((byte)(255)))));
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.GrdGridConsulta1.RowTemplate.Height = 20;
            this.GrdGridConsulta1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.GrdGridConsulta1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GrdGridConsulta1.Size = new System.Drawing.Size(990, 200);
            this.GrdGridConsulta1.StandardTab = true;
            this.GrdGridConsulta1.TabIndex = 427;
            this.GrdGridConsulta1.TabStop = false;
            this.ttpFormulario.SetToolTip(this.GrdGridConsulta1, "Botón derecho del ratón para más opciones");
            this.GrdGridConsulta1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.GrdGridConsulta1_CellFormatting);
            this.GrdGridConsulta1.SelectionChanged += new System.EventHandler(this.GrdGridConsulta1_SelectionChanged);
            this.GrdGridConsulta1.Enter += new System.EventHandler(this.GrdGridConsulta1_Enter);
            this.GrdGridConsulta1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GrdGridConsulta1_MouseClick);
            // 
            // ide_0010
            // 
            this.ide_0010.DataPropertyName = "ide_0010";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.NullValue = "-";
            this.ide_0010.DefaultCellStyle = dataGridViewCellStyle9;
            this.ide_0010.HeaderText = "Identificador";
            this.ide_0010.MaxInputLength = 10;
            this.ide_0010.Name = "ide_0010";
            this.ide_0010.ReadOnly = true;
            this.ide_0010.Visible = false;
            // 
            // eje_0010
            // 
            this.eje_0010.DataPropertyName = "eje_0010";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.NullValue = "-";
            this.eje_0010.DefaultCellStyle = dataGridViewCellStyle10;
            this.eje_0010.FillWeight = 60F;
            this.eje_0010.HeaderText = "Año";
            this.eje_0010.MaxInputLength = 4;
            this.eje_0010.Name = "eje_0010";
            this.eje_0010.ReadOnly = true;
            this.eje_0010.Width = 60;
            // 
            // rel_0010
            // 
            this.rel_0010.DataPropertyName = "rel_0010";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N0";
            dataGridViewCellStyle11.NullValue = "-";
            this.rel_0010.DefaultCellStyle = dataGridViewCellStyle11;
            this.rel_0010.FillWeight = 80F;
            this.rel_0010.HeaderText = "Número";
            this.rel_0010.MaxInputLength = 10;
            this.rel_0010.Name = "rel_0010";
            this.rel_0010.ReadOnly = true;
            this.rel_0010.Width = 80;
            // 
            // des_0010
            // 
            this.des_0010.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.des_0010.DataPropertyName = "des_0010";
            this.des_0010.HeaderText = "Denominación";
            this.des_0010.MaxInputLength = 300;
            this.des_0010.Name = "des_0010";
            this.des_0010.ReadOnly = true;
            // 
            // fec_0010
            // 
            this.fec_0010.DataPropertyName = "fec_0010";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.Format = "d";
            dataGridViewCellStyle12.NullValue = "-";
            this.fec_0010.DefaultCellStyle = dataGridViewCellStyle12;
            this.fec_0010.FillWeight = 90F;
            this.fec_0010.HeaderText = "F. relación";
            this.fec_0010.MaxInputLength = 10;
            this.fec_0010.Name = "fec_0010";
            this.fec_0010.ReadOnly = true;
            this.fec_0010.ToolTipText = "Fecha de la relación";
            this.fec_0010.Width = 90;
            // 
            // fcp_0010
            // 
            this.fcp_0010.DataPropertyName = "fcp_0010";
            this.fcp_0010.FillWeight = 90F;
            this.fcp_0010.HeaderText = "F. caducidad";
            this.fcp_0010.Name = "fcp_0010";
            this.fcp_0010.ReadOnly = true;
            this.fcp_0010.ToolTipText = "Fecha caducidad pagos";
            this.fcp_0010.Width = 90;
            // 
            // fgf_0010
            // 
            this.fgf_0010.DataPropertyName = "fgf_0010";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.Format = "d";
            dataGridViewCellStyle13.NullValue = "-";
            this.fgf_0010.DefaultCellStyle = dataGridViewCellStyle13;
            this.fgf_0010.FillWeight = 90F;
            this.fgf_0010.HeaderText = "F. generación";
            this.fgf_0010.MaxInputLength = 10;
            this.fgf_0010.Name = "fgf_0010";
            this.fgf_0010.ReadOnly = true;
            this.fgf_0010.ToolTipText = "Fecha generación m-997";
            this.fgf_0010.Width = 90;
            // 
            // tot_0010
            // 
            this.tot_0010.DataPropertyName = "tot_0010";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Format = "N2";
            dataGridViewCellStyle14.NullValue = "-";
            this.tot_0010.DefaultCellStyle = dataGridViewCellStyle14;
            this.tot_0010.FillWeight = 110F;
            this.tot_0010.HeaderText = "Total importe";
            this.tot_0010.MaxInputLength = 18;
            this.tot_0010.Name = "tot_0010";
            this.tot_0010.ReadOnly = true;
            // 
            // nre_0010
            // 
            this.nre_0010.DataPropertyName = "nre_0010";
            this.nre_0010.HeaderText = "Número de documentos ";
            this.nre_0010.MaxInputLength = 6;
            this.nre_0010.Name = "nre_0010";
            this.nre_0010.ReadOnly = true;
            this.nre_0010.Visible = false;
            // 
            // fei_0010
            // 
            this.fei_0010.DataPropertyName = "fei_0010";
            dataGridViewCellStyle15.Format = "d";
            this.fei_0010.DefaultCellStyle = dataGridViewCellStyle15;
            this.fei_0010.HeaderText = "Fecha extracción";
            this.fei_0010.Name = "fei_0010";
            this.fei_0010.ReadOnly = true;
            this.fei_0010.Visible = false;
            // 
            // sre_0010
            // 
            this.sre_0010.DataPropertyName = "sre_0010";
            this.sre_0010.HeaderText = "Situación relación";
            this.sre_0010.Name = "sre_0010";
            this.sre_0010.ReadOnly = true;
            this.sre_0010.Visible = false;
            // 
            // Label_Info_1
            // 
            this.Label_Info_1.BackColor = System.Drawing.Color.Transparent;
            this.Label_Info_1.CausesValidation = false;
            this.Label_Info_1.Depth = 0;
            this.Label_Info_1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Info_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_Info_1.FormatoCorporativo = false;
            this.Label_Info_1.Location = new System.Drawing.Point(210, 90);
            this.Label_Info_1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_Info_1.Name = "Label_Info_1";
            this.Label_Info_1.Size = new System.Drawing.Size(580, 15);
            this.Label_Info_1.TabIndex = 433;
            this.Label_Info_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label_Info_2
            // 
            this.Label_Info_2.BackColor = System.Drawing.Color.Transparent;
            this.Label_Info_2.CausesValidation = false;
            this.Label_Info_2.Depth = 0;
            this.Label_Info_2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Info_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_Info_2.FormatoCorporativo = false;
            this.Label_Info_2.Location = new System.Drawing.Point(210, 320);
            this.Label_Info_2.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_Info_2.Name = "Label_Info_2";
            this.Label_Info_2.Size = new System.Drawing.Size(580, 15);
            this.Label_Info_2.TabIndex = 434;
            this.Label_Info_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ide_0010_1
            // 
            this.ide_0010_1.DataPropertyName = "ide_0010_1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.NullValue = "-";
            this.ide_0010_1.DefaultCellStyle = dataGridViewCellStyle3;
            this.ide_0010_1.FillWeight = 60F;
            this.ide_0010_1.HeaderText = "Identificador";
            this.ide_0010_1.MaxInputLength = 10;
            this.ide_0010_1.Name = "ide_0010_1";
            this.ide_0010_1.ReadOnly = true;
            this.ide_0010_1.Visible = false;
            this.ide_0010_1.Width = 60;
            // 
            // ejeord
            // 
            this.ejeord.DataPropertyName = "ejeord";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N0";
            dataGridViewCellStyle4.NullValue = "-";
            this.ejeord.DefaultCellStyle = dataGridViewCellStyle4;
            this.ejeord.FillWeight = 90F;
            this.ejeord.HeaderText = "Año-Orden";
            this.ejeord.MaxInputLength = 11;
            this.ejeord.Name = "ejeord";
            this.ejeord.ReadOnly = true;
            this.ejeord.Width = 90;
            // 
            // ibater
            // 
            this.ibater.DataPropertyName = "ibater";
            this.ibater.FillWeight = 200F;
            this.ibater.HeaderText = "I.B.A.N.";
            this.ibater.MaxInputLength = 34;
            this.ibater.Name = "ibater";
            this.ibater.ReadOnly = true;
            this.ibater.Width = 200;
            // 
            // nfa_0001
            // 
            this.nfa_0001.DataPropertyName = "nfa_0001";
            this.nfa_0001.FillWeight = 180F;
            this.nfa_0001.HeaderText = "Número factura";
            this.nfa_0001.MaxInputLength = 20;
            this.nfa_0001.Name = "nfa_0001";
            this.nfa_0001.ReadOnly = true;
            this.nfa_0001.Width = 180;
            // 
            // razsoc
            // 
            this.razsoc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.razsoc.DataPropertyName = "razsoc";
            this.razsoc.HeaderText = "Razón social";
            this.razsoc.MaxInputLength = 200;
            this.razsoc.Name = "razsoc";
            this.razsoc.ReadOnly = true;
            // 
            // fee_0001
            // 
            this.fee_0001.DataPropertyName = "fee_0001";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Format = "d";
            dataGridViewCellStyle5.NullValue = "-";
            this.fee_0001.DefaultCellStyle = dataGridViewCellStyle5;
            this.fee_0001.FillWeight = 80F;
            this.fee_0001.HeaderText = "F. registro";
            this.fee_0001.MaxInputLength = 10;
            this.fee_0001.Name = "fee_0001";
            this.fee_0001.ReadOnly = true;
            this.fee_0001.Width = 80;
            // 
            // liq_0001
            // 
            this.liq_0001.DataPropertyName = "liq_0001";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = "-";
            this.liq_0001.DefaultCellStyle = dataGridViewCellStyle6;
            this.liq_0001.HeaderText = "Importe";
            this.liq_0001.MaxInputLength = 18;
            this.liq_0001.Name = "liq_0001";
            this.liq_0001.ReadOnly = true;
            // 
            // ide_0001
            // 
            this.ide_0001.DataPropertyName = "ide_0001";
            this.ide_0001.HeaderText = "Identificador documento";
            this.ide_0001.MaxInputLength = 10;
            this.ide_0001.Name = "ide_0001";
            this.ide_0001.ReadOnly = true;
            this.ide_0001.Visible = false;
            // 
            // iderel
            // 
            this.iderel.DataPropertyName = "iderel";
            this.iderel.HeaderText = "Identificador relación";
            this.iderel.MaxInputLength = 10;
            this.iderel.Name = "iderel";
            this.iderel.ReadOnly = true;
            this.iderel.Visible = false;
            // 
            // sdo_0010_1
            // 
            this.sdo_0010_1.DataPropertyName = "sdo_0010_1";
            this.sdo_0010_1.HeaderText = "Situación documento";
            this.sdo_0010_1.MaxInputLength = 2;
            this.sdo_0010_1.Name = "sdo_0010_1";
            this.sdo_0010_1.ReadOnly = true;
            this.sdo_0010_1.Visible = false;
            // 
            // Relpag_221
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 590);
            this.Controls.Add(this.Label_Info_2);
            this.Controls.Add(this.Label_Info_1);
            this.Controls.Add(this.Label_2_1);
            this.Controls.Add(this.Label_2_2);
            this.Controls.Add(this.GrdGridConsulta2);
            this.Controls.Add(this.Label_1_1);
            this.Controls.Add(this.Label_1_2);
            this.Controls.Add(this.GrdGridConsulta1);
            this.Controls.Add(this.Panel);
            this.Name = "Relpag_221";
            this.OcultarComboVista = true;
            this.Text = "Relaciones de pagos. Modelo 997";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Relpag_221_FormClosing);
            this.Load += new System.EventHandler(this.Relpag_221_Load);
            this.Controls.SetChildIndex(this.Panel, 0);
            this.Controls.SetChildIndex(this.imgIntsa, 0);
            this.Controls.SetChildIndex(this.cmbVista, 0);
            this.Controls.SetChildIndex(this.GrdGridConsulta1, 0);
            this.Controls.SetChildIndex(this.Label_1_2, 0);
            this.Controls.SetChildIndex(this.Label_1_1, 0);
            this.Controls.SetChildIndex(this.GrdGridConsulta2, 0);
            this.Controls.SetChildIndex(this.Label_2_2, 0);
            this.Controls.SetChildIndex(this.Label_2_1, 0);
            this.Controls.SetChildIndex(this.Label_Info_1, 0);
            this.Controls.SetChildIndex(this.Label_Info_2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.imgIntsa)).EndInit();
            this.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Label_2_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdGridConsulta2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdGridConsulta1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_Info_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_Info_2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel Panel;
        private ClaseIntsa.Controles.btnBotonGeneral Boton_1;
        private ClaseIntsa.Controles.btnBotonGeneral Boton_2;
        private ClaseIntsa.Controles.btnBotonGeneral Boton_3;
        private ClaseIntsa.Controles.btnBotonGeneral Boton_4;
        private ClaseIntsa.Controles.btnBotonGeneral Boton_6;
        private ClaseIntsa.Controles.btnBotonGeneral Boton_5;
        private ClaseIntsa.Controles.lblLabelGeneral Label_2_1;
        private ClaseIntsa.Controles.lblLabelGeneral Label_2_2;
        private ClaseIntsa.Controles.grdGridConsulta GrdGridConsulta2;
        private ClaseIntsa.Controles.lblLabelGeneral Label_1_1;
        private ClaseIntsa.Controles.lblLabelGeneral Label_1_2;
        private ClaseIntsa.Controles.grdGridConsulta GrdGridConsulta1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ide_0010;
        private System.Windows.Forms.DataGridViewTextBoxColumn eje_0010;
        private System.Windows.Forms.DataGridViewTextBoxColumn rel_0010;
        private System.Windows.Forms.DataGridViewTextBoxColumn des_0010;
        private System.Windows.Forms.DataGridViewTextBoxColumn fec_0010;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcp_0010;
        private System.Windows.Forms.DataGridViewTextBoxColumn fgf_0010;
        private System.Windows.Forms.DataGridViewTextBoxColumn tot_0010;
        private System.Windows.Forms.DataGridViewTextBoxColumn nre_0010;
        private System.Windows.Forms.DataGridViewTextBoxColumn fei_0010;
        private System.Windows.Forms.DataGridViewTextBoxColumn sre_0010;
        private ClaseIntsa.Controles.lblLabelGeneral Label_Info_1;
        private ClaseIntsa.Controles.lblLabelGeneral Label_Info_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ide_0010_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ejeord;
        private System.Windows.Forms.DataGridViewTextBoxColumn ibater;
        private System.Windows.Forms.DataGridViewTextBoxColumn nfa_0001;
        private System.Windows.Forms.DataGridViewTextBoxColumn razsoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn fee_0001;
        private System.Windows.Forms.DataGridViewTextBoxColumn liq_0001;
        private System.Windows.Forms.DataGridViewTextBoxColumn ide_0001;
        private System.Windows.Forms.DataGridViewTextBoxColumn iderel;
        private System.Windows.Forms.DataGridViewTextBoxColumn sdo_0010_1;
    }
}
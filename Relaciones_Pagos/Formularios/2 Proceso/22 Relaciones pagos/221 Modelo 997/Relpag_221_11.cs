﻿using ClaseIntsa;
using ClaseIntsa.Controles;
using ClaseIntsa.Formularios;
using ClaseIntsa.Funciones;
using ClaseIntsa.Propiedades;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace Relaciones_Pagos
{
    public partial class Relpag_221_11 : frmFormularioSimple, ITf_Relpag
    {
        #region Interface
        public ITf_Relpag Form_Padre { get; set; }
        public grdGridConsulta[] _Cuadricula;
        public int?[] _Identificador;

        /// <summary>
        /// Devolución del formulario general (ITf_Relpag)
        /// </summary>
        /// <param name="Cuadricula">Cuadrículas del formulario</param>
        /// <param name="Identificador">Identificadores de registro de las cuadrículas</param>
        /// <param name="Cuadricula_Activa">Cuadrícula que va a recibir el foco</param>
        public void Retorno_Formulario(grdGridConsulta[] Cuadricula, int?[] Identificador, byte Cuadricula_Activa = 0)
        {
            for (short i = 0; i < Cuadricula.Length; i++)
            {
                if (Identificador[i] < 0)
                {
                    Id_Cuadricula[i] = 0;
                    Cuadricula[i].CargarGrid((int)Identificador[i]);
                }
                else
                {
                    if (Identificador[i] != 0)
                    {
                        Id_Cuadricula[i] = 0;
                        Cuadricula[i].CargarGrid((int)Identificador[i]);
                    }
                }
            }

            Cuadricula[Cuadricula_Activa].Select();
        }

        #endregion

        #region Propiedades

        /// <summary>
        /// Acción actual
        /// </summary>
        public sbyte Accion_Actual { get; set; } = -1;

        /// <summary>
        /// Matriz de identificadores actuales de cada cuadrícula
        /// </summary>
        private int[] Id_Cuadricula = new int[1];

        #endregion Propiedades

        #region Inicialización del formulario

        /// <summary>
        ///  Inicialización de componentes u asignación cadena conexión
        /// </summary>
        /// <param name="Numero_Cuadriculas">Número de cuadriculas a manipular</param>
        public Relpag_221_11(short Numero_Cuadriculas)
        {
            InitializeComponent();
            this.CadenaConexion = Variables_Globales.CadenaConexionAplicativo;
            Array.Resize(ref _Cuadricula, Numero_Cuadriculas);
            Array.Resize(ref _Identificador, Numero_Cuadriculas);
        }

        #endregion Inicialización del formulario

        #region Carga del formulario

        /// <summary>
        /// Carga formulario, inicialización de variables y preparación formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Relpag_221_11_Load(object sender, EventArgs e)
        {
            Refresca_Panel();
            _Identificador[0] = 0;

            GrdGridConsulta1.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GrdGridConsulta1.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GrdGridConsulta1.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            Asignar_Valores();
 
            Combo4_1.CargarCombo();
            Combo4_1.SelectedValue = 0;

            Accion_Actual = 0;
            GrdGridConsulta1.SQLInicial = Sql_Inicial_Cuadricula_1((int)_Identificador[0]);
            GrdGridConsulta1.CargarGrid();

            Refresca_Panel();
            Text4.Focus();
        }

        #endregion Carga del formulario

        #region Funciones 

        /// <summary>
        /// Asigna los valores iniciales
        /// </summary>
        private void Asignar_Valores()
        {
            Text2.Text = Propiedades_Aplicativo.Ejercicio_Trabajo.ToString();
            Text3.Text = "";

            if (Text2.Text == DateTime.Now.Year.ToString())
                Text5.Text = DateTime.Now.ToString("dd/MM/yyyy");
            else
                Text5.Text = "31/12/" + Text2.Text;

            Text5_1.Text = Convert.ToDateTime(Text5.Text).AddDays(-10).ToString("dd/MM/yyyy");
            Text6.Text = DateTime.Now.ToString("dd/MM/yyyy");
            Text7.Text = Convert.ToDateTime(Text5.Text).AddDays(20).ToString("dd/MM/yyyy");
        }

        /// <summary>
        /// Recarga la cuadricula
        /// </summary>
        private void Recargar_Cuadricula()
        {
            if (Accion_Actual == 0)
            {
                GrdGridConsulta1.SQLInicial = Sql_Inicial_Cuadricula_1((int)_Identificador[0]);
                GrdGridConsulta1.CargarGrid();
                Text9.Text = "0";
                Text10.Text = "0";
            }

        }

        /// <summary>
        /// Genera la SQL inicial de la cuadrícula. 'RELACIONES DE PAGOS. MODELO-977. DETALLE'
        /// </summary>
        /// <returns>Cadena 'SQL'</returns>
        private string Sql_Inicial_Cuadricula_1(int Id_0010 = 0)
        {
            string Cadena_Sql;
            string[] Sql_Cadena = new string[2];
            string[] Cadena_Select = new string[2];
            string[] Cadena_From = new string[2];
            string[] Cadena_Where = new string[2];

            string Restriccion_Identificador = $"relpag_v_0010_1.ide_0010 = {Id_0010}";
            string Restriccion_Sin_Relacion = "sdr_0001 = 0";
            string Restriccion_Fecha_Aprobacion = $"acf_0001 <= '{Text5.Text.Trim()}'";
            string Restriccion_Registro = $"fee_0001 <= '{Text5_1.Text.Trim()}'";
            string Restriccion_Situacion_Aprobacion = $"sac_0001 = '4'";
            string Restriccion_Canal_Pago = $"cpa_0001 = ''";
            string Restriccion_Aplicada = $"pap_0001 IN ('1', '4')";
            string Restriccion_Situacion_Contable = $"sco_0001 = '4'";
            string Restriccion_Importe = $"liq_0001 > 0";
            string Restriccion_Ejercicio = $"eco_0001 = {Propiedades_Aplicativo.Ejercicio_Trabajo}";
            string Restriccion_Clase_Documento = "";

            switch (Convert.ToByte(Combo4_1.SelectedValue))
            {
                case 0:                 // TODOS
                    {
                        Restriccion_Clase_Documento = "tif_0001 IN ('1', '6')";

                        break;
                    }

                case 1:                 // NORMALES
                    {
                        Restriccion_Clase_Documento = "tif_0001 = '1'";

                        break;
                    }

                case 2:                 // PAGOS FIJOS
                    {
                        Restriccion_Clase_Documento = "tif_0001 = '6'";

                        break;
                    }
            }

            Cadena_Select[0] = "SELECT 'X' regsel, eje_0001 + '-' + ord_0001 ejeord, Funciones_Generales.dbo.Normaliza_Iban (iba_0001) ibater, nfa_0001, razsoc, fee_0001, liq_0001, bic_0001, ide_0001, ide_0010_1 idereg, obs_0010_1 obsdoc,";
            Cadena_Select[0] += " Funciones_Generales.dbo.Validar_Iban (iba_0001) ibaval";
            Cadena_From[0] = $" FROM  {Variables_Globales.NombreBaseDatosAplicativo}.dbo.relpag_v_0010_1";
            Cadena_Where[0] = Restriccion_Identificador;

            Cadena_Select[1] = "SELECT ' ' regsel, eje_0001 + '-' + ord_0001 ejeord, Funciones_Generales.dbo.Normaliza_Iban (iba_0001) ibater, nfa_0001, Funciones_Generales.dbo.Normaliza_Razon_Social(ape_0001, nom_0001) AS razsoc,";
            Cadena_Select[1] += " fee_0001, liq_0001, bic_0001, ide_0001, 0 idereg, space (125) obsdoc,  Funciones_Generales.dbo.Validar_Iban (iba_0001) ibaval";
            Cadena_From[1] = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.relpag_0001";

            Cadena_Where[1] = Restriccion_Sin_Relacion + " AND " + Restriccion_Fecha_Aprobacion;
            Cadena_Where[1] += " AND " + Restriccion_Registro + " AND " + Restriccion_Situacion_Aprobacion;
            Cadena_Where[1] += " AND " + Restriccion_Canal_Pago + " AND " + Restriccion_Aplicada;
            Cadena_Where[1] += " AND " + Restriccion_Situacion_Contable + " AND " + Restriccion_Importe;
            Cadena_Where[1] += " AND " + Restriccion_Ejercicio + " AND " + Restriccion_Clase_Documento;

            for (byte i = 0; i < Sql_Cadena.Length; i++)
                Sql_Cadena[i] = Cadena_Select[i] + Cadena_From[i] + " WHERE " + Cadena_Where[i];

            Cadena_Sql = Sql_Cadena[0];

            for (byte i = 1; i < Sql_Cadena.Length; i++)
                Cadena_Sql += $" UNION ALL {Sql_Cadena[i]}";

            return Cadena_Sql;
        }

        #endregion Funciones 

        #region Funciones manipulación tablas

        #endregion

        #region Botonera

        /// <summary>
        /// Refresco del panel de botones
        /// </summary>
        private void Refresca_Panel()
        {
            Control[] Boton;
            btnBotonGeneral Boton_Panel;

            for (byte i = 0; i < Panel.Controls.Count; i++)
            {
                Boton = Panel.Controls.Find("Boton_" + Convert.ToString(i + 1), false);

                if (Boton.Length != 0)
                {
                    Boton_Panel = (btnBotonGeneral)Boton[0];
                    Boton_Panel.Refrescar();
                }
            }
        }

        /// <summary>
        /// Grabar registro. Refresco del botón
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_1_Refresco(object sender, EventArgs e)
        {
            Boton_1.Enabled = (Accion_Actual == 0) && Convert.ToInt16(Text9.Text) != 0;
        }

        /// <summary>
        /// Verifica datos e inserta registro
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_1_Click(object sender, EventArgs e)
        {
            DataTable Asignaciones = (DataTable)GrdGridConsulta1.DataSource;
            Tuple<short, decimal> Totales;
            int Numero_Relacion;
            string Nombre_Secuencia;

            Accion_Actual = 2;
            Refresca_Panel();

            if (ValidarControles())
            {
                Nombre_Secuencia = "rel_0010_" + Text2.Text;
                // DESACTIVA CONTROLES
                Funciones_Genericas.Activar_Controles("1", this, false);

                if (Text3.Text.Trim() == string.Empty)
                {
                    Control_Secuencias.Existe_Secuencia(CadenaConexion, Variables_Globales.NombreBaseDatosAplicativo, Nombre_Secuencia, "Secuencia número de relacion por año");
                    Numero_Relacion = Funciones_Aplicativo.Verificar_Secuencia(CadenaConexion, Variables_Globales.NombreBaseDatosAplicativo, Nombre_Secuencia, 1, Convert.ToInt16(Text2.Text));
                    Text3.Text = Numero_Relacion.ToString();
                }
                else
                    Numero_Relacion = Convert.ToInt32(Text2.Text);

                if (Selecciones.Verificar_Registros_Marcados(Asignaciones))
                {
                    _Identificador[0] = Desencadenantes.Insertar_Relpag_0010(CadenaConexion, Convert.ToInt16(Text2.Text), Convert.ToInt32(Text3.Text), Text4.Text, Text5.Text, Text6.Text, Text7.Text);
                    Text1.Text = Convert.ToString(_Identificador[0]);

                    Totales = Selecciones.Recorrer_Registros_Relpag_0010_1(CadenaConexion, Asignaciones, (int)_Identificador[0]);
                    Desencadenantes.Actualizar_Relpag_0010(CadenaConexion, (int)_Identificador[0], Totales.Item1, Totales.Item2);

                    Text9.Text = Totales.Item1.ToString();
                    Text10.Text = Totales.Item2.ToString();

                    Accion_Actual = 1;
                    Refresca_Panel();
                    Boton_2.Focus();
                }
                else
                {
                    PresentaMensaje("DEBE EXISTIR AL MENOS UN DOCUMENTO MARCADO", 2, Color.Red);
                    Accion_Actual = 0;
                    Refresca_Panel();
                    GrdGridConsulta1.Select();
                }
            }
            else
            {
                Accion_Actual = 0;
                Refresca_Panel();

                SelectNextControl(ControlFoco.Parent, true, true, true, true);
                SelectNextControl(ControlFoco, true, true, true, true);
            }
        }

        /// <summary>
        /// Nueva alta. Refresco del botón
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_2_Refresco(object sender, EventArgs e)
        {
            Boton_2.Enabled = (Accion_Actual == 1);
        }

        /// <summary>
        /// Limpia los datos del formulario para la nueva alta.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_2_Click(object sender, EventArgs e)
        {
            Funciones_Genericas.Activar_Controles("1", this, true);            // ACTIVA CONTROLES
            Funciones_Genericas.Inicializar_Controles(this);                      // INICIALIZAR CONTROLES
            Asignar_Valores();

            GrdGridConsulta1.SQLInicial = Sql_Inicial_Cuadricula_1();
            GrdGridConsulta1.CargarGrid();

            Accion_Actual = 0;
            Refresca_Panel();
            Text4.Focus();
        }

        #endregion

        #region Campos del formulario

        /// <summary>
        /// Validación del número de relación
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text3_Validating(object sender, CancelEventArgs e)
        {
            if (Text3.Text.Trim() == string.Empty)
            {
                if (Accion_Actual == 0)
                    PresentaMensaje("NÚMERO RELACIÓN. NUMERACIÓN AUTOMÁTICA", 3, Color.DarkGreen);
            }
            else
            {
                if (Funciones_Genericas.Solo_Numeros(Text3.Text))
                {
                    if (Text3.Text.Trim() == "0")
                    {
                        PresentaMensaje("NÚMERO RELACIÓN. DEBE DE SER MAYOR QUE CERO", 2, Color.Red);
                        ControlFoco = Text3;
                        e.Cancel = true;
                    }
                    else
                    {
                        if (Funciones_Aplicativo.Identificador_Relpag_0010(CadenaConexion, Convert.ToInt16(Text2.Text), Convert.ToInt32(Text3.Text)) != 0)
                        {
                            PresentaMensaje($"NÚMERO RELACIÓN [{Text2.Text}-{Text3.Text.Trim()}]. EXISTENTE", 2, Color.Red);
                            ControlFoco = Text3;
                            e.Cancel = true;
                        }
                    }
                }
                else
                {
                    PresentaMensaje("NÚMERO RELACIÓN. FORMATO ERRÓNEO (0 a 9).", 2, Color.DarkGreen);
                    ControlFoco = Text3;
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Validación de la descripción de la relación
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text4_Validating(object sender, CancelEventArgs e)
        {
            if (Text4.Text == string.Empty)
            {
                PresentaMensaje("DESCRIPCIÓN RELACIÓN. NO PUEDE ESTAR VACIA", 2, Color.DarkGreen);
                ControlFoco = Text4;
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Actualiza la cuadrícula
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Combo4_1_SelectedValueChanged(object sender, EventArgs e)
        {
            Recargar_Cuadricula();
        }

        /// <summary>
        /// Validación de la fecha de la relación y actualización cuadrícula
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        private void Text5_Validating(object sender, CancelEventArgs e)
        {
            if (Text5.Text.Trim() == "/  /")
            {
                Text5.Text = DateTime.Now.ToString("dd/MM/yyyy");
                Text5_1.Text = Convert.ToDateTime(Text5.Text).AddDays(-10).ToString("dd/MM/yyyy");
                Text7.Text = Convert.ToDateTime(Text5.Text).AddDays(20).ToString("dd/MM/yyyy");
            }

            Text5.ValidateText();

            if (!Text5.FormatoCorrecto)
            {
                PresentaMensaje("FECHA RELACIÓN. FORMATO INCORRECTO", 2, Color.Red);
                ControlFoco = Text5;
                e.Cancel = true;
            }
            else
            {
                if (Text2.Text != Convert.ToDateTime(Text5.Text).Year.ToString())
                {
                    PresentaMensaje("AÑO FECHA RELACIÓN. NO COINCIDE AÑO DE LA RELACIÓN", 2, Color.Red);
                    ControlFoco = Text5;
                    e.Cancel = true;
                }
            }

            if (!e.Cancel)
            {
                if (Text5.Cambio_Valor)
                    Recargar_Cuadricula();
            }
        }

        /// <summary>
        /// Validación de la fecha de registro y actualización cuadrícula
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text5_1_Validating(object sender, CancelEventArgs e)
        {
            if (Text5_1.Text.Trim() == "/  /")
            {
                Text5_1.Text = Convert.ToDateTime(Text5.Text).AddDays(-20).ToString("dd/MM/yyyy");
            }

            Text5_1.ValidateText();

            if (!Text5_1.FormatoCorrecto)
            {
                PresentaMensaje("FECHA REGISTRO. FORMATO INCORRECTO", 2, Color.Red);
                ControlFoco = Text5_1;
                e.Cancel = true;
            }
            else
            {
                if (Convert.ToDateTime(Text5_1.Text) > Convert.ToDateTime(Text5.Text))
                {
                    PresentaMensaje("FECHA REGISTRO. MAYOR QUE FECHA RELACIÓN", 2, Color.Red);
                    ControlFoco = Text5_1;
                    e.Cancel = true;
                }
            }

            if (!e.Cancel)
            {
                if (Text5_1.Cambio_Valor)
                    Recargar_Cuadricula();
            }
        }

        /// <summary>
        /// Validación de la fecha de caducidad de los pagos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text7_Validating(object sender, CancelEventArgs e)
        {
            if (Text7.Text.Trim() == "/  /")
            {
                PresentaMensaje("FECHA CADUCIDAD DE PAGOS. OBLIGATORIA", 2, Color.Red);
                ControlFoco = Text7;
                e.Cancel = true;
            }
            else
            {
                Text7.ValidateText();

                if (!Text7.FormatoCorrecto)
                {
                    PresentaMensaje("FECHA CADUCIDAD DE PAGOS. FORMATO INCORRECTO", 2, Color.Red);
                    ControlFoco = Text7;
                    e.Cancel = true;
                }
                else
                {
                    if (Convert.ToDateTime(Text7.Text) <= Convert.ToDateTime(Text5.Text))
                    {
                        PresentaMensaje("FECHA CADUCIDAD DE PAGOS. MENOR O IGUAL FECHA RELACIÓN", 2, Color.Red);
                        ControlFoco = Text7;
                        e.Cancel = true;
                    }
                }
            }
        }

        #region Cuadrículas

        /// <summary>
        /// Cambio de registro 'RELACIÓN DE PAGOS. DOCUMENTOS'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_SelectionChanged(object sender, EventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            string Iban;
            string Bic_Swift;
            byte Iban_Valido;

            int Identificador;

            if (Cuadricula.SelectedRows.Count > 0)
            {
                Identificador = Convert.ToInt32(Cuadricula.SelectedRows[0].Cells[Cuadricula.ColumnaClaveIdentidad].Value);

                if (Identificador != Id_Cuadricula[0]
                  &&
                      Identificador != 0)
                {
                    Id_Cuadricula[0] = Identificador;
                    Funciones_Genericas._Numerador_Registros(Cuadricula, Label_1_2);

                    Iban_Valido = Convert.ToByte(Cuadricula.SelectedRows[0].Cells["ibaval"].Value);
                    Iban = Convert.ToString(Cuadricula.SelectedRows[0].Cells["ibater"].Value);
                    Bic_Swift = Convert.ToString(Cuadricula.SelectedRows[0].Cells["bic_0001"].Value);
                    Label_Info_2.Text = "";

                    if (Iban_Valido == 0)
                    {
                        if (Iban.Trim() != string.Empty)
                        {
                            Label_Info_2.Text = "DOCUMENTO. I.B.A.N. ERRÓNEO";
                            Label_Info_2.ForeColor = Color.Red;
                        }
                        else
                        {
                            Label_Info_2.Text = "DOCUMENTO. SIN I.B.A.N.";
                            Label_Info_2.ForeColor = Color.Green;
                        }
                    }
                    else
                    {
                        if (Bic_Swift.Trim() == string.Empty)
                        {
                            Label_Info_2.Text = "DOCUMENTO. SIN B.I.C./S.W.I.F.T.";
                            Label_Info_2.ForeColor = Color.DarkOrange;
                        }
                    }

                    Refresca_Panel();
                }
            }
        }

        /// <summary>
        /// Formatea la cuadrícula 'RELACIÓN DE PAGOS. DOCUMENTOS'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            string Marcada = Convert.ToString(Cuadricula.Rows[e.RowIndex].Cells["regsel"].Value);
            byte Iban_Valido = Convert.ToByte(Cuadricula.Rows[e.RowIndex].Cells["ibaval"].Value);
            string Iban = Cuadricula.Rows[e.RowIndex].Cells["ibaval"].Value.ToString();
            string Bic_Switf = Cuadricula.Rows[e.RowIndex].Cells["bic_0001"].Value.ToString();
            Color Color_Letra;

            if (Iban_Valido == 0)
            {
                if (Iban.Trim() != string.Empty)
                    Color_Letra = Color.Red;
                else
                    Color_Letra = Color.Green;
            }
            else
            {
                if (Bic_Switf.Trim() == string.Empty)
                    Color_Letra = Color.DarkOrange;
                else
                {
                    if (Marcada.Trim() == string.Empty)
                        Color_Letra = Color.FromArgb(0, 100, 255);
                    else
                        Color_Letra = Color.Purple;
                }
            }

            e.CellStyle.ForeColor = Color_Letra;
            e.CellStyle.SelectionForeColor = Color_Letra;
        }

        /// <summary>
        /// Marca/desmarca registro seleccionado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            string Marca;
            short Registros;
            decimal Total_Importe;
            decimal Importe;
            byte Iban_Valido;
            string Bic_Switf;

            if (Accion_Actual == 0)
            {
                if (Cuadricula.SelectedRows.Count > 0)
                {
                    Marca = Cuadricula.SelectedRows[0].Cells["regsel"].Value.ToString();
                    Importe = Convert.ToDecimal(Cuadricula.SelectedRows[0].Cells["liq_0001"].Value);
                    Registros = Convert.ToInt16(Text9.Text);
                    Total_Importe = Convert.ToDecimal(Text10.Text);
                    Iban_Valido = Convert.ToByte(Cuadricula.SelectedRows[0].Cells["ibaval"].Value);
                    Bic_Switf = Cuadricula.SelectedRows[0].Cells["bic_0001"].Value.ToString();

                    if (e.KeyCode == Keys.Space)
                    {
                        if (Iban_Valido == 1
                             &&
                             Bic_Switf.Trim() != string.Empty)
                        {
                            if (Marca.Trim() == string.Empty)
                            {
                                Cuadricula.SelectedRows[0].Cells["regsel"].Value = "X";
                                Registros++;
                                Total_Importe += Importe;
                            }
                            else
                            {
                                Cuadricula.SelectedRows[0].Cells["regsel"].Value = "";
                                Registros--;
                                Total_Importe -= Importe;
                            }

                            Text9.Text = Registros.ToString();
                            Text10.Text = Total_Importe.ToString();
                            Cuadricula.Refresh();
                            Refresca_Panel();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Selecciona un registro con doble click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            string Marca;
            short Registros;
            decimal Total_Importe;
            decimal Importe;
            byte Iban_Valido;
            string Bic_Switf;

            if (Accion_Actual == 0)
            {
                if (e.Button == MouseButtons.Left)
                {
                    var Fila_Columna = Cuadricula.HitTest(e.X, e.Y);

                    if (Cuadricula.Rows.Count != 0)
                    {
                        if (Fila_Columna.RowIndex >= 0)
                        {
                            Cuadricula.ClearSelection();
                            Cuadricula.Rows[Fila_Columna.RowIndex].Selected = true;

                            Marca = Convert.ToString(Cuadricula.Rows[Fila_Columna.RowIndex].Cells["regsel"].Value);
                            Importe = Convert.ToDecimal(Cuadricula.Rows[Fila_Columna.RowIndex].Cells["liq_0001"].Value);
                            Registros = Convert.ToInt16(Text9.Text);
                            Total_Importe = Convert.ToDecimal(Text10.Text);

                            Iban_Valido = Convert.ToByte(Cuadricula.Rows[Fila_Columna.RowIndex].Cells["ibaval"].Value);
                            Bic_Switf = Cuadricula.Rows[Fila_Columna.RowIndex].Cells["bic_0001"].Value.ToString();

                            if (Iban_Valido == 1
                                 &&
                                 Bic_Switf.Trim() != string.Empty)
                            {
                                if (Marca.Trim() == string.Empty)
                                {
                                    Cuadricula.SelectedRows[0].Cells["regsel"].Value = "X";
                                    Registros++;
                                    Total_Importe += Importe;
                                }
                                else
                                {
                                    Cuadricula.SelectedRows[0].Cells["regsel"].Value = "";
                                    Registros--;
                                    Total_Importe -= Importe;
                                }

                                Text9.Text = Registros.ToString();
                                Text10.Text = Total_Importe.ToString();
                                Cuadricula.Refresh();
                                Refresca_Panel();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Mostrar la etiqueta cuando recibe en foco
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_Enter(object sender, EventArgs e)
        {
            if (Accion_Actual == 0)
            {
                if (GrdGridConsulta1.Rows.Count != 0)
                    Label_Info_1.Visible = true;
            }
        }

        /// <summary>
        /// Ocultar la etiqueta cuando recibe en foco
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_Leave(object sender, EventArgs e)
        {
            if (Accion_Actual == 0)
            {
                if (GrdGridConsulta1.Rows.Count != 0)
                    Label_Info_1.Visible = false;
            }
        }

        #region Menú contextual cuadrícula 'RELACIÓN DE PAGOS. DOCUMENTOS'

        /// <summary>
        /// Crea menú contextual de acciones cuadrícula 'CLASIFICACIÓN DE EMPLEADOS PÚBLICOS. ASIGNACIONES'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_MouseClick(object sender, MouseEventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            byte Iban_Valido;
            string Bic_Switf;

            if (Accion_Actual == 0)
            {
                if (e.Button == MouseButtons.Right)
                {
                    ContextMenu Menu_Contextual = new ContextMenu();
                    Cuadricula.Focus();

                    var Fila_Columna = Cuadricula.HitTest(e.X, e.Y);
                    MenuItem Item_Menu;

                    if (Cuadricula.Rows.Count != 0)
                    {
                        if (Fila_Columna.RowIndex >= 0)
                        {
                            Cuadricula.ClearSelection();
                            Cuadricula.Rows[Fila_Columna.RowIndex].Selected = true;
                            Iban_Valido = Convert.ToByte(Cuadricula.SelectedRows[0].Cells["ibaval"].Value);
                            Bic_Switf = Cuadricula.SelectedRows[0].Cells["bic_0001"].Value.ToString();

                            #region 1 Marcar registro

                            if (Iban_Valido == 1
                                 &&
                                 Bic_Switf.Trim() != string.Empty
                                 &&
                                Cuadricula.SelectedRows[0].Cells["regsel"].Value.ToString().Trim() == string.Empty)
                            {
                                Item_Menu = new MenuItem
                                {
                                    Text = "Marcar registro"
                                };

                                Item_Menu.Click += Marcar_Registro_Click;
                                Menu_Contextual.MenuItems.Add(Item_Menu);
                            }

                            #endregion Marcar registro

                            #region 2 Desmarcar registro

                            if (Iban_Valido == 1
                                 &&
                                 Bic_Switf.Trim() != string.Empty
                                 &&
                                Cuadricula.SelectedRows[0].Cells["regsel"].Value.ToString().Trim() == "X")
                            {
                                Item_Menu = new MenuItem
                                {
                                    Text = "Desmarcar registro"
                                };

                                Item_Menu.Click += Desmarcar_Registro_Click;
                                Menu_Contextual.MenuItems.Add(Item_Menu);
                            }

                            #endregion Desmarcar registro

                            #region Modificación I.B.A.N.

                                Item_Menu = new MenuItem
                                {
                                    Text = "Modificar I.B.A.N."
                                };

                                Item_Menu.Click += Modificar_Iban_Click;
                            Menu_Contextual.MenuItems.Add(Item_Menu);

                            #endregion Modificación I.B.A.N./S.W.I.F.T.

                        }
                    }

                    Menu_Contextual.Show(Cuadricula, new Point(e.X, e.Y));
                }
            }
        }

        /// <summary>
        /// Marca registro
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Marcar_Registro_Click(Object sender, EventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            short Registros = Convert.ToInt16(Text9.Text);
            decimal Total_Importe = Convert.ToDecimal(Text10.Text);
            decimal Importe = Convert.ToDecimal(Cuadricula.SelectedRows[0].Cells["liq_0001"].Value);

            Registros++;
            Total_Importe += Importe;

            Cuadricula.SelectedRows[0].Cells["regsel"].Value = "X";
            Text9.Text = Registros.ToString();
            Text10.Text = Total_Importe.ToString();
            Cuadricula.Refresh();
            Refresca_Panel();
        }

        /// <summary>
        /// Quita la marca 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Desmarcar_Registro_Click(Object sender, EventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            short Registros = Convert.ToInt16(Text9.Text);
            decimal Total_Importe = Convert.ToDecimal(Text10.Text);
            decimal Importe = Convert.ToDecimal(Cuadricula.SelectedRows[0].Cells["liq_0001"].Value);

            Registros--;
            Total_Importe -= Importe;

            Cuadricula.SelectedRows[0].Cells["regsel"].Value = "";
            Text9.Text = Registros.ToString();
            Text10.Text = Total_Importe.ToString();
            Cuadricula.Refresh();
            Refresca_Panel();
        }

        /// <summary>
        /// Abre un formulario para modificar el I.B.A.N.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Modificar_Iban_Click(Object sender, EventArgs e)
        {
            Relpag_221_5 Form_Altas = new Relpag_221_5(1);
            Form_Altas._Cuadricula[0] = GrdGridConsulta1;
            Form_Altas._Identificador[0] = Id_Cuadricula[0];
            Form_Altas._Iban = GrdGridConsulta1.SelectedRows[0].Cells["ibater"].Value.ToString();
            Form_Altas._Bic_Switf = GrdGridConsulta1.SelectedRows[0].Cells["bic_0001"].Value.ToString();

            Form_Altas.Form_Padre = this;
            Funciones_Globales.AbrirFormulario(Form_Altas, true, false);

        }

         #endregion Menú contextual cuadrícula 'RELACIÓN DE PAGOS. DOCUMENTOS'

        #endregion Cuadrículas

        #endregion Campos formulario

        #region Cierre Formulario

        /// <summary>
        ///  Cierre del formulario y asignación del valor del último identificador dato de alta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Relpag_221_11_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form_Padre.Retorno_Formulario(_Cuadricula, _Identificador, 0);
        }

        #endregion

        //
    }
    //
}

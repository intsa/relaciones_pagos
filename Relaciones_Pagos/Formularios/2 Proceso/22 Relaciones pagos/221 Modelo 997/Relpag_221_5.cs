﻿using ClaseIntsa.Clases;
using ClaseIntsa.Controles;
using ClaseIntsa.Formularios;
using ClaseIntsa.Funciones;
using ClaseIntsa.Propiedades;
using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Relaciones_Pagos
{
    public partial class Relpag_221_5 : frmFormularioSimple
    {
        #region Interface

        public ITf_Relpag Form_Padre { get; set; }
        public grdGridConsulta[] _Cuadricula;
        public int?[] _Identificador;
        public string _Iban;
        public string _Bic_Switf;

        #endregion Interface

        #region Propiedades

        /// <summary>
        /// Acción ejecutada actual
        /// </summary>
        public sbyte Accion_Actual { get; set; } = -1;

        #endregion Propiedades

        #region Inicialización del formulario

        /// <summary>
        ///  Inicialización de componentes y asignación cadena conexión
        /// </summary>
        public Relpag_221_5(short Numero_Cuadriculas)
        {
            InitializeComponent();
            this.CadenaConexion = Variables_Globales.CadenaConexionAplicativo;
            Array.Resize(ref _Cuadricula, Numero_Cuadriculas);
            Array.Resize(ref _Identificador, Numero_Cuadriculas);
        }

        #endregion Inicialización del formulario

        #region Carga del formulario

        /// <summary>
        /// Carga formulario, inicialización de variables y preparación formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Relpag_221_5_Load(object sender, EventArgs e)
        {
            Presenta_Datos();
            Accion_Actual = 0;
            Iban_Panel.Controls[1].Focus();
            Refresca_Panel();
        }

        #endregion Carga del formulario

        #region Funciones

        /// <summary>
        /// Presenta los datos en el formulario
        /// </summary>
        private void Presenta_Datos()
        {
            string Iban_Auxiliar = _Iban;

            Text1.Text = _Bic_Switf.Trim();

            if (_Iban.Trim() == string.Empty)
                Iban_Auxiliar = "ES";

            Iban_Panel.Presentar_Iban(Iban_Auxiliar);
            Funciones_Genericas.Activar_Controles("2", this, true);         // ACTIVAR CONTROLES
        }

        #endregion Funciones

        #region Funciones de manipulación de base de datos

        #endregion

        #region Botonera

        /// <- Refresca el panel superior de botones ->
        /// Parámetros
        private void Refresca_Panel()
        {
            Control[] Boton;
            btnBotonGeneral Boton_Panel;

            for (byte i = 0; i < Panel.Controls.Count; i++)
            {
                Boton = Panel.Controls.Find("Boton_" + Convert.ToString(i + 1), false);

                if (Boton.Length != 0)
                {
                    Boton_Panel = (btnBotonGeneral)Boton[0];
                    Boton_Panel.Refrescar();
                }
            }
        }

        /// <- Refresco botón -1 ->
        /// <- Boton_1_Refresco ->
        /// Parámetros
        /// <1 sender>
        /// <2 e>

        /// <summary>
        ///  Refresco botón de actualización datos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_1_Refresco(object sender, EventArgs e)
        {
            Boton_1.Enabled = (Accion_Actual == 0);
        }

        /// <summary>
        /// Ejecuta los procesos de actualización
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_1_Click(object sender, EventArgs e)
        {
            Accion_Actual = 2;
            Refresca_Panel();
            Boton_1.Select();

            if (ValidarControles())
            {
                Funciones_Genericas.Activar_Controles("2", this, false);         // DESACTIVA CONTROLES
                Iban_Panel.Activar_Controles(false);          // DESACTIVA CONTROLES

                 Desencadenantes.Actualizar_Relpag_0001(CadenaConexion, (int)_Identificador[0], Iban_Panel.Iban_Normalizado, Text1.Text);

                Accion_Actual = 1;
                Refresca_Panel();
                imgIntsa.Focus();
            }
            else            // VALIDA B.I.C.
            {
                Iban_Panel.Activar_Controles(true);          // ACTIVA CONTROLES
                Funciones_Genericas.Activar_Controles("2", this, true);         // ACTIVA CONTROLES

                Accion_Actual = 0;
                Refresca_Panel();

                if (ControlFoco == null)
                {
                    if (Control_Erroneo != null)
                    {
                        SelectNextControl(Control_Erroneo.Parent, true, true, true, true);
                        SelectNextControl(Control_Erroneo, true, true, true, true);

                    }
                }

            }
        }

        /// <summary>
        ///  Refresco botón de actualización datos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_2_Refresco(object sender, EventArgs e)
        {
            Boton_2.Enabled = (Accion_Actual == 0);
        }

        /// <summary>
        /// Pone los valores originales
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_2_Click(object sender, EventArgs e)
        {
            Presenta_Datos();
            Text1.Focus();
        }

        #endregion Botonera

        #region Campos formulario

        /// <summary>
        /// Validación B.I.C./S.W.I.F.T. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text1_Validating(object sender, CancelEventArgs e)
        {
            if (Text1.Enabled)
            {
                if (Text1.Text.Trim() == string.Empty)
                {
                    PresentaMensaje("B.I.C./S.W.I.F.T. OBLIGATORIO", 2, Color.Red);
                    ControlFoco = Text1;
                    e.Cancel = true;

                }
                else
                {
                    if (Text1.Text.Trim().Length != 11)
                    {
                        PresentaMensaje("B.I.C./S.W.I.F.T. LONGITUD INCORRECTA. DEBE DE TENER 11 CARACTERES", 2, Color.Red);
                        ControlFoco = Text1;
                        e.Cancel = true;
                    }
                }
            }
        }

        #endregion Campos formulario

        #region Cierre formulario

        /// <summary>
        ///  Cierre del formulario y asignación del valor del último identificador dato de alta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Relpag_221_5_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form_Padre.Retorno_Formulario(_Cuadricula, _Identificador, 0);
        }


        #endregion

        //
    }
    //
}
﻿namespace Relaciones_Pagos
{
    partial class Relpag_221_5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Relpag_221_5));
            this.Panel = new System.Windows.Forms.Panel();
            this.Boton_1 = new ClaseIntsa.Controles.btnBotonGeneral(this.components);
            this.Label_1_1 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label1 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Text1 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Iban_Panel = new ClaseIntsa.Controles.Panel_Iban(this.components);
            this.Boton_2 = new ClaseIntsa.Controles.btnBotonGeneral(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.imgIntsa)).BeginInit();
            this.Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label_1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text1)).BeginInit();
            this.SuspendLayout();
            // 
            // imgIntsa
            // 
            this.imgIntsa.Location = new System.Drawing.Point(620, 30);
            this.imgIntsa.Margin = new System.Windows.Forms.Padding(4);
            this.imgIntsa.Size = new System.Drawing.Size(170, 50);
            // 
            // cmbVista
            // 
            this.cmbVista.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbVista.BackColor = System.Drawing.Color.White;
            this.cmbVista.DisplayMember = "DESCRIPCION";
            this.cmbVista.Location = new System.Drawing.Point(-130, -20);
            this.cmbVista.Size = new System.Drawing.Size(130, 23);
            this.cmbVista.TabIndex = 2;
            this.cmbVista.ValueMember = "ID";
            this.cmbVista.Visible = false;
            // 
            // lblMensajes
            // 
            this.lblMensajes.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.lblMensajes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMensajes.Size = new System.Drawing.Size(800, 20);
            // 
            // BarraProgreso
            // 
            this.BarraProgreso.Location = new System.Drawing.Point(640, 0);
            this.BarraProgreso.Margin = new System.Windows.Forms.Padding(4);
            this.BarraProgreso.Size = new System.Drawing.Size(160, 20);
            // 
            // Panel
            // 
            this.Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Panel.BackColor = System.Drawing.Color.White;
            this.Panel.Controls.Add(this.Boton_2);
            this.Panel.Controls.Add(this.Boton_1);
            this.Panel.Font = new System.Drawing.Font("Calibri", 10F);
            this.Panel.ForeColor = System.Drawing.Color.Transparent;
            this.Panel.Location = new System.Drawing.Point(210, 30);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(400, 50);
            this.Panel.TabIndex = 2;
            this.Panel.TabStop = true;
            // 
            // Boton_1
            // 
            this.Boton_1.FlatAppearance.BorderSize = 0;
            this.Boton_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton_1.Font = new System.Drawing.Font("Calibri", 10F);
            this.Boton_1.Image = ((System.Drawing.Image)(resources.GetObject("Boton_1.Image")));
            this.Boton_1.ImagenDeshabilitado = ((System.Drawing.Image)(resources.GetObject("Boton_1.ImagenDeshabilitado")));
            this.Boton_1.ImagenFoco = ((System.Drawing.Image)(resources.GetObject("Boton_1.ImagenFoco")));
            this.Boton_1.ImagenNormal = ((System.Drawing.Image)(resources.GetObject("Boton_1.ImagenNormal")));
            this.Boton_1.Location = new System.Drawing.Point(0, 0);
            this.Boton_1.Name = "Boton_1";
            this.Boton_1.Size = new System.Drawing.Size(50, 50);
            this.Boton_1.TabIndex = 0;
            this.ttpFormulario.SetToolTip(this.Boton_1, "Actualizar datos");
            this.Boton_1.UseVisualStyleBackColor = false;
            this.Boton_1.Refresco += new System.EventHandler(this.Boton_1_Refresco);
            this.Boton_1.Click += new System.EventHandler(this.Boton_1_Click);
            // 
            // Label_1_1
            // 
            this.Label_1_1.AutoSize = true;
            this.Label_1_1.BackColor = System.Drawing.Color.Transparent;
            this.Label_1_1.CausesValidation = false;
            this.Label_1_1.Depth = 0;
            this.Label_1_1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_1_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_1_1.FormatoCorporativo = false;
            this.Label_1_1.Location = new System.Drawing.Point(135, 90);
            this.Label_1_1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_1_1.Name = "Label_1_1";
            this.Label_1_1.Size = new System.Drawing.Size(46, 15);
            this.Label_1_1.TabIndex = 430;
            this.Label_1_1.Text = "I.B.A.N.";
            this.Label_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label1
            // 
            this.Label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label1.AutoSize = true;
            this.Label1.BackColor = System.Drawing.Color.Transparent;
            this.Label1.Datos_Auxiliares = "1";
            this.Label1.Depth = 0;
            this.Label1.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label1.FormatoCorporativo = false;
            this.Label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label1.Location = new System.Drawing.Point(5, 90);
            this.Label1.Margin = new System.Windows.Forms.Padding(0);
            this.Label1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(83, 15);
            this.Label1.TabIndex = 670;
            this.Label1.Text = "B.I.C./S.W.I.F.T.";
            // 
            // Text1
            // 
            this.Text1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Text1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Text1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Text1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Text1.BusquedaF8 = false;
            this.Text1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Text1.Datos_Auxiliares = "2";
            this.Text1.Depth = 0;
            this.Text1.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text1.GridNumero = ((byte)(0));
            this.Text1.Location = new System.Drawing.Point(5, 115);
            this.Text1.MaxLength = 11;
            this.Text1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text1.Multiline = false;
            this.Text1.Name = "Text1";
            this.Text1.PasswordChar = '\0';
            this.Text1.ReadOnly = false;
            this.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Text1.SelectedText = "";
            this.Text1.SelectionLength = 0;
            this.Text1.SelectionStart = 0;
            this.Text1.Size = new System.Drawing.Size(120, 23);
            this.Text1.TabIndex = 0;
            this.Text1.TabStop = false;
            this.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Text1.TextoMayusculas = false;
            this.Text1.Tipo = "";
            this.Text1.TipoActualizacion = "I";
            this.ttpFormulario.SetToolTip(this.Text1, "B.I.C./S.W.I.F.T.");
            this.Text1.UseSystemPasswordChar = false;
            this.Text1.Validating += new System.ComponentModel.CancelEventHandler(this.Text1_Validating);
            // 
            // Iban_Panel
            // 
            this.Iban_Panel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Iban_Panel.BackColor = System.Drawing.Color.Snow;
            this.Iban_Panel.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Iban_Panel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Iban_Panel.Iban_Normalizado = "";
            this.Iban_Panel.Location = new System.Drawing.Point(190, 90);
            this.Iban_Panel.Longitud_Iban = ((byte)(0));
            this.Iban_Panel.MinimumSize = new System.Drawing.Size(490, 50);
            this.Iban_Panel.Name = "Iban_Panel";
            this.Iban_Panel.Obligatorio = true;
            this.Iban_Panel.Size = new System.Drawing.Size(490, 50);
            this.Iban_Panel.TabIndex = 1;
            // 
            // Boton_2
            // 
            this.Boton_2.FlatAppearance.BorderSize = 0;
            this.Boton_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton_2.Font = new System.Drawing.Font("Calibri", 10F);
            this.Boton_2.Image = ((System.Drawing.Image)(resources.GetObject("Boton_2.Image")));
            this.Boton_2.ImagenDeshabilitado = ((System.Drawing.Image)(resources.GetObject("Boton_2.ImagenDeshabilitado")));
            this.Boton_2.ImagenFoco = ((System.Drawing.Image)(resources.GetObject("Boton_2.ImagenFoco")));
            this.Boton_2.ImagenNormal = ((System.Drawing.Image)(resources.GetObject("Boton_2.ImagenNormal")));
            this.Boton_2.Location = new System.Drawing.Point(150, 0);
            this.Boton_2.Name = "Boton_2";
            this.Boton_2.Size = new System.Drawing.Size(50, 50);
            this.Boton_2.TabIndex = 1;
            this.ttpFormulario.SetToolTip(this.Boton_2, "Valores originales");
            this.Boton_2.UseVisualStyleBackColor = false;
            this.Boton_2.Refresco += new System.EventHandler(this.Boton_2_Refresco);
            this.Boton_2.Click += new System.EventHandler(this.Boton_2_Click);
            // 
            // Relpag_221_5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(800, 160);
            this.Controls.Add(this.Iban_Panel);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Text1);
            this.Controls.Add(this.Label_1_1);
            this.Controls.Add(this.Panel);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Relpag_221_5";
            this.Text = "Parámetros generales";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Relpag_221_5_FormClosing);
            this.Load += new System.EventHandler(this.Relpag_221_5_Load);
            this.Controls.SetChildIndex(this.Panel, 0);
            this.Controls.SetChildIndex(this.cmbVista, 0);
            this.Controls.SetChildIndex(this.imgIntsa, 0);
            this.Controls.SetChildIndex(this.Label_1_1, 0);
            this.Controls.SetChildIndex(this.Text1, 0);
            this.Controls.SetChildIndex(this.Label1, 0);
            this.Controls.SetChildIndex(this.Iban_Panel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.imgIntsa)).EndInit();
            this.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Label_1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel Panel;
        private ClaseIntsa.Controles.btnBotonGeneral Boton_1;
        private ClaseIntsa.Controles.lblLabelGeneral Label_1_1;
        private ClaseIntsa.Controles.lblLabelGeneral Label1;
        private ClaseIntsa.Controles.txtTextoGeneral Text1;
        private ClaseIntsa.Controles.Panel_Iban Iban_Panel;
        private ClaseIntsa.Controles.btnBotonGeneral Boton_2;
    }
}
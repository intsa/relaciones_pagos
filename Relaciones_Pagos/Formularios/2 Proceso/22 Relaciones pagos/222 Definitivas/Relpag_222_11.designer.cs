﻿namespace Relaciones_Pagos
{
    partial class Relpag_222_11
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Relpag_222_11));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Panel = new System.Windows.Forms.Panel();
            this.Boton_2 = new ClaseIntsa.Controles.btnBotonGeneral(this.components);
            this.Boton_1 = new ClaseIntsa.Controles.btnBotonGeneral(this.components);
            this.Label_1_1 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label_1_2 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label_Info_1 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Text5 = new ClaseIntsa.Controles.txtNumeroGeneral(this.components);
            this.Text6 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Label5 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label6 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Text4 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Label4 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label7 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Text7 = new ClaseIntsa.Controles.txtFechaGeneral(this.components);
            this.Text11 = new ClaseIntsa.Controles.txtNumeroGeneral(this.components);
            this.Label11 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Text12 = new ClaseIntsa.Controles.txtNumeroGeneral(this.components);
            this.Label12 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.GrdGridConsulta1 = new ClaseIntsa.Controles.grdGridConsulta(this.components);
            this.regsel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ejeord = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ibater = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nfa_0001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.razsoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fee_0001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.liq_0001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bic_0001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ide_0001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idereg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ideori = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Label_Info_2 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Text2 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Label2 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label3 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label_1 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label8 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Combo8 = new ClaseIntsa.Controles.cmbComboGeneral(this.components);
            this.Text10 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Label10 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Text1 = new ClaseIntsa.Controles.txtNumeroGeneral(this.components);
            this.Label_2 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label_3 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Text9 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Label9 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Text3 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.imgIntsa)).BeginInit();
            this.Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Label_1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_Info_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdGridConsulta1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_Info_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text3)).BeginInit();
            this.SuspendLayout();
            // 
            // imgIntsa
            // 
            this.imgIntsa.Location = new System.Drawing.Point(820, 30);
            this.imgIntsa.Size = new System.Drawing.Size(170, 50);
            // 
            // cmbVista
            // 
            this.cmbVista.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbVista.DisplayMember = "DESCRIPCION";
            this.cmbVista.Location = new System.Drawing.Point(0, 20);
            this.cmbVista.Size = new System.Drawing.Size(130, 23);
            this.cmbVista.ValueMember = "ID";
            // 
            // lblMensajes
            // 
            this.lblMensajes.Size = new System.Drawing.Size(1000, 20);
            // 
            // BarraProgreso
            // 
            this.BarraProgreso.Location = new System.Drawing.Point(880, 0);
            // 
            // Panel
            // 
            this.Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Panel.Controls.Add(this.Boton_2);
            this.Panel.Controls.Add(this.Boton_1);
            this.Panel.Font = new System.Drawing.Font("Calibri", 10F);
            this.Panel.ForeColor = System.Drawing.Color.Transparent;
            this.Panel.Location = new System.Drawing.Point(410, 30);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(400, 50);
            this.Panel.TabIndex = 13;
            this.Panel.TabStop = true;
            // 
            // Boton_2
            // 
            this.Boton_2.FlatAppearance.BorderSize = 0;
            this.Boton_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton_2.Font = new System.Drawing.Font("Calibri", 10F);
            this.Boton_2.Image = ((System.Drawing.Image)(resources.GetObject("Boton_2.Image")));
            this.Boton_2.ImagenDeshabilitado = ((System.Drawing.Image)(resources.GetObject("Boton_2.ImagenDeshabilitado")));
            this.Boton_2.ImagenFoco = ((System.Drawing.Image)(resources.GetObject("Boton_2.ImagenFoco")));
            this.Boton_2.ImagenNormal = ((System.Drawing.Image)(resources.GetObject("Boton_2.ImagenNormal")));
            this.Boton_2.Location = new System.Drawing.Point(150, 0);
            this.Boton_2.Name = "Boton_2";
            this.Boton_2.Size = new System.Drawing.Size(50, 50);
            this.Boton_2.TabIndex = 1;
            this.ttpFormulario.SetToolTip(this.Boton_2, "Nueva alta");
            this.Boton_2.UseVisualStyleBackColor = false;
            this.Boton_2.Refresco += new System.EventHandler(this.Boton_2_Refresco);
            this.Boton_2.Click += new System.EventHandler(this.Boton_2_Click);
            // 
            // Boton_1
            // 
            this.Boton_1.FlatAppearance.BorderSize = 0;
            this.Boton_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton_1.Font = new System.Drawing.Font("Calibri", 10F);
            this.Boton_1.Image = ((System.Drawing.Image)(resources.GetObject("Boton_1.Image")));
            this.Boton_1.ImagenDeshabilitado = ((System.Drawing.Image)(resources.GetObject("Boton_1.ImagenDeshabilitado")));
            this.Boton_1.ImagenFoco = ((System.Drawing.Image)(resources.GetObject("Boton_1.ImagenFoco")));
            this.Boton_1.ImagenNormal = ((System.Drawing.Image)(resources.GetObject("Boton_1.ImagenNormal")));
            this.Boton_1.Location = new System.Drawing.Point(0, 0);
            this.Boton_1.Name = "Boton_1";
            this.Boton_1.Size = new System.Drawing.Size(50, 50);
            this.Boton_1.TabIndex = 0;
            this.ttpFormulario.SetToolTip(this.Boton_1, "Grabar ");
            this.Boton_1.UseVisualStyleBackColor = false;
            this.Boton_1.Refresco += new System.EventHandler(this.Boton_1_Refresco);
            this.Boton_1.Click += new System.EventHandler(this.Boton_1_Click);
            // 
            // Label_1_1
            // 
            this.Label_1_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Label_1_1.AutoSize = true;
            this.Label_1_1.BackColor = System.Drawing.Color.Transparent;
            this.Label_1_1.CausesValidation = false;
            this.Label_1_1.Depth = 0;
            this.Label_1_1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_1_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_1_1.FormatoCorporativo = false;
            this.Label_1_1.Location = new System.Drawing.Point(5, 200);
            this.Label_1_1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_1_1.Name = "Label_1_1";
            this.Label_1_1.Size = new System.Drawing.Size(86, 15);
            this.Label_1_1.TabIndex = 626;
            this.Label_1_1.Text = "DOCUMENTOS";
            this.Label_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label_1_2
            // 
            this.Label_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_1_2.BackColor = System.Drawing.Color.Transparent;
            this.Label_1_2.CausesValidation = false;
            this.Label_1_2.Depth = 0;
            this.Label_1_2.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label_1_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_1_2.FormatoCorporativo = false;
            this.Label_1_2.Location = new System.Drawing.Point(905, 200);
            this.Label_1_2.Margin = new System.Windows.Forms.Padding(0);
            this.Label_1_2.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_1_2.Name = "Label_1_2";
            this.Label_1_2.Size = new System.Drawing.Size(90, 15);
            this.Label_1_2.TabIndex = 627;
            this.Label_1_2.Text = "-";
            this.Label_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label_Info_1
            // 
            this.Label_Info_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_Info_1.BackColor = System.Drawing.Color.Transparent;
            this.Label_Info_1.CausesValidation = false;
            this.Label_Info_1.Depth = 0;
            this.Label_Info_1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Info_1.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.Label_Info_1.FormatoCorporativo = false;
            this.Label_Info_1.Location = new System.Drawing.Point(175, 200);
            this.Label_Info_1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_Info_1.Name = "Label_Info_1";
            this.Label_Info_1.Size = new System.Drawing.Size(650, 15);
            this.Label_Info_1.TabIndex = 628;
            this.Label_Info_1.Text = "(Espaciador o doble click) Marca/desmarca un registro";
            this.Label_Info_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Label_Info_1.Visible = false;
            // 
            // Text5
            // 
            this.Text5._TabStop = false;
            this.Text5.BackColor = System.Drawing.Color.White;
            this.Text5.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Text5.Datos_Auxiliares = "1";
            this.Text5.Decimales = 0;
            this.Text5.Depth = 0;
            this.Text5.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text5.GridNumero = ((byte)(0));
            this.Text5.Location = new System.Drawing.Point(315, 115);
            this.Text5.MaxLength = 10;
            this.Text5.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text5.Name = "Text5";
            this.Text5.PasswordChar = '\0';
            this.Text5.ReadOnly = false;
            this.Text5.SelectedText = "";
            this.Text5.SelectionLength = 0;
            this.Text5.SelectionStart = 0;
            this.Text5.Size = new System.Drawing.Size(50, 23);
            this.Text5.TabIndex = 4;
            this.Text5.TabStop = false;
            this.Text5.Text = "0";
            this.Text5.Text_Vacio = true;
            this.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ttpFormulario.SetToolTip(this.Text5, "Número de relación");
            this.Text5.UseSystemPasswordChar = false;
            this.Text5.Validating += new System.ComponentModel.CancelEventHandler(this.Text5_Validating);
            // 
            // Text6
            // 
            this.Text6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Text6.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Text6.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Text6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Text6.BusquedaF8 = false;
            this.Text6.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Text6.Datos_Auxiliares = "1";
            this.Text6.Depth = 0;
            this.Text6.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text6.ForeColor = System.Drawing.Color.Green;
            this.Text6.GridNumero = ((byte)(0));
            this.Text6.Location = new System.Drawing.Point(375, 115);
            this.Text6.MaxLength = 100;
            this.Text6.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text6.Multiline = false;
            this.Text6.Name = "Text6";
            this.Text6.PasswordChar = '\0';
            this.Text6.ReadOnly = false;
            this.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Text6.SelectedText = "";
            this.Text6.SelectionLength = 0;
            this.Text6.SelectionStart = 0;
            this.Text6.Size = new System.Drawing.Size(535, 23);
            this.Text6.TabIndex = 5;
            this.Text6.TabStop = false;
            this.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Text6.TextoMayusculas = false;
            this.Text6.Tipo = "";
            this.Text6.TipoActualizacion = "I";
            this.ttpFormulario.SetToolTip(this.Text6, "Descripción de la relación");
            this.Text6.UseSystemPasswordChar = false;
            this.Text6.Validating += new System.ComponentModel.CancelEventHandler(this.Text6_Validating);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.BackColor = System.Drawing.Color.Transparent;
            this.Label5.CausesValidation = false;
            this.Label5.Datos_Auxiliares = "1";
            this.Label5.Depth = 0;
            this.Label5.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label5.FormatoCorporativo = false;
            this.Label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label5.Location = new System.Drawing.Point(315, 90);
            this.Label5.Margin = new System.Windows.Forms.Padding(0);
            this.Label5.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(50, 15);
            this.Label5.TabIndex = 634;
            this.Label5.Text = "Número";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.CausesValidation = false;
            this.Label6.Datos_Auxiliares = "1";
            this.Label6.Depth = 0;
            this.Label6.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label6.FormatoCorporativo = false;
            this.Label6.Location = new System.Drawing.Point(375, 90);
            this.Label6.Margin = new System.Windows.Forms.Padding(0);
            this.Label6.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(73, 15);
            this.Label6.TabIndex = 632;
            this.Label6.Text = "Descripción";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Text4
            // 
            this.Text4._TabStop = false;
            this.Text4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Text4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Text4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Text4.BusquedaF8 = true;
            this.Text4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Text4.Datos_Auxiliares = "1";
            this.Text4.Depth = 0;
            this.Text4.Enabled = false;
            this.Text4.F8_AplicarFormato = true;
            this.Text4.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text4.GridNumero = ((byte)(0));
            this.Text4.Location = new System.Drawing.Point(270, 115);
            this.Text4.MaxLength = 4;
            this.Text4.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text4.Multiline = false;
            this.Text4.Name = "Text4";
            this.Text4.PasswordChar = '\0';
            this.Text4.ReadOnly = true;
            this.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Text4.SelectedText = "";
            this.Text4.SelectionLength = 0;
            this.Text4.SelectionStart = 0;
            this.Text4.Size = new System.Drawing.Size(35, 23);
            this.Text4.TabIndex = 3;
            this.Text4.TabStop = false;
            this.Text4.Tag = "";
            this.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Text4.Tipo = "";
            this.ttpFormulario.SetToolTip(this.Text4, "Año relación");
            this.Text4.UseSystemPasswordChar = false;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.BackColor = System.Drawing.Color.Transparent;
            this.Label4.CausesValidation = false;
            this.Label4.Datos_Auxiliares = "1";
            this.Label4.Depth = 0;
            this.Label4.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label4.FormatoCorporativo = false;
            this.Label4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label4.Location = new System.Drawing.Point(273, 90);
            this.Label4.Margin = new System.Windows.Forms.Padding(0);
            this.Label4.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(28, 15);
            this.Label4.TabIndex = 635;
            this.Label4.Tag = "";
            this.Label4.Text = "Año";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label7
            // 
            this.Label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label7.AutoSize = true;
            this.Label7.BackColor = System.Drawing.Color.Transparent;
            this.Label7.CausesValidation = false;
            this.Label7.Datos_Auxiliares = "1";
            this.Label7.Depth = 0;
            this.Label7.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label7.FormatoCorporativo = false;
            this.Label7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label7.Location = new System.Drawing.Point(925, 90);
            this.Label7.Margin = new System.Windows.Forms.Padding(0);
            this.Label7.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(64, 15);
            this.Label7.TabIndex = 638;
            this.Label7.Tag = "";
            this.Label7.Text = "F. relación";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Text7
            // 
            this.Text7._TabStop = false;
            this.Text7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Text7.BackColor = System.Drawing.Color.White;
            this.Text7.Datos_Auxiliares = "1";
            this.Text7.Depth = 0;
            this.Text7.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text7.GridNumero = ((byte)(0));
            this.Text7.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.Text7.Location = new System.Drawing.Point(920, 115);
            this.Text7.Mask = "00/00/0000";
            this.Text7.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text7.Name = "Text7";
            this.Text7.ReadOnly = true;
            this.Text7.Size = new System.Drawing.Size(75, 23);
            this.Text7.TabIndex = 6;
            this.Text7.TabStop = false;
            this.Text7.Tag = "";
            this.Text7.Text = "  /  /";
            this.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Text7.Tipo = "";
            this.ttpFormulario.SetToolTip(this.Text7, "Fecha de la relación");
            this.Text7.ValidatingType = typeof(System.DateTime);
            this.Text7.ValorInicial = "\"  /  /    \"";
            // 
            // Text11
            // 
            this.Text11._TabStop = false;
            this.Text11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Text11.BackColor = System.Drawing.Color.White;
            this.Text11.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Text11.Datos_Auxiliares = "1";
            this.Text11.Decimales = 0;
            this.Text11.Depth = 0;
            this.Text11.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text11.GridNumero = ((byte)(0));
            this.Text11.Location = new System.Drawing.Point(805, 170);
            this.Text11.MaxLength = 32767;
            this.Text11.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text11.Name = "Text11";
            this.Text11.PasswordChar = '\0';
            this.Text11.ReadOnly = true;
            this.Text11.SelectedText = "";
            this.Text11.SelectionLength = 0;
            this.Text11.SelectionStart = 0;
            this.Text11.Size = new System.Drawing.Size(80, 23);
            this.Text11.TabIndex = 10;
            this.Text11.TabStop = false;
            this.Text11.Text = "0";
            this.Text11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ttpFormulario.SetToolTip(this.Text11, "Número de documentos en la relación");
            this.Text11.UseSystemPasswordChar = false;
            // 
            // Label11
            // 
            this.Label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label11.AutoSize = true;
            this.Label11.BackColor = System.Drawing.Color.Transparent;
            this.Label11.CausesValidation = false;
            this.Label11.Datos_Auxiliares = "1";
            this.Label11.Depth = 0;
            this.Label11.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label11.FormatoCorporativo = false;
            this.Label11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label11.Location = new System.Drawing.Point(808, 145);
            this.Label11.Margin = new System.Windows.Forms.Padding(0);
            this.Label11.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(75, 15);
            this.Label11.TabIndex = 649;
            this.Label11.Tag = "";
            this.Label11.Text = "Documentos";
            this.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Text12
            // 
            this.Text12._TabStop = false;
            this.Text12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Text12.BackColor = System.Drawing.Color.White;
            this.Text12.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Text12.Datos_Auxiliares = "1";
            this.Text12.Depth = 0;
            this.Text12.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text12.GridNumero = ((byte)(0));
            this.Text12.Location = new System.Drawing.Point(895, 170);
            this.Text12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Text12.MaxLength = 16;
            this.Text12.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text12.Name = "Text12";
            this.Text12.PasswordChar = '\0';
            this.Text12.ReadOnly = true;
            this.Text12.SelectedText = "";
            this.Text12.SelectionLength = 0;
            this.Text12.SelectionStart = 0;
            this.Text12.Size = new System.Drawing.Size(100, 23);
            this.Text12.TabIndex = 11;
            this.Text12.TabStop = false;
            this.Text12.Text = "0,00";
            this.Text12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Text12.Tipo = "";
            this.ttpFormulario.SetToolTip(this.Text12, "Total coste");
            this.Text12.UseSystemPasswordChar = false;
            // 
            // Label12
            // 
            this.Label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label12.AutoSize = true;
            this.Label12.BackColor = System.Drawing.Color.Transparent;
            this.Label12.CausesValidation = false;
            this.Label12.Datos_Auxiliares = "1";
            this.Label12.Depth = 0;
            this.Label12.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label12.FormatoCorporativo = false;
            this.Label12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label12.Location = new System.Drawing.Point(904, 145);
            this.Label12.Margin = new System.Windows.Forms.Padding(0);
            this.Label12.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(83, 15);
            this.Label12.TabIndex = 651;
            this.Label12.Text = "Total relación";
            this.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ttpFormulario.SetToolTip(this.Label12, "Importe total de la relación");
            // 
            // GrdGridConsulta1
            // 
            this.GrdGridConsulta1.AllowUserToAddRows = false;
            this.GrdGridConsulta1.AllowUserToDeleteRows = false;
            this.GrdGridConsulta1.AllowUserToResizeColumns = false;
            this.GrdGridConsulta1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            this.GrdGridConsulta1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.GrdGridConsulta1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GrdGridConsulta1.BackgroundColor = System.Drawing.Color.White;
            this.GrdGridConsulta1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GrdGridConsulta1.CampoOrdenInicial = "ejeord";
            this.GrdGridConsulta1.CausesValidation = false;
            this.GrdGridConsulta1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.GrdGridConsulta1.ColumnaClaveIdentidad = "ide_0001";
            this.GrdGridConsulta1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GrdGridConsulta1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.GrdGridConsulta1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrdGridConsulta1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.regsel,
            this.ejeord,
            this.ibater,
            this.nfa_0001,
            this.razsoc,
            this.fee_0001,
            this.liq_0001,
            this.bic_0001,
            this.ide_0001,
            this.idereg,
            this.ideori});
            this.GrdGridConsulta1.EnableHeadersVisualStyles = false;
            this.GrdGridConsulta1.Expandible = false;
            this.GrdGridConsulta1.GridColor = System.Drawing.Color.LightGray;
            this.GrdGridConsulta1.Location = new System.Drawing.Point(5, 225);
            this.GrdGridConsulta1.LongitudMinima = 220;
            this.GrdGridConsulta1.Margin = new System.Windows.Forms.Padding(5);
            this.GrdGridConsulta1.MostrarRegSeleccionados = false;
            this.GrdGridConsulta1.MultiSelect = false;
            this.GrdGridConsulta1.Name = "GrdGridConsulta1";
            this.GrdGridConsulta1.ReadOnly = true;
            this.GrdGridConsulta1.RowHeadersVisible = false;
            this.GrdGridConsulta1.RowHeadersWidth = 20;
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Calibri", 10F);
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(133)))), ((int)(((byte)(255)))));
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.GrdGridConsulta1.RowTemplate.Height = 20;
            this.GrdGridConsulta1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.GrdGridConsulta1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GrdGridConsulta1.Size = new System.Drawing.Size(990, 220);
            this.GrdGridConsulta1.StandardTab = true;
            this.GrdGridConsulta1.TabIndex = 12;
            this.GrdGridConsulta1.TabStop = false;
            this.ttpFormulario.SetToolTip(this.GrdGridConsulta1, "Botón derecho del ratón para más opciones");
            this.GrdGridConsulta1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.GrdGridConsulta1_CellFormatting);
            this.GrdGridConsulta1.SelectionChanged += new System.EventHandler(this.GrdGridConsulta1_SelectionChanged);
            this.GrdGridConsulta1.Enter += new System.EventHandler(this.GrdGridConsulta1_Enter);
            this.GrdGridConsulta1.Leave += new System.EventHandler(this.GrdGridConsulta1_Leave);
            this.GrdGridConsulta1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GrdGridConsulta1_MouseClick);
            this.GrdGridConsulta1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.GrdGridConsulta1_MouseDoubleClick);
            this.GrdGridConsulta1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.GrdGridConsulta1_PreviewKeyDown);
            // 
            // regsel
            // 
            this.regsel.DataPropertyName = "regsel";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.regsel.DefaultCellStyle = dataGridViewCellStyle3;
            this.regsel.FillWeight = 16F;
            this.regsel.HeaderText = "";
            this.regsel.MaxInputLength = 1;
            this.regsel.Name = "regsel";
            this.regsel.ReadOnly = true;
            this.regsel.ToolTipText = "Seleccionado";
            this.regsel.Width = 16;
            // 
            // ejeord
            // 
            this.ejeord.DataPropertyName = "ejeord";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ejeord.DefaultCellStyle = dataGridViewCellStyle4;
            this.ejeord.FillWeight = 90F;
            this.ejeord.HeaderText = "Año-Orden";
            this.ejeord.MaxInputLength = 11;
            this.ejeord.Name = "ejeord";
            this.ejeord.ReadOnly = true;
            this.ejeord.ToolTipText = "Orden factura";
            this.ejeord.Width = 90;
            // 
            // ibater
            // 
            this.ibater.DataPropertyName = "ibater";
            this.ibater.FillWeight = 200F;
            this.ibater.HeaderText = "I.B.A.N.";
            this.ibater.MaxInputLength = 42;
            this.ibater.Name = "ibater";
            this.ibater.ReadOnly = true;
            this.ibater.Width = 200;
            // 
            // nfa_0001
            // 
            this.nfa_0001.DataPropertyName = "nfa_0001";
            this.nfa_0001.FillWeight = 140F;
            this.nfa_0001.HeaderText = "Número factura";
            this.nfa_0001.Name = "nfa_0001";
            this.nfa_0001.ReadOnly = true;
            this.nfa_0001.Width = 140;
            // 
            // razsoc
            // 
            this.razsoc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.razsoc.DataPropertyName = "razsoc";
            this.razsoc.HeaderText = "Razón social";
            this.razsoc.Name = "razsoc";
            this.razsoc.ReadOnly = true;
            // 
            // fee_0001
            // 
            this.fee_0001.DataPropertyName = "fee_0001";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Format = "d";
            this.fee_0001.DefaultCellStyle = dataGridViewCellStyle5;
            this.fee_0001.FillWeight = 80F;
            this.fee_0001.HeaderText = "F. registro";
            this.fee_0001.MaxInputLength = 10;
            this.fee_0001.Name = "fee_0001";
            this.fee_0001.ReadOnly = true;
            this.fee_0001.Width = 80;
            // 
            // liq_0001
            // 
            this.liq_0001.DataPropertyName = "liq_0001";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = "-";
            this.liq_0001.DefaultCellStyle = dataGridViewCellStyle6;
            this.liq_0001.HeaderText = "Importe";
            this.liq_0001.MaxInputLength = 18;
            this.liq_0001.Name = "liq_0001";
            this.liq_0001.ReadOnly = true;
            // 
            // bic_0001
            // 
            this.bic_0001.DataPropertyName = "bic_0001";
            this.bic_0001.HeaderText = "BIC/SWIFT";
            this.bic_0001.MaxInputLength = 11;
            this.bic_0001.Name = "bic_0001";
            this.bic_0001.ReadOnly = true;
            this.bic_0001.Visible = false;
            // 
            // ide_0001
            // 
            this.ide_0001.DataPropertyName = "ide_0001";
            this.ide_0001.HeaderText = "Identificador documento";
            this.ide_0001.MaxInputLength = 10;
            this.ide_0001.Name = "ide_0001";
            this.ide_0001.ReadOnly = true;
            this.ide_0001.Visible = false;
            // 
            // idereg
            // 
            this.idereg.DataPropertyName = "idereg";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.NullValue = "-";
            this.idereg.DefaultCellStyle = dataGridViewCellStyle7;
            this.idereg.FillWeight = 60F;
            this.idereg.HeaderText = "Identificador registro";
            this.idereg.MaxInputLength = 10;
            this.idereg.Name = "idereg";
            this.idereg.ReadOnly = true;
            this.idereg.Visible = false;
            this.idereg.Width = 60;
            // 
            // ideori
            // 
            this.ideori.DataPropertyName = "ideori";
            this.ideori.HeaderText = "Identificador registro Modelo-997";
            this.ideori.MaxInputLength = 10;
            this.ideori.Name = "ideori";
            this.ideori.ReadOnly = true;
            this.ideori.Visible = false;
            // 
            // Label_Info_2
            // 
            this.Label_Info_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_Info_2.BackColor = System.Drawing.Color.Transparent;
            this.Label_Info_2.CausesValidation = false;
            this.Label_Info_2.Depth = 0;
            this.Label_Info_2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Info_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_Info_2.FormatoCorporativo = false;
            this.Label_Info_2.Location = new System.Drawing.Point(5, 450);
            this.Label_Info_2.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_Info_2.Name = "Label_Info_2";
            this.Label_Info_2.Size = new System.Drawing.Size(990, 15);
            this.Label_Info_2.TabIndex = 655;
            this.Label_Info_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label_Info_2.Visible = false;
            // 
            // Text2
            // 
            this.Text2._TabStop = false;
            this.Text2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Text2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Text2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Text2.BusquedaF8 = true;
            this.Text2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Text2.Datos_Auxiliares = "1";
            this.Text2.Depth = 0;
            this.Text2.Enabled = false;
            this.Text2.F8_AplicarFormato = true;
            this.Text2.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text2.GridNumero = ((byte)(0));
            this.Text2.Location = new System.Drawing.Point(90, 115);
            this.Text2.MaxLength = 4;
            this.Text2.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text2.Multiline = false;
            this.Text2.Name = "Text2";
            this.Text2.PasswordChar = '\0';
            this.Text2.ReadOnly = true;
            this.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Text2.SelectedText = "";
            this.Text2.SelectionLength = 0;
            this.Text2.SelectionStart = 0;
            this.Text2.Size = new System.Drawing.Size(35, 23);
            this.Text2.TabIndex = 1;
            this.Text2.TabStop = false;
            this.Text2.Tag = "";
            this.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Text2.Tipo = "";
            this.ttpFormulario.SetToolTip(this.Text2, "Año relación modelo-997");
            this.Text2.UseSystemPasswordChar = false;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.BackColor = System.Drawing.Color.Transparent;
            this.Label2.CausesValidation = false;
            this.Label2.Datos_Auxiliares = "1";
            this.Label2.Depth = 0;
            this.Label2.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label2.FormatoCorporativo = false;
            this.Label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label2.Location = new System.Drawing.Point(93, 90);
            this.Label2.Margin = new System.Windows.Forms.Padding(0);
            this.Label2.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(28, 15);
            this.Label2.TabIndex = 659;
            this.Label2.Tag = "";
            this.Label2.Text = "Año";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.BackColor = System.Drawing.Color.Transparent;
            this.Label3.CausesValidation = false;
            this.Label3.Datos_Auxiliares = "1";
            this.Label3.Depth = 0;
            this.Label3.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label3.FormatoCorporativo = false;
            this.Label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label3.Location = new System.Drawing.Point(135, 90);
            this.Label3.Margin = new System.Windows.Forms.Padding(0);
            this.Label3.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(50, 15);
            this.Label3.TabIndex = 658;
            this.Label3.Text = "Número";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label_1
            // 
            this.Label_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Label_1.AutoSize = true;
            this.Label_1.BackColor = System.Drawing.Color.Transparent;
            this.Label_1.CausesValidation = false;
            this.Label_1.Datos_Auxiliares = "1";
            this.Label_1.Depth = 0;
            this.Label_1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_1.FormatoCorporativo = false;
            this.Label_1.Location = new System.Drawing.Point(5, 90);
            this.Label_1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_1.Name = "Label_1";
            this.Label_1.Size = new System.Drawing.Size(80, 15);
            this.Label_1.TabIndex = 660;
            this.Label_1.Text = "MODELO 997";
            this.Label_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.BackColor = System.Drawing.Color.Transparent;
            this.Label8.Datos_Auxiliares = "1";
            this.Label8.Depth = 0;
            this.Label8.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label8.FormatoCorporativo = false;
            this.Label8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label8.Location = new System.Drawing.Point(90, 145);
            this.Label8.Margin = new System.Windows.Forms.Padding(0);
            this.Label8.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(85, 15);
            this.Label8.TabIndex = 662;
            this.Label8.Text = "Canal de pago";
            // 
            // Combo8
            // 
            this.Combo8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Combo8.BackColor = System.Drawing.Color.White;
            this.Combo8.CamposPorElemento = ((short)(7));
            this.Combo8.Datos_Auxiliares = "1";
            this.Combo8.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.Combo8.DropDownHeight = 105;
            this.Combo8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Combo8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Combo8.Font = new System.Drawing.Font("Calibri", 10F);
            this.Combo8.ForeColor = System.Drawing.Color.Transparent;
            this.Combo8.FormattingEnabled = true;
            this.Combo8.GridNumero = ((byte)(0));
            this.Combo8.IntegralHeight = false;
            this.Combo8.ItemHeight = 16;
            this.Combo8.Location = new System.Drawing.Point(90, 171);
            this.Combo8.MaxDropDownItems = 10;
            this.Combo8.Name = "Combo8";
            this.Combo8.Size = new System.Drawing.Size(440, 22);
            this.Combo8.TabIndex = 7;
            this.Combo8.TipoCombo = ((byte)(2));
            this.ttpFormulario.SetToolTip(this.Combo8, "Canal de pago");
            this.Combo8.ValorInicial = "1";
            this.Combo8.SelectedValueChanged += new System.EventHandler(this.Combo8_SelectedValueChanged);
            // 
            // Text10
            // 
            this.Text10._TabStop = false;
            this.Text10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Text10.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Text10.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Text10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Text10.BusquedaF8 = false;
            this.Text10.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Text10.Datos_Auxiliares = "1";
            this.Text10.Depth = 0;
            this.Text10.Deshabilitado = true;
            this.Text10.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text10.ForeColor = System.Drawing.Color.Green;
            this.Text10.GridNumero = ((byte)(0));
            this.Text10.Location = new System.Drawing.Point(675, 170);
            this.Text10.MaxLength = 100;
            this.Text10.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text10.Multiline = false;
            this.Text10.Name = "Text10";
            this.Text10.PasswordChar = '\0';
            this.Text10.ReadOnly = true;
            this.Text10.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Text10.SelectedText = "";
            this.Text10.SelectionLength = 0;
            this.Text10.SelectionStart = 0;
            this.Text10.Size = new System.Drawing.Size(120, 23);
            this.Text10.TabIndex = 9;
            this.Text10.TabStop = false;
            this.Text10.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Text10.TextoMayusculas = false;
            this.Text10.Tipo = "";
            this.Text10.TipoActualizacion = "I";
            this.ttpFormulario.SetToolTip(this.Text10, "Descripción de la relación");
            this.Text10.UseSystemPasswordChar = false;
            // 
            // Label10
            // 
            this.Label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label10.AutoSize = true;
            this.Label10.BackColor = System.Drawing.Color.Transparent;
            this.Label10.Datos_Auxiliares = "1";
            this.Label10.Depth = 0;
            this.Label10.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label10.FormatoCorporativo = false;
            this.Label10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label10.Location = new System.Drawing.Point(675, 145);
            this.Label10.Margin = new System.Windows.Forms.Padding(0);
            this.Label10.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(77, 15);
            this.Label10.TabIndex = 664;
            this.Label10.Text = "Identificador";
            // 
            // Text1
            // 
            this.Text1._TabStop = false;
            this.Text1.BackColor = System.Drawing.Color.White;
            this.Text1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Text1.DarFormato = false;
            this.Text1.Datos_Auxiliares = "1";
            this.Text1.Decimales = 0;
            this.Text1.Depth = 0;
            this.Text1.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text1.GridNumero = ((byte)(0));
            this.Text1.Location = new System.Drawing.Point(5, 115);
            this.Text1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Text1.MaxLength = 10;
            this.Text1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text1.Name = "Text1";
            this.Text1.PasswordChar = '\0';
            this.Text1.ReadOnly = true;
            this.Text1.SelectedText = "";
            this.Text1.SelectionLength = 0;
            this.Text1.SelectionStart = 0;
            this.Text1.Size = new System.Drawing.Size(75, 23);
            this.Text1.TabIndex = 0;
            this.Text1.TabStop = false;
            this.Text1.Text = "0";
            this.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Text1.Tipo = "";
            this.ttpFormulario.SetToolTip(this.Text1, "Identificador registro");
            this.Text1.UseSystemPasswordChar = false;
            // 
            // Label_2
            // 
            this.Label_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Label_2.AutoSize = true;
            this.Label_2.BackColor = System.Drawing.Color.Transparent;
            this.Label_2.CausesValidation = false;
            this.Label_2.Datos_Auxiliares = "1";
            this.Label_2.Depth = 0;
            this.Label_2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_2.FormatoCorporativo = false;
            this.Label_2.Location = new System.Drawing.Point(195, 90);
            this.Label_2.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_2.Name = "Label_2";
            this.Label_2.Size = new System.Drawing.Size(66, 15);
            this.Label_2.TabIndex = 665;
            this.Label_2.Text = "DEFINITIVA";
            this.Label_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label_3
            // 
            this.Label_3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Label_3.AutoSize = true;
            this.Label_3.BackColor = System.Drawing.Color.Transparent;
            this.Label_3.CausesValidation = false;
            this.Label_3.Datos_Auxiliares = "1";
            this.Label_3.Depth = 0;
            this.Label_3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_3.FormatoCorporativo = false;
            this.Label_3.Location = new System.Drawing.Point(5, 145);
            this.Label_3.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_3.Name = "Label_3";
            this.Label_3.Size = new System.Drawing.Size(75, 15);
            this.Label_3.TabIndex = 666;
            this.Label_3.Text = "ORDENANTE";
            this.Label_3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Text9
            // 
            this.Text9._TabStop = false;
            this.Text9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Text9.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Text9.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Text9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Text9.BusquedaF8 = false;
            this.Text9.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.Text9.Datos_Auxiliares = "1";
            this.Text9.Depth = 0;
            this.Text9.Deshabilitado = true;
            this.Text9.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text9.ForeColor = System.Drawing.Color.Green;
            this.Text9.GridNumero = ((byte)(0));
            this.Text9.Location = new System.Drawing.Point(540, 170);
            this.Text9.MaxLength = 100;
            this.Text9.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text9.Multiline = false;
            this.Text9.Name = "Text9";
            this.Text9.PasswordChar = '\0';
            this.Text9.ReadOnly = true;
            this.Text9.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Text9.SelectedText = "";
            this.Text9.SelectionLength = 0;
            this.Text9.SelectionStart = 0;
            this.Text9.Size = new System.Drawing.Size(125, 23);
            this.Text9.TabIndex = 8;
            this.Text9.TabStop = false;
            this.Text9.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Text9.TextoMayusculas = false;
            this.Text9.Tipo = "";
            this.Text9.TipoActualizacion = "I";
            this.ttpFormulario.SetToolTip(this.Text9, "Descripción de la relación");
            this.Text9.UseSystemPasswordChar = false;
            // 
            // Label9
            // 
            this.Label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label9.AutoSize = true;
            this.Label9.BackColor = System.Drawing.Color.Transparent;
            this.Label9.Datos_Auxiliares = "1";
            this.Label9.Depth = 0;
            this.Label9.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label9.FormatoCorporativo = false;
            this.Label9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label9.Location = new System.Drawing.Point(540, 145);
            this.Label9.Margin = new System.Windows.Forms.Padding(0);
            this.Label9.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(83, 15);
            this.Label9.TabIndex = 668;
            this.Label9.Text = "B.I.C./S.W.I.F.T.";
            // 
            // Text3
            // 
            this.Text3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Text3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Text3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Text3.BusquedaF8 = true;
            this.Text3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Text3.Datos_Auxiliares = "1";
            this.Text3.Depth = 0;
            this.Text3.F8_AplicarFormato = true;
            this.Text3.Font = new System.Drawing.Font("Calibri", 10F);
            this.Text3.GridNumero = ((byte)(0));
            this.Text3.Location = new System.Drawing.Point(135, 115);
            this.Text3.MaxLength = 4;
            this.Text3.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Text3.Multiline = false;
            this.Text3.Name = "Text3";
            this.Text3.PasswordChar = '\0';
            this.Text3.ReadOnly = false;
            this.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Text3.SelectedText = "";
            this.Text3.SelectionLength = 0;
            this.Text3.SelectionStart = 0;
            this.Text3.Size = new System.Drawing.Size(50, 23);
            this.Text3.TabIndex = 2;
            this.Text3.TabStop = false;
            this.Text3.Tag = "";
            this.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Text3.Tipo = "";
            this.ttpFormulario.SetToolTip(this.Text3, "Número relación modelo-997");
            this.Text3.UseSystemPasswordChar = false;
            this.Text3.TextChanged += new System.EventHandler(this.Text3_TextChanged);
            this.Text3.Validating += new System.ComponentModel.CancelEventHandler(this.Text3_Validating);
            // 
            // Relpag_222_11
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 490);
            this.Controls.Add(this.Text3);
            this.Controls.Add(this.Label9);
            this.Controls.Add(this.Text9);
            this.Controls.Add(this.Label_3);
            this.Controls.Add(this.Label_2);
            this.Controls.Add(this.Label10);
            this.Controls.Add(this.Text10);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.Combo8);
            this.Controls.Add(this.Label_1);
            this.Controls.Add(this.Text2);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label_Info_2);
            this.Controls.Add(this.GrdGridConsulta1);
            this.Controls.Add(this.Text12);
            this.Controls.Add(this.Label12);
            this.Controls.Add(this.Label11);
            this.Controls.Add(this.Text11);
            this.Controls.Add(this.Text7);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.Text4);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Text5);
            this.Controls.Add(this.Text6);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Text1);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Label_Info_1);
            this.Controls.Add(this.Label_1_2);
            this.Controls.Add(this.Label_1_1);
            this.Controls.Add(this.Panel);
            this.Name = "Relpag_222_11";
            this.OcultarComboVista = true;
            this.Text = "Relaciones de pagos. Definitivas. Altas";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Relpag_222_11_FormClosing);
            this.Load += new System.EventHandler(this.Relpag_222_11_Load);
            this.Controls.SetChildIndex(this.Panel, 0);
            this.Controls.SetChildIndex(this.Label_1_1, 0);
            this.Controls.SetChildIndex(this.Label_1_2, 0);
            this.Controls.SetChildIndex(this.Label_Info_1, 0);
            this.Controls.SetChildIndex(this.Label6, 0);
            this.Controls.SetChildIndex(this.Text1, 0);
            this.Controls.SetChildIndex(this.Label5, 0);
            this.Controls.SetChildIndex(this.Text6, 0);
            this.Controls.SetChildIndex(this.Text5, 0);
            this.Controls.SetChildIndex(this.Label4, 0);
            this.Controls.SetChildIndex(this.Text4, 0);
            this.Controls.SetChildIndex(this.Label7, 0);
            this.Controls.SetChildIndex(this.Text7, 0);
            this.Controls.SetChildIndex(this.Text11, 0);
            this.Controls.SetChildIndex(this.Label11, 0);
            this.Controls.SetChildIndex(this.Label12, 0);
            this.Controls.SetChildIndex(this.Text12, 0);
            this.Controls.SetChildIndex(this.GrdGridConsulta1, 0);
            this.Controls.SetChildIndex(this.Label_Info_2, 0);
            this.Controls.SetChildIndex(this.Label3, 0);
            this.Controls.SetChildIndex(this.Label2, 0);
            this.Controls.SetChildIndex(this.Text2, 0);
            this.Controls.SetChildIndex(this.Label_1, 0);
            this.Controls.SetChildIndex(this.Combo8, 0);
            this.Controls.SetChildIndex(this.Label8, 0);
            this.Controls.SetChildIndex(this.Text10, 0);
            this.Controls.SetChildIndex(this.Label10, 0);
            this.Controls.SetChildIndex(this.imgIntsa, 0);
            this.Controls.SetChildIndex(this.cmbVista, 0);
            this.Controls.SetChildIndex(this.Label_2, 0);
            this.Controls.SetChildIndex(this.Label_3, 0);
            this.Controls.SetChildIndex(this.Text9, 0);
            this.Controls.SetChildIndex(this.Label9, 0);
            this.Controls.SetChildIndex(this.Text3, 0);
            ((System.ComponentModel.ISupportInitialize)(this.imgIntsa)).EndInit();
            this.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Label_1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_Info_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdGridConsulta1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_Info_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel Panel;
        private ClaseIntsa.Controles.btnBotonGeneral Boton_1;
        private ClaseIntsa.Controles.btnBotonGeneral Boton_2;
        private ClaseIntsa.Controles.lblLabelGeneral Label_1_1;
        private ClaseIntsa.Controles.lblLabelGeneral Label_1_2;
        private ClaseIntsa.Controles.lblLabelGeneral Label_Info_1;
        private ClaseIntsa.Controles.txtNumeroGeneral Text5;
        private ClaseIntsa.Controles.txtTextoGeneral Text6;
        private ClaseIntsa.Controles.lblLabelGeneral Label5;
        private ClaseIntsa.Controles.lblLabelGeneral Label6;
        private ClaseIntsa.Controles.txtTextoGeneral Text4;
        private ClaseIntsa.Controles.lblLabelGeneral Label4;
        private ClaseIntsa.Controles.lblLabelGeneral Label7;
        private ClaseIntsa.Controles.txtFechaGeneral Text7;
        private ClaseIntsa.Controles.txtNumeroGeneral Text11;
        private ClaseIntsa.Controles.lblLabelGeneral Label11;
        private ClaseIntsa.Controles.txtNumeroGeneral Text12;
        private ClaseIntsa.Controles.lblLabelGeneral Label12;
        private ClaseIntsa.Controles.grdGridConsulta GrdGridConsulta1;
        private ClaseIntsa.Controles.lblLabelGeneral Label_Info_2;
        private ClaseIntsa.Controles.txtTextoGeneral Text2;
        private ClaseIntsa.Controles.lblLabelGeneral Label2;
        private ClaseIntsa.Controles.lblLabelGeneral Label3;
        private ClaseIntsa.Controles.lblLabelGeneral Label_1;
        private ClaseIntsa.Controles.lblLabelGeneral Label8;
        private ClaseIntsa.Controles.cmbComboGeneral Combo8;
        private ClaseIntsa.Controles.txtTextoGeneral Text10;
        private ClaseIntsa.Controles.lblLabelGeneral Label10;
        private ClaseIntsa.Controles.txtNumeroGeneral Text1;
        private ClaseIntsa.Controles.lblLabelGeneral Label_2;
        private ClaseIntsa.Controles.lblLabelGeneral Label_3;
        private ClaseIntsa.Controles.txtTextoGeneral Text9;
        private ClaseIntsa.Controles.lblLabelGeneral Label9;
        private ClaseIntsa.Controles.txtTextoGeneral Text3;
        private System.Windows.Forms.DataGridViewTextBoxColumn regsel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ejeord;
        private System.Windows.Forms.DataGridViewTextBoxColumn ibater;
        private System.Windows.Forms.DataGridViewTextBoxColumn nfa_0001;
        private System.Windows.Forms.DataGridViewTextBoxColumn razsoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn fee_0001;
        private System.Windows.Forms.DataGridViewTextBoxColumn liq_0001;
        private System.Windows.Forms.DataGridViewTextBoxColumn bic_0001;
        private System.Windows.Forms.DataGridViewTextBoxColumn ide_0001;
        private System.Windows.Forms.DataGridViewTextBoxColumn idereg;
        private System.Windows.Forms.DataGridViewTextBoxColumn ideori;
    }
}
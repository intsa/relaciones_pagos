﻿using ClaseIntsa.Controles;
using ClaseIntsa.Formularios;
using ClaseIntsa.Funciones;
using ClaseIntsa.Propiedades;
using DatosGenerales.Funciones;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace Relaciones_Pagos
{
    public partial class Relpag_222_21 : frmFormularioSimple
    {
        #region Interface
        public ITf_Relpag Form_Padre { get; set; }
        public grdGridConsulta[] _Cuadricula;
        public int?[] _Identificador;

        #endregion

        #region Propiedades

        /// <summary>
        /// Acción actual
        /// </summary>
        public sbyte Accion_Actual { get; set; } = -1;

        /// <summary>
        /// Matriz de identificadores actuales de cada cuadrícula
        /// </summary>
        private int[] Id_Cuadricula = new int[1];

        /// <summary>
        /// Número de identificación del ordenante
        /// </summary>
        public string Nif_Ordenante { get; set; } = "";

        /// <summary>
        /// Número de regsitros de la relacion original
        /// </summary>
        private short Registros_Relacion { get; set; } = 0;

        /// <summary>
        /// Importe total relación original
        /// </summary>
        private decimal Total_Relacion { get; set; } = 0;

        #endregion Propiedades

        #region Inicialización del formulario

        /// <summary>
        ///  Inicialización de componentes u asignación cadena conexión
        /// </summary>
        /// <param name="Numero_Cuadriculas">Número de cuadriculas a manipular</param>
        public Relpag_222_21(short Numero_Cuadriculas)
        {
            InitializeComponent();
            this.CadenaConexion = Variables_Globales.CadenaConexionAplicativo;
            Array.Resize(ref _Cuadricula, Numero_Cuadriculas);
            Array.Resize(ref _Identificador, Numero_Cuadriculas);
        }

        #endregion Inicialización del formulario

        #region Carga del formulario

        /// <summary>
        /// Carga formulario, inicialización de variables y preparación formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Relpag_222_21_Load(object sender, EventArgs e)
        {
            Refresca_Panel();

            Nif_Ordenante = Funciones_Generales._Obtener_Nif_Empresa(Variables_Globales.CodigoEmpresa, CadenaConexion);

            GrdGridConsulta1.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GrdGridConsulta1.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GrdGridConsulta1.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            Combo8.SQLInicial = Sql_Cuadro_8();
            Combo8.CargarCombo();

            if (Presenta_Datos((int)_Identificador[0]) == 0)
            {
                PresentaMensaje("RELACIÓN DE GASTOS. INEXISTENTE", 5, Color.Red);
                Funciones_Genericas.Activar_Controles("1", this, false);                // DESACTIVA CONTROLES
            }
            else
            {
                Refresca_Panel();
                Text4.Focus();
            }

        }

        #endregion Carga del formulario

        #region Funciones 

        /// <summary>
        /// Presenta los datos del 'RELACIONES DE PAGOS. MODELO-977
        /// </summary>
        /// <param name="Id_0020">Identificador registro'RELACIONES DE PAGOS. DEFINITIVAS'</param>
        private byte Presenta_Datos(int Id_0020)
        {
            Tuple<short, int> Relacion;
            byte Existe;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0020 Temporal = new Relpag_0020(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();
                    Temporal.ide_0020 = Id_0020;
                    Temporal.Cargar();

                    if (Temporal.ide_0020 == 0)
                        Existe = 0;
                    else
                    {
                        Existe = 1;
                        Registros_Relacion = Temporal.nre_0020;
                        Total_Relacion = Temporal.tot_0020;
                        Relacion = Funciones_Aplicativo.Relacion_Relpag_0010(CadenaConexion, Temporal.ide_0010);

                        Text1.Text = Temporal.ide_0020.ToString();
                        Text2.Text = Relacion.Item1.ToString();
                        Text3.Text = Relacion.Item2.ToString();
                        Text4.Text = Temporal.eje_0020.ToString();
                        Text5.Text = Temporal.rel_0020.ToString();
                        Text6.Text = Temporal.des_0020;
                        Text7.Text = Temporal.fec_0020;
                        Combo8.SelectedValue = Temporal.ide_1000;
                        Text9.Text = Temporal.bic_0020;
                        Text10.Text = Temporal.ior_0020;
                        Text11.Text = Registros_Relacion.ToString();
                        Text12.Text = Total_Relacion.ToString();

                        GrdGridConsulta1.SQLInicial = Sql_Inicial_Cuadricula_1(Temporal.ide_0020, Temporal.ide_0010);
                        GrdGridConsulta1.CargarGrid();
                        Funciones_Genericas._Numerador_Registros(GrdGridConsulta1, Label_1_2);
                        Accion_Actual = 0;
                        Refresca_Panel();
                    }

                    return Existe;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Genera la 'SQL' para cargar el cuadro combinado
        /// </summary>
        /// <returns>Cadena 'SQL'</returns>
        private string Sql_Cuadro_8()
        {
            string Cadena_Sql;
            string Cadena_Select;
            string Cadena_From;
            string Cadena_Where;
            string Restriccion_Iban = $"iba_1000 != ''";

            Cadena_Select = "SELECT ide_1000, Funciones_Generales.dbo.Normaliza_Iban ('IBAN' + iba_1000) + ' - ' + LTRIM (RTRIM(den_1000)) ibaden, iba_1000, bic_1000, cta_1000, sor_1000, ior_1000";
            Cadena_From = $" FROM  {Variables_Globales.NombreBaseDatosAplicativo}.dbo.relpag_1000";
            Cadena_Where = Restriccion_Iban;

            Cadena_Sql = Cadena_Select + Cadena_From + " WHERE " + Cadena_Where;

            return Cadena_Sql;
        }

        /// <summary>
        /// Genera la SQL inicial de la cuadrícula. 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'
        /// </summary>
        /// <returns>Cadena 'SQL'</returns>
        private string Sql_Inicial_Cuadricula_1(int Ide_0020 = 0, int Id_M997 = 0)
        {
            string Cadena_Sql;
            string[] Sql_Cadena = new string[2];
            string[] Cadena_Select = new string[2];
            string[] Cadena_From = new string[2];
            string[] Cadena_Where = new string[2];
            string[] Restriccion_Identificador = { $"ide_0020 = {Ide_0020}", $"ide_0010 = {Id_M997}" };
            string Restriccion_Situacion = $"sdo_0010_1 = 1";
            string Restriccion_Existe = $"ide_0010_1 NOT IN (SELECT ide_0010_1 FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.relpag_v_0020_1)";

            Cadena_Select[0] = "SELECT 'X' regsel, eje_0001 + '-' + ord_0001 ejeord, Funciones_Generales.dbo.Normaliza_Iban (iba_0001) ibater, nfa_0001, razsoc, fee_0001, liq_0001, bic_0001, ide_0001, ide_0020_1 idereg, ide_0010_1 ideori";
            Cadena_From[0] = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.relpag_v_0020_1";
            Cadena_Where[0] = Restriccion_Identificador[0];

            Cadena_Select[1] = "SELECT ' ' regsel, eje_0001 + '-' + ord_0001 ejeord, Funciones_Generales.dbo.Normaliza_Iban (iba_0001) ibater, nfa_0001, razsoc, fee_0001, liq_0001, bic_0001, ide_0001, 0 idereg, ide_0010_1 ideori";
            Cadena_From[1] = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.relpag_v_0010_1";
            Cadena_Where[1] = Restriccion_Identificador[1] + " AND " + Restriccion_Situacion + " AND " + Restriccion_Existe;

            for (byte i = 0; i < Sql_Cadena.Length; i++)
                Sql_Cadena[i] = Cadena_Select[i] + Cadena_From[i] + " WHERE " + Cadena_Where[i];

            Cadena_Sql = Sql_Cadena[0];

            for (byte i = 1; i < Sql_Cadena.Length; i++)
                Cadena_Sql += $" UNION ALL { Sql_Cadena[i]}";

            return Cadena_Sql;
        }

        #endregion Funciones 

        #region Funciones manipulación tablas

        #endregion

        #region Botonera

        /// <summary>
        /// Refresco del panel de botones
        /// </summary>
        private void Refresca_Panel()
        {
            Control[] Boton;
            btnBotonGeneral Boton_Panel;

            for (byte i = 0; i < Panel.Controls.Count; i++)
            {
                Boton = Panel.Controls.Find("Boton_" + Convert.ToString(i + 1), false);

                if (Boton.Length != 0)
                {
                    Boton_Panel = (btnBotonGeneral)Boton[0];
                    Boton_Panel.Refrescar();
                }
            }
        }

        /// <summary>
        /// Grabar registro. Refresco del botón
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_1_Refresco(object sender, EventArgs e)
        {
            Boton_1.Enabled = (Accion_Actual == 0) && Convert.ToInt16(Text11.Text) != 0;
        }

        /// <summary>
        /// Verifica datos e inserta registro
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Boton_1_Click(object sender, EventArgs e)
        {
            DataTable Asignaciones = (DataTable)GrdGridConsulta1.DataSource;
            Tuple<short, decimal> Totales;
            int Numero_Relacion;
            string Nombre_Secuencia;
            string Iban_Ordenante;
            string Bic_Ordenante;
            string Cuenta_Contable;
            string Sufijo_Ordenante;
            string Id_Ordenante;

            Accion_Actual = 2;
            Refresca_Panel();

            if (ValidarControles())
            {
                Nombre_Secuencia = "rel_0020_" + Text4.Text;
                Funciones_Genericas.Activar_Controles("1", this, false);                // DESACTIVA CONTROLES

                if (Text5.Text.Trim() == string.Empty)
                {
                    Control_Secuencias.Existe_Secuencia(CadenaConexion, Variables_Globales.NombreBaseDatosAplicativo, Nombre_Secuencia, "Secuencia número de relacion por año");
                    Numero_Relacion = Funciones_Aplicativo.Verificar_Secuencia(CadenaConexion, Variables_Globales.NombreBaseDatosAplicativo, Nombre_Secuencia, 1, Convert.ToInt16(Text4.Text));
                    Text5.Text = Numero_Relacion.ToString();
                }
                else
                    Numero_Relacion = Convert.ToInt32(Text4.Text);

                if (Selecciones.Verificar_Registros_Marcados(Asignaciones))
                {
                    Iban_Ordenante = ((DataTable)Combo8.DataSource).Rows[Combo8.SelectedIndex].ItemArray[2].ToString();
                    Bic_Ordenante = Text9.Text;
                    Cuenta_Contable = ((DataTable)Combo8.DataSource).Rows[Combo8.SelectedIndex].ItemArray[4].ToString();
                    Sufijo_Ordenante = ((DataTable)Combo8.DataSource).Rows[Combo8.SelectedIndex].ItemArray[5].ToString();
                    Id_Ordenante = Text10.Text;

                    Desencadenantes.Actualizar_Relpag_0020(CadenaConexion, (int)_Identificador[0], Text6.Text, Convert.ToInt32(Combo8.SelectedValue), Iban_Ordenante, Bic_Ordenante,
                                                                                                  Cuenta_Contable, Sufijo_Ordenante, Id_Ordenante);

                    Totales = Selecciones.Recorrer_Registros_Relpag_0020_1(CadenaConexion, Asignaciones, (int)_Identificador[0]);
                    Desencadenantes.Actualizar_Relpag_0020(CadenaConexion, (int)_Identificador[0], Totales.Item1, Totales.Item2);


                    Text11.Text = Totales.Item1.ToString();
                    Text12.Text = Totales.Item2.ToString();

                    Accion_Actual = 1;
                    Refresca_Panel();
                    imgIntsa.Focus();
                }
                else
                {
                    PresentaMensaje("DEBE EXISTIR AL MENOS UN DOCUMENTO MARCADO", 2, Color.Red);
                    Accion_Actual = 0;
                    Refresca_Panel();
                    GrdGridConsulta1.Select();
                }
            }
            else
            {
                Accion_Actual = 0;
                Refresca_Panel();

                SelectNextControl(ControlFoco.Parent, true, true, true, true);
                SelectNextControl(ControlFoco, true, true, true, true);
            }
        }

        #endregion

        #region Campos del formulario

        /// <summary>
        /// Validación de la descripción de la relación
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text6_Validating(object sender, CancelEventArgs e)
        {
            if (Text6.Text == string.Empty)
            {
                PresentaMensaje("DESCRIPCIÓN RELACIÓN. NO PUEDE ESTAR VACIA", 2, Color.DarkGreen);
                ControlFoco = Text6;
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Actualiza los valores relacionados
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Combo8_SelectedValueChanged(object sender, EventArgs e)
        {
            string Pais;
            string Sufijo_Ordenante;

            if (Accion_Actual == 0)
            {
                Text9.Text = ((DataTable)Combo8.DataSource).Rows[Combo8.SelectedIndex].ItemArray[3].ToString();

                Pais = ((DataTable)Combo8.DataSource).Rows[Combo8.SelectedIndex].ItemArray[2].ToString().Left(2);
                Sufijo_Ordenante = ((DataTable)Combo8.DataSource).Rows[Combo8.SelectedIndex].ItemArray[5].ToString();
                Text10.Text = Funciones_Aplicativo.Sepa_Identificador_Ordenante(Nif_Ordenante, Pais, Sufijo_Ordenante);
            }
        }

        #region Cuadrículas

        /// <summary>
        /// Cambio de registro 'RELACIÓN DE PAGOS. DOCUMENTOS'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_SelectionChanged(object sender, EventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            string Iban;
            string Bic_Swift;

            int Identificador;

            if (Cuadricula.SelectedRows.Count > 0)
            {
                Identificador = Convert.ToInt32(Cuadricula.SelectedRows[0].Cells[Cuadricula.ColumnaClaveIdentidad].Value);

                if (Identificador != Id_Cuadricula[0]
                  &&
                      Identificador != 0)
                {
                    Id_Cuadricula[0] = Identificador;
                    Funciones_Genericas._Numerador_Registros(Cuadricula, Label_1_2);

                    Iban = Convert.ToString(Cuadricula.SelectedRows[0].Cells["ibater"].Value);
                    Bic_Swift = Convert.ToString(Cuadricula.SelectedRows[0].Cells["bic_0001"].Value);

                    if (Iban.Trim() == string.Empty)
                    {
                        Label_Info_2.Text = "DOCUMENTO. SIN I.B.A.N.";
                        Label_Info_2.ForeColor = Color.Red;
                    }
                    else
                    {
                        if (Bic_Swift.Trim() == string.Empty)
                        {
                            Label_Info_2.Text = "DOCUMENTO. SIN B.I.C./S.W.I.F.T.";
                            Label_Info_2.ForeColor = Color.OrangeRed;
                        }
                    }

                    Refresca_Panel();
                }
            }
        }

        /// <summary>
        /// Formatea la cuadrícula 'RELACIÓN DE PAGOS. DOCUMENTOS'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            string Marcada = Convert.ToString(Cuadricula.Rows[e.RowIndex].Cells["regsel"].Value);
            string Iban = Convert.ToString(Cuadricula.Rows[e.RowIndex].Cells["ibater"].Value);
            string Bic_Switf = Convert.ToString(Cuadricula.Rows[e.RowIndex].Cells["bic_0001"].Value);
            Color Color_Letra;

            if (Iban.Trim() == string.Empty)
                Color_Letra = Color.Red;
            else
            {
                if (Bic_Switf.Trim() == string.Empty)
                    Color_Letra = Color.DarkOrange;
                else
                {
                    if (Marcada.Trim() == string.Empty)
                        Color_Letra = Color.FromArgb(0, 100, 255);
                    else
                        Color_Letra = Color.Purple;
                }
            }

            e.CellStyle.ForeColor = Color_Letra;
            e.CellStyle.SelectionForeColor = Color_Letra;
        }

        /// <summary>
        /// Marca/desmarca registro seleccionado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            string Marca;
            short Registros;
            decimal Total_Importe;
            decimal Importe;

            if (Accion_Actual == 0)
            {
                if (Cuadricula.SelectedRows.Count > 0)
                {
                    Marca = Convert.ToString(Cuadricula.SelectedRows[0].Cells["regsel"].Value);
                    Importe = Convert.ToDecimal(Cuadricula.SelectedRows[0].Cells["liq_0001"].Value);
                    Registros = Convert.ToInt16(Text11.Text);
                    Total_Importe = Convert.ToDecimal(Text12.Text);

                    if (e.KeyCode == Keys.Space)
                    {
                        if (Marca.Trim() == string.Empty)
                        {
                            Cuadricula.SelectedRows[0].Cells["regsel"].Value = "X";
                            Registros++;
                            Total_Importe = Total_Importe + Importe;
                        }
                        else
                        {
                            Cuadricula.SelectedRows[0].Cells["regsel"].Value = "";
                            Registros--;
                            Total_Importe = Total_Importe - Importe;
                        }

                        Text11.Text = Registros.ToString();
                        Text12.Text = Total_Importe.ToString();
                        Cuadricula.Refresh();
                        Refresca_Panel();
                    }
                }
            }
        }

        /// <summary>
        /// Selecciona un registro con doble click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            string Marca;
            short Registros;
            decimal Total_Importe;
            decimal Importe;

            if (Accion_Actual == 0)
            {
                if (e.Button == MouseButtons.Left)
                {
                    var Fila_Columna = Cuadricula.HitTest(e.X, e.Y);

                    if (Cuadricula.Rows.Count != 0)
                    {
                        if (Fila_Columna.RowIndex >= 0)
                        {
                            Cuadricula.ClearSelection();
                            Cuadricula.Rows[Fila_Columna.RowIndex].Selected = true;

                            Marca = Convert.ToString(Cuadricula.Rows[Fila_Columna.RowIndex].Cells["regsel"].Value);
                            Importe = Convert.ToDecimal(Cuadricula.Rows[Fila_Columna.RowIndex].Cells["liq_0001"].Value);
                            Registros = Convert.ToInt16(Text11.Text);
                            Total_Importe = Convert.ToDecimal(Text12.Text);

                            if (Marca.Trim() == string.Empty)
                            {
                                Cuadricula.SelectedRows[0].Cells["regsel"].Value = "X";
                                Registros++;
                                Total_Importe = Total_Importe + Importe;
                            }
                            else
                            {
                                Cuadricula.SelectedRows[0].Cells["regsel"].Value = "";
                                Registros--;
                                Total_Importe = Total_Importe - Importe;
                            }

                            Text11.Text = Registros.ToString();
                            Text12.Text = Total_Importe.ToString();
                            Cuadricula.Refresh();
                            Refresca_Panel();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Mostrar la etiqueta cuando recibe en foco
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_Enter(object sender, EventArgs e)
        {
            if (Accion_Actual == 0)
            {
                if (GrdGridConsulta1.Rows.Count != 0)
                    Label_Info_1.Visible = true;
            }
        }

        /// <summary>
        /// Ocultar la etiqueta cuando recibe en foco
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_Leave(object sender, EventArgs e)
        {
            if (Accion_Actual == 0)
            {
                if (GrdGridConsulta1.Rows.Count != 0)
                    Label_Info_1.Visible = false;
            }
        }

        #region Menú contextual cuadrícula 'RELACIÓN DE PAGOS. DOCUMENTOS'

        /// <summary>
        /// Crea menú contextual de acciones cuadrícula 'CLASIFICACIÓN DE EMPLEADOS PÚBLICOS. ASIGNACIONES'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_MouseClick(object sender, MouseEventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;

            if (Accion_Actual == 0)
            {
                if (e.Button == MouseButtons.Right)
                {
                    ContextMenu Menu_Contextual = new ContextMenu();
                    Cuadricula.Focus();

                    var Fila_Columna = Cuadricula.HitTest(e.X, e.Y);
                    MenuItem Item_Menu;

                    if (Cuadricula.Rows.Count != 0)
                    {
                        if (Fila_Columna.RowIndex >= 0)
                        {
                            Cuadricula.ClearSelection();
                            Cuadricula.Rows[Fila_Columna.RowIndex].Selected = true;

                            #region 1 Marcar registro

                            if (Convert.ToString(Cuadricula.SelectedRows[0].Cells["regsel"].Value).Trim() == string.Empty)
                            {
                                Item_Menu = new MenuItem();
                                Item_Menu.Text = "Marcar registro";
                                Item_Menu.Click += Marcar_Registro_Click;
                                Menu_Contextual.MenuItems.Add(Item_Menu);
                            }
                            #endregion Marcar registro

                            #region 2 Desmarcar registro

                            if (Convert.ToString(Cuadricula.SelectedRows[0].Cells["regsel"].Value) == "X")
                            {
                                Item_Menu = new MenuItem();
                                Item_Menu.Text = "Desmarcar registro";
                                Item_Menu.Click += Desmarcar_Registro_Click;
                                Menu_Contextual.MenuItems.Add(Item_Menu);
                            }

                            #endregion Desmarcar registro
                        }
                    }

                    Menu_Contextual.Show(Cuadricula, new Point(e.X, e.Y));
                }
            }
        }

        /// <summary>
        /// Marca registro
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Marcar_Registro_Click(Object sender, EventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            short Registros = Convert.ToInt16(Text11.Text);
            decimal Total_Importe = Convert.ToDecimal(Text12.Text);
            decimal Importe = Convert.ToDecimal(Cuadricula.SelectedRows[0].Cells["liq_0001"].Value);

            Registros++;
            Total_Importe += Importe;

            Cuadricula.SelectedRows[0].Cells["regsel"].Value = "X";
            Text11.Text = Registros.ToString();
            Text12.Text = Total_Importe.ToString();
            Cuadricula.Refresh();
            Refresca_Panel();
        }

        /// <summary>
        /// Quita la marca 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Desmarcar_Registro_Click(Object sender, EventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            short Registros = Convert.ToInt16(Text11.Text);
            decimal Total_Importe = Convert.ToDecimal(Text12.Text);
            decimal Importe = Convert.ToDecimal(Cuadricula.SelectedRows[0].Cells["liq_0001"].Value);

            Registros--;
            Total_Importe -= Importe;

            Cuadricula.SelectedRows[0].Cells["regsel"].Value = "";
            Text11.Text = Registros.ToString();
            Text12.Text = Total_Importe.ToString();
            Cuadricula.Refresh();
            Refresca_Panel();
        }

        #endregion Menú contextual cuadrícula 'RELACIÓN DE PAGOS. DOCUMENTOS'

        #endregion Cuadrículas

        #endregion Campos formulario

        #region Cierre Formulario

        /// <summary>
        ///  Cierre del formulario y asignación del valor del último identificador dato de alta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Relpag_222_21_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form_Padre.Retorno_Formulario(_Cuadricula, _Identificador, 0);
        }

        #endregion

        //
    }
    //
}

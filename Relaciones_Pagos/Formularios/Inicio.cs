﻿using ClaseIntsa.Controles;
using ClaseIntsa.DataBase;
using ClaseIntsa.Formularios;
using ClaseIntsa.Funciones;
using ClaseIntsa.Propiedades;
using DatosComunes.Formularios;
using DatosGenerales.Formularios;
using System;
using System.Linq;

namespace Relaciones_Pagos
{
    public partial class Inicio : frm_MDI
    {
        public Inicio()
        {
            InitializeComponent();
            this.SuspendLayout();
            //CREAR EL FORMULARIO
            Botones();
            //COMPROBAR PERMISOS
            ComprobarAccesos();

            //IMAGEN DE FONDO
            //panelContenedor.BackgroundImageLayout = ImageLayout.Stretch;
            //panelContenedor.BackgroundImage = Properties.Resources.calculator_385506_1920;

            this.ResumeLayout();
        }

        private void Inicio_Load(object sender, EventArgs e)
        {
            Timer2.Enabled = true;
        }

        private void Inicio_Shown(object sender, EventArgs e)
        {
            //COMPRUEBO SI SE HAN HECHO TODAS LAS ACTUALIZACIONES
            long VersionBBDD = claseBaseDatos.solicitaVersionBD(Variables_Globales.InstanciaBD, Variables_Globales.NombreBaseDatosAplicativo);

            if (VersionBBDD != Variables_Globales.VersionAplicativo)
            {
                frmMessageBox.Mostrar("LA VERSIÓN DE APLICATIVO NO COINCIDE CON LA DE LA BASE DE DATOS.");
                this.Close();
                this.Dispose();
            }

        }

        public void Botones()
        {
            new frm_MenuDatosComunes();
            //***************************************************
            //OJO EL IDENTIFICADOR 9 SE RESERVA PARA FAVORITOS  *
            //***************************************************

            CrearBotonMDI("btnInicio", "1 Inicio", "1", "0", true, ImgNormal: ClaseIntsa.Properties.Resources.MDI_HomeBlanco, ImgSelecionado: ClaseIntsa.Properties.Resources.MDI_HomeGold);
            CrearBotonMDI("btndatosGenerales", "1.1 Menús Generales", "11", "1", true);
            CrearBotonMDI("btnMenuGenerales", "1.1.1 Datos Generales", "111", "11", false, abrirForm: "DatosGenerales.Formularios.frm_MenuGeneral");
            CrearBotonMDI("btnMenuComunes", "1.1.2 Datos Comunes", "112", "11", false, abrirForm: "DatosComunes.Formularios.frm_MenuDatosComunes");
            CrearBotonMDI("btnSelecciones", "1.2 Seleciones", "12", "1", true);
            CrearBotonMDI("btnDatosTrabajo", "1.2.1 Datos de trabajo", "121", "12", false, true, abrirForm: null);

            btnBotonMDI oBtnDatosTrabajo = MenuVertical.Controls.Find("btnDatosTrabajo", true).FirstOrDefault() as btnBotonMDI;

            if (!oBtnDatosTrabajo.Expandir)
            {
                oBtnDatosTrabajo.Click += btnDatosTrabajo_Click;
            }

            void btnDatosTrabajo_Click(object sender, EventArgs e)
            {
                Funciones_Globales.AbrirFormulario(new frm_Login(), true, true);

                if (Variables_Globales.LoginOK)
                {
                    Funciones_Inicio.Bases_Datos_Aplicativo();

                    //SI EXISTE LA BASE DE DATOS
                    if (claseBaseDatos.existe(Variables_Globales.InstanciaBD, Variables_Globales.NombreBaseDatosAplicativo))
                    {
                        //SI NO ESTÁ ACTUALIZANDO LA BASE DE DATOS
                        if (!claseBaseDatos.solicitaBloqueoBD(Variables_Globales.InstanciaBD, Variables_Globales.NombreBaseDatosAplicativo))
                        {
                            long VersionBBDD = claseBaseDatos.solicitaVersionBD(Variables_Globales.InstanciaBD, Variables_Globales.NombreBaseDatosAplicativo);

                            // SI LA VERSION DE LA BASE DE DATOS ES MENOR QUE LA DEL APLICATIVO.. ACTUALIZO LA BASE DE DATOS
                            if (VersionBBDD < Variables_Globales.VersionAplicativo)
                            {
                                // BLOQUEAR LA BASE DE DATOS PARA QUE NADIE ENTRE EN EL PROCESO, ACTUALIZAR Y DESBLOQUEAR
                                claseBaseDatos.ActualizarPropiedadBD(Variables_Globales.InstanciaBD, Variables_Globales.NombreBaseDatosAplicativo, "bloqueado", "si");
                                BaseDatos.Actualizar(Variables_Globales.InstanciaBD, Variables_Globales.NombreBaseDatosAplicativo, VersionBBDD);
                                claseBaseDatos.ActualizarPropiedadBD(Variables_Globales.InstanciaBD, Variables_Globales.NombreBaseDatosAplicativo, "bloqueado", "no");
                            }
                            Variables_Globales.CadenaConexionAplicativo = "Data Source=" + Variables_Globales.InstanciaBD + "; Initial Catalog = " + Variables_Globales.NombreBaseDatosAplicativo + "; User ID = intsa; Password = +sire2011";
                            Text = Variables_Globales.TextTituloFormMDI;
                            ComprobarAccesos();
                        }
                        else
                        {
                            frmMessageBox.Mostrar("LA BASE DE DATOS '" + Variables_Globales.NombreBaseDatosAplicativo + "' ESTÁ SIENDO ACTUALIZA EN OTRO EQUIPO. PRUEBE DE NUEVO EN UNOS SEGUNDOS");
                        }
                    }
                    else
                    {
                        frmMessageBox.Mostrar("NO EXISTE LA BASE DE DATOS: " + Variables_Globales.NombreBaseDatosAplicativo);
                        btnDatosTrabajo_Click(sender, e);
                    }
                }
            }

            CrearBotonMDI("btnDatosPropios", "1.3 Datos propios", "13", "1", true);
            CrearBotonMDI("Boton_Relpag_131", "1.3.1 Parámetos generales", "131", "13", false, abrirForm: "Relaciones_Pagos.Relpag_131");

            #region Proceso
            {
                CrearBotonMDI("btnProceso", "2 Proceso", "2", "0", true, ImgNormal: ClaseIntsa.Properties.Resources.MDI_BusinessBlanco, ImgSelecionado: ClaseIntsa.Properties.Resources.MDI_BusinessGold);
                CrearBotonMDI("Boton_Relpag_22", "2.2 Relaciones de pago", "22", "2", true);
                CrearBotonMDI("Boton_Relpag_221", "2.2.1 Modelo 997", "221", "22", false, abrirForm: "Relaciones_Pagos.Relpag_221");
                CrearBotonMDI("Boton_Relpag_222", "2.2.2 Verificadas", "222", "22", false, abrirForm: "Relaciones_Pagos.Relpag_222");

            }
            #endregion Proceso

            #region 3 -Documentos
            {
                CrearBotonMDI("btnInformes", "3 Documentos", "3", "0", true, ImgNormal: ClaseIntsa.Properties.Resources.MDI_DocumentosBlanco, ImgSelecionado: ClaseIntsa.Properties.Resources.MDI_DocumentosGold);

            }
            #endregion Documentos

            #region 4 - Informes
            {
                CrearBotonMDI("btnInformes", "4 Informes", "4", "0", true, ImgNormal: ClaseIntsa.Properties.Resources.MDI_Informes_Blanco, ImgSelecionado: ClaseIntsa.Properties.Resources.MDI_Informes_Gold);
                CrearBotonMDI("Boton_Relpag_41", "4.1 Gestión presupuesto", "41", "4", true);
                CrearBotonMDI("Boton_Relpag_411", "4.1.1 Presupuesto de gastos", "411", "41", true);
                CrearBotonMDI("Boton_Relpag_4111 ", "4.1.1.1 Presupuesto de gastos. Emisión general", "4111", "411", false, abrirForm: "Relaciones_Pagos.Relpag_4111");
                CrearBotonMDI("Boton_Relpag_412", "4.1.2 Presupuesto de ingresos", "412", "41", true);
                CrearBotonMDI("Boton_Relpag_4121 ", "4.1.2.1  Presupuesto de ingresos. Emisión general", "4121", "412", false, abrirForm: "Relaciones_Pagos.Relpag_4121");
                CrearBotonMDI("Boton_Relpag_413", "4.1.3 Anexo de inversiones", "413", "41", true);
                CrearBotonMDI("Boton_Relpag_4131 ", "4.1.3.1  Anexo de inversiones. Emisión general", "4131", "413", false, abrirForm: "Relaciones_Pagos.Relpag_4131");
                CrearBotonMDI("Boton_Relpag_414", "4.1.4 Anexo de personal", "414", "41", true);
                CrearBotonMDI("Boton_Relpag_4141 ", "4.1.4.1  Anexo de personal. Emisión general", "4141", "414", false, abrirForm: "Relaciones_Pagos.Relpag_4141");
                CrearBotonMDI("Boton_Relpag_415 ", "4.1.5  Gestión presupuesto. Emisión general", "415", "41", false, abrirForm: "Relaciones_Pagos.Relpag_415");
                CrearBotonMDI("Boton_Relpag_42", "4.2 Histórico", "42", "4", true);
                CrearBotonMDI("Boton_Relpag_421", "4.2.1 Presupuesto de gastos", "421", "42", true);
                CrearBotonMDI("Boton_Relpag_4211 ", "4.2.1.1 Histórico. Presupuesto de gastos. Emisión general", "4211", "421", false, abrirForm: "Relaciones_Pagos.Relpag_4211");

                CrearBotonMDI("Boton_Relpag_424", "4.2.4 Anexo de personal", "424", "42", true);
                CrearBotonMDI("Boton_Relpag_4241 ", "4.2.4.1 Histórico. Anexo de personal. Emisión general", "4241", "424", false, abrirForm: "Relaciones_Pagos.Relpag_4241");
            }
            #endregion Informes

            #region 5 -Enlaces
            {
                CrearBotonMDI("btnEnlaces", "5 Enlaces", "5", "0", true, ImgNormal: ClaseIntsa.Properties.Resources.MDI_EnlacesBlanco, ImgSelecionado: ClaseIntsa.Properties.Resources.MDI_EnlacesGold);

            }
            #endregion Enlaces

            #region 6 -Especiales
            {
                CrearBotonMDI("btnEspeciales", "6 Especiales", "6", "0", true, ImgNormal: ClaseIntsa.Properties.Resources.MDI_EspecialesBlanco, ImgSelecionado: ClaseIntsa.Properties.Resources.MDI_EspecialesGold);
                CrearBotonMDI("btnAperturaEjer", "6.1 Apertura Ejercicio", "61", "6", false);
                CrearBotonMDI("Boton_Relpag_62", "6.2 Importación de datos", "62", "6", false, abrirForm: "Relaciones_Pagos.Relpag_62");

            }
            #endregion Especiales

            #region 7 -Históricos
            {
                CrearBotonMDI("btnHistorico", "7 Históricos", "7", "0", true, ImgNormal: ClaseIntsa.Properties.Resources.MDI_Historico_Blanco, ImgSelecionado: ClaseIntsa.Properties.Resources.MDI_Historico_Gold);
                CrearBotonMDI("b_71", "7.1 Presupuestos definitivos", "71", "7", true);
                CrearBotonMDI("Boton_Relpag_711", "7.1.1 Traspaso", "711", "71", false, abrirForm: "Relaciones_Pagos.Relpag_711");
                CrearBotonMDI("Boton_Relpag_72", "7.2 Aplicaciones presupuestarias", "72", "7", false, abrirForm: "Relaciones_Pagos.Relpag_72");
                CrearBotonMDI("Boton_Relpag_73", "7.3 Anexo de inversiones", "73", "7", false, abrirForm: "Relaciones_Pagos.Relpag_73");
                CrearBotonMDI("Boton_Relpag_74", "7.4 Anexo de personal", "74", "7", false, abrirForm: "Relaciones_Pagos.Relpag_74");

            }
            #endregion Especiales

            CrearBotonMDI("btnUtilidades", "8. Utilidades", "8", "0", true, ImgNormal: ClaseIntsa.Properties.Resources.MDI_BuildBlanco, ImgSelecionado: ClaseIntsa.Properties.Resources.MDI_BuildGold);

            //CASO EN EL QUE SE QUIERA MANEJAR EL EVENTO CLICK DE UNA MANERA ESPECIAL
            CrearBotonMDI("btnVersiones", "8.1. Versiones", "81", "8", false, abrirForm: null);
            btnBotonMDI oBtnConsultarVer = MenuVertical.Controls.Find("btnVersiones", true).FirstOrDefault() as btnBotonMDI;

            if (!oBtnConsultarVer.Expandir)
            {
                oBtnConsultarVer.Click += new EventHandler((sender, e) =>
                {
                    long[] Versiones_Aplicativo = { Variables_Globales.VersionAplicativoDatosGenerales, Propiedades_Aplicativo.Version_Comunes, Propiedades_Aplicativo.Version_Auxiliares, Variables_Globales.VersionAplicativo, Propiedades_Aplicativo.Version_Comunes_Conta, Propiedades_Aplicativo.Version_Historico_Conta };
                    string[] BaseDatos = { "Generales", "Comunes", "Auxiliares", Variables_Globales.NombreBaseDatosAplicativo, Propiedades_Aplicativo.Bdd_Comunes, Propiedades_Aplicativo.Bdd_Historico };
                    long Versiones_Bdd = 0;
                    string Mensaje = "";

                    for (byte i = 0; i < BaseDatos.Length; i++)
                    {
                        Versiones_Bdd = claseBaseDatos.solicitaVersionBD(Variables_Globales.InstanciaBD, BaseDatos[i]);
                        Mensaje += BaseDatos[i] + "\n";
                        Mensaje += "Base de datos / Aplicativo : " + Versiones_Bdd.ToString() + " / " + Versiones_Aplicativo[i].ToString() + "\n\n";
                    }

                    frmMessageBox.Mostrar(Mensaje, "VERSIONES", frmMessageBox.TipoBotones.Aceptar, frmMessageBox.Iconos.Info);
                });
            }
        }

        public void ComprobarAccesos()
        {
            btnBotonMDI oBtnMenuGenerales = MenuVertical.Controls.Find("btnMenuGenerales", true).FirstOrDefault() as btnBotonMDI;
            oBtnMenuGenerales.Enabled = Variables_Globales.AdminUsuario;

        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            Label1.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        }
    }
}

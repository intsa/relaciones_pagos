﻿using ClaseIntsa.Controles;
using ClaseIntsa.Formularios;
using ClaseIntsa.Funciones;
using ClaseIntsa.Propiedades;
using System;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Relaciones_Pagos
{
    public partial class Relpag_001 : frmFormularioSimple
    {
        #region Propiedades

        /// <summary>
        /// Acción ejecutada actual
        /// </summary>
        public sbyte Accion_Actual { get; set; } = 2;

        /// <summary>
        /// Identificador de registro de la cuadrícula
        /// </summary>
        private string Id_Cuadricula { get; set; } = "";

        #endregion Propiedades

        #region Carga del formulario

        /// <summary>
        ///  Inicialización de componentes y asignación cadena conexión
        /// </summary>
        public Relpag_001()
        {
            InitializeComponent();
            this.CadenaConexion = Variables_Globales.CadenaConexionAplicativo;
        }

        #endregion Carga del formulario

        #region Inicialización del formulario

        /// <summary>
        /// Carga formulario, inicialización de variables y preparación formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Relpag_001_Load(object sender, EventArgs e)
        {
            Tuple<int, byte> Documento_Pago;
            short Ejercicio = Funciones_Aplicativo.Ejercicio_Relpag_8000(CadenaConexion, 1);
            DataTable Traspaso;
            DataTable Documentos;
            string Id_Registro;
            int Id_0001;
            int i = 0;
            string Directorio_Raiz = @"C:\Bdd_Fox\";
            string[] Tablas = new string[4];

            string Conexion_Oledb =  $"Provider=VFPOLEDB.1;Data Source = {Directorio_Raiz}; Collating Sequence=machine";

            this.Show();
            this.Refresh();

            GrdGridConsulta1.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GrdGridConsulta1.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GrdGridConsulta1.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GrdGridConsulta1.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            if (Ejercicio == 0)
            {
                Ejercicio = Convert.ToInt16(DateTime.Now.Year);
                Desencadenantes.Insertar_Relpag_8000(CadenaConexion, Ejercicio);
            }

            Propiedades_Aplicativo.Ejercicio_Trabajo = Ejercicio;

            PresentaMensaje("ACTUALIZANDO DATOS", 1, Color.Blue);

            Desencadenantes_Importacion.Eliminar_Relpag_0001(CadenaConexion);
            Control_Secuencias.Inicializar_Secuencia (CadenaConexion, Variables_Globales.NombreBaseDatosAplicativo, "ide_0001");

            Tablas[0] = $@"\Relaciones_Pagos\rpf00100";
            Traspaso = Seleccion_Datos(0, Conexion_Oledb, Tablas);
            Recorrer_Canales(Traspaso);

            Tablas[0] = $@"\Relaciones_Pagos\rpf00500";
            Traspaso = Seleccion_Datos(1, Conexion_Oledb, Tablas);;
            Recorrer_Formas(Traspaso);

            Tablas[0] = $@"\Facturas\taf00001";
            Tablas[1] = $@"\Facturas\taf00004";
            Tablas[2] = $@"\Facturas\taf00001_1";
            Tablas[3] = $@"\Datos_Comunes\dcf00009";
            Documentos = Seleccion_Datos(2, Conexion_Oledb, Tablas);
            GrdGridConsulta1.TablaDatosSQLInicial = Documentos;
            GrdGridConsulta1.DataSource = Documentos;

            GrdGridConsulta1.Select();
            GrdGridConsulta1.Refresh();
            
            Traspaso = Seleccion_Datos(3, Conexion_Oledb, Tablas);

            BarraProgreso.Activar(ValorMaximo: Traspaso.Rows.Count);

            foreach (DataRow Fila in Traspaso.Rows)
            {
                PresentaMensaje("ACTUALIZANDO DATOS", 1, Color.Black);
                i++;
                BarraProgreso.Incrementar();
                Id_Registro = (string)Fila["ipp_004"];


                Documento_Pago = Funciones_Aplicativo.Identificador_Relpag_0001(CadenaConexion, Id_Registro);

                if (Documento_Pago.Item1 == 0)
                {
                    Id_0001 = Funciones_Aplicativo.Verificar_Secuencia(CadenaConexion, Variables_Globales.NombreBaseDatosAplicativo, "ide_0001", 2);
                    Desencadenantes_Importacion.Insertar_Relpag_0001(CadenaConexion, Id_0001, Fila);
                }
                else
                {
                    if (Documento_Pago.Item2 == 0)
                        Desencadenantes_Importacion.Actualizar_Relpag_0001(CadenaConexion, Documento_Pago.Item1, Fila);
                }

                GrdGridConsulta1.Posicionar("ipp_004", Id_Registro, 1);

                if (i % 50 == 0)
                    GrdGridConsulta1.Refresh();
            }

            Accion_Actual = 0;
            GrdGridConsulta1.Refresh();
            PresentaMensaje("ACTUALIZACIÓN REALIZADA", 1, Color.Blue);

            this.Close();
        }

        #endregion Inicialización del formulario

        #region Funciones

        /// <summary>
        /// Genera la SQL inicial de la cuadrícula. 'DOCUMENTOS PENDIENTES DE PAGO'
        /// </summary>
        /// <returns>Cadena 'SQL'</returns>
        private string Sql_Cuadricula_1(byte Indice, string [] Tablas)
        {
            string Cadena_Sql;
            string Cadena_Select = "";
            string Cadena_From = "";
            string Cadena_Where = "";

            switch (Indice)
            {
                case 0:                                                     // CANALES DE PAGO
                    {
                        Cadena_Select = "SELECT can_100, den_100, iba_100, bic_100, cta_100, sor_100, ior_100";
                        Cadena_From = $" FROM {Tablas[0]}";

                        break;
                    }

                case 1:                                                     // FORMAS DE PAGO
                    {
                        Cadena_Select = "SELECT cod_500, den_500, cns_500";
                        Cadena_From = $" FROM{Tablas[0]}";

                        break;
                    }

                case 2:                                                     // DOCUMENTOS PENDIENTES DE PAGO. CUADRÍCULA
                    {
                        Cadena_Select = "SELECT ipp_004, (eje_001 + '-' + num_001) AS ejeord, fac_001, fee_001, nif_001, LEFT (ALLTRIM (ALLTRIM (ape_001) + ' ' + ALLTRIM (nom_001)) + SPACE (200), 200) AS razsoc,";
                        Cadena_Select += " (bas_004 + imp_004 - irp_004) AS impliq";
                        Cadena_From = $" FROM {Tablas[1]} LEFT OUTER JOIN {Tablas[0]}";
                        Cadena_From += " ON Taf00004.ifa_004 = Taf00001.ifa_001";
                        Cadena_Where = "sco_004 NOT IN ('0','R') AND (bas_004 + imp_004 - irp_004) > 0";

                        break;

                    }

                case 3:                                                     // DOCUMENTOS PENDIENTES DE PAGO. TRASPASO
                    {
                        Cadena_Select = "SELECT ipp_004, (eje_001 + '-' + num_001) AS ejeord, fac_001, fee_001, nif_001, LEFT (ALLTRIM (ALLTRIM (ape_001) + ' ' + ALLTRIM (nom_001)) + SPACE (200), 200) AS razsoc,";
                        Cadena_Select += " bas_004, imp_004, iba_004, irp_004, (bas_004 + imp_004 - irp_004) AS impliq, acf_001, sac_001, cap_001, pap_001, tif_001, sco_004,";
                        Cadena_Select += " eje_001, cfa_001, num_001, ape_001, nom_001, nre_001, eje_004, exp_004, doc_004, nas_001, ejs_001, res_001, nes_001, nss_001, ifa_001, tec_001,";
                        Cadena_Select += " sig_009, dom_009, nu1_009, le1_009, nu2_009, le2_009, kms_009, por_009, esc_009, pla_009, pue_009, loc_009,";
                        Cadena_Select += " cop_009, pob_009, pro_009, pai_009,";
                        Cadena_Select += $" (SELECT iba_001_1 + '·' + bic_001_1 FROM {Tablas[2]} WHERE taf00001_1.irf_001_1 = Taf00001.ifa_001) as datban";
                        Cadena_From = $" FROM {Tablas[1]} LEFT OUTER JOIN {Tablas[0]} LEFT OUTER JOIN  {Tablas[3]} ";
                        Cadena_From += " ON Taf00001.tec_001 = nui_009 ON Taf00004.ifa_004 = Taf00001.ifa_001";
                        Cadena_Where = "sco_004 NOT IN ('0','R') AND (bas_004 + imp_004 - irp_004) > 0";

                        break;

                    }

            }

            Cadena_Sql = Cadena_Select + Cadena_From;

            if (Cadena_Where != string.Empty)
                Cadena_Sql += $" WHERE {Cadena_Where}";

            return Cadena_Sql;
        }

        /// <summary>
        /// Conexión con la base de datos y obtención de un DataTable con los datos a actualizar
        /// </summary>
        /// <returns>Datable datos selección</returns>
        private DataTable Seleccion_Datos(byte Indice, string Conexion_Odbc, string[] Tablas )
        {
            string Cadena_Sql;
            DataTable Documentos = new DataTable();

            using (OleDbConnection Cadena_Oledb = new OleDbConnection(Conexion_Odbc))
            {
                Cadena_Oledb.Open();
                Cadena_Sql = Sql_Cuadricula_1(Indice, Tablas);
                OleDbDataAdapter Data_Reader = new OleDbDataAdapter(Cadena_Sql, Cadena_Oledb);
                Data_Reader.Fill(Documentos);
                Cadena_Oledb.Close();

                 return Documentos;
            }
        }

        /// <summary>
        /// Recorre DataTal de 'CANALES DE PAGO' e inserta o actualiza registros
        /// </summary>
        /// <param name="Detalles">DataTable 'CANALES DE PAGO'</param>
        private void Recorrer_Canales(DataTable Detalles)
        {
            int Id_1000;
            string Codigo;

            foreach (DataRow Fila in Detalles.Rows)
            {
                Codigo = (string)Fila["can_100"];
                Id_1000 = Funciones_Aplicativo.Identificador_Relpag_1000(CadenaConexion, Codigo);

                if (Id_1000 == 0)
                    Desencadenantes_Importacion.Insertar_Relpag_1000(CadenaConexion, Fila);
                else
                    Desencadenantes_Importacion.Actualizar_Relpag_1000(CadenaConexion, Id_1000, Fila);

            }


        }

        /// <summary>
        /// Recorre DataTal de 'FORMAS DE PAGO' e inserta o actualiza registros
        /// </summary>
        /// <param name="Detalles">DataTable 'FORMAS DE PAGO'</param>
        private void Recorrer_Formas(DataTable Detalles)
        {
            int Id_1005;
            string Codigo;

            foreach (DataRow Fila in Detalles.Rows)
            {
                Codigo = (string)Fila["cod_500"];
                Id_1005 = Funciones_Aplicativo.Identificador_Relpag_1005(CadenaConexion, Codigo);

                if (Id_1005 == 0)
                    Desencadenantes_Importacion.Insertar_Relpag_1005(CadenaConexion, Fila);
                else
                    Desencadenantes_Importacion.Actualizar_Relpag_1005(CadenaConexion, Id_1005, Fila);

            }


        }

        #endregion Funciones

        #region Funciones de manipulación de base de datos

        #endregion Funciones de manipulación de base de datos

        #region Botonera

        #endregion Botonera

        #region Campos formulario

        /// <summary>
        /// Cambio de registro  'APLICACIONES PRESUPUESTARIAS. GASTOS'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GrdGridConsulta1_SelectionChanged(object sender, EventArgs e)
        {
            grdGridConsulta Cuadricula = GrdGridConsulta1;
            string Identificador;

            if (Cuadricula.SelectedRows.Count > 0)
            {
                Identificador = Convert.ToString(Cuadricula.SelectedRows[0].Cells[Cuadricula.ColumnaClaveIdentidad].Value);

                if (Identificador != Id_Cuadricula
                     &&
                     Identificador != string.Empty)
                {
                    Id_Cuadricula = Identificador;
                    Funciones_Genericas._Numerador_Registros(Cuadricula, Label_1_2);
                }
            }
        }

        #endregion Campos formulario

        #region Cierre formulario

        /// <summary>
        ///  Cierre del formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Relpag_001_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Accion_Actual == 2)
            {
                PresentaMensaje("NO SE PUEDE CERRAR EL FORMULARIO. HAY PROCESOS PENDIENTES", 3, Color.Red);
                e.Cancel = true;
            }
        }

        #endregion

        //
    }
    //
}
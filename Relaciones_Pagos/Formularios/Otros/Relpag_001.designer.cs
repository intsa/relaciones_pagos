﻿namespace Relaciones_Pagos
{
    partial class Relpag_001
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GrdGridConsulta1 = new ClaseIntsa.Controles.grdGridConsulta(this.components);
            this.Label_1_2 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label_1_1 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Label_Info_1 = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.ipp_004 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ejeord = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fac_001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fee_001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nif_001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.razsoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.impliq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.imgIntsa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdGridConsulta1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_1_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_Info_1)).BeginInit();
            this.SuspendLayout();
            // 
            // imgIntsa
            // 
            this.imgIntsa.Location = new System.Drawing.Point(820, 30);
            this.imgIntsa.Margin = new System.Windows.Forms.Padding(4);
            this.imgIntsa.Size = new System.Drawing.Size(170, 50);
            // 
            // cmbVista
            // 
            this.cmbVista.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbVista.BackColor = System.Drawing.Color.White;
            this.cmbVista.DisplayMember = "DESCRIPCION";
            this.cmbVista.Location = new System.Drawing.Point(-130, -20);
            this.cmbVista.Size = new System.Drawing.Size(130, 23);
            this.cmbVista.TabIndex = 2;
            this.cmbVista.ValueMember = "ID";
            this.cmbVista.Visible = false;
            // 
            // lblMensajes
            // 
            this.lblMensajes.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.lblMensajes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMensajes.Size = new System.Drawing.Size(1000, 20);
            // 
            // BarraProgreso
            // 
            this.BarraProgreso.Location = new System.Drawing.Point(840, 0);
            this.BarraProgreso.Margin = new System.Windows.Forms.Padding(4);
            this.BarraProgreso.Size = new System.Drawing.Size(160, 20);
            // 
            // GrdGridConsulta1
            // 
            this.GrdGridConsulta1.AllowUserToAddRows = false;
            this.GrdGridConsulta1.AllowUserToDeleteRows = false;
            this.GrdGridConsulta1.AllowUserToResizeColumns = false;
            this.GrdGridConsulta1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            this.GrdGridConsulta1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.GrdGridConsulta1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GrdGridConsulta1.BackgroundColor = System.Drawing.Color.White;
            this.GrdGridConsulta1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GrdGridConsulta1.CausesValidation = false;
            this.GrdGridConsulta1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.GrdGridConsulta1.ColumnaClaveIdentidad = "ipp_004";
            this.GrdGridConsulta1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GrdGridConsulta1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.GrdGridConsulta1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrdGridConsulta1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ipp_004,
            this.ejeord,
            this.fac_001,
            this.fee_001,
            this.nif_001,
            this.razsoc,
            this.impliq});
            this.GrdGridConsulta1.EnableHeadersVisualStyles = false;
            this.GrdGridConsulta1.Expandible = false;
            this.GrdGridConsulta1.GridColor = System.Drawing.Color.LightGray;
            this.GrdGridConsulta1.Location = new System.Drawing.Point(5, 110);
            this.GrdGridConsulta1.LongitudMinima = 300;
            this.GrdGridConsulta1.Margin = new System.Windows.Forms.Padding(5);
            this.GrdGridConsulta1.MostrarRegSeleccionados = false;
            this.GrdGridConsulta1.MultiSelect = false;
            this.GrdGridConsulta1.Name = "GrdGridConsulta1";
            this.GrdGridConsulta1.ReadOnly = true;
            this.GrdGridConsulta1.RowHeadersVisible = false;
            this.GrdGridConsulta1.RowHeadersWidth = 20;
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Calibri", 10F);
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(133)))), ((int)(((byte)(255)))));
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LemonChiffon;
            this.GrdGridConsulta1.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.GrdGridConsulta1.RowTemplate.Height = 20;
            this.GrdGridConsulta1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.GrdGridConsulta1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GrdGridConsulta1.Size = new System.Drawing.Size(990, 300);
            this.GrdGridConsulta1.StandardTab = true;
            this.GrdGridConsulta1.TabIndex = 0;
            this.GrdGridConsulta1.TabStop = false;
            this.GrdGridConsulta1.SelectionChanged += new System.EventHandler(this.GrdGridConsulta1_SelectionChanged);
            // 
            // Label_1_2
            // 
            this.Label_1_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_1_2.BackColor = System.Drawing.Color.Transparent;
            this.Label_1_2.CausesValidation = false;
            this.Label_1_2.Depth = 0;
            this.Label_1_2.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label_1_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_1_2.FormatoCorporativo = false;
            this.Label_1_2.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Label_1_2.Location = new System.Drawing.Point(905, 90);
            this.Label_1_2.Margin = new System.Windows.Forms.Padding(0);
            this.Label_1_2.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_1_2.Name = "Label_1_2";
            this.Label_1_2.Size = new System.Drawing.Size(90, 15);
            this.Label_1_2.TabIndex = 1;
            this.Label_1_2.Text = "-";
            this.Label_1_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label_1_1
            // 
            this.Label_1_1.AutoSize = true;
            this.Label_1_1.BackColor = System.Drawing.Color.Transparent;
            this.Label_1_1.CausesValidation = false;
            this.Label_1_1.Depth = 0;
            this.Label_1_1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_1_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_1_1.FormatoCorporativo = false;
            this.Label_1_1.Location = new System.Drawing.Point(5, 90);
            this.Label_1_1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_1_1.Name = "Label_1_1";
            this.Label_1_1.Size = new System.Drawing.Size(206, 15);
            this.Label_1_1.TabIndex = 422;
            this.Label_1_1.Text = "DOCUMENTOS PENDIENTES DE PAGO";
            this.Label_1_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label_Info_1
            // 
            this.Label_Info_1.AutoSize = true;
            this.Label_Info_1.BackColor = System.Drawing.Color.Transparent;
            this.Label_Info_1.CausesValidation = false;
            this.Label_Info_1.Depth = 0;
            this.Label_Info_1.Font = new System.Drawing.Font("Calibri", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Info_1.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.Label_Info_1.FormatoCorporativo = false;
            this.Label_Info_1.Location = new System.Drawing.Point(270, 47);
            this.Label_Info_1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_Info_1.Name = "Label_Info_1";
            this.Label_Info_1.Size = new System.Drawing.Size(371, 23);
            this.Label_Info_1.TabIndex = 423;
            this.Label_Info_1.Text = "ESPERE MIENTRAS SE ACTUALIZAN LOS DATOS";
            this.Label_Info_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ipp_004
            // 
            this.ipp_004.DataPropertyName = "ipp_004";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.NullValue = null;
            this.ipp_004.DefaultCellStyle = dataGridViewCellStyle3;
            this.ipp_004.FillWeight = 80F;
            this.ipp_004.HeaderText = "Identificador";
            this.ipp_004.MaxInputLength = 6;
            this.ipp_004.MinimumWidth = 2;
            this.ipp_004.Name = "ipp_004";
            this.ipp_004.ReadOnly = true;
            this.ipp_004.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ipp_004.Visible = false;
            this.ipp_004.Width = 80;
            // 
            // ejeord
            // 
            this.ejeord.DataPropertyName = "ejeord";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ejeord.DefaultCellStyle = dataGridViewCellStyle4;
            this.ejeord.FillWeight = 90F;
            this.ejeord.HeaderText = "Año-Orden";
            this.ejeord.MaxInputLength = 11;
            this.ejeord.Name = "ejeord";
            this.ejeord.ReadOnly = true;
            this.ejeord.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ejeord.Width = 90;
            // 
            // fac_001
            // 
            this.fac_001.DataPropertyName = "fac_001";
            this.fac_001.FillWeight = 150F;
            this.fac_001.HeaderText = "Número factura";
            this.fac_001.MaxInputLength = 20;
            this.fac_001.Name = "fac_001";
            this.fac_001.ReadOnly = true;
            this.fac_001.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.fac_001.Width = 150;
            // 
            // fee_001
            // 
            this.fee_001.DataPropertyName = "fee_001";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Format = "d";
            dataGridViewCellStyle5.NullValue = null;
            this.fee_001.DefaultCellStyle = dataGridViewCellStyle5;
            this.fee_001.FillWeight = 90F;
            this.fee_001.HeaderText = "F. registro";
            this.fee_001.MaxInputLength = 10;
            this.fee_001.Name = "fee_001";
            this.fee_001.ReadOnly = true;
            this.fee_001.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.fee_001.Width = 90;
            // 
            // nif_001
            // 
            this.nif_001.DataPropertyName = "nif_001";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.nif_001.DefaultCellStyle = dataGridViewCellStyle6;
            this.nif_001.FillWeight = 90F;
            this.nif_001.HeaderText = "N.I.F.";
            this.nif_001.MaxInputLength = 9;
            this.nif_001.Name = "nif_001";
            this.nif_001.ReadOnly = true;
            this.nif_001.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.nif_001.Width = 90;
            // 
            // razsoc
            // 
            this.razsoc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.razsoc.DataPropertyName = "razsoc";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.NullValue = null;
            this.razsoc.DefaultCellStyle = dataGridViewCellStyle7;
            this.razsoc.HeaderText = "Razón Social";
            this.razsoc.MaxInputLength = 200;
            this.razsoc.Name = "razsoc";
            this.razsoc.ReadOnly = true;
            this.razsoc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // impliq
            // 
            this.impliq.DataPropertyName = "impliq";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            dataGridViewCellStyle8.NullValue = "-";
            this.impliq.DefaultCellStyle = dataGridViewCellStyle8;
            this.impliq.HeaderText = "Total a pagar";
            this.impliq.MaxInputLength = 18;
            this.impliq.Name = "impliq";
            this.impliq.ReadOnly = true;
            // 
            // Relpag_001
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1000, 430);
            this.Controls.Add(this.Label_Info_1);
            this.Controls.Add(this.Label_1_1);
            this.Controls.Add(this.Label_1_2);
            this.Controls.Add(this.GrdGridConsulta1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Relpag_001";
            this.Text = "Actualización de datos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Relpag_001_FormClosing);
            this.Load += new System.EventHandler(this.Relpag_001_Load);
            this.Controls.SetChildIndex(this.cmbVista, 0);
            this.Controls.SetChildIndex(this.imgIntsa, 0);
            this.Controls.SetChildIndex(this.GrdGridConsulta1, 0);
            this.Controls.SetChildIndex(this.Label_1_2, 0);
            this.Controls.SetChildIndex(this.Label_1_1, 0);
            this.Controls.SetChildIndex(this.Label_Info_1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.imgIntsa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdGridConsulta1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_1_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label_Info_1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ClaseIntsa.Controles.grdGridConsulta GrdGridConsulta1;
        private ClaseIntsa.Controles.lblLabelGeneral Label_1_2;
        private ClaseIntsa.Controles.lblLabelGeneral Label_1_1;
        private ClaseIntsa.Controles.lblLabelGeneral Label_Info_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ipp_004;
        private System.Windows.Forms.DataGridViewTextBoxColumn ejeord;
        private System.Windows.Forms.DataGridViewTextBoxColumn fac_001;
        private System.Windows.Forms.DataGridViewTextBoxColumn fee_001;
        private System.Windows.Forms.DataGridViewTextBoxColumn nif_001;
        private System.Windows.Forms.DataGridViewTextBoxColumn razsoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn impliq;
    }
}
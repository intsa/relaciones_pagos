﻿using ClaseIntsa.Propiedades;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Relaciones_Pagos
{
    public static class Control_Secuencias
    {
        #region Control de secuencias SQL

        /// <summary>
        /// Verifica si existe una secuencia en 'SQL'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Nombre_Bdd">Nombre de la base de datos</param>
        /// <param name="Secuencia">Nombre de la secuencia</param>
        /// <param name="Descripcion">Descripción de la secuencia</param>
        /// <param name="Valor_Inicial">Valor inicial </param>
        /// <param name="Incremento">Incremento</param>
        /// <param name="Valor_Minimo">Valor mínimo</param>
        /// <param name="Valor_Maximo">Valor máximo</param>
        public static void Existe_Secuencia(string CadenaConexion, string Nombre_Bdd, string Secuencia, string Descripcion, int Valor_Inicial = 1, int Incremento = 1, int Valor_Minimo = 1, int Valor_Maximo = 2147483647)
        {
            bool Existe_Secuencia;
            string Cadena_Sql;

            string Cadena_Use = "USE " + Nombre_Bdd;
            string Cadena_Select;
            string Cadena_From;
            string Cadena_Where;

            string Restriccion_Objeto = $"object_id = OBJECT_ID('dbo.{Secuencia.Trim()}')";
            string Restriccion_Tipo = " type = 'SO'";

            Cadena_Where = " WHERE " + Restriccion_Objeto + " and " + Restriccion_Tipo;

            Cadena_Select = " SELECT name";
            Cadena_From = " FROM sys.objects";
            Cadena_Sql = Cadena_Use + Cadena_Select + Cadena_From + Cadena_Where;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    Existe_Secuencia = Data_Reader.HasRows;
                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    if (!Existe_Secuencia)
                        Crear_Secuencia(CadenaConexion, Nombre_Bdd, Secuencia, Descripcion, Valor_Inicial, Incremento, Valor_Minimo, Valor_Maximo);

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Creación una secuencia
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Nombre_Bdd">Nombre de la base de datos</param>
        /// <param name="Secuencia">Nombre de la secuencia</param>
        /// <param name="Descripcion">Descripción de la secuencia</param>
        /// <param name="Valor_Inicial">Valor inicial </param>
        /// <param name="Incremento">Incremento</param>
        /// <param name="Valor_Minimo">Valor mínimo</param>
        /// <param name="Valor_Maximo">Valor máximo</param>
        public static void Crear_Secuencia(string CadenaConexion, string Nombre_Bdd, string Secuencia, string Descripcion, int Valor_Inicial = 1, int Incremento = 1, int Valor_Minimo = 1, int Valor_Maximo = 2147483647)
        {
            string Cadena_Sql;

            string Cadena_Use = "USE " + Nombre_Bdd;
            string Cadena_Create = $" CREATE SEQUENCE dbo.{Secuencia} as int";
            string Cadena_Comienzo = $" START WITH {Valor_Inicial}";
            string Cadena_Incremento = $" INCREMENT BY {Incremento}";
            string Cadena_Minimo = $" MINVALUE {Valor_Minimo}";
            string Cadena_Maximo = $" MAXVALUE {Valor_Maximo}";
            string Cadena_Cache = " NO CACHE";
            string Cadena_Descripcion = $" EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'{Descripcion.Trim()}' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'SEQUENCE', @level1name = N'{Secuencia}'";

            Cadena_Sql = Cadena_Use + Cadena_Create + Cadena_Comienzo + Cadena_Incremento + Cadena_Minimo + Cadena_Maximo + Cadena_Cache + Cadena_Descripcion;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    Comando_Sql.Connection = Cadena_Conexion;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Comando_Sql.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Creación una secuencia
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Nombre_Bdd">Nombre de la base de datos</param>
        /// <param name="Secuencia">Nombre de la secuencia</param>
        /// <param name="Valor_Actual">Valor a inicializar</param>
        public static void Inicializar_Secuencia(string CadenaConexion, string Nombre_Bdd, string Secuencia, int Valor_Actual = 1)
        {
            string Cadena_Sql;

            string Cadena_Use = "USE " + Nombre_Bdd;
            string Cadena_Inicializar = $" ALTER SEQUENCE {Secuencia} RESTART WITH {Valor_Actual}";

            Cadena_Sql = Cadena_Use + Cadena_Inicializar;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    Comando_Sql.Connection = Cadena_Conexion;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Comando_Sql.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion Control de secuencias SQL

        //
    }
    //
}
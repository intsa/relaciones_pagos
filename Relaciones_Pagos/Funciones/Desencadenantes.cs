﻿using ClaseIntsa.Propiedades;
using ClaseIntsa.Funciones;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace Relaciones_Pagos
{
    public static class Desencadenantes_Importacion
    {
        #region Tabla Tabla Relpag_0001 ('DOCUMENTOS PENDIENTES DE PAGO')

        /// <summary>
        /// Actualiza registros tabla DOCUMENTOS PENDIENTES DE PAGO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Registro">DataRow de la tabla de Visualfoxpro</param>
        public static void Insertar_Relpag_0001(string CadenaConexion, int Id_0001, DataRow Registro)
        {
            string[] Referencia_Seguex = new string[4];
            string[] Datos_Bancarios;
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0001 Temporal = new Relpag_0001(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Datos_Bancarios = Registro["datban"].ToString().Split('·');

                    Referencia_Seguex[0] = Registro["ejs_001"].ToString().Trim();
                    Referencia_Seguex[1] = Registro["res_001"].ToString().Trim();
                    Referencia_Seguex[2] = Registro["nes_001"].ToString().Trim();
                    Referencia_Seguex[3] = Registro["nss_001"].ToString().Trim();

                    Temporal.ide_0001 = Id_0001;
                    Temporal.ian_0001 =Registro["ipp_004"].ToString().Trim();
                    Temporal.ifa_0001 =Registro["ifa_001"].ToString().Trim();
                    Temporal.eje_0001 =Registro["eje_001"].ToString().Trim();
                    Temporal.lfr_0001 =Registro["cfa_001"].ToString().Trim();
                    Temporal.ord_0001 =Registro["num_001"].ToString().Trim();
                    Temporal.nfa_0001 =Registro["fac_001"].ToString().Trim();
                    Temporal.fee_0001 = Convert.ToDateTime(Registro["fee_001"]).ToString("dd/MM/yyyy");
                    Temporal.nif_0001 = Registro["nif_001"].ToString().Trim();
                    Temporal.ape_0001 =Registro["ape_001"].ToString().Trim();
                    Temporal.nom_0001 =Registro["nom_001"].ToString().Trim();
                    Temporal.iba_0001 = Datos_Bancarios[0];
                    Temporal.bic_0001 = Datos_Bancarios[1];
                    Temporal.bas_0001 = Convert.ToDecimal(Registro["bas_004"]);
                    Temporal.imp_0001 = Convert.ToDecimal(Registro["imp_004"]);
                    Temporal.bre_0001 = Convert.ToDecimal(Registro["iba_004"]);
                    Temporal.ret_0001 = Convert.ToDecimal(Registro["irp_004"]);

                    if (Convert.ToDateTime(Registro["acf_001"]).ToString("dd/MM/yyyy") == "30/12/1899")
                        Temporal.acf_0001 = null;
                    else
                        Temporal.acf_0001 = Convert.ToDateTime(Registro["acf_001"]).ToString("dd/MM/yyyy");

                    Temporal.sac_0001 = Registro["sac_001"].ToString().Trim();
                    Temporal.pap_0001 = Registro["pap_001"].ToString().Trim();
                    Temporal.tif_0001 = Registro["tif_001"].ToString().Trim();
                    Temporal.sco_0001 = Registro["sco_004"].ToString().Trim();
                    Temporal.res_0001 = Funciones_Genericas._Normalizar_Cadenas(Referencia_Seguex, '-');
                    Temporal.nre_0001 = Registro["nre_001"].ToString().Trim();
                    Temporal.con_0001 = Registro["nas_001"].ToString().Trim();
                    Temporal.eco_0001 = Registro["eje_004"].ToString().Trim();
                    Temporal.exp_0001 = Registro["exp_004"].ToString().Trim();
                    Temporal.doc_0001 = Registro["doc_004"].ToString().Trim();
                    Temporal.tvi_0001 = Registro["sig_009"].ToString().Trim();
                    Temporal.nvi_0001 = Registro["dom_009"].ToString().Trim();
                    Temporal.n01_0001 = Registro["nu1_009"].ToString().Trim();
                    Temporal.l01_0001 = Registro["le1_009"].ToString().Trim();
                    Temporal.n02_0001 = Registro["nu2_009"].ToString().Trim();
                    Temporal.l02_0001 = Registro["le2_009"].ToString().Trim();

                    if (Registro["kms_009"].ToString().Trim() == string.Empty
                        ||
                        Registro["kms_009"].ToString().Trim() == "0,00")
                        Temporal.kms_0001 = null;
                    else
                        Temporal.kms_0001 = Convert.ToDecimal(Registro["kms_009"].ToString().Trim().Replace('.', ','));

                    Temporal.por_0001 = Registro["por_009"].ToString().Trim();
                    Temporal.esc_0001 = Registro["esc_009"].ToString().Trim();
                    Temporal.pla_0001 = Registro["pla_009"].ToString().Trim();
                    Temporal.pue_0001 = Registro["pue_009"].ToString().Trim();
                    Temporal.loc_0001 = Registro["loc_009"].ToString().Trim();
                    Temporal.cpo_0001 = Registro["cop_009"].ToString().Trim();
                    Temporal.mun_0001 = Registro["pob_009"].ToString().Trim();
                    Temporal.pro_0001 = Registro["pro_009"].ToString().Trim();
                    Temporal.npa_0001 = Registro["pai_009"].ToString().Trim();
                    Temporal.sdr_0001 = 0;
                    Temporal.uar_0001 = Variables_Globales.IdUsuario;
                    Temporal.far_0001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_0001 = Variables_Globales.IpPrivada;
                    Temporal.uua_0001 = Variables_Globales.IdUsuario;
                    Temporal.fua_0001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_0001 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'DOCUMENTOS PENDIENTES DE PAGO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0001">Identificador registro único 'DOCUMENTOS PENDIENTES DE PAGO'</param>
        /// <param name="Registro">DataRow de la tabla de Visualfoxpro</param>
        public static void Actualizar_Relpag_0001(string CadenaConexion, int Id_0001, DataRow Registro)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0001 Temporal = new Relpag_0001(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();
                    Temporal.ide_0001 = Id_0001;

                    Temporal.ian_0001 = (string)Registro["ipp_004"];
                    Temporal.ifa_0001 = (string)Registro["ifa_001"];
                    Temporal.eje_0001 = (string)Registro["eje_001"];
                    Temporal.lfr_0001 = (string)Registro["cfa_001"];
                    Temporal.ord_0001 = (string)Registro["num_001"];
                    Temporal.nfa_0001 = (string)Registro["fac_001"];
                    Temporal.fee_0001 = Convert.ToDateTime(Registro["fee_001"]).ToString("dd/MM/yyyy");
                    Temporal.nif_0001 = (string)Registro["nif_001"];
                    Temporal.ape_0001 = (string)Registro["ape_001"];
                    Temporal.nom_0001 = (string)Registro["nom_001"];
                    Temporal.iba_0001 = (string)Registro["iba_001_1"];
                    Temporal.bic_0001 = (string)Registro["bic_001_1"];
                    Temporal.bas_0001 = Convert.ToDecimal(Registro["bas_004"]);
                    Temporal.imp_0001 = Convert.ToDecimal(Registro["imp_004"]);
                    Temporal.bre_0001 = Convert.ToDecimal(Registro["iba_004"]);
                    Temporal.ret_0001 = Convert.ToDecimal(Registro["irp_004"]);

                    if (Convert.ToDateTime(Registro["acf_001"]).ToString("dd/MM/yyyy") == "30/12/1899")
                        Temporal.acf_0001 = null;
                    else
                        Temporal.acf_0001 = Convert.ToDateTime(Registro["acf_001"]).ToString("dd/MM/yyyy");

                    Temporal.sac_0001 = (string)Registro["sac_001"];
                    Temporal.pap_0001 = (string)Registro["pap_001"];
                    Temporal.tif_0001 = (string)Registro["tif_001"];
                    Temporal.sco_0001 = (string)Registro["sco_004"];
                    Temporal.eco_0001 = (string)Registro["eje_004"];
                    Temporal.sdr_0001 = 0;
                    Temporal.dip_0001 = Variables_Globales.IpPrivada;
                    Temporal.uua_0001 = Variables_Globales.IdUsuario;
                    Temporal.fua_0001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.Actualizar();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Elimina los documentos no incluidos en relacíones tabla 'DOCUMENTOS PENDIENTES DE PAGO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        public static void Eliminar_Relpag_0001(string CadenaConexion, byte Situacion = 0)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0001_Extras Temporal = new Relpag_0001_Extras(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();
                    Temporal.sdr_0001 = Situacion;

                    Temporal.Eliminar_Extras();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Relpag_0001 ('DOCUMENTOS DE PAGO')

        #region Tabla Relpag_1000 ('CANALES DE PAGO')

        /// <summary>
        /// Actualiza registros tabla 'CANALES DE PAGO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Registro">DataRow de la tabla de Visualfoxpro</param>
        public static void Insertar_Relpag_1000(string CadenaConexion, DataRow Registro)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_1000 Temporal = new Relpag_1000(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.cod_1000 = (string)Registro["can_100"];
                    Temporal.den_1000 = (string)Registro["den_100"];
                    Temporal.iba_1000 = (string)Registro["iba_100"];
                    Temporal.bic_1000 = (string)Registro["bic_100"];
                    Temporal.cta_1000 = (string)Registro["cta_100"];
                    Temporal.sor_1000 = (string)Registro["sor_100"];
                    Temporal.ior_1000 = (string)Registro["ior_100"];
                    Temporal.uar_1000 = Variables_Globales.IdUsuario;
                    Temporal.far_1000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_1000 = Variables_Globales.IpPrivada;
                    Temporal.uua_1000 = Variables_Globales.IdUsuario;
                    Temporal.fua_1000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_1000 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla  'CANALES DE PAGO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_1000">Identificador registro 'CANALES DE PAGO''</param>
        /// <param name="Registro">DataRow de la tabla de Visualfoxpro</param>
        public static void Actualizar_Relpag_1000(string CadenaConexion, int Id_1000, DataRow Registro)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_1000 Temporal = new Relpag_1000(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.ide_1000 = Id_1000;

                    Temporal.cod_1000 = (string)Registro["can_100"];
                    Temporal.den_1000 = (string)Registro["den_100"];
                    Temporal.iba_1000 = (string)Registro["iba_100"];
                    Temporal.bic_1000 = (string)Registro["bic_100"];
                    Temporal.cta_1000 = (string)Registro["cta_100"];
                    Temporal.sor_1000 = (string)Registro["sor_100"];
                    Temporal.ior_1000 = (string)Registro["ior_100"];
                    Temporal.dip_1000 = Variables_Globales.IpPrivada;
                    Temporal.uua_1000 = Variables_Globales.IdUsuario;
                    Temporal.fua_1000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Relpag_1000 (''CANALES DE PAGO')

        #region Tabla Relpag_1005 ('FORMAS DE PAGO')

        /// <summary>
        /// Actualiza registros tabla 'FORMAS DE PAGO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Registro">DataRow de la tabla de Visualfoxpro</param>
        public static void Insertar_Relpag_1005(string CadenaConexion, DataRow Registro)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_1005 Temporal = new Relpag_1005(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.cod_1005 = (string)Registro["cod_500"];
                    Temporal.den_1005 = (string)Registro["den_500"];
                    Temporal.cns_1005 = (string)Registro["cns_500"];
                    Temporal.uar_1005 = Variables_Globales.IdUsuario;
                    Temporal.far_1005 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_1005 = Variables_Globales.IpPrivada;
                    Temporal.uua_1005 = Variables_Globales.IdUsuario;
                    Temporal.fua_1005 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_1005 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla  'FORMAS DE PAGO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_1005">Identificador registro 'FORMAS DE PAGO''</param>
        /// <param name="Registro">DataRow de la tabla de Visualfoxpro</param>
        public static void Actualizar_Relpag_1005(string CadenaConexion, int Id_1005, DataRow Registro)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_1005 Temporal = new Relpag_1005(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.ide_1005 = Id_1005;

                    Temporal.cod_1005 = (string)Registro["cod_500"];
                    Temporal.den_1005 = (string)Registro["den_500"];
                    Temporal.cns_1005 = (string)Registro["cns_500"];
                    Temporal.dip_1005 = Variables_Globales.IpPrivada;
                    Temporal.uua_1005 = Variables_Globales.IdUsuario;
                    Temporal.fua_1005 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Relpag_1000 (''FORMAS DE PAGO')

    }

    public static class Desencadenantes
    {

        #region Tabla Tabla Relpag_0001 ('DOCUMENTOS PENDIENTES DE PAGO')

        /// <summary>
        /// Actualiza registros tabla 'DOCUMENTOS PENDIENTES DE PAGO'
        /// Documento incluído en relación
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0001">Identificador registro único 'DOCUMENTOS PENDIENTES DE PAGO'</param>
        /// <param name="Registro">DataRow de la tabla de Visualfoxpro</param>
        public static void Actualizar_Relpag_0001(string CadenaConexion, int Id_0001, byte Incluida)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0001_Extras Temporal = new Relpag_0001_Extras(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();
                    Temporal.ide_0001 = Id_0001;

                    Temporal.sdr_0001 = Incluida;
                    Temporal.dip_0001 = Variables_Globales.IpPrivada;
                    Temporal.uua_0001 = Variables_Globales.IdUsuario;
                    Temporal.fua_0001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar_Extras();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'DOCUMENTOS PENDIENTES DE PAGO'
        /// I.B.A.N.
        /// B.I.C./S.W.I.F.T.
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0001">Identificador registro único 'DOCUMENTOS PENDIENTES DE PAGO'</param>
        /// <param name="Registro">DataRow de la tabla de Visualfoxpro</param>
        public static void Actualizar_Relpag_0001(string CadenaConexion, int Id_0001, string Iban, string Bic_Swift)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0001_Extras Temporal = new Relpag_0001_Extras(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();
                    Temporal.ide_0001 = Id_0001;

                    Temporal.iba_0001 = Iban;
                    Temporal.bic_0001 = Bic_Swift;
                    Temporal.dip_0001 = Variables_Globales.IpPrivada;
                    Temporal.uua_0001 = Variables_Globales.IdUsuario;
                    Temporal.fua_0001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar_Datos_Bancarios_Extras();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Relpag_0001 ('DOCUMENTOS DE PAGO')

        #region Tabla Relpag_0010 ('RELACIONES DE PAGOS. MODELO-997')

        /// <summary>
        /// Inserta registros tabla 'RELACIONES DE PAGOS. MODELO-997'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Ejercicio">Año de la relación</param>
        /// <param name="Numero_Relacion">Número relación</param>
        /// <param name="Descripcion">Descripción relación</param>
        /// <param name="Fecha_Relacion">Fecha relación</param>
        /// <param name="Fecha_Incorporacion">Fecha de incorporación de datos</param>
        /// <param name="Fecha_Caducidad">Fecha caducidad de pago</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Relpag_0010(string CadenaConexion, short Ejercicio, int Numero_Relacion, string Descripcion, string Fecha_Relacion, string Fecha_Incorporacion, string Fecha_Caducidad)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0010 Temporal = new Relpag_0010(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.eje_0010 = Ejercicio;
                    Temporal.rel_0010 = Numero_Relacion;
                    Temporal.des_0010 = Descripcion;
                    Temporal.fec_0010 = Fecha_Relacion;
                    Temporal.fei_0010 = Fecha_Incorporacion;
                    Temporal.fcp_0010 = Fecha_Caducidad;
                    Temporal.fgf_0010 = null;
                    Temporal.nre_0010 = 0;
                    Temporal.tot_0010 = 0;
                    Temporal.uar_0010 = Variables_Globales.IdUsuario;
                    Temporal.far_0010 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_0010 = Variables_Globales.IpPrivada;
                    Temporal.uua_0010 = Variables_Globales.IdUsuario;
                    Temporal.fua_0010 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_0010 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_0010;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'RELACIONES DE PAGOS. MODELO-997'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0010">Identificador registro único 'RELACIONES DE PAGOS. MODELO-997'</param>
        /// <param name="Descripcion">Descripción relación</param>
        /// <param name="Fecha_Relacion">Fecha relación</param>
        /// <param name="Fecha_Caducidad">Fecha caducidad de pago</param>  
        public static void Actualizar_Relpag_0010(string CadenaConexion, int Id_0010, string Descripcion, string Fecha_Relacion, string Fecha_Caducidad)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0010_Extras Temporal = new Relpag_0010_Extras(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.ide_0010 = Id_0010;

                    Temporal.des_0010 = Descripcion;
                    Temporal.fec_0010 = Fecha_Relacion;
                    Temporal.fcp_0010 = Fecha_Caducidad;
                    Temporal.dip_0010 = Variables_Globales.IpPrivada;
                    Temporal.uua_0010 = Variables_Globales.IdUsuario;
                    Temporal.fua_0010 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar_Extras();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'RELACIONES DE PAGOS. MODELO-997'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0010">Identificador registro único 'RELACIONES DE PAGOS. MODELO-997'</param>
        /// <param name="Importe">Importe de un documento</param>
        /// <param name="Actualiza">Semáforo actualización/desactulización</param>  
        public static void Actualizar_Relpag_0010(string CadenaConexion, int Id_0010, decimal Importe, sbyte Actualiza = 1)
        {
            short Documentos = Actualiza;
            decimal Importe_Actualizacion = Importe * Actualiza;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0010_Extras Temporal = new Relpag_0010_Extras(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();
                    Temporal.ide_0010 = Id_0010;

                    Temporal.Actualizar_Extras(Documentos, Importe_Actualizacion);
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'RELACIONES DE PAGOS. MODELO-997'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0010">Identificador registro único 'RELACIONES DE PAGOS. MODELO-997'</param>
        /// <param name="Documentos">Número de documentos de la relación</param>
        /// <param name="Total_Importe">Importe total de la relación</param>  
        public static void Actualizar_Relpag_0010(string CadenaConexion, int Id_0010, short Documentos, decimal Total_Importe)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0010_Extras Temporal = new Relpag_0010_Extras(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();
                    Temporal.ide_0010 = Id_0010;
                    Temporal.nre_0010 = Documentos;
                    Temporal.tot_0010 = Total_Importe;

                    Temporal.Actualizar_Totales_Extras();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'RELACIONES DE PAGOS. MODELO-997'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0010">Identificador registro único 'RELACIONES DE PAGOS. MODELO-997'</param>
        /// <param name="Modelo_997">Contenido del fichero del modelo-997</param>
        /// <param name="Fecha_Generacion">Fecha generación del modelo-997</param>  
        /// <param name="Situacion">Situación de la relación</param>  
        public static void Actualizar_Relpag_0010(string CadenaConexion, int Id_0010, string Modelo_997, string Fecha_Generacion, byte Situacion)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0010_Extras Temporal = new Relpag_0010_Extras(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();
                    Temporal.ide_0010 = Id_0010;

                    Temporal.fgf_0010 = Fecha_Generacion;
                    Temporal.sre_0010 = Situacion;
                    Temporal.mod_0010 = Modelo_997;

                    Temporal.Actualizar_Situacion_Extras();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Relpag_8000 ('PARÁMETROS GENERALES. EJERCICIO DE TRABAJO')

        #region Tabla Relpag_0010_1 ('RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS')

        /// <summary>
        /// Inserta registros tabla 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0010">Identificador registro 'RELACIONES DE PAGOS. MODELO-997'</param>
        /// <param name="Id_0001">Identificador registro 'DOCUMENTOS PENDIENTES DE PAGO</param>
        /// <param name="Observaciones">Observaciones</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Relpag_0010_1(string CadenaConexion, int Id_0010, int Id_0001, string Observaciones)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0010_1 Temporal = new Relpag_0010_1(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.ide_0010 = Id_0010;
                    Temporal.ide_0001 = Id_0001;
                    Temporal.obs_0010_1 = Observaciones;
                    Temporal.uar_0010_1 = Variables_Globales.IdUsuario;
                    Temporal.far_0010_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_0010_1 = Variables_Globales.IpPrivada;
                    Temporal.uua_0010_1 = Variables_Globales.IdUsuario;
                    Temporal.fua_0010_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_0010_1 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_0010_1;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0010_1">Identificador registro 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'</param>
        /// <param name="Observaciones">Observaciones</param>
        public static void Actualizar_Relpag_0010_1(string CadenaConexion, int Id_0010_1, string Observaciones)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0010_1_Extras Temporal = new Relpag_0010_1_Extras(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.ide_0010_1 = Id_0010_1;
                    Temporal.obs_0010_1 = Observaciones;
                    Temporal.dip_0010_1 = Variables_Globales.IpPrivada;
                    Temporal.uua_0010_1 = Variables_Globales.IdUsuario;
                    Temporal.fua_0010_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar_Extras();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0010_1">Identificador registro 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'</param>
        /// <param name="Situacion">Situación del documento</param>
        public static void Actualizar_Relpag_0010_1(string CadenaConexion, int Id_0010_1, byte Situacion)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0010_1_Extras Temporal = new Relpag_0010_1_Extras(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.ide_0010_1 = Id_0010_1;

                    Temporal.sdo_0010_1 = Situacion;
                    Temporal.dip_0010_1 = Variables_Globales.IpPrivada;
                    Temporal.uua_0010_1 = Variables_Globales.IdUsuario;
                    Temporal.fua_0010_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar_Situacion_Extras();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Elimina registros tabla 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0010_1">Identificador registro 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'</param>
        /// <returns>Identificador registro</returns>
        public static void Eliminar_Relpag_0010_1(string CadenaConexion, int Id_0010_1)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0010_1 Temporal = new Relpag_0010_1(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.ide_0010_1 = Id_0010_1;

                    Temporal.Eliminar();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Relpag_0010_1 ('RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS')

        #region Tabla Relpag_0020 ('RELACIONES DE PAGOS. DEFINITIVAS')

        /// <summary>
        /// Inserta registros tabla 'RELACIONES DE PAGOS. DEFINITIVAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0010">Identificador registro 'RELACIONES DE PAGOS. MODELO-997'</param>
        /// <param name="Ejercicio">Año de la relación</param>
        /// <param name="Numero_Relacion">Número relación</param>
        /// <param name="Descripcion">Descripción relación</param>
        /// <param name="Fecha_Relacion">Fecha relación</param>
        /// <param name="Id_1000">Identificador registro 'CANALES DE PAGO'</param>
        /// <param name="Iban_Ordenante">I.B.A.N. ordenante</param>
        /// <param name="Bic_Ordenante">B.I.C.-S.W.I.F.T. ordenante</param>
        /// <param name="Cuenta_Contable">Cuenta contable 'CANAL DE PAGO'</param>
        /// <param name="Sufijo_Ordenante">Sufijo ordenante</param>
        /// <param name="Id_Ordenante">Identificador ordenante</param>
        /// <param name="Id_1005">Identificador registro 'FORMAS DE PAGO'</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Relpag_0020(string CadenaConexion, int Id_0010, short Ejercicio, int Numero_Relacion, string Descripcion, string Fecha_Relacion, int Id_1000, string Iban_Ordenante,
                                                                               string Bic_Ordenante, string Cuenta_Contable, string Sufijo_Ordenante, string Id_Ordenante, int Id_1005)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0020 Temporal = new Relpag_0020(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.ide_0010 = Id_0010;
                    Temporal.eje_0020 = Ejercicio;
                    Temporal.rel_0020 = Numero_Relacion;
                    Temporal.des_0020 = Descripcion;
                    Temporal.fec_0020 = Fecha_Relacion;
                    Temporal.ide_1000 = Id_1000;
                    Temporal.iba_0020 = Iban_Ordenante;
                    Temporal.bic_0020 = Bic_Ordenante;
                    Temporal.cta_0020 = Cuenta_Contable;
                    Temporal.sor_0020 = Sufijo_Ordenante;
                    Temporal.ior_0020 = Id_Ordenante;
                    Temporal.ide_1005 = Id_1005;
                    Temporal.fgs_0020 = null;
                    Temporal.fco_0020 = null;
                    Temporal.nre_0020 = 0;
                    Temporal.tot_0020 = 0;
                    Temporal.sre_0020 = 0;
                    Temporal.uar_0020 = Variables_Globales.IdUsuario;
                    Temporal.far_0020 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_0020 = Variables_Globales.IpPrivada;
                    Temporal.uua_0020 = Variables_Globales.IdUsuario;
                    Temporal.fua_0020 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_0020 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_0020;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'RELACIONES DE PAGOS. DEFINITIVAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0020">Identificador registro único 'RELACIONES DE PAGOS. DEFINITIVAS'</param>
        /// <param name="Descripcion">Descripción relación</param>
        /// <param name="Id_1000">Identificador registro 'CANALES DE PAGO'</param>
        /// <param name="Iban_Ordenante">I.B.A.N. ordenante</param>
        /// <param name="Bic_Ordenante">B.I.C.-S.W.I.F.T. ordenante</param>
        /// <param name="Cuenta_Contable">Cuenta contable 'CANAL DE PAGO'</param>
        /// <param name="Sufijo_Ordenante">Sufijo ordenante</param>
        /// <param name="Id_Ordenante">Identificador ordenante</param>
        public static void Actualizar_Relpag_0020(string CadenaConexion, int Id_0020, string Descripcion, int Id_1000, string Iban_Ordenante, string Bic_Ordenante, string Cuenta_Contable,
                                                                                      string Sufijo_Ordenante, string Id_Ordenante)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0020_Extras Temporal = new Relpag_0020_Extras(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.ide_0020 = Id_0020;

                    Temporal.des_0020 = Descripcion;
                    Temporal.ide_1000 = Id_1000;
                    Temporal.iba_0020 = Iban_Ordenante;
                    Temporal.bic_0020 = Bic_Ordenante;
                    Temporal.cta_0020 = Cuenta_Contable;
                    Temporal.sor_0020 = Sufijo_Ordenante;
                    Temporal.ior_0020 = Id_Ordenante;
                    Temporal.fgs_0020 = null;
                    Temporal.sre_0020 = 0;
                    Temporal.sep_0020 = null;
                    Temporal.dip_0020 = Variables_Globales.IpPrivada;
                    Temporal.uua_0020 = Variables_Globales.IdUsuario;
                    Temporal.fua_0020 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar_Extras();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'RELACIONES DE PAGOS. DEFINITIVAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0020">Identificador registro único 'RELACIONES DE PAGOS. DEFINITIVAS'</param>
        /// <param name="Importe">Importe de un documento</param>
        /// <param name="Actualiza">Semáforo actualización/desactulización</param>  
        public static void Actualizar_Relpag_0020(string CadenaConexion, int Id_0020, decimal Importe, sbyte Actualiza = 1)
        {
            short Documentos = Actualiza;
            decimal Importe_Actualizacion = Importe * Actualiza;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0020_Extras Temporal = new Relpag_0020_Extras(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();
                    Temporal.ide_0020 = Id_0020;

                    Temporal.Actualizar_Extras(Documentos, Importe_Actualizacion);
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'RELACIONES DE PAGOS. DEFINITIVAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0020">Identificador registro único 'RELACIONES DE PAGOS. DEFINITIVAS'</param>
        /// <param name="Documentos">Número de documentos de la relación</param>
        /// <param name="Total_Importe">Importe total de la relación</param>  
        public static void Actualizar_Relpag_0020(string CadenaConexion, int Id_0020, short Documentos, decimal Total_Importe)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0020_Extras Temporal = new Relpag_0020_Extras(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();
                    Temporal.ide_0020 = Id_0020;
                    Temporal.nre_0020 = Documentos;
                    Temporal.tot_0020 = Total_Importe;

                    Temporal.Actualizar_Totales_Extras();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'RELACIONES DE PAGOS. DEFINITIVAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0020">Identificador registro único 'RELACIONES DE PAGOS. DEFINITIVAS'</param>
        /// <param name="Sepa">Contenido del fichero SEPA de transferencias</param>
        /// <param name="Fecha_Generacion">Fecha generación del modelo-997</param>  
        /// <param name="Situacion">Situación de la relación</param>  
        public static void Actualizar_Relpag_0020(string CadenaConexion, int Id_0020, string Sepa, string Fecha_Generacion, byte Situacion)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0020_Extras Temporal = new Relpag_0020_Extras(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();
                    Temporal.ide_0020 = Id_0020;

                    Temporal.fgs_0020 = Fecha_Generacion;
                    Temporal.sre_0020 = Situacion;
                    Temporal.sep_0020 = Sepa;

                    Temporal.Actualizar_Situacion_Extras();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Elimina registros tabla 'RELACIONES DE PAGOS. DEFINITIVAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0020">Identificador registro único 'RELACIONES DE PAGOS. DEFINITIVAS'</param>
        public static void Eliminar_Relpag_0020(string CadenaConexion, int Id_0020)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0020 Temporal = new Relpag_0020(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();
                    Temporal.ide_0020 = Id_0020;

                    Temporal.Eliminar();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Relpag_0020 ('RELACIONES DE PAGOS. DEFINITIVAS')

        #region Tabla Relpag_0020_1 ('RELACIONES DE PAGOS. DEFINITIVAS. DOCUMENTOS')

        /// <summary>
        /// Inserta registros tabla 'RELACIONES DE PAGOS.DEFINITIVAS. DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0020">Identificador registro 'RELACIONES DE PAGOS. MODELO-997'</param>
        /// <param name="Id_0001">Identificador registro 'DOCUMENTOS PENDIENTES DE PAGO'</param>
        /// <param name="Id_0010_1">Identificador registro 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Relpag_0020_1(string CadenaConexion, int Id_0020, int Id_0001, int Id_0010_1)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0020_1 Temporal = new Relpag_0020_1(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.ide_0020 = Id_0020;
                    Temporal.ide_0001 = Id_0001;
                    Temporal.ide_0010_1 = Id_0010_1;
                    Temporal.sdo_0020_1 = 0;
                    Temporal.uar_0020_1 = Variables_Globales.IdUsuario;
                    Temporal.far_0020_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_0020_1 = Variables_Globales.IpPrivada;
                    Temporal.uua_0020_1 = Variables_Globales.IdUsuario;
                    Temporal.fua_0020_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_0020_1 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_0010_1;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'RELACIONES DE PAGOS. DEFINITIVAS. DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0020_1">Identificador registro 'RELACIONES DE PAGOS. DEFINITIVAS. DOCUMENTOS'</param>
        /// <param name="Situacion">Situación del documento</param>
        public static void Actualizar_Relpag_0020_1(string CadenaConexion, int Id_0020_1, byte Situacion)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0020_1_Extras Temporal = new Relpag_0020_1_Extras(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.ide_0020_1 = Id_0020_1;

                    Temporal.sdo_0020_1 = Situacion;
                    Temporal.dip_0020_1 = Variables_Globales.IpPrivada;
                    Temporal.uua_0020_1 = Variables_Globales.IdUsuario;
                    Temporal.fua_0020_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar_Situacion_Extras();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Elimina registros tabla 'RELACIONES DE PAGOS. DEFINITIVAS. DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_0020_1">Identificador registro 'RELACIONES DE PAGOS. DEFINITIVAS. DOCUMENTOS'</param>
        /// <returns>Identificador registro</returns>
        public static void Eliminar_Relpag_0020_1(string CadenaConexion, int Id_0020_1)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_0020_1 Temporal = new Relpag_0020_1(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.ide_0020_1 = Id_0020_1;

                    Temporal.Eliminar();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Relpag_0020_1 ('RELACIONES DE PAGOS. DEFINITIVAS. DOCUMENTOS')

        #region Tabla Relpag_8000 ('PARÁMETROS GENERALES. EJERCICIO DE TRABAJO')

        /// <summary>
        /// Actualiza registros tabla 'PARÁMETROS GENERALES. EJERCICIO DE TRABAJO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Id_1005">Identificador registro 'FORMAS DE PAGO'</param>
        public static void Insertar_Relpag_8000(string CadenaConexion, short Ejercicio, int Id_1005 = 0)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_8000 Temporal = new Relpag_8000(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.eje_8000 = Ejercicio;

                    if (Id_1005 == 0)
                        Temporal.ide_1005 = null;
                    else
                        Temporal.ide_1005 = Id_1005;

                    Temporal.uar_8000 = Variables_Globales.IdUsuario;
                    Temporal.far_8000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_8000 = Variables_Globales.IpPrivada;
                    Temporal.uua_8000 = Variables_Globales.IdUsuario;
                    Temporal.fua_8000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_8000 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'PARÁMETROS GENERALES. EJERCICIO DE TRABAJO'
        /// · ide_1005 Identificador registro 'FORMAS DE PAGO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_8000">Identificador registro único 'PARÁMETROS GENERALES. EJERCICIO DE TRABAJO'</param>
        /// <param name="Id_1005">Identificador registro 'FORMAS DE PAGO'</param>
        public static void Actualizar_Relpag_8000(string CadenaConexion, int Id_8000, int Id_1005 = 0)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_8000 Temporal = new Relpag_8000(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.ide_8000 = Id_8000;

                    if (Id_1005 == 0)
                        Temporal.ide_1005 = null;
                    else
                        Temporal.ide_1005 = Id_1005;

                    Temporal.dip_8000 = Variables_Globales.IpPrivada;
                    Temporal.uua_8000 = Variables_Globales.IdUsuario;
                    Temporal.fua_8000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'PARÁMETROS GENERALES. EJERCICIO DE TRABAJO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión aplicativo</param>
        /// <param name="Id_8000">Identificador registro único 'PARÁMETROS GENERALES. EJERCICIO DE TRABAJO'</param>
        /// <param name="Ejercicio">Ejercicio</param>
        public static void Actualizar_Relpag_8000(string CadenaConexion, int Id_8000, short Ejercicio)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Relpag_8000 Temporal = new Relpag_8000(Cadena_Conexion, Variables_Globales.NombreBaseDatosAplicativo);

                    Cadena_Conexion.Open();

                    Temporal.ide_8000 = Id_8000;

                    Temporal.eje_8000 = Ejercicio;
                    Temporal.dip_8000 = Variables_Globales.IpPrivada;
                    Temporal.uua_8000 = Variables_Globales.IdUsuario;
                    Temporal.fua_8000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Relpag_8000 ('PARÁMETROS GENERALES. EJERCICIO DE TRABAJO')

    }
    //
}
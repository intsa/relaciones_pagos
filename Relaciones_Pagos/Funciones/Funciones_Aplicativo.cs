﻿using ClaseIntsa.DataBase;
using ClaseIntsa.Funciones;
using ClaseIntsa.Propiedades;
using DatosGenerales.Funciones;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace Relaciones_Pagos
{
    public static class Funciones_Aplicativo
    {
        #region Generales 

        /// <summary>
        /// Obtiene el ejercicio de trabajo
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Id_Registro">Identificador registro único</param>
        /// <returns>Ejercicio de trabajo</returns>
        public static short Ejercicio_Relpag_8000(string CadenaConexion, byte Id_Registro)
        {
            short Ejercicio = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT eje_8000";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.Relpag_8000";
            string Restriccion_Codigo = "ide_8000 = " + Id_Registro.ToString();
            string Cadena_Where = " WHERE " + Restriccion_Codigo;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Ejercicio = (short)Data_Reader["eje_8000"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Ejercicio;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el ejercicio de trabajo
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Id_Registro">Identificador registro único</param>
        /// <returns>Ejercicio de trabajo</returns>
        public static int Forma_Pago_Relpag_8000(string CadenaConexion, byte Id_8000)
        {
            int Id_1005 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1005";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.Relpag_8000";
            string Restriccion_Codigo = $"ide_8000 = {Id_8000}";
            string Cadena_Where = " WHERE " + Restriccion_Codigo;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_1005 = (int)Data_Reader["ide_1005"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_1005;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Verifica  el siguiente número de la secuencia
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Nombre_Bdd">Nombre base de datos</param>
        /// <param name="Secuencia">Nombre de la secuencia</param>
        /// <param name="Id_Funcion">Identificador de funcion a utilizar
        /// <param name="Ejercicio">Identificador de funcion a utilizar
        /// 1 - Número de relación
        /// </param>
        /// <param name="Ejercicio">Ejercicio de la secuencia</param>
        /// <returns>Número de secuencia válido</returns>
        public static int Verificar_Secuencia(string CadenaConexion, string Nombre_Bdd, string Secuencia, byte Id_Funcion, short Ejercicio = 0)
        {
            bool Secuencia_Valida = false;
            int Numero_Secuencia = 0;
            int Id_Secuencia = 0;

            while (!Secuencia_Valida)
            {
                Numero_Secuencia = Secuencias.Siguiente_Secuencia(CadenaConexion, Nombre_Bdd, Secuencia);

                if (Numero_Secuencia != 0)
                {
                    switch (Id_Funcion)
                    {
                        case 1:             // NÚMERO DE RELACIÓN
                            {
                                Id_Secuencia = Identificador_Relpag_0010(CadenaConexion, Ejercicio, Numero_Secuencia);

                                break;
                            }

                        case 2:             // IDENTIFICADOR DOCUMENTOS PENDIENTES
                            {
                                Id_Secuencia = Identificador_Relpag_0001(CadenaConexion, Numero_Secuencia);

                                break;
                            }


                    }

                    if (Id_Secuencia == 0)
                        Secuencia_Valida = true;

                }
            }

            return Numero_Secuencia;
        }

        /// <summary>
        /// Gerena el campo y el archivo del modelo-997
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Id_0010">Identificador registro 'RELACIONES DE PAGO. MODELO-997'</param>
        /// <param name="Fecha_Extraccion"></param>
        /// <param name="Fecha_Generacion"></param>
        /// <param name="Fecha_Caducidad">Fecha caducidad de pago</param>
        /// <returns></returns>
        public static string Generar_Modelo_997(string CadenaConexion, int Id_0010, string Fecha_Extraccion, string Fecha_Generacion, string Fecha_Caducidad)
        {
            DataTable Documentos;
            string Archivo_Modelo_997;
            string Nif_Entidad = Funciones_Generales._Obtener_Nif_Empresa(Variables_Globales.CodigoEmpresa, CadenaConexion).Trim().PadRight(12, ' ').Left(12);
            string Nif_Tercero;
            string Razon_Social;
            string Importe;
            string Observaciones;
            string Registros;
            int Id_0001;
            int Id_0010_1;

            Archivo_Modelo_997 = $"CT{Nif_Entidad}{Fecha_Extraccion}IP{Fecha_Generacion}".PadRight(210, ' ').Left(210) + Environment.NewLine;
            Archivo_Modelo_997 += $"CP{Nif_Entidad}{Fecha_Extraccion}IP{Fecha_Generacion}{Fecha_Caducidad}".PadRight(210, ' ').Left(210) + Environment.NewLine;

            Documentos = Modelo_997(CadenaConexion, Id_0010);

            foreach (DataRow Fila in Documentos.Rows)
            {
                Id_0001 = (int)Fila["ide_0001"];
                Id_0010_1 = (int)Fila["ide_0010_1"];
                Nif_Tercero = Fila["nif_0001"].ToString().Trim().PadRight(12, ' ').Left(12);
                Razon_Social = Fila["razsoc"].ToString().Trim().PadRight(40, ' ').Left(40);
                Importe = Fila["impdoc"].ToString().Trim().PadLeft(13, '0').Right(13);
                Observaciones = $"Referencia : [{Id_0010_1}-{Id_0001}]  {Fila["obs_0010_1"].ToString().Trim()}".PadRight(210, ' ').Left(210);
                Archivo_Modelo_997 += $"DI{Nif_Tercero}{Razon_Social}{Importe}".PadRight(210, ' ').Left(210) + Environment.NewLine;
            }

            Registros = Documentos.Rows.Count.ToString().PadLeft(6, '0').Right(6);
            Archivo_Modelo_997 += $"FP{Nif_Entidad}{Fecha_Extraccion}IP{Fecha_Generacion}{Registros}".PadRight(210, ' ').Left(210) + Environment.NewLine;
            Registros = (Documentos.Rows.Count + 4).ToString().PadLeft(8, '0').Right(8);
            Archivo_Modelo_997 += $"FT{Nif_Entidad}{Fecha_Extraccion}IP{Fecha_Generacion}{Registros}".PadRight(210, ' ').Left(210);

            return Archivo_Modelo_997;
        }

        /// <summary>
        /// Genera DataTable con lod datos para obtener el modelo-997
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Id_0010">Identificador registro 'RELACIONES DE PAGO. MODELO-997'</param>
        /// <returns></returns>
        private static DataTable Modelo_997(string CadenaConexion, int Id_0010)
        {
            DataTable Documentos = new DataTable();
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0010_1, ide_0001, nif_0001, razsoc, CAST (liq_0001 * 100 as bigint) impdoc, obs_0010_1";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.relpag_v_0010_1";
            string Restriccion_Identificador = $"ide_0010 = {Id_0010}";
            string Cadena_Where = " WHERE " + Restriccion_Identificador;
            string Cadena_Order = " ORDER BY ide_0001";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where + Cadena_Order;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();
                    Documentos.Load(Data_Reader);
                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Documentos;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Genera DataTable con lod datos para obtener el SEPA-34
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Id_0020">Identificador registro 'RELACIONES DE PAGO. VERIFICADAS'</param>
        /// <returns>DataTable con los documentos que componen la remesa</returns>
        public static DataTable Sepa_34(string CadenaConexion, int Id_0020)
        {
            DataTable Documentos = new DataTable();
            string Bdd_Aplicativo = Variables_Globales.NombreBaseDatosAplicativo;
            string Cadena_Sql;
            string Cadena_Select;
            string Cadena_From;
            string Restriccion_Identificador = $"ide_0020 = {Id_0020}";
            string Cadena_Where = " WHERE " + Restriccion_Identificador;
            string Cadena_Order = " ORDER BY razter, relpag_0001.ide_0001";

            Cadena_Select = "SELECT ide_0020_1 idereg, eje_0001 ejefac, relpag_0001.ide_0001 idedoc, liq_0001 impliq, nfa_0001 numfac, nre_0001 regseg, iba_0001 ibater, bic_0001 bicter,";
            Cadena_Select += " nif_0001 nifter, Funciones_Generales.dbo.Normaliza_Razon_Social(ape_0001, nom_0001) razter,";
            Cadena_Select += " Funciones_Generales.dbo.Normaliza_Domicilio(tvi_0001, nvi_0001, n01_0001, l01_0001, n02_0001, l02_0001, kms_0001, por_0001, esc_0001, pla_0001, pue_0001, loc_0001) domter,";
            Cadena_Select += " cpo_0001 cpoter, mun_0001 munter, pro_0001 proter, npa_0001 paiter";
            Cadena_From = $" FROM {Bdd_Aplicativo}.dbo.relpag_0020_1 INNER JOIN {Bdd_Aplicativo}.dbo.relpag_0001 ON relpag_0020_1.ide_0001 = relpag_0001.ide_0001";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where + Cadena_Order;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.HasRows)
                        Documentos.Load(Data_Reader);

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Documentos;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// General el archivo XML SEPA-34
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Documentos">DataTable de documetntos que componen la relación</param>
        /// <param name="Registro_Cabecera">DataTable de documetntos que componen la relación</param>
        public static string Sepa_34_Generar_XML(string CadenaConexion, DateTime Fecha_Generacion, DataTable Documentos, DataRow Registro_Cabecera, string Directorio_Sepa, string Nombre_Archivo)
        {
            XmlDocument Sepa_Xml = new XmlDocument();
            DataRow Registro_Empresa = Funciones_Aplicativo.Datos_Empresa(CadenaConexion, Variables_Globales.IdEmpresa);
            string Ejercicio_Remesa = Registro_Cabecera["ejerel"].ToString();
            string Fecha_Actual = Fecha_Generacion.ToString("yyyy-MM-ddTHH:mm:ss");
            string Iban_Ordenante = Registro_Cabecera["ibaord"].ToString().Trim();
            string Siglas_Pais_Ordenante = Iban_Ordenante.Left(2);
            string Bic_Ordenante = Registro_Cabecera["bicord"].ToString().Trim();
            string Nif_Ordenante = Registro_Empresa["niford"].ToString().Trim();
            string Identificador_Ordenante = Registro_Cabecera["ideord"].ToString().Trim();
            string Sufijo_Ordenante = Registro_Cabecera["suford"].ToString().Trim();
            string Numero_Documentos = Registro_Cabecera["numdoc"].ToString();
            string Importe_Total = Registro_Cabecera["imptot"].ToString().Replace(',', '.');
            string Id_Remesa = Registro_Cabecera["ejerel"].ToString() + "REL" + Registro_Cabecera["numrel"].ToString() + "ID" + Registro_Cabecera["idereg"].ToString();
            string Fecha_Remesa = Convert.ToDateTime(Registro_Cabecera["fecrel"]).ToString("yyyy-MM-dd");
            string Razon_Ordenante = Registro_Empresa["razord"].ToString();
            string Domicilio_Ordenante = Registro_Empresa["domord"].ToString();
            string Cpostal_Ordenante = Registro_Empresa["cpoord"].ToString();
            string Municipio_Ordenante = Registro_Empresa["munord"].ToString();
            string Provincia_Ordenante = Registro_Empresa["proord"].ToString();

            XmlDeclaration xmlDeclaration = Sepa_Xml.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = Sepa_Xml.DocumentElement;
            Sepa_Xml.InsertBefore(xmlDeclaration, root);

            XmlElement Raiz = Sepa_Xml.CreateElement("Document");
            Sepa_Xml.AppendChild(Raiz);
            Raiz.SetAttribute("xmlns", "urn:iso:std:iso:20022:tech:xsd:pain.001.001.03");
            Raiz.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

            XmlElement CstmrCdtTrfInitn = Sepa_Xml.CreateElement("CstmrCdtTrfInitn");
            Raiz.AppendChild(CstmrCdtTrfInitn);

            // NODO 1
            XmlElement GrpHdr = Sepa_Xml.CreateElement("GrpHdr");
            CstmrCdtTrfInitn.AppendChild(GrpHdr);

            // NODO 1.1  IDENTIFICADOR DEL MENSAJE
            XmlElement MsgId = Sepa_Xml.CreateElement("MsgId");
            MsgId.AppendChild(Sepa_Xml.CreateTextNode($@"REPA-{Fecha_Actual}"));
            GrpHdr.AppendChild(MsgId);

            // NODO 1.2 FECHA DE CREACIÓN
            XmlElement CreDtTm = Sepa_Xml.CreateElement("CreDtTm");
            CreDtTm.AppendChild(Sepa_Xml.CreateTextNode(Fecha_Actual));
            GrpHdr.AppendChild(CreDtTm);

            // NODO 1.6 NÚMERO DE DOCUMENTOS 
            XmlElement NbOfTxs = Sepa_Xml.CreateElement("NbOfTxs");
            NbOfTxs.AppendChild(Sepa_Xml.CreateTextNode(Numero_Documentos));
            GrpHdr.AppendChild(NbOfTxs);

            // NODO 1.7 IMPORTE TOTAL
            XmlElement CtrlSum = Sepa_Xml.CreateElement("CtrlSum");
            CtrlSum.AppendChild(Sepa_Xml.CreateTextNode(Importe_Total));
            GrpHdr.AppendChild(CtrlSum);

            // NODO 1.8
            XmlElement InitgPty = Sepa_Xml.CreateElement("InitgPty");
            GrpHdr.AppendChild(InitgPty);

            // NODO 1.8.1 NOMBRE DEL ORDENANTE
            XmlElement Nm = Sepa_Xml.CreateElement("Nm");
            Nm.AppendChild(Sepa_Xml.CreateTextNode(Razon_Ordenante));
            InitgPty.AppendChild(Nm);

            // NODO 1.8.2
            XmlElement Id = Sepa_Xml.CreateElement("Id");
            InitgPty.AppendChild(Id);

            // NODO 1.8.3
            XmlElement OrgId = Sepa_Xml.CreateElement("OrgId");
            Id.AppendChild(OrgId);

            XmlElement Othr = Sepa_Xml.CreateElement("Othr");
            OrgId.AppendChild(Othr);

            // NODO 1.8.4
            // IDENTIFICADOR ORDENANTE
            XmlElement Id1 = Sepa_Xml.CreateElement("Id");
            Id1.AppendChild(Sepa_Xml.CreateTextNode(Nif_Ordenante + Sufijo_Ordenante));
            Othr.AppendChild(Id1);

            // NODO 1.8.5
            XmlElement SchmeNm = Sepa_Xml.CreateElement("SchmeNm");
            Othr.AppendChild(SchmeNm);

            XmlElement Cd1 = Sepa_Xml.CreateElement("Cd");
            Cd1.AppendChild(Sepa_Xml.CreateTextNode("CORE"));
            SchmeNm.AppendChild(Cd1);

            // NODO 2
            XmlElement PmtInf = Sepa_Xml.CreateElement("PmtInf");
            CstmrCdtTrfInitn.AppendChild(PmtInf);

            // 2.1 IDENTIFICADOR DEL PAGO (aaaaRELnn...nnnIDnn...nnn
            XmlElement PmtInfId = Sepa_Xml.CreateElement("PmtInfId");
            PmtInfId.AppendChild(Sepa_Xml.CreateTextNode(Id_Remesa));
            PmtInf.AppendChild(PmtInfId);

            //  2.2 MÉTODO DE PAGO (TRF=TRANSFERENCIA - CHK=CHEQUE BANCARIO O CHEQUE-NÓMINA)
            XmlElement PmtMtd = Sepa_Xml.CreateElement("PmtMtd");
            PmtMtd.AppendChild(Sepa_Xml.CreateTextNode("TRF"));
            PmtInf.AppendChild(PmtMtd);

            // 2.3 INDICADOR DE APUNTE EN CUENTA
            XmlElement BtchBookg = Sepa_Xml.CreateElement("BtchBookg");
            BtchBookg.AppendChild(Sepa_Xml.CreateTextNode("false"));
            PmtInf.AppendChild(BtchBookg);

            // 2.4 NÚMERO DE DOCUMENTOS 
            XmlElement NbOfTxs_1 = Sepa_Xml.CreateElement("NbOfTxs");
            NbOfTxs_1.AppendChild(Sepa_Xml.CreateTextNode(Numero_Documentos));
            PmtInf.AppendChild(NbOfTxs_1);

            // 2.5  IMPORTE TOTAL
            XmlElement CtrlSum_1 = Sepa_Xml.CreateElement("CtrlSum");
            CtrlSum_1.AppendChild(Sepa_Xml.CreateTextNode(Importe_Total));
            PmtInf.AppendChild(CtrlSum_1);

            // 2.6 
            XmlElement PmtTpInf = Sepa_Xml.CreateElement("PmtTpInf");
            PmtInf.AppendChild(PmtTpInf);

            // 2.7 PRIORIDAD DE LA INSTRUCCIÓN - GRADO DE URGENCIA HIGH - NORM
            XmlElement InstrPrty = Sepa_Xml.CreateElement("InstrPrty");
            InstrPrty.AppendChild(Sepa_Xml.CreateTextNode("NORM"));
            PmtTpInf.AppendChild(InstrPrty);

            // 2.8 
            XmlElement SvcLvl = Sepa_Xml.CreateElement("SvcLvl");
            PmtTpInf.AppendChild(SvcLvl);

            XmlElement Cd = Sepa_Xml.CreateElement("Cd");
            Cd.AppendChild(Sepa_Xml.CreateTextNode("SEPA"));
            SvcLvl.AppendChild(Cd);

            // 2.9 
            XmlElement LclInstrm = Sepa_Xml.CreateElement("LclInstrm");
            PmtTpInf.AppendChild(LclInstrm);

            XmlElement Cd_Core = Sepa_Xml.CreateElement("Cd");
            Cd_Core.AppendChild(Sepa_Xml.CreateTextNode("CORE"));
            LclInstrm.AppendChild(Cd_Core);

            // 2.10 
            XmlElement CtgyPurp = Sepa_Xml.CreateElement("CtgyPurp");
            PmtTpInf.AppendChild(CtgyPurp);

            XmlElement Cd_Othr = Sepa_Xml.CreateElement("Cd");
            Cd_Othr.AppendChild(Sepa_Xml.CreateTextNode("OTHR"));
            CtgyPurp.AppendChild(Cd_Othr);

            // 2.17 FECHA PAGO
            XmlElement ReqdExctnDt = Sepa_Xml.CreateElement("ReqdExctnDt");
            ReqdExctnDt.AppendChild(Sepa_Xml.CreateTextNode(Fecha_Remesa));
            PmtInf.AppendChild(ReqdExctnDt);

            // 2.18 DATOS DEL ORDENANTE
            XmlElement Dbtr = Sepa_Xml.CreateElement("Dbtr");
            PmtInf.AppendChild(Dbtr);

            // 2.19 NOMBRE DEL ORDENANTE
            XmlElement Nm1 = Sepa_Xml.CreateElement("Nm");
            Nm1.AppendChild(Sepa_Xml.CreateTextNode(Razon_Ordenante));
            Dbtr.AppendChild(Nm1);

            // 2.19.1 DIRECCIÓN DEL ORDENANTE
            XmlElement PstlAdr = Sepa_Xml.CreateElement("PstlAdr");
            Dbtr.AppendChild(PstlAdr);

            // 2.19.2 PAÍS SEGÚN CÓDIGO ISO 3166 ALPHA-2
            XmlElement Ctry = Sepa_Xml.CreateElement("Ctry");
            Ctry.AppendChild(Sepa_Xml.CreateTextNode(Siglas_Pais_Ordenante));
            PstlAdr.AppendChild(Ctry);

            // 2.19.3 DIRECCIÓN ORDENANTE
            XmlElement AdrLine = Sepa_Xml.CreateElement("AdrLine");
            AdrLine.AppendChild(Sepa_Xml.CreateTextNode(Domicilio_Ordenante.Left(70)));
            PstlAdr.AppendChild(AdrLine);

            // 2.19.3 POBLACIÓN (CÓDIGO POSTAL-MUNICIPIO)
            XmlElement AdrLine1 = Sepa_Xml.CreateElement("AdrLine");
            AdrLine1.AppendChild(Sepa_Xml.CreateTextNode(Cpostal_Ordenante + "-" + Municipio_Ordenante));
            PstlAdr.AppendChild(AdrLine1);

            // 2.19.3 PROVINCIA
            XmlElement AdrLine2 = Sepa_Xml.CreateElement("AdrLine");
            AdrLine2.AppendChild(Sepa_Xml.CreateTextNode(Provincia_Ordenante));
            PstlAdr.AppendChild(AdrLine2);

            // 2.20 DATOS BANCARIOS ORDENANTE (I.B.A.N.)
            XmlElement DbtrAcct = Sepa_Xml.CreateElement("DbtrAcct");
            PmtInf.AppendChild(DbtrAcct);

            // 2.20.1
            XmlElement Id2 = Sepa_Xml.CreateElement("Id");
            DbtrAcct.AppendChild(Id2);

            // 2.20.2 I.B.A.N.
            XmlElement IBAN = Sepa_Xml.CreateElement("IBAN");
            IBAN.AppendChild(Sepa_Xml.CreateTextNode(Iban_Ordenante));
            Id2.AppendChild(IBAN);

            // 2.21
            XmlElement DbtrAgt = Sepa_Xml.CreateElement("DbtrAgt");
            PmtInf.AppendChild(DbtrAgt);

            // 2.21.1
            XmlElement FinInstnId = Sepa_Xml.CreateElement("FinInstnId");
            DbtrAgt.AppendChild(FinInstnId);

            // 2.21.2 B.I.C.-S.W.I.F.T. ORDENANTE
            XmlElement BIC = Sepa_Xml.CreateElement("BIC");
            BIC.AppendChild(Sepa_Xml.CreateTextNode(Bic_Ordenante));
            FinInstnId.AppendChild(BIC);

            // 2.24
            XmlElement ChrgBr = Sepa_Xml.CreateElement("ChrgBr");
            ChrgBr.AppendChild(Sepa_Xml.CreateTextNode("SLEV"));
            PmtInf.AppendChild(ChrgBr);

            #region DOCUMENTOS

            string Id_Documento;
            string Importe_Documento;
            string Iban_Tercero;
            string Bic_Tercero;

            // INSERCIÓN DE LOS DOCUMENTOS
            foreach (DataRow Fila in Documentos.Rows)
            {
                Id_Documento = Ejercicio_Remesa + "IR" + Registro_Cabecera["idereg"].ToString() + "IDR" + Fila["idereg"].ToString() + "IDP" + Fila["idedoc"].ToString();
                Importe_Documento = Fila["impliq"].ToString().Replace(',', '.');
                Iban_Tercero = Fila["ibater"].ToString();
                Bic_Tercero = Fila["bicter"].ToString();

                // 2.29 
                XmlElement CdtTrfTxInf = Sepa_Xml.CreateElement("CdtTrfTxInf");
                PmtInf.AppendChild(CdtTrfTxInf);

                // 2.29.1
                XmlElement PmtId = Sepa_Xml.CreateElement("PmtId");
                CdtTrfTxInf.AppendChild(PmtId);

                // 2.29.1.1 IDENTIFICADOR DOCUMENTO (aaaaIRn...nIRn...nIDPn...n)
                // aaaa - Ejercicio
                // IR - Constante. Identificador registro remesa
                // n - Identificador registro remesa
                // IR - Constante. Identificador registro documento remesa
                // n - Identificador registro documento remesa
                // IDP - Constante. Identificador registro documento de pago
                // n - Identificador registro documento de pago
                XmlElement InstrId = Sepa_Xml.CreateElement("InstrId");
                InstrId.AppendChild(Sepa_Xml.CreateTextNode(Id_Documento));
                PmtId.AppendChild(InstrId);

                // 2.29.1.2
                XmlElement EndToEndId = Sepa_Xml.CreateElement("EndToEndId");
                EndToEndId.AppendChild(Sepa_Xml.CreateTextNode(Id_Documento));
                PmtId.AppendChild(EndToEndId);

                // 2.30 IMPORTE DEL DOCUMENTO
                XmlElement Amt = Sepa_Xml.CreateElement("Amt");
                CdtTrfTxInf.AppendChild(Amt);

                // 2.2.30.1
                XmlElement InstdAmt = Sepa_Xml.CreateElement("InstdAmt");
                InstdAmt.AppendChild(Sepa_Xml.CreateTextNode(Importe_Documento));
                InstdAmt.SetAttribute("Ccy", "EUR");
                Amt.AppendChild(InstdAmt);

                XmlElement CdtrAgt = Sepa_Xml.CreateElement("CdtrAgt");
                CdtTrfTxInf.AppendChild(CdtrAgt);

                XmlElement FinInstnId1 = Sepa_Xml.CreateElement("FinInstnId");
                CdtrAgt.AppendChild(FinInstnId1);

                // B.I.C./S.W.I.F.T. TERCERO
                XmlElement Xbic_Tercero = Sepa_Xml.CreateElement("BIC");
                Xbic_Tercero.AppendChild(Sepa_Xml.CreateTextNode(Bic_Tercero));
                FinInstnId1.AppendChild(Xbic_Tercero);

                XmlElement Cdtr = Sepa_Xml.CreateElement("Cdtr");
                CdtTrfTxInf.AppendChild(Cdtr);

                // RAZÓN SOCIAL
                XmlElement Razon = Sepa_Xml.CreateElement("Nm");
                Razon.AppendChild(Sepa_Xml.CreateTextNode(Fila["razter"].ToString()));
                Cdtr.AppendChild(Razon);

                XmlElement Direccion = Sepa_Xml.CreateElement("PstlAdr");
                Cdtr.AppendChild(Direccion);

                // PAIS
                XmlElement Pais = Sepa_Xml.CreateElement("Ctry");
                Pais.AppendChild(Sepa_Xml.CreateTextNode(Iban_Tercero.Left(2)));
                Direccion.AppendChild(Pais);

                // DOMICILIO TERCERO
                XmlElement Direccion1 = Sepa_Xml.CreateElement("AdrLine");
                Direccion1.AppendChild(Sepa_Xml.CreateTextNode(Fila["domter"].ToString()));
                Direccion.AppendChild(Direccion1);

                // MUNICIPIO
                XmlElement Municipio = Sepa_Xml.CreateElement("AdrLine");
                Municipio.AppendChild(Sepa_Xml.CreateTextNode(Fila["cpoter"].ToString() + "-" + Fila["proter"].ToString()));
                Direccion.AppendChild(Municipio);

                // PROVINCIA
                XmlElement Provincia = Sepa_Xml.CreateElement("AdrLine");
                Provincia.AppendChild(Sepa_Xml.CreateTextNode(Fila["proter"].ToString()));
                Direccion.AppendChild(Provincia);

                XmlElement CdtrAcct1 = Sepa_Xml.CreateElement("CdtrAcct");
                CdtTrfTxInf.AppendChild(CdtrAcct1);

                // I.B.A.N. TERCERO
                XmlElement Id_Iban_Tercero = Sepa_Xml.CreateElement("Id");
                CdtrAcct1.AppendChild(Id_Iban_Tercero);

                XmlElement Xiban_Tercero = Sepa_Xml.CreateElement("IBAN");
                Xiban_Tercero.AppendChild(Sepa_Xml.CreateTextNode(Iban_Tercero));
                Id_Iban_Tercero.AppendChild(Xiban_Tercero);

                XmlElement RmtInf = Sepa_Xml.CreateElement("RmtInf");
                CdtTrfTxInf.AppendChild(RmtInf);

                // CONCEPTO
                XmlElement Ustrd = Sepa_Xml.CreateElement("Ustrd");
                Ustrd.AppendChild(Sepa_Xml.CreateTextNode("PAGO SU FACTURA :" + Fila["numfac"].ToString() + "REFERENCIA : " + Fila["regseg"].ToString()));
                RmtInf.AppendChild(Ustrd);
            }

            #endregion DOCUMENTOS

             Sepa_Xml.Save(Directorio_Sepa + @"\" + Nombre_Archivo);

            return Sepa_Xml.InnerXml;
        }

        /// <summary>
        /// Calcula el identificador SEPA del ordenante
        /// </summary>
        /// <param name="Numero_Identificacion">Número identificación ordenante</param>
        /// <param name="Pais_Ordenante">Pais del ordenante</param>
        /// <param name="Sufijo_Ordenante">Sufijo SEPA del ordenante</param>
        /// <returns></returns>
        public static string Sepa_Identificador_Ordenante(string Numero_Identificacion, string Pais_Ordenante, string Sufijo_Ordenante)
        {
            string Id_Sepa;
            string Id_Auxiliar = Numero_Identificacion.Trim() + Pais_Ordenante.Trim() + "00";
            string Letras_Numeros = "";
            string Caracter_Auxiliar;
            string Id_Definitvo = "";
            string Cid_97;
            int Indice;
            int Numero_Auxiliar;
            int Id_97;
            int Numero_Digitos;

            for (byte i = 1; i <= 26; i++)
                Letras_Numeros += (char)(64 + i);

            for (byte i = 0; i < Id_Auxiliar.Length; i++)
            {
                Caracter_Auxiliar = Id_Auxiliar.Substring(i, 1);

                if (!Caracter_Auxiliar.EsNumerico())
                {
                    Indice = Letras_Numeros.IndexOf(Caracter_Auxiliar);
                    Caracter_Auxiliar = (Indice + 10).ToString();
                }

                Id_Definitvo += Caracter_Auxiliar;
            }

            Id_Definitvo = Id_Definitvo.Trim();

            Numero_Auxiliar = Convert.ToInt32(Id_Definitvo.Left(9));
            Id_Definitvo = Id_Definitvo.Substring(9);
            Id_97 = Numero_Auxiliar % 97;
            Cid_97 = $"00{Id_97}".Right(2);

            Numero_Digitos = Id_Definitvo.Trim().Length / 7;

            for (int i = 1; i <= Numero_Digitos; i++)
            {
                Numero_Auxiliar = Convert.ToInt32(Cid_97 + Id_Definitvo.Left(7));
                Id_Definitvo = Id_Definitvo.Substring(7);

                Id_97 = Numero_Auxiliar % 97;
                Cid_97 = $"00{Id_97}".Right(2);
            }

            if (Id_Definitvo.Trim() != string.Empty)
            {
                Numero_Auxiliar = Convert.ToInt32(Cid_97 + Id_Definitvo.Left(7));
                Id_97 = Numero_Auxiliar % 97;
            }

            Id_97 = 98 - Id_97;
            Cid_97 = $"00{Id_97}".Right(2);

            Id_Sepa = Pais_Ordenante + Cid_97 + Sufijo_Ordenante.Trim() + Numero_Identificacion;

            return Id_Sepa;
        }

        #endregion Generales 

        #region Verificación

        /// <summary>
        /// Verifica si existe el registro tabla 'DOCUMENTOS DE PAGO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Id_Original">Identificador registro original 'DOCUMENTOS DE PAGO'</param>
        /// <returns>
        /// Item1 - Identificador registro 'DOCUMENTOS DE PAGO'
        /// Item2 - Si está incluído en una relación de pagos
        /// </returns>
        public static Tuple<int, byte> Identificador_Relpag_0001(string CadenaConexion, string Id_Original)
        {
            int Id_0001 = 0;
            byte Incluida_Relacion = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0001, sdr_0001";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.Relpag_0001";
            string Restriccion_Identificador = "ian_0001 = '" + Id_Original.Trim() + "'";
            string Cadena_Where = " WHERE " + Restriccion_Identificador;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_0001 = (int)Data_Reader["ide_0001"];
                        Incluida_Relacion = (byte)Data_Reader["sdr_0001"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, byte>(Id_0001, Incluida_Relacion);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Verifica si existe el registro tabla 'DOCUMENTOS DE PAGO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Id_0001">Identificador registro 'DOCUMENTOS DE PAGO'</param>
        /// <returns>Identificador registro 'DOCUMENTOS DE PAGO'/// </returns>
        public static int Identificador_Relpag_0001(string CadenaConexion, int Id_0001)
        {
            int Id_Registro = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0001";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.Relpag_0001";
            string Restriccion_Identificador = $"ide_0001 = {Id_0001}";
            string Cadena_Where = " WHERE " + Restriccion_Identificador;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_Registro = (int)Data_Reader["ide_0001"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_Registro;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene identificador registro 'RELACIONES DE PAGO. MODELO-997'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio de la relación</param>
        /// <param name="Relacion">Número de la relación</param>
        /// <returns>
        /// Item1 : Identificador registro
        /// Item2 : Fecha relación
        /// </returns>
        public static int Identificador_Relpag_0010(string CadenaConexion, short Ejercicio, int Relacion)
        {
            int Id_0010 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0010";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.Relpag_0010";
            string Restriccion_Ejercicio = $"eje_0010 = {Ejercicio}";
            string Restriccion_Relacion = $"rel_0010 = {Relacion}";
            string Cadena_Where = " WHERE " + Restriccion_Ejercicio + " AND " + Restriccion_Relacion;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_0010 = (int)Data_Reader["ide_0010"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_0010;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene identificador registro 'RELACIONES DE PAGO. MODELO-997'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio de la relación</param>
        /// <param name="Relacion">Número de la relación</param>
        /// <returns>
        /// Item1 : Identificador registro
        /// Item2 : Descripción
        /// Item3 : Fecha relación
        /// </returns>
        public static Tuple<int, string, string> Relacion_Relpag_0010(string CadenaConexion, short Ejercicio, int Relacion)
        {
            int Id_0010 = 0;
            string Descripcion = "";
            string Fecha_Relacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0010, des_0010, fec_0010";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.Relpag_0010";
            string Restriccion_Ejercicio = $"eje_0010 = {Ejercicio}";
            string Restriccion_Relacion = $"rel_0010 = {Relacion}";
            string Cadena_Where = " WHERE " + Restriccion_Ejercicio + " AND " + Restriccion_Relacion;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_0010 = (int)Data_Reader["ide_0010"];
                        Descripcion = Data_Reader["des_0010"].ToString();
                        Fecha_Relacion = Data_Reader["fec_0010"].ToString();
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string, string>(Id_0010, Descripcion, Fecha_Relacion);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene identificador registro 'RELACIONES DE PAGO. MODELO-997'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio de la relación</param>
        /// <param name="Relacion">Número de la relación</param>
        /// <returns>
        /// Item1 : Año relación
        /// Item2 : Número relación
        /// </returns>
        public static Tuple<short, int> Relacion_Relpag_0010(string CadenaConexion, int Id_0010)
        {
            short Ano_Relacion = 0;
            int Relacion = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT eje_0010, rel_0010";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.Relpag_0010";
            string Restriccion_Ejercicio = $"ide_0010 = {Id_0010}";
            string Cadena_Where = " WHERE " + Restriccion_Ejercicio;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Ano_Relacion = (short)Data_Reader["eje_0010"];
                        Relacion = (int)Data_Reader["rel_0010"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<short, int>(Ano_Relacion, Relacion);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el contenido del modelo-997 tabla 'RELACIONES DE PAGO. MODELO-997'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Id_0010">Identificador registro 'RELACIONES DE PAGO. MODELO-997'</param>
        /// <returns>Modelo-997'</returns>
        public static string M_997_Relpag_0010(string CadenaConexion, int Id_0010)
        {
            string Modelo_997 = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT mod_0010";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.Relpag_0010";
            string Restriccion_Identificador = $"ide_0010 = {Id_0010}";
            string Cadena_Where = " WHERE " + Restriccion_Identificador;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Modelo_997 = (string)Data_Reader["mod_0010"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Modelo_997;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el contenido del Sepa_34 tabla 'RELACIONES DE PAGO. VERIFICADAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Id_0020">Identificador registro 'RELACIONES DE PAGO. VERIFICADAS'</param>
        /// <returns>Sepa_34'</returns>
        public static string Sepa_34_Relpag_0020(string CadenaConexion, int Id_0020)
        {
            string Sepa_34 = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT sep_0020";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.Relpag_0020";
            string Restriccion_Identificador = $"ide_0020 = {Id_0020}";
            string Cadena_Where = " WHERE " + Restriccion_Identificador;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Sepa_34 = (string)Data_Reader["sep_0020"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Sepa_34;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene identificador registro 'RELACIONES DE PAGO. DEFINITIVAS''
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio de la relación</param>
        /// <param name="Relacion">Número de la relación</param>
        /// <returns>identificador registro</returns>
        public static int Identificador_Relpag_0020(string CadenaConexion, short Ejercicio, int Relacion)
        {
            int Id_0020 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0020";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.Relpag_0020";
            string Restriccion_Ejercicio = $"eje_0020 = {Ejercicio}";
            string Restriccion_Relacion = $"rel_0020 = {Relacion}";
            string Cadena_Where = " WHERE " + Restriccion_Ejercicio + " AND " + Restriccion_Relacion;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_0020 = (int)Data_Reader["ide_0020"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_0020;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene identificador registro 'CANALES DE PAGO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Canal_Pago">Código 'CANAL DE PAGO'</param>
        /// <returns>identificador registro</returns>
        public static int Identificador_Relpag_1000(string CadenaConexion, string Canal_Pago)
        {
            int Id_1000 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1000";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.Relpag_1000";
            string Restriccion_Codigo = $"cod_1000 = '{Canal_Pago}'";
            string Cadena_Where = " WHERE " + Restriccion_Codigo;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_1000 = (int)Data_Reader["ide_1000"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_1000;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene identificador registro 'FORMAS DE PAGO''
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Forma_Pago">Código 'FORMA DE PAGO''</param>
        /// <returns>identificador registro</returns>
        public static int Identificador_Relpag_1005(string CadenaConexion, string Forma_Pago)
        {
            int Id_1005 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1005";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.Relpag_1005";
            string Restriccion_Codigo = $"cod_1005 = '{Forma_Pago}'";
            string Cadena_Where = " WHERE " + Restriccion_Codigo;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_1005 = (int)Data_Reader["ide_1005"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_1005;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene identificador registro y la denominación 'FORMAS DE PAGO''
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Id_1005">Identificador registro 'FORMA DE PAGO''</param>
        /// <returns>
        /// Item1 : Identificador registro
        /// Item2 : Denominación
        /// </returns>
        public static Tuple<int, string> Identificador_Relpag_1005(string CadenaConexion, int? Id_1005)
        {
            int Id_Registro = 0;
            string Denominacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1005, den_1005";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.Relpag_1005";
            string Restriccion_Identificador = $"ide_1005 = {Id_1005}";
            string Restriccion_Norma = $"cns_1005 != ''";
            string Cadena_Where = " WHERE " + Restriccion_Identificador + " AND " + Restriccion_Norma;

            if (Id_1005 is null)
                return Tuple.Create<int, string>(Id_Registro, Denominacion);
            else
            {
                try
                {
                    using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                    {
                        Cadena_Conexion.Open();
                        SqlCommand Comando_Sql = new SqlCommand();
                        SqlDataReader Data_Reader;
                        Comando_Sql.Connection = Cadena_Conexion;

                        Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                        Comando_Sql.CommandText = Cadena_Sql;
                        Data_Reader = Comando_Sql.ExecuteReader();

                        if (Data_Reader.Read())
                        {
                            Id_Registro = (int)Data_Reader["ide_1005"];
                            Denominacion = Data_Reader["den_1005"].ToString();
                        }

                        Data_Reader.Close();
                        Comando_Sql = null;
                        Cadena_Conexion.Close();

                        return Tuple.Create<int, string>(Id_Registro, Denominacion);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Verificación

        public static DataRow Datos_Cabecera_Remesa(string CadenaConexion, int Id_0020)
        {
            DataRow Datos_Cabecera = null;

            DataTable Documentos = new DataTable();
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0020 idereg, eje_0020 ejerel, rel_0020 numrel, fec_0020 fecrel, iba_0020 ibaord, bic_0020 bicord, sor_0020 suford, ior_0020 ideord, nre_0020 numdoc, tot_0020 imptot";
            string Cadena_From = $" FROM {Variables_Globales.NombreBaseDatosAplicativo}.dbo.relpag_0020";
            string Restriccion_Identificador = $"ide_0020 = {Id_0020}";
            string Cadena_Where = " WHERE " + Restriccion_Identificador;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();
                    Documentos.Load(Data_Reader);

                    if (Documentos.Rows.Count != 0)
                        Datos_Cabecera = Documentos.Rows[0];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Datos_Cabecera;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        public static DataRow Datos_Empresa(string CadenaConexion, int Id_0004)
        {
            DataRow Datos_Cabecera = null;

            DataTable Data_Table = new DataTable();
            string Cadena_Sql;
            string Cadena_Select;
            string Cadena_From;
            string Restriccion_Identificador = $"ide_0004 = {Id_0004}";
            string Cadena_Where = " WHERE " + Restriccion_Identificador;

            Cadena_Select = "SELECT nif_0004 niford, raz_0004 razord, Funciones_Generales.dbo.Normaliza_Domicilio(tvi_0004, nvi_0004, n01_0004, l01_0004, n02_0004, l02_0004, kms_0004, por_0004, esc_0004, pla_0004, pue_0004, loc_0004) domord,";
            Cadena_Select += " cpo_0004 cpoord, mun_0004 munord, pro_0004 proord, npa_0004 paiord";
            Cadena_From = " FROM Generales.dbo.datgen_0004";


            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();
                    Data_Table.Load(Data_Reader);

                    if (Data_Table.Rows.Count != 0)
                        Datos_Cabecera = Data_Table.Rows[0];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Datos_Cabecera;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }


    }

}
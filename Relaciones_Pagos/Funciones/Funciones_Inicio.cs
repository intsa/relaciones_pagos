﻿using ClaseIntsa.Propiedades;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Relaciones_Pagos
{
    public static class Funciones_Inicio
    {
        #region Funciones de inicio y selecciones (Formulrio Inicio)

        /// <summary>
        /// Asigna los nombre de la base de datos del aplicativo con empresa y ejercicio
        /// </summary>
        public static void Bases_Datos_Aplicativo()
        {
            string Nombres_Bdd = "Rel_Pag_EEEEEE".Replace("EEEEEE", Variables_Globales.CodigoEmpresa);
            Variables_Globales.NombreBaseDatosAplicativo = Nombres_Bdd;
        }

        /// <summary>
        /// Asigna los nombre de las bases de datos auxiliares con empresa y ejercicio
        /// </summary>
        public static void Bases_Datos_Auxiliares()
        {
            string[] Nombres_Bdd = { "Adm_Loc_Con_His_EEEEEE", "Adm_Loc_Con_Com_EEEEEE", "Adm_Loc_Con_EEEEEE_AAAA" };

            for (byte i = 0; i < Nombres_Bdd.Length; i++)
                Nombres_Bdd[i] = Nombres_Bdd[i].Replace("EEEEEE", Variables_Globales.CodigoEmpresa).Replace("AAAA", Propiedades_Aplicativo.Ejercicio_Trabajo.ToString());

            Propiedades_Aplicativo.Bdd_Historico = Nombres_Bdd[0];
            Propiedades_Aplicativo.Bdd_Comunes = Nombres_Bdd[1];
            Propiedades_Aplicativo.Bdd_Contabilidad = Nombres_Bdd[2];
        }

        #endregion
    }

    public static class Selecciones
    {
        #region Selecciones y actualizaciones

        /// <summary>
        /// Verifica si existen registros marcados
        /// </summary>
        /// <param name="Asignaciones">DataTable de la cuadrícula</param>
        /// <returns>Si existen o no registros marcados</returns>
        public static bool Verificar_Registros_Marcados(DataTable Asignaciones)
        {
            bool Correcto = false;

            foreach (DataRow Registro in Asignaciones.Rows)
            {
                if ((string)Registro["regsel"] == "X")
                {
                    Correcto = true;
                    break;
                }
            }

            return Correcto;
        }

        /// <summary>
        /// Recorre el DataTable e inserta/elimina registro 'RELACIONES DE PAGOS. MODELO-997. DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Documentos">DataTable de la cuadrícula</param>
        /// <param name="Id_0010">Identificador registro 'RELACIONES DE PAGOS. MODELO-997'</param>
        public static Tuple<short, decimal> Recorrer_Registros_Relpag_0010_1(string CadenaConexion, DataTable Documentos, int Id_0010)
        {
            string Marcado;
            string Observaciones;
            int Id_0001;
            int Id_0010_1;
            decimal Importe;
            short Registros = 0;
            decimal Total_Importe = 0;

            foreach (DataRow Registro in Documentos.Rows)
            {
                Marcado = (string)Registro["regsel"];
                Observaciones = (string)Registro["obsdoc"];
                Id_0001 = (int)Registro["ide_0001"];
                Id_0010_1 = (int)Registro["idereg"];
                Importe = (decimal)Registro["liq_0001"];

                if (Marcado.Trim() == "X")
                {
                    Registros++;
                    Total_Importe += Importe;

                    if (Id_0010_1 == 0)
                    {
                        Desencadenantes.Insertar_Relpag_0010_1(CadenaConexion, Id_0010, Id_0001, Observaciones);
                        Desencadenantes.Actualizar_Relpag_0001(CadenaConexion, Id_0001, 1);
                    }
                    else
                    {
//                        Desencadenantes.Actualizar_Relpag_0010_1(CadenaConexion, Id_0010_1, Observaciones);
                    }
                }
                else
                {
                    if (Id_0010_1 != 0)
                    {
                        Desencadenantes.Eliminar_Relpag_0010_1(CadenaConexion, Id_0010_1);
                        Desencadenantes.Actualizar_Relpag_0001(CadenaConexion, Id_0001, 0);
                    }
                }
            }

            return Tuple.Create<short, decimal>(Registros, Total_Importe);
        }

        /// <summary>
        /// Recorre el DataTable e inserta/elimina registro 'RELACIONES DE PAGOS. DEFINITIVAS. DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Documentos">DataTable de la cuadrícula</param>
        /// <param name="Id_0020">Identificador registro 'RELACIONES DE PAGOS. DEFINITIVAS'</param>
        public static Tuple<short, decimal> Recorrer_Registros_Relpag_0020_1(string CadenaConexion, DataTable Documentos, int Id_0020)
        {
            string Marcado;
            int Id_0001;
            int Id_0010_1;
            int Id_0020_1;
            decimal Importe;
            short Registros = 0;
            decimal Total_Importe = 0;

            foreach (DataRow Registro in Documentos.Rows)
            {
                Marcado = (string)Registro["regsel"];
                Id_0001 = (int)Registro["ide_0001"];
                Id_0020_1 = (int)Registro["idereg"];
                Id_0010_1 = (int)Registro["ideori"];
                Importe = (decimal)Registro["liq_0001"];

                if (Marcado.Trim() == "X")
                {
                    Registros++;
                    Total_Importe += Importe;

                    if (Id_0020_1 == 0)
                        Desencadenantes.Insertar_Relpag_0020_1(CadenaConexion, Id_0020, Id_0001, Id_0010_1);
                }
                else
                {
                    if (Id_0020_1 != 0)
                        Desencadenantes.Eliminar_Relpag_0020_1(CadenaConexion, Id_0020_1);
                }
            }

            return Tuple.Create<short, decimal>(Registros, Total_Importe);
        }

        #endregion Selecciones y actualizaciones

    }

}
﻿using ClaseIntsa.Controles;
using System.Data;

namespace Relaciones_Pagos
{
    public interface ITf_Relpag_1
    {
        void Retorno_Formulario_Multiseleccion(grdGridConsulta Cuadricula, int Identificador, DataTable Cuentas_Seleccionadas);
    }
}

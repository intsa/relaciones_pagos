using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

public class Relpag_0001_Extras
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001;                                          // 1 CLAVE_PRIMARIA   
    private string _ian_0001;                                       // 2   CLAVE_UNICA 
    private string _ifa_0001;                                       // 3    
    private string _eje_0001;                                       // 4    
    private string _lfr_0001;                                       // 5    
    private string _ord_0001;                                       // 6    
    private string _nfa_0001;                                       // 7    
    private string _fee_0001;                                       // 8    
    private string _ter_0001;                                       // 9    
    private string _nif_0001;                                       // 10    
    private string _ape_0001;                                       // 11    
    private string _nom_0001;                                       // 12    
    private string _iba_0001;                                       // 13    
    private string _bic_0001;                                       // 14    
    private decimal _bas_0001;                                      // 15    
    private decimal _imp_0001;                                      // 16    
    private decimal _bre_0001;                                      // 17    
    private decimal _ret_0001;                                      // 18    
    private decimal? _liq_0001;                                     // 19    CALCULADO
    private string _acf_0001;                                       // 20    
    private string _sac_0001;                                       // 21    
    private string _cpa_0001;                                       // 22    
    private string _pap_0001;                                       // 23    
    private string _tif_0001;                                       // 24    
    private string _sco_0001;                                       // 25    
    private string _res_0001;                                       // 26    
    private string _nre_0001;                                       // 27    
    private string _con_0001;                                       // 28    
    private string _eco_0001;                                       // 29    
    private string _exp_0001;                                       // 30    
    private string _doc_0001;                                       // 31    
    private string _tvi_0001;                                       // 32    
    private string _nvi_0001;                                       // 33    
    private string _n01_0001;                                       // 34    
    private string _l01_0001;                                       // 35    
    private string _n02_0001;                                       // 36    
    private string _l02_0001;                                       // 37    
    private decimal? _kms_0001;                                     // 38    
    private string _por_0001;                                       // 39    
    private string _esc_0001;                                       // 40    
    private string _pla_0001;                                       // 41    
    private string _pue_0001;                                       // 42    
    private string _loc_0001;                                       // 43    
    private string _cpo_0001;                                       // 44    
    private string _mun_0001;                                       // 45    
    private string _pro_0001;                                       // 46    
    private string _npa_0001;                                       // 47    
    private byte _sdr_0001;                                         // 48    
    private int _uar_0001;                                          // 49    
    private string _far_0001;                                       // 50    
    private string _dip_0001;                                       // 51    
    private int _uua_0001;                                          // 52    
    private string _fua_0001;                                       // 53    

    #endregion

    #region Propiedades

    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public string ian_0001
    {
        get { return _ian_0001; }
        set { _ian_0001 = value; }
    }
    public string ifa_0001
    {
        get { return _ifa_0001; }
        set { _ifa_0001 = value; }
    }
    public string eje_0001
    {
        get { return _eje_0001; }
        set { _eje_0001 = value; }
    }
    public string lfr_0001
    {
        get { return _lfr_0001; }
        set { _lfr_0001 = value; }
    }
    public string ord_0001
    {
        get { return _ord_0001; }
        set { _ord_0001 = value; }
    }
    public string nfa_0001
    {
        get { return _nfa_0001; }
        set { _nfa_0001 = value; }
    }
    public string fee_0001
    {
        get { return _fee_0001; }
        set { _fee_0001 = value; }
    }
    public string ter_0001
    {
        get { return _ter_0001; }
        set { _ter_0001 = value; }
    }
    public string nif_0001
    {
        get { return _nif_0001; }
        set { _nif_0001 = value; }
    }
    public string ape_0001
    {
        get { return _ape_0001; }
        set { _ape_0001 = value; }
    }
    public string nom_0001
    {
        get { return _nom_0001; }
        set { _nom_0001 = value; }
    }
    public string iba_0001
    {
        get { return _iba_0001; }
        set { _iba_0001 = value; }
    }
    public string bic_0001
    {
        get { return _bic_0001; }
        set { _bic_0001 = value; }
    }
    public decimal bas_0001
    {
        get { return _bas_0001; }
        set { _bas_0001 = value; }
    }
    public decimal imp_0001
    {
        get { return _imp_0001; }
        set { _imp_0001 = value; }
    }
    public decimal bre_0001
    {
        get { return _bre_0001; }
        set { _bre_0001 = value; }
    }
    public decimal ret_0001
    {
        get { return _ret_0001; }
        set { _ret_0001 = value; }
    }
    public decimal? liq_0001
    {
        get { return _liq_0001; }
        set { _liq_0001 = value; }
    }
    public string acf_0001
    {
        get { return _acf_0001; }
        set { _acf_0001 = value; }
    }
    public string sac_0001
    {
        get { return _sac_0001; }
        set { _sac_0001 = value; }
    }
    public string cpa_0001
    {
        get { return _cpa_0001; }
        set { _cpa_0001 = value; }
    }
    public string pap_0001
    {
        get { return _pap_0001; }
        set { _pap_0001 = value; }
    }
    public string tif_0001
    {
        get { return _tif_0001; }
        set { _tif_0001 = value; }
    }
    public string sco_0001
    {
        get { return _sco_0001; }
        set { _sco_0001 = value; }
    }
    public string res_0001
    {
        get { return _res_0001; }
        set { _res_0001 = value; }
    }
    public string nre_0001
    {
        get { return _nre_0001; }
        set { _nre_0001 = value; }
    }
    public string con_0001
    {
        get { return _con_0001; }
        set { _con_0001 = value; }
    }
    public string eco_0001
    {
        get { return _eco_0001; }
        set { _eco_0001 = value; }
    }
    public string exp_0001
    {
        get { return _exp_0001; }
        set { _exp_0001 = value; }
    }
    public string doc_0001
    {
        get { return _doc_0001; }
        set { _doc_0001 = value; }
    }
    public string tvi_0001
    {
        get { return _tvi_0001; }
        set { _tvi_0001 = value; }
    }
    public string nvi_0001
    {
        get { return _nvi_0001; }
        set { _nvi_0001 = value; }
    }
    public string n01_0001
    {
        get { return _n01_0001; }
        set { _n01_0001 = value; }
    }
    public string l01_0001
    {
        get { return _l01_0001; }
        set { _l01_0001 = value; }
    }
    public string n02_0001
    {
        get { return _n02_0001; }
        set { _n02_0001 = value; }
    }
    public string l02_0001
    {
        get { return _l02_0001; }
        set { _l02_0001 = value; }
    }
    public decimal? kms_0001
    {
        get { return _kms_0001; }
        set { _kms_0001 = value; }
    }
    public string por_0001
    {
        get { return _por_0001; }
        set { _por_0001 = value; }
    }
    public string esc_0001
    {
        get { return _esc_0001; }
        set { _esc_0001 = value; }
    }
    public string pla_0001
    {
        get { return _pla_0001; }
        set { _pla_0001 = value; }
    }
    public string pue_0001
    {
        get { return _pue_0001; }
        set { _pue_0001 = value; }
    }
    public string loc_0001
    {
        get { return _loc_0001; }
        set { _loc_0001 = value; }
    }
    public string cpo_0001
    {
        get { return _cpo_0001; }
        set { _cpo_0001 = value; }
    }
    public string mun_0001
    {
        get { return _mun_0001; }
        set { _mun_0001 = value; }
    }
    public string pro_0001
    {
        get { return _pro_0001; }
        set { _pro_0001 = value; }
    }
    public string npa_0001
    {
        get { return _npa_0001; }
        set { _npa_0001 = value; }
    }
    public byte sdr_0001
    {
        get { return _sdr_0001; }
        set { _sdr_0001 = value; }
    }
    public int uar_0001
    {
        get { return _uar_0001; }
        set { _uar_0001 = value; }
    }
    public string far_0001
    {
        get { return _far_0001; }
        set { _far_0001 = value; }
    }
    public string dip_0001
    {
        get { return _dip_0001; }
        set { _dip_0001 = value; }
    }
    public int uua_0001
    {
        get { return _uua_0001; }
        set { _uua_0001 = value; }
    }
    public string fua_0001
    {
        get { return _fua_0001; }
        set { _fua_0001 = value; }
    }

    #endregion

    #region Constructores

    public Relpag_0001_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001 = 0;
        _ian_0001 = "";
        _ifa_0001 = "";
        _eje_0001 = "";
        _lfr_0001 = "";
        _ord_0001 = "";
        _nfa_0001 = "";
        _fee_0001 = "";
        _ter_0001 = "";
        _nif_0001 = "";
        _ape_0001 = "";
        _nom_0001 = "";
        _iba_0001 = "";
        _bic_0001 = "";
        _bas_0001 = 0;
        _imp_0001 = 0;
        _bre_0001 = 0;
        _ret_0001 = 0;
        _liq_0001 = null;
        _acf_0001 = null;
        _sac_0001 = "";
        _cpa_0001 = "";
        _pap_0001 = "";
        _tif_0001 = "";
        _sco_0001 = "";
        _res_0001 = "";
        _nre_0001 = "";
        _con_0001 = "";
        _eco_0001 = "";
        _exp_0001 = "";
        _doc_0001 = "";
        _tvi_0001 = "";
        _nvi_0001 = "";
        _n01_0001 = "";
        _l01_0001 = "";
        _n02_0001 = "";
        _l02_0001 = "";
        _kms_0001 = null;
        _por_0001 = "";
        _esc_0001 = "";
        _pla_0001 = "";
        _pue_0001 = "";
        _loc_0001 = "";
        _cpo_0001 = "";
        _mun_0001 = "";
        _pro_0001 = "";
        _npa_0001 = "";
        _sdr_0001 = 0;
        _uar_0001 = 0;
        _far_0001 = "";
        _dip_0001 = "";
        _uua_0001 = 0;
        _fua_0001 = "";
    }

    #endregion

    #region Métodos públicos

    /// <summary>
    /// Actualiza la situación del documento relacionado
    /// </summary>
    public void Actualizar_Datos_Bancarios_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon
        };

        strSQL = string.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.relpag_0001 SET");
        strSQL += string.Concat(" iba_0001 = @iba_0001");
        strSQL += string.Concat(", bic_0001 = @bic_0001");
        strSQL += string.Concat(", dip_0001 = @dip_0001");
        strSQL += string.Concat(", uua_0001 = @uua_0001");
        strSQL += string.Concat(", fua_0001 = @fua_0001");
        strSQL += string.Concat(" WHERE ide_0001 = ", _ide_0001);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@iba_0001", _iba_0001);
        Command.Parameters.AddWithValue("@bic_0001", _bic_0001);
        Command.Parameters.AddWithValue("@dip_0001", _dip_0001);
        Command.Parameters.AddWithValue("@uua_0001", _uua_0001);
        Command.Parameters.AddWithValue("@fua_0001", _fua_0001);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza la situación del documento relacionado
    /// </summary>
    public void Actualizar_Extras()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon
        };

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.relpag_0001 SET");
        strSQL += String.Concat(" sdr_0001 = @sdr_0001");
        strSQL += String.Concat(", dip_0001 = @dip_0001");
        strSQL += String.Concat(", uua_0001 = @uua_0001");
        strSQL += String.Concat(", fua_0001 = @fua_0001");
        strSQL += String.Concat(" WHERE ide_0001 = ", _ide_0001);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@sdr_0001", _sdr_0001);
        Command.Parameters.AddWithValue("@dip_0001", _dip_0001);
        Command.Parameters.AddWithValue("@uua_0001", _uua_0001);
        Command.Parameters.AddWithValue("@fua_0001", _fua_0001);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Elimina todos los registro no marcados como relacionados
    /// </summary>
    public void Eliminar_Extras()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = "DELETE FROM " + _BaseDatos + ".dbo.relpag_0001 WHERE sdr_0001 = " + _sdr_0001
        };

        Command.ExecuteNonQuery();
    }

    #endregion

}
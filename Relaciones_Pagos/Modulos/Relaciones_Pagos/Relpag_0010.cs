using System;
using System.Data;
using System.Data.SqlClient;

public class Relpag_0010
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0010;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private Int16 _eje_0010;                                        // 2   CLAVE_UNICA 
    private int _rel_0010;                                          // 3   CLAVE_UNICA 
    private string _des_0010;                                       // 4    
    private string _fec_0010;                                       // 5    
    private string _fei_0010;                                       // 6    
    private string _fcp_0010;                                       // 7    
    private string _fgf_0010;                                       // 8    
    private Int16 _nre_0010;                                        // 9    
    private decimal _tot_0010;                                      // 10    
    private byte _sre_0010;                                         // 11    
    private string _mod_0010;                                       // 12    
    private int _uar_0010;                                          // 13    
    private string _far_0010;                                       // 14    
    private string _dip_0010;                                       // 15    
    private int _uua_0010;                                          // 16    
    private string _fua_0010;                                       // 17    

    #endregion

    #region Propiedades

    public int ide_0010
    {
        get { return _ide_0010; }
        set { _ide_0010 = value; }
    }
    public Int16 eje_0010
    {
        get { return _eje_0010; }
        set { _eje_0010 = value; }
    }
    public int rel_0010
    {
        get { return _rel_0010; }
        set { _rel_0010 = value; }
    }
    public string des_0010
    {
        get { return _des_0010; }
        set { _des_0010 = value; }
    }
    public string fec_0010
    {
        get { return _fec_0010; }
        set { _fec_0010 = value; }
    }
    public string fei_0010
    {
        get { return _fei_0010; }
        set { _fei_0010 = value; }
    }
    public string fcp_0010
    {
        get { return _fcp_0010; }
        set { _fcp_0010 = value; }
    }
    public string fgf_0010
    {
        get { return _fgf_0010; }
        set { _fgf_0010 = value; }
    }
    public Int16 nre_0010
    {
        get { return _nre_0010; }
        set { _nre_0010 = value; }
    }
    public decimal tot_0010
    {
        get { return _tot_0010; }
        set { _tot_0010 = value; }
    }
    public byte sre_0010
    {
        get { return _sre_0010; }
        set { _sre_0010 = value; }
    }
    public string mod_0010
    {
        get { return _mod_0010; }
        set { _mod_0010 = value; }
    }
    public int uar_0010
    {
        get { return _uar_0010; }
        set { _uar_0010 = value; }
    }
    public string far_0010
    {
        get { return _far_0010; }
        set { _far_0010 = value; }
    }
    public string dip_0010
    {
        get { return _dip_0010; }
        set { _dip_0010 = value; }
    }
    public int uua_0010
    {
        get { return _uua_0010; }
        set { _uua_0010 = value; }
    }
    public string fua_0010
    {
        get { return _fua_0010; }
        set { _fua_0010 = value; }
    }

    #endregion

    #region Constructores

    public Relpag_0010(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0010 = 0;
        _eje_0010 = 0;
        _rel_0010 = 0;
        _des_0010 = "";
        _fec_0010 = "";
        _fei_0010 = "";
        _fcp_0010 = "";
        _fgf_0010 = null;
        _nre_0010 = 0;
        _tot_0010 = 0;
        _sre_0010 = 0;
        _mod_0010 = "";
        _uar_0010 = 0;
        _far_0010 = "";
        _dip_0010 = "";
        _uua_0010 = 0;
        _fua_0010 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = "INSERT INTO " + _BaseDatos + ".dbo.relpag_0010 (" +
        "  eje_0010" + ", rel_0010" + ", des_0010" + ", fec_0010" + ", fei_0010" + ", fcp_0010" + ", fgf_0010" + ", nre_0010" + ", tot_0010" + ", sre_0010" +
        ", mod_0010" + ", uar_0010" + ", far_0010" + ", dip_0010" + ", uua_0010" + ", fua_0010" +
        ") OUTPUT INSERTED.ide_0010 VALUES (" +
        "  @eje_0010" + ", @rel_0010" + ", @des_0010" + ", @fec_0010" + ", @fei_0010" + ", @fcp_0010" + ", @fgf_0010" + ", @nre_0010" + ", @tot_0010" + ", @sre_0010" +
        ", @mod_0010" + ", @uar_0010" + ", @far_0010" + ", @dip_0010" + ", @uua_0010" + ", @fua_0010" +
         ")"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_0010", _eje_0010);
        Command.Parameters.AddWithValue("@rel_0010", _rel_0010);
        Command.Parameters.AddWithValue("@des_0010", _des_0010);
        Command.Parameters.AddWithValue("@fec_0010", _fec_0010);
        Command.Parameters.AddWithValue("@fei_0010", _fei_0010);
        Command.Parameters.AddWithValue("@fcp_0010", _fcp_0010);

        if (_fgf_0010 == null)
            Command.Parameters.Add("@fgf_0010", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fgf_0010", _fgf_0010);

        Command.Parameters.AddWithValue("@nre_0010", _nre_0010);
        Command.Parameters.AddWithValue("@tot_0010", _tot_0010);
        Command.Parameters.AddWithValue("@sre_0010", _sre_0010);
        Command.Parameters.AddWithValue("@mod_0010", _mod_0010);
        Command.Parameters.AddWithValue("@uar_0010", _uar_0010);
        Command.Parameters.AddWithValue("@far_0010", _far_0010);
        Command.Parameters.AddWithValue("@dip_0010", _dip_0010);
        Command.Parameters.AddWithValue("@uua_0010", _uua_0010);
        Command.Parameters.AddWithValue("@fua_0010", _fua_0010);

        Command.Connection = _sqlCon;
        _ide_0010 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.relpag_0010 WHERE ide_0010 = " + _ide_0010;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0010 = (int)dr["ide_0010"];

            if (AsignarPropiedades)
            {
                eje_0010 = (Int16)dr["eje_0010"];
                rel_0010 = (int)dr["rel_0010"];
                des_0010 = (string)dr["des_0010"];
                fec_0010 = dr["fec_0010"].ToString();
                fei_0010 = dr["fei_0010"].ToString();
                fcp_0010 = dr["fcp_0010"].ToString();
                fgf_0010 = DBNull.Value.Equals(dr["fgf_0010"]) ? (string)null : dr["fgf_0010"].ToString();
                nre_0010 = (Int16)dr["nre_0010"];
                tot_0010 = (decimal)dr["tot_0010"];
                sre_0010 = (byte)Convert.ToByte(dr["sre_0010"]);
                mod_0010 = (string)dr["mod_0010"];
                uar_0010 = (int)dr["uar_0010"];
                far_0010 = dr["far_0010"].ToString();
                dip_0010 = (string)dr["dip_0010"];
                uua_0010 = (int)dr["uua_0010"];
                fua_0010 = dr["fua_0010"].ToString();
            }
        }
        else
            ide_0010 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = "DELETE FROM " + _BaseDatos + ".dbo.relpag_0010 WHERE ide_0010 = " + _ide_0010
        };

        Command.ExecuteNonQuery();
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.relpag_0010 SET");
        strSQL += String.Concat(" eje_0010 = @eje_0010");
        strSQL += String.Concat(", rel_0010 = @rel_0010");
        strSQL += String.Concat(", des_0010 = @des_0010");
        strSQL += String.Concat(", fec_0010 = @fec_0010");
        strSQL += String.Concat(", fei_0010 = @fei_0010");
        strSQL += String.Concat(", fcp_0010 = @fcp_0010");
        strSQL += String.Concat(", fgf_0010 = @fgf_0010");
        strSQL += String.Concat(", nre_0010 = @nre_0010");
        strSQL += String.Concat(", tot_0010 = @tot_0010");
        strSQL += String.Concat(", sre_0010 = @sre_0010");
        strSQL += String.Concat(", mod_0010 = @mod_0010");
        strSQL += String.Concat(", dip_0010 = @dip_0010");
        strSQL += String.Concat(", uua_0010 = @uua_0010");
        strSQL += String.Concat(", fua_0010 = @fua_0010");
        strSQL += String.Concat(" WHERE ide_0010 = ", _ide_0010);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_0010", _eje_0010);
        Command.Parameters.AddWithValue("@rel_0010", _rel_0010);
        Command.Parameters.AddWithValue("@des_0010", _des_0010);
        Command.Parameters.AddWithValue("@fec_0010", _fec_0010);
        Command.Parameters.AddWithValue("@fei_0010", _fei_0010);
        Command.Parameters.AddWithValue("@fcp_0010", _fcp_0010);

        if (_fgf_0010 == null)
            Command.Parameters.Add("@fgf_0010", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fgf_0010", _fgf_0010);

        Command.Parameters.AddWithValue("@nre_0010", _nre_0010);
        Command.Parameters.AddWithValue("@tot_0010", _tot_0010);
        Command.Parameters.AddWithValue("@sre_0010", _sre_0010);
        Command.Parameters.AddWithValue("@mod_0010", _mod_0010);
        Command.Parameters.AddWithValue("@dip_0010", _dip_0010);
        Command.Parameters.AddWithValue("@uua_0010", _uua_0010);
        Command.Parameters.AddWithValue("@fua_0010", _fua_0010);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: eje_0010 rel_0010
    public void Cargar_eje_0010_rel_0010(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.relpag_0010 WHERE eje_0010 = " + _eje_0010 + " AND rel_0010 = " + _rel_0010 + "";

        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0010 = (int)dr["ide_0010"];

            if (AsignarPropiedades)
            {
                eje_0010 = (Int16)dr["eje_0010"];
                rel_0010 = (int)dr["rel_0010"];
                des_0010 = (string)dr["des_0010"];
                fec_0010 = dr["fec_0010"].ToString();
                fei_0010 = dr["fei_0010"].ToString();
                fcp_0010 = dr["fcp_0010"].ToString();
                fgf_0010 = DBNull.Value.Equals(dr["fgf_0010"]) ? (string)null : dr["fgf_0010"].ToString();
                nre_0010 = (Int16)dr["nre_0010"];
                tot_0010 = (decimal)dr["tot_0010"];
                sre_0010 = (byte)Convert.ToByte(dr["sre_0010"]);
                mod_0010 = (string)dr["mod_0010"];
                uar_0010 = (int)dr["uar_0010"];
                far_0010 = dr["far_0010"].ToString();
                dip_0010 = (string)dr["dip_0010"];
                uua_0010 = (int)dr["uua_0010"];
                fua_0010 = dr["fua_0010"].ToString();
            }
        }
        else
            ide_0010 = 0;

        dr.Close();
    }

    #endregion

}
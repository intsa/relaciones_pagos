using System;
using System.Data.SqlClient;

public class Relpag_0010_1_Extras
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0010_1;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_0010;                                          // 2   CLAVE_UNICA 
    private int _ide_0001;                                          // 3   CLAVE_UNICA 
    private string _obs_0010_1;                                     // 4    
    private byte _sdo_0010_1;                                       // 5    
    private int _uar_0010_1;                                        // 6    
    private string _far_0010_1;                                     // 7    
    private string _dip_0010_1;                                     // 8    
    private int _uua_0010_1;                                        // 9    
    private string _fua_0010_1;                                     // 10    

    #endregion

    #region Propiedades

    public int ide_0010_1
    {
        get { return _ide_0010_1; }
        set { _ide_0010_1 = value; }
    }
    public int ide_0010
    {
        get { return _ide_0010; }
        set { _ide_0010 = value; }
    }
    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public string obs_0010_1
    {
        get { return _obs_0010_1; }
        set { _obs_0010_1 = value; }
    }
    public byte sdo_0010_1
    {
        get { return _sdo_0010_1; }
        set { _sdo_0010_1 = value; }
    }
    public int uar_0010_1
    {
        get { return _uar_0010_1; }
        set { _uar_0010_1 = value; }
    }
    public string far_0010_1
    {
        get { return _far_0010_1; }
        set { _far_0010_1 = value; }
    }
    public string dip_0010_1
    {
        get { return _dip_0010_1; }
        set { _dip_0010_1 = value; }
    }
    public int uua_0010_1
    {
        get { return _uua_0010_1; }
        set { _uua_0010_1 = value; }
    }
    public string fua_0010_1
    {
        get { return _fua_0010_1; }
        set { _fua_0010_1 = value; }
    }

    #endregion

    #region Constructores

    public Relpag_0010_1_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0010_1 = 0;
        _ide_0010 = 0;
        _ide_0001 = 0;
        _obs_0010_1 = "";
        _sdo_0010_1 = 0;
        _uar_0010_1 = 0;
        _far_0010_1 = "";
        _dip_0010_1 = "";
        _uua_0010_1 = 0;
        _fua_0010_1 = "";
    }

    #endregion

    #region Metodos Públicos

    /// <summary>
    /// Actualiza las observaciones
    /// </summary>
    public void Actualizar_Extras()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon
        };

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.relpag_0010_1 SET");
        strSQL += String.Concat(" obs_0010_1 = @obs_0010_1");
        strSQL += String.Concat(", dip_0010_1 = @dip_0010_1");
        strSQL += String.Concat(", uua_0010_1 = @uua_0010_1");
        strSQL += String.Concat(", fua_0010_1 = @fua_0010_1");
        strSQL += String.Concat(" WHERE ide_0010_1 = ", _ide_0010_1);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@obs_0010_1", _obs_0010_1);
        Command.Parameters.AddWithValue("@dip_0010_1", _dip_0010_1);
        Command.Parameters.AddWithValue("@uua_0010_1", _uua_0010_1);
        Command.Parameters.AddWithValue("@fua_0010_1", _fua_0010_1);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza la situación del documento
    /// </summary>
    public void Actualizar_Situacion_Extras()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon
        };

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.relpag_0010_1 SET");
        strSQL += String.Concat(" sdo_0010_1 = @sdo_0010_1");
        strSQL += String.Concat(", dip_0010_1 = @dip_0010_1");
        strSQL += String.Concat(", uua_0010_1 = @uua_0010_1");
        strSQL += String.Concat(", fua_0010_1 = @fua_0010_1");
        strSQL += String.Concat(" WHERE ide_0010_1 = ", _ide_0010_1);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@sdo_0010_1", _sdo_0010_1);
        Command.Parameters.AddWithValue("@dip_0010_1", _dip_0010_1);
        Command.Parameters.AddWithValue("@uua_0010_1", _uua_0010_1);
        Command.Parameters.AddWithValue("@fua_0010_1", _fua_0010_1);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }


    #endregion

}
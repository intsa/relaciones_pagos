using System;
using System.Data;
using System.Data.SqlClient;

public class Relpag_0020
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0020;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_0010;                                          // 2    
    private string _irp_0020;                                       // 3    
    private short _eje_0020;                                        // 4   CLAVE_UNICA 
    private int _rel_0020;                                          // 5   CLAVE_UNICA 
    private string _des_0020;                                       // 6    
    private string _fec_0020;                                       // 7    
    private int _ide_1000;                                          // 8    
    private string _iba_0020;                                       // 9    
    private string _bic_0020;                                       // 10    
    private string _cta_0020;                                       // 11    
    private string _sor_0020;                                       // 12    
    private string _ior_0020;                                       // 13    
    private int _ide_1005;                                          // 14    
    private string _fgs_0020;                                       // 15    
    private string _fco_0020;                                       // 16    
    private short _nre_0020;                                        // 17    
    private decimal _tot_0020;                                      // 18    
    private byte _sre_0020;                                         // 19    
    private string _sep_0020;                                       // 20    
    private int _uar_0020;                                          // 21    
    private string _far_0020;                                       // 22    
    private string _dip_0020;                                       // 23    
    private int _uua_0020;                                          // 24    
    private string _fua_0020;                                       // 25    

    #endregion

    #region Propiedades

    public int ide_0020
    {
        get { return _ide_0020; }
        set { _ide_0020 = value; }
    }
    public int ide_0010
    {
        get { return _ide_0010; }
        set { _ide_0010 = value; }
    }
    public string irp_0020
    {
        get { return _irp_0020; }
        set { _irp_0020 = value; }
    }
    public short eje_0020
    {
        get { return _eje_0020; }
        set { _eje_0020 = value; }
    }
    public int rel_0020
    {
        get { return _rel_0020; }
        set { _rel_0020 = value; }
    }
    public string des_0020
    {
        get { return _des_0020; }
        set { _des_0020 = value; }
    }
    public string fec_0020
    {
        get { return _fec_0020; }
        set { _fec_0020 = value; }
    }
    public int ide_1000
    {
        get { return _ide_1000; }
        set { _ide_1000 = value; }
    }
    public string iba_0020
    {
        get { return _iba_0020; }
        set { _iba_0020 = value; }
    }
    public string bic_0020
    {
        get { return _bic_0020; }
        set { _bic_0020 = value; }
    }
    public string cta_0020
    {
        get { return _cta_0020; }
        set { _cta_0020 = value; }
    }
    public string sor_0020
    {
        get { return _sor_0020; }
        set { _sor_0020 = value; }
    }
    public string ior_0020
    {
        get { return _ior_0020; }
        set { _ior_0020 = value; }
    }
    public int ide_1005
    {
        get { return _ide_1005; }
        set { _ide_1005 = value; }
    }
    public string fgs_0020
    {
        get { return _fgs_0020; }
        set { _fgs_0020 = value; }
    }
    public string fco_0020
    {
        get { return _fco_0020; }
        set { _fco_0020 = value; }
    }
    public short nre_0020
    {
        get { return _nre_0020; }
        set { _nre_0020 = value; }
    }
    public decimal tot_0020
    {
        get { return _tot_0020; }
        set { _tot_0020 = value; }
    }
    public byte sre_0020
    {
        get { return _sre_0020; }
        set { _sre_0020 = value; }
    }
    public string sep_0020
    {
        get { return _sep_0020; }
        set { _sep_0020 = value; }
    }
    public int uar_0020
    {
        get { return _uar_0020; }
        set { _uar_0020 = value; }
    }
    public string far_0020
    {
        get { return _far_0020; }
        set { _far_0020 = value; }
    }
    public string dip_0020
    {
        get { return _dip_0020; }
        set { _dip_0020 = value; }
    }
    public int uua_0020
    {
        get { return _uua_0020; }
        set { _uua_0020 = value; }
    }
    public string fua_0020
    {
        get { return _fua_0020; }
        set { _fua_0020 = value; }
    }

    #endregion

    #region Constructores

    public Relpag_0020(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0020 = 0;
        _ide_0010 = 0;
        _irp_0020 = "";
        _eje_0020 = 0;
        _rel_0020 = 0;
        _des_0020 = "";
        _fec_0020 = "";
        _ide_1000 = 0;
        _iba_0020 = "";
        _bic_0020 = "";
        _cta_0020 = "";
        _sor_0020 = "";
        _ior_0020 = "";
        _ide_1005 = 0;
        _fgs_0020 = null;
        _fco_0020 = null;
        _nre_0020 = 0;
        _tot_0020 = 0;
        _sre_0020 = 0;
        _sep_0020 = "";
        _uar_0020 = 0;
        _far_0020 = "";
        _dip_0020 = "";
        _uua_0020 = 0;
        _fua_0020 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = "INSERT INTO " + _BaseDatos + ".dbo.relpag_0020 (" +
        "  ide_0010" + ", irp_0020" + ", eje_0020" + ", rel_0020" + ", des_0020" + ", fec_0020" + ", ide_1000" + ", iba_0020" + ", bic_0020" + ", cta_0020" +
        ", sor_0020" + ", ior_0020" + ", ide_1005" + ", fgs_0020" + ", fco_0020" + ", nre_0020" + ", tot_0020" + ", sre_0020" + ", sep_0020" + ", uar_0020" +
        ", far_0020" + ", dip_0020" + ", uua_0020" + ", fua_0020" +
        ") OUTPUT INSERTED.ide_0020 VALUES (" +
        "  @ide_0010" + ", @irp_0020" + ", @eje_0020" + ", @rel_0020" + ", @des_0020" + ", @fec_0020" + ", @ide_1000" + ", @iba_0020" + ", @bic_0020" + ", @cta_0020" +
        ", @sor_0020" + ", @ior_0020" + ", @ide_1005" + ", @fgs_0020" + ", @fco_0020" + ", @nre_0020" + ", @tot_0020" + ", @sre_0020" + ", @sep_0020" + ", @uar_0020" +
        ", @far_0020" + ", @dip_0020" + ", @uua_0020" + ", @fua_0020" +
         ")"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0010", _ide_0010);
        Command.Parameters.AddWithValue("@irp_0020", _irp_0020);
        Command.Parameters.AddWithValue("@eje_0020", _eje_0020);
        Command.Parameters.AddWithValue("@rel_0020", _rel_0020);
        Command.Parameters.AddWithValue("@des_0020", _des_0020);
        Command.Parameters.AddWithValue("@fec_0020", _fec_0020);
        Command.Parameters.AddWithValue("@ide_1000", _ide_1000);
        Command.Parameters.AddWithValue("@iba_0020", _iba_0020);
        Command.Parameters.AddWithValue("@bic_0020", _bic_0020);
        Command.Parameters.AddWithValue("@cta_0020", _cta_0020);
        Command.Parameters.AddWithValue("@sor_0020", _sor_0020);
        Command.Parameters.AddWithValue("@ior_0020", _ior_0020);
        Command.Parameters.AddWithValue("@ide_1005", _ide_1005);

        if (_fgs_0020 == null)
            Command.Parameters.Add("@fgs_0020", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fgs_0020", _fgs_0020);

        if (_fco_0020 == null)
            Command.Parameters.Add("@fco_0020", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fco_0020", _fco_0020);

        Command.Parameters.AddWithValue("@nre_0020", _nre_0020);
        Command.Parameters.AddWithValue("@tot_0020", _tot_0020);
        Command.Parameters.AddWithValue("@sre_0020", _sre_0020);

        if (_sep_0020 == null)
            Command.Parameters.Add("@sep_0020", SqlDbType.NText).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@sep_0020", _sep_0020);

        Command.Parameters.AddWithValue("@uar_0020", _uar_0020);
        Command.Parameters.AddWithValue("@far_0020", _far_0020);
        Command.Parameters.AddWithValue("@dip_0020", _dip_0020);
        Command.Parameters.AddWithValue("@uua_0020", _uua_0020);
        Command.Parameters.AddWithValue("@fua_0020", _fua_0020);
        Command.Connection = _sqlCon;

        _ide_0020 = (int)Command.ExecuteScalar();

        Command = null;

    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {

        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "Select * FROM " + _BaseDatos + ".dbo.relpag_0020 WHERE ide_0020 = " + _ide_0020;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0020 = (int)dr["ide_0020"];

            if (AsignarPropiedades)
            {
                ide_0010 = (int)dr["ide_0010"];
                irp_0020 = (string)dr["irp_0020"];
                eje_0020 = (short)dr["eje_0020"];
                rel_0020 = (int)dr["rel_0020"];
                des_0020 = (string)dr["des_0020"];
                fec_0020 = dr["fec_0020"].ToString();
                ide_1000 = (int)dr["ide_1000"];
                iba_0020 = (string)dr["iba_0020"];
                bic_0020 = (string)dr["bic_0020"];
                cta_0020 = (string)dr["cta_0020"];
                sor_0020 = (string)dr["sor_0020"];
                ior_0020 = (string)dr["ior_0020"];
                ide_1005 = (int)dr["ide_1005"];
                fgs_0020 = DBNull.Value.Equals(dr["fgs_0020"]) ? (string)null : dr["fgs_0020"].ToString();
                fco_0020 = DBNull.Value.Equals(dr["fco_0020"]) ? (string)null : dr["fco_0020"].ToString();
                nre_0020 = (short)dr["nre_0020"];
                tot_0020 = (decimal)dr["tot_0020"];
                sre_0020 = (byte)Convert.ToByte(dr["sre_0020"]);
                sep_0020 = DBNull.Value.Equals(dr["sep_0020"]) ? (string)null : (string)dr["sep_0020"];
                uar_0020 = (int)dr["uar_0020"];
                far_0020 = dr["far_0020"].ToString();
                dip_0020 = (string)dr["dip_0020"];
                uua_0020 = (int)dr["uua_0020"];
                fua_0020 = dr["fua_0020"].ToString();
            }
        }
        else
        {
            ide_0020 = 0;
        }

        dr.Close();

        Command = null;

    }

    //METODO ELIMINAR
    public void Eliminar()
    {

        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;
        Command.CommandText = "DELETE FROM " + _BaseDatos + ".dbo.relpag_0020 WHERE ide_0020 = " + _ide_0020;
        Command.ExecuteNonQuery();

        Command = null;

    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {

        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.relpag_0020 Set ");
        strSQL += String.Concat("  ide_0010 = @ide_0010");
        strSQL += String.Concat(", irp_0020 = @irp_0020");
        strSQL += String.Concat(", eje_0020 = @eje_0020");
        strSQL += String.Concat(", rel_0020 = @rel_0020");
        strSQL += String.Concat(", des_0020 = @des_0020");
        strSQL += String.Concat(", fec_0020 = @fec_0020");
        strSQL += String.Concat(", ide_1000 = @ide_1000");
        strSQL += String.Concat(", iba_0020 = @iba_0020");
        strSQL += String.Concat(", bic_0020 = @bic_0020");
        strSQL += String.Concat(", cta_0020 = @cta_0020");
        strSQL += String.Concat(", sor_0020 = @sor_0020");
        strSQL += String.Concat(", ior_0020 = @ior_0020");
        strSQL += String.Concat(", ide_1005 = @ide_1005");
        strSQL += String.Concat(", fgs_0020 = @fgs_0020");
        strSQL += String.Concat(", fco_0020 = @fco_0020");
        strSQL += String.Concat(", nre_0020 = @nre_0020");
        strSQL += String.Concat(", tot_0020 = @tot_0020");
        strSQL += String.Concat(", sre_0020 = @sre_0020");
        strSQL += String.Concat(", sep_0020 = @sep_0020");
        strSQL += String.Concat(", dip_0020 = @dip_0020");
        strSQL += String.Concat(", uua_0020 = @uua_0020");
        strSQL += String.Concat(", fua_0020 = @fua_0020");
        strSQL += String.Concat(" WHERE ide_0020 = ", _ide_0020);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0010", _ide_0010);
        Command.Parameters.AddWithValue("@irp_0020", _irp_0020);
        Command.Parameters.AddWithValue("@eje_0020", _eje_0020);
        Command.Parameters.AddWithValue("@rel_0020", _rel_0020);
        Command.Parameters.AddWithValue("@des_0020", _des_0020);
        Command.Parameters.AddWithValue("@fec_0020", _fec_0020);
        Command.Parameters.AddWithValue("@ide_1000", _ide_1000);
        Command.Parameters.AddWithValue("@iba_0020", _iba_0020);
        Command.Parameters.AddWithValue("@bic_0020", _bic_0020);
        Command.Parameters.AddWithValue("@cta_0020", _cta_0020);
        Command.Parameters.AddWithValue("@sor_0020", _sor_0020);
        Command.Parameters.AddWithValue("@ior_0020", _ior_0020);
        Command.Parameters.AddWithValue("@ide_1005", _ide_1005);

        if (_fgs_0020 == null)
            Command.Parameters.Add("@fgs_0020", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fgs_0020", _fgs_0020);

        if (_fco_0020 == null)
            Command.Parameters.Add("@fco_0020", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fco_0020", _fco_0020);

        Command.Parameters.AddWithValue("@nre_0020", _nre_0020);
        Command.Parameters.AddWithValue("@tot_0020", _tot_0020);
        Command.Parameters.AddWithValue("@sre_0020", _sre_0020);

        if (_sep_0020 == null)
            Command.Parameters.Add("@sep_0020", SqlDbType.NText).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@sep_0020", _sep_0020);

        Command.Parameters.AddWithValue("@dip_0020", _dip_0020);
        Command.Parameters.AddWithValue("@uua_0020", _uua_0020);
        Command.Parameters.AddWithValue("@fua_0020", _fua_0020);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: eje_0020 rel_0020
    public void Cargar_eje_0020_rel_0020(bool AsignarPropiedades = true)
    {

        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "Select * FROM " + _BaseDatos + ".dbo.relpag_0020 WHERE eje_0020 = " + _eje_0020 + " AND rel_0020 = " + _rel_0020 + "";

        dr = Command.ExecuteReader();

        if (dr.Read())
        {

            ide_0020 = (int)dr["ide_0020"];

            if (AsignarPropiedades)
            {
                ide_0010 = (int)dr["ide_0010"];
                irp_0020 = (string)dr["irp_0020"];
                eje_0020 = (short)dr["eje_0020"];
                rel_0020 = (int)dr["rel_0020"];
                des_0020 = (string)dr["des_0020"];
                fec_0020 = dr["fec_0020"].ToString();
                ide_1000 = (int)dr["ide_1000"];
                iba_0020 = (string)dr["iba_0020"];
                bic_0020 = (string)dr["bic_0020"];
                cta_0020 = (string)dr["cta_0020"];
                sor_0020 = (string)dr["sor_0020"];
                ior_0020 = (string)dr["ior_0020"];
                ide_1005 = (int)dr["ide_1005"];
                fgs_0020 = DBNull.Value.Equals(dr["fgs_0020"]) ? (string)null : dr["fgs_0020"].ToString();
                fco_0020 = DBNull.Value.Equals(dr["fco_0020"]) ? (string)null : dr["fco_0020"].ToString();
                nre_0020 = (short)dr["nre_0020"];
                tot_0020 = (decimal)dr["tot_0020"];
                sre_0020 = (byte)Convert.ToByte(dr["sre_0020"]);
                sep_0020 = DBNull.Value.Equals(dr["sep_0020"]) ? (string)null : (string)dr["sep_0020"];
                uar_0020 = (int)dr["uar_0020"];
                far_0020 = dr["far_0020"].ToString();
                dip_0020 = (string)dr["dip_0020"];
                uua_0020 = (int)dr["uua_0020"];
                fua_0020 = dr["fua_0020"].ToString();
            }
        }
        else
        {
            ide_0020 = 0;
        }

        dr.Close();

        Command = null;

    }

    #endregion

}
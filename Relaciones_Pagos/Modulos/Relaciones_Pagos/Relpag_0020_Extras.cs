using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

public class Relpag_0020_Extras
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0020;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_0010;                                          // 2    
    private string _irp_0020;                                       // 3    
    private short _eje_0020;                                        // 4   CLAVE_UNICA 
    private int _rel_0020;                                          // 5   CLAVE_UNICA 
    private string _des_0020;                                       // 6    
    private string _fec_0020;                                       // 7    
    private int _ide_1000;                                          // 8    
    private string _iba_0020;                                       // 9    
    private string _bic_0020;                                       // 10    
    private string _cta_0020;                                       // 11    
    private string _sor_0020;                                       // 12    
    private string _ior_0020;                                       // 13    
    private int _ide_1005;                                          // 14    
    private string _fgs_0020;                                       // 15    
    private string _fco_0020;                                       // 16    
    private short _nre_0020;                                        // 17    
    private decimal _tot_0020;                                      // 18    
    private byte _sre_0020;                                         // 19    
    private string _sep_0020;                                       // 20    
    private int _uar_0020;                                          // 21    
    private string _far_0020;                                       // 22    
    private string _dip_0020;                                       // 23    
    private int _uua_0020;                                          // 24    
    private string _fua_0020;                                       // 25    

    #endregion

    #region Propiedades

    public int ide_0020
    {
        get { return _ide_0020; }
        set { _ide_0020 = value; }
    }
    public int ide_0010
    {
        get { return _ide_0010; }
        set { _ide_0010 = value; }
    }
    public string irp_0020
    {
        get { return _irp_0020; }
        set { _irp_0020 = value; }
    }
    public short eje_0020
    {
        get { return _eje_0020; }
        set { _eje_0020 = value; }
    }
    public int rel_0020
    {
        get { return _rel_0020; }
        set { _rel_0020 = value; }
    }
    public string des_0020
    {
        get { return _des_0020; }
        set { _des_0020 = value; }
    }
    public string fec_0020
    {
        get { return _fec_0020; }
        set { _fec_0020 = value; }
    }
    public int ide_1000
    {
        get { return _ide_1000; }
        set { _ide_1000 = value; }
    }
    public string iba_0020
    {
        get { return _iba_0020; }
        set { _iba_0020 = value; }
    }
    public string bic_0020
    {
        get { return _bic_0020; }
        set { _bic_0020 = value; }
    }
    public string cta_0020
    {
        get { return _cta_0020; }
        set { _cta_0020 = value; }
    }
    public string sor_0020
    {
        get { return _sor_0020; }
        set { _sor_0020 = value; }
    }
    public string ior_0020
    {
        get { return _ior_0020; }
        set { _ior_0020 = value; }
    }
    public int ide_1005
    {
        get { return _ide_1005; }
        set { _ide_1005 = value; }
    }
    public string fgs_0020
    {
        get { return _fgs_0020; }
        set { _fgs_0020 = value; }
    }
    public string fco_0020
    {
        get { return _fco_0020; }
        set { _fco_0020 = value; }
    }
    public short nre_0020
    {
        get { return _nre_0020; }
        set { _nre_0020 = value; }
    }
    public decimal tot_0020
    {
        get { return _tot_0020; }
        set { _tot_0020 = value; }
    }
    public byte sre_0020
    {
        get { return _sre_0020; }
        set { _sre_0020 = value; }
    }
    public string sep_0020
    {
        get { return _sep_0020; }
        set { _sep_0020 = value; }
    }
    public int uar_0020
    {
        get { return _uar_0020; }
        set { _uar_0020 = value; }
    }
    public string far_0020
    {
        get { return _far_0020; }
        set { _far_0020 = value; }
    }
    public string dip_0020
    {
        get { return _dip_0020; }
        set { _dip_0020 = value; }
    }
    public int uua_0020
    {
        get { return _uua_0020; }
        set { _uua_0020 = value; }
    }
    public string fua_0020
    {
        get { return _fua_0020; }
        set { _fua_0020 = value; }
    }

    #endregion

    #region Constructores

    public Relpag_0020_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0020 = 0;
        _ide_0010 = 0;
        _irp_0020 = "";
        _eje_0020 = 0;
        _rel_0020 = 0;
        _des_0020 = "";
        _fec_0020 = "";
        _ide_1000 = 0;
        _iba_0020 = "";
        _bic_0020 = "";
        _cta_0020 = "";
        _sor_0020 = "";
        _ior_0020 = "";
        _ide_1005 = 0;
        _fgs_0020 = null;
        _fco_0020 = null;
        _nre_0020 = 0;
        _tot_0020 = 0;
        _sre_0020 = 0;
        _sep_0020 = "";
        _uar_0020 = 0;
        _far_0020 = "";
        _dip_0020 = "";
        _uua_0020 = 0;
        _fua_0020 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO ACTUALIZAR
    public void Actualizar_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = string.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.relpag_0020 SET");
        strSQL += string.Concat(" des_0020 = @des_0020");
        strSQL += string.Concat(", ide_1000 = @ide_1000");
        strSQL += string.Concat(", iba_0020 = @iba_0020");
        strSQL += string.Concat(", bic_0020 = @bic_0020");
        strSQL += string.Concat(", cta_0020 = @cta_0020");
        strSQL += string.Concat(", sor_0020 = @sor_0020");
        strSQL += string.Concat(", ior_0020 = @ior_0020");
        strSQL += string.Concat(", fgs_0020 = @fgs_0020");
        strSQL += string.Concat(", nre_0020 = @nre_0020");
        strSQL += string.Concat(", sep_0020 = @sep_0020");
        strSQL += string.Concat(", dip_0020 = @dip_0020");
        strSQL += string.Concat(", uua_0020 = @uua_0020");
        strSQL += string.Concat(", fua_0020 = @fua_0020");
        strSQL += string.Concat(" WHERE ide_0020 = ", _ide_0020);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@des_0020", _des_0020);
        Command.Parameters.AddWithValue("@ide_1000", _ide_1000);
        Command.Parameters.AddWithValue("@iba_0020", _iba_0020);
        Command.Parameters.AddWithValue("@bic_0020", _bic_0020);
        Command.Parameters.AddWithValue("@cta_0020", _cta_0020);
        Command.Parameters.AddWithValue("@sor_0020", _sor_0020);
        Command.Parameters.AddWithValue("@ior_0020", _ior_0020);

        if (_fgs_0020 == null)
            Command.Parameters.Add("@fgs_0020", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fgs_0020", _fgs_0020);

        Command.Parameters.AddWithValue("@nre_0020", _nre_0020);

        if (_sep_0020 == null)
            Command.Parameters.Add("@sep_0020", SqlDbType.NText).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@sep_0020", _sep_0020);

        Command.Parameters.AddWithValue("@dip_0020", _dip_0020);
        Command.Parameters.AddWithValue("@uua_0020", _uua_0020);
        Command.Parameters.AddWithValue("@fua_0020", _fua_0020);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza campos del registro
    /// </summary>
    public void Actualizar_Extras(short Documentos, decimal Importe)
    {
        string Cadena_Sql;
        string Cadena_Update = $"UPDATE {_BaseDatos}.dbo.Relpag_0020";
        string Cadena_Valores;
        string Cadena_Where;

        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon
        };

        Cadena_Valores = $" SET nre_0020 = nre_0020 + {Documentos},";
        Cadena_Valores += $" tot_0020 = tot_0020 + {Convert.ToString(Importe).Replace(",", ".")}";
        Cadena_Where = " WHERE ide_0020 = " + _ide_0020;
        Cadena_Sql = Cadena_Update + Cadena_Valores + Cadena_Where;

        Command.CommandText = Cadena_Sql;

        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza los totales de la relación
    /// </summary>
    public void Actualizar_Totales_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = string.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.relpag_0020 SET");
        strSQL += string.Concat(" nre_0020 = @nre_0020");
        strSQL += string.Concat(", tot_0020 = @tot_0020");
        strSQL += string.Concat(" WHERE ide_0020 = ", _ide_0020);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@nre_0020", _nre_0020);
        Command.Parameters.AddWithValue("@tot_0020", _tot_0020);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza datos registro
    /// · Fecha generación MODELO-997
    /// · Situación relación
    /// · Contenido del MODELO-997
    /// </summary>
    public void Actualizar_Situacion_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon
        };

        strSQL = string.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.relpag_0020 SET");
        strSQL += string.Concat(" fgs_0020 = @fgs_0020");
        strSQL += string.Concat(", sre_0020 = @sre_0020");
        strSQL += string.Concat(", sep_0020 = @sep_0020");
        strSQL += string.Concat(" WHERE ide_0020 = ", _ide_0020);

        // ASIGNACIÓN DE PARÁMETROS
        if (_fgs_0020 == null)
            Command.Parameters.Add("@fgs_0020", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fgs_0020", fgs_0020);

        Command.Parameters.AddWithValue("@sre_0020", _sre_0020);

        if (_sep_0020 == null)
            Command.Parameters.Add("@sep_0020", SqlDbType.Xml).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@sep_0020", _sep_0020);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion

}
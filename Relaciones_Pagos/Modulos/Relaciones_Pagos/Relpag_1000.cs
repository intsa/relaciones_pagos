using System;
using System.Data.SqlClient;

public class Relpag_1000
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1000;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private string _cod_1000;                                       // 2   CLAVE_UNICA 
    private string _den_1000;                                       // 3    
    private string _iba_1000;                                       // 4    
    private string _bic_1000;                                       // 5    
    private string _cta_1000;                                       // 6    
    private string _sor_1000;                                       // 7    
    private string _ior_1000;                                       // 8    
    private int _uar_1000;                                          // 9    
    private string _far_1000;                                       // 10    
    private string _dip_1000;                                       // 11    
    private int _uua_1000;                                          // 12    
    private string _fua_1000;                                       // 13    

    #endregion

    #region Propiedades

    public int ide_1000
    {
        get { return _ide_1000; }
        set { _ide_1000 = value; }
    }
    public string cod_1000
    {
        get { return _cod_1000; }
        set { _cod_1000 = value; }
    }
    public string den_1000
    {
        get { return _den_1000; }
        set { _den_1000 = value; }
    }
    public string iba_1000
    {
        get { return _iba_1000; }
        set { _iba_1000 = value; }
    }
    public string bic_1000
    {
        get { return _bic_1000; }
        set { _bic_1000 = value; }
    }
    public string cta_1000
    {
        get { return _cta_1000; }
        set { _cta_1000 = value; }
    }
    public string sor_1000
    {
        get { return _sor_1000; }
        set { _sor_1000 = value; }
    }
    public string ior_1000
    {
        get { return _ior_1000; }
        set { _ior_1000 = value; }
    }
    public int uar_1000
    {
        get { return _uar_1000; }
        set { _uar_1000 = value; }
    }
    public string far_1000
    {
        get { return _far_1000; }
        set { _far_1000 = value; }
    }
    public string dip_1000
    {
        get { return _dip_1000; }
        set { _dip_1000 = value; }
    }
    public int uua_1000
    {
        get { return _uua_1000; }
        set { _uua_1000 = value; }
    }
    public string fua_1000
    {
        get { return _fua_1000; }
        set { _fua_1000 = value; }
    }

    #endregion

    #region Constructores

    public Relpag_1000(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1000 = 0;
        _cod_1000 = "";
        _den_1000 = "";
        _iba_1000 = "";
        _bic_1000 = "";
        _cta_1000 = "";
        _sor_1000 = "";
        _ior_1000 = "";
        _uar_1000 = 0;
        _far_1000 = "";
        _dip_1000 = "";
        _uua_1000 = 0;
        _fua_1000 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = "INSERT INTO " + _BaseDatos + ".dbo.relpag_1000 (" +
        "  cod_1000" + ", den_1000" + ", iba_1000" + ", bic_1000" + ", cta_1000" + ", sor_1000" + ", ior_1000" + ", uar_1000" + ", far_1000" + ", dip_1000" +
        ", uua_1000" + ", fua_1000" +
        ") OUTPUT INSERTED.ide_1000 VALUES (" +
        "  @cod_1000" + ", @den_1000" + ", @iba_1000" + ", @bic_1000" + ", @cta_1000" + ", @sor_1000" + ", @ior_1000" + ", @uar_1000" + ", @far_1000" + ", @dip_1000" +
        ", @uua_1000" + ", @fua_1000" +
         ")"
        };

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_1000", _cod_1000);
        Command.Parameters.AddWithValue("@den_1000", _den_1000);
        Command.Parameters.AddWithValue("@iba_1000", _iba_1000);
        Command.Parameters.AddWithValue("@bic_1000", _bic_1000);
        Command.Parameters.AddWithValue("@cta_1000", _cta_1000);
        Command.Parameters.AddWithValue("@sor_1000", _sor_1000);
        Command.Parameters.AddWithValue("@ior_1000", _ior_1000);
        Command.Parameters.AddWithValue("@uar_1000", _uar_1000);
        Command.Parameters.AddWithValue("@far_1000", _far_1000);
        Command.Parameters.AddWithValue("@dip_1000", _dip_1000);
        Command.Parameters.AddWithValue("@uua_1000", _uua_1000);
        Command.Parameters.AddWithValue("@fua_1000", _fua_1000);

        Command.Connection = _sqlCon;
        _ide_1000 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.relpag_1000 WHERE ide_1000 = " + _ide_1000;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1000 = (int)dr["ide_1000"];

            if (AsignarPropiedades)
            {
                cod_1000 = (string)dr["cod_1000"];
                den_1000 = (string)dr["den_1000"];
                iba_1000 = (string)dr["iba_1000"];
                bic_1000 = (string)dr["bic_1000"];
                cta_1000 = (string)dr["cta_1000"];
                sor_1000 = (string)dr["sor_1000"];
                ior_1000 = (string)dr["ior_1000"];
                uar_1000 = (int)dr["uar_1000"];
                far_1000 = dr["far_1000"].ToString();
                dip_1000 = (string)dr["dip_1000"];
                uua_1000 = (int)dr["uua_1000"];
                fua_1000 = dr["fua_1000"].ToString();
            }
        }
        else
        {
            ide_1000 = 0;
        }

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = "DELETE FROM " + _BaseDatos + ".dbo.relpag_1000 WHERE ide_1000 = " + _ide_1000
        };

        Command.ExecuteNonQuery();
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon
        };

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.relpag_1000 SET");
        strSQL += String.Concat(" cod_1000 = @cod_1000");
        strSQL += String.Concat(", den_1000 = @den_1000");
        strSQL += String.Concat(", iba_1000 = @iba_1000");
        strSQL += String.Concat(", bic_1000 = @bic_1000");
        strSQL += String.Concat(", cta_1000 = @cta_1000");
        strSQL += String.Concat(", sor_1000 = @sor_1000");
        strSQL += String.Concat(", ior_1000 = @ior_1000");
        strSQL += String.Concat(", dip_1000 = @dip_1000");
        strSQL += String.Concat(", uua_1000 = @uua_1000");
        strSQL += String.Concat(", fua_1000 = @fua_1000");
        strSQL += String.Concat(" WHERE ide_1000 = ", _ide_1000);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_1000", _cod_1000);
        Command.Parameters.AddWithValue("@den_1000", _den_1000);
        Command.Parameters.AddWithValue("@iba_1000", _iba_1000);
        Command.Parameters.AddWithValue("@bic_1000", _bic_1000);
        Command.Parameters.AddWithValue("@cta_1000", _cta_1000);
        Command.Parameters.AddWithValue("@sor_1000", _sor_1000);
        Command.Parameters.AddWithValue("@ior_1000", _ior_1000);
        Command.Parameters.AddWithValue("@uar_1000", _uar_1000);
        Command.Parameters.AddWithValue("@far_1000", _far_1000);
        Command.Parameters.AddWithValue("@dip_1000", _dip_1000);
        Command.Parameters.AddWithValue("@uua_1000", _uua_1000);
        Command.Parameters.AddWithValue("@fua_1000", _fua_1000);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: cod_1000
    public void Cargar_cod_1000(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.relpag_1000 WHERE cod_1000 = '" + _cod_1000 + "'";

        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1000 = (int)dr["ide_1000"];

            if (AsignarPropiedades)
            {
                cod_1000 = (string)dr["cod_1000"];
                den_1000 = (string)dr["den_1000"];
                iba_1000 = (string)dr["iba_1000"];
                bic_1000 = (string)dr["bic_1000"];
                cta_1000 = (string)dr["cta_1000"];
                sor_1000 = (string)dr["sor_1000"];
                ior_1000 = (string)dr["ior_1000"];
                uar_1000 = (int)dr["uar_1000"];
                far_1000 = dr["far_1000"].ToString();
                dip_1000 = (string)dr["dip_1000"];
                uua_1000 = (int)dr["uua_1000"];
                fua_1000 = dr["fua_1000"].ToString();
            }
        }
        else
        {
            ide_1000 = 0;
        }

        dr.Close();
    }

    #endregion

}
using System;
using System.Data.SqlClient;

public class Relpag_1005
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1005;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private string _cod_1005;                                       // 2    
    private string _den_1005;                                       // 3    
    private string _cns_1005;                                       // 4    
    private int _uar_1005;                                          // 5    
    private string _far_1005;                                       // 6    
    private string _dip_1005;                                       // 7    
    private int _uua_1005;                                          // 8    
    private string _fua_1005;                                       // 9    

    #endregion

    #region Propiedades

    public int ide_1005
    {
        get { return _ide_1005; }
        set { _ide_1005 = value; }
    }
    public string cod_1005
    {
        get { return _cod_1005; }
        set { _cod_1005 = value; }
    }
    public string den_1005
    {
        get { return _den_1005; }
        set { _den_1005 = value; }
    }
    public string cns_1005
    {
        get { return _cns_1005; }
        set { _cns_1005 = value; }
    }
    public int uar_1005
    {
        get { return _uar_1005; }
        set { _uar_1005 = value; }
    }
    public string far_1005
    {
        get { return _far_1005; }
        set { _far_1005 = value; }
    }
    public string dip_1005
    {
        get { return _dip_1005; }
        set { _dip_1005 = value; }
    }
    public int uua_1005
    {
        get { return _uua_1005; }
        set { _uua_1005 = value; }
    }
    public string fua_1005
    {
        get { return _fua_1005; }
        set { _fua_1005 = value; }
    }

    #endregion

    #region Constructores

    public Relpag_1005(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1005 = 0;
        _cod_1005 = "";
        _den_1005 = "";
        _cns_1005 = "";
        _uar_1005 = 0;
        _far_1005 = "";
        _dip_1005 = "";
        _uua_1005 = 0;
        _fua_1005 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = "INSERT INTO " + _BaseDatos + ".dbo.relpag_1005 (" +
        "  cod_1005" + ", den_1005" + ", cns_1005" + ", uar_1005" + ", far_1005" + ", dip_1005" + ", uua_1005" + ", fua_1005" +
        ") OUTPUT INSERTED.ide_1005 VALUES (" +
        "  @cod_1005" + ", @den_1005" + ", @cns_1005" + ", @uar_1005" + ", @far_1005" + ", @dip_1005" + ", @uua_1005" + ", @fua_1005" +
         ")"
        };

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_1005", _cod_1005);
        Command.Parameters.AddWithValue("@den_1005", _den_1005);
        Command.Parameters.AddWithValue("@cns_1005", _cns_1005);
        Command.Parameters.AddWithValue("@uar_1005", _uar_1005);
        Command.Parameters.AddWithValue("@far_1005", _far_1005);
        Command.Parameters.AddWithValue("@dip_1005", _dip_1005);
        Command.Parameters.AddWithValue("@uua_1005", _uua_1005);
        Command.Parameters.AddWithValue("@fua_1005", _fua_1005);

        Command.Connection = _sqlCon;
        _ide_1005 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.relpag_1005 WHERE ide_1005 = " + _ide_1005;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1005 = (int)dr["ide_1005"];

            if (AsignarPropiedades)
            {
                cod_1005 = (string)dr["cod_1005"];
                den_1005 = (string)dr["den_1005"];
                cns_1005 = (string)dr["cns_1005"];
                uar_1005 = (int)dr["uar_1005"];
                far_1005 = dr["far_1005"].ToString();
                dip_1005 = (string)dr["dip_1005"];
                uua_1005 = (int)dr["uua_1005"];
                fua_1005 = dr["fua_1005"].ToString();
            }
        }
        else
        {
            ide_1005 = 0;
        }

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = "DELETE FROM " + _BaseDatos + ".dbo.relpag_1005 WHERE ide_1005 = " + _ide_1005
        };

        Command.ExecuteNonQuery();
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.relpag_1005 SET");
        strSQL += String.Concat(" cod_1005 = @cod_1005");
        strSQL += String.Concat(", den_1005 = @den_1005");
        strSQL += String.Concat(", cns_1005 = @cns_1005");
        strSQL += String.Concat(", dip_1005 = @dip_1005");
        strSQL += String.Concat(", uua_1005 = @uua_1005");
        strSQL += String.Concat(", fua_1005 = @fua_1005");
        strSQL += String.Concat(" WHERE ide_1005 = ", _ide_1005);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_1005", _cod_1005);
        Command.Parameters.AddWithValue("@den_1005", _den_1005);
        Command.Parameters.AddWithValue("@cns_1005", _cns_1005);
        Command.Parameters.AddWithValue("@dip_1005", _dip_1005);
        Command.Parameters.AddWithValue("@uua_1005", _uua_1005);
        Command.Parameters.AddWithValue("@fua_1005", _fua_1005);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: cod_1005
    public void Cargar_cod_1005(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.relpag_1005 WHERE cod_1005 = '" + _cod_1005 + "'";

        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1005 = (int)dr["ide_1005"];

            if (AsignarPropiedades)
            {
                cod_1005 = (string)dr["cod_1005"];
                den_1005 = (string)dr["den_1005"];
                cns_1005 = (string)dr["cns_1005"];
                uar_1005 = (int)dr["uar_1005"];
                far_1005 = dr["far_1005"].ToString();
                dip_1005 = (string)dr["dip_1005"];
                uua_1005 = (int)dr["uua_1005"];
                fua_1005 = dr["fua_1005"].ToString();
            }
        }
        else
        {
            ide_1005 = 0;
        }

        dr.Close();
    }

    #endregion

}
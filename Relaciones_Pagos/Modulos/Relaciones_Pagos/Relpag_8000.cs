using System;
using System.Data;
using System.Data.SqlClient;

public class Relpag_8000
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_8000;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private short _eje_8000;                                        // 2    
    private int? _ide_1005;                                         // 3    
    private int _uar_8000;                                          // 4    
    private string _far_8000;                                       // 5    
    private string _dip_8000;                                       // 6    
    private int _uua_8000;                                          // 7    
    private string _fua_8000;                                       // 8    

    #endregion

    #region Propiedades

    public int ide_8000
    {
        get { return _ide_8000; }
        set { _ide_8000 = value; }
    }
    public short eje_8000
    {
        get { return _eje_8000; }
        set { _eje_8000 = value; }
    }
    public int? ide_1005
    {
        get { return _ide_1005; }
        set { _ide_1005 = value; }
    }
    public int uar_8000
    {
        get { return _uar_8000; }
        set { _uar_8000 = value; }
    }
    public string far_8000
    {
        get { return _far_8000; }
        set { _far_8000 = value; }
    }
    public string dip_8000
    {
        get { return _dip_8000; }
        set { _dip_8000 = value; }
    }
    public int uua_8000
    {
        get { return _uua_8000; }
        set { _uua_8000 = value; }
    }
    public string fua_8000
    {
        get { return _fua_8000; }
        set { _fua_8000 = value; }
    }

    #endregion

    #region Constructores

    public Relpag_8000(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_8000 = 0;
        _eje_8000 = 0;
        _ide_1005 = null;
        _uar_8000 = 0;
        _far_8000 = "";
        _dip_8000 = "";
        _uua_8000 = 0;
        _fua_8000 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = "INSERT INTO " + _BaseDatos + ".dbo.relpag_8000 (" +
        "  eje_8000" + ", ide_1005" + ", uar_8000" + ", far_8000" + ", dip_8000" + ", uua_8000" + ", fua_8000" +
        ") OUTPUT INSERTED.ide_8000 VALUES (" +
        "  @eje_8000" + ", @ide_1005" + ", @uar_8000" + ", @far_8000" + ", @dip_8000" + ", @uua_8000" + ", @fua_8000" +
         ")"
        };

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_8000", _eje_8000);

        if (_ide_1005 == null)
            Command.Parameters.Add("@ide_1005", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ide_1005", _ide_1005);

        Command.Parameters.AddWithValue("@uar_8000", _uar_8000);
        Command.Parameters.AddWithValue("@far_8000", _far_8000);
        Command.Parameters.AddWithValue("@dip_8000", _dip_8000);
        Command.Parameters.AddWithValue("@uua_8000", _uua_8000);
        Command.Parameters.AddWithValue("@fua_8000", _fua_8000);

        Command.Connection = _sqlCon;
        _ide_8000 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.relpag_8000 WHERE ide_8000 = " + _ide_8000;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_8000 = (int)dr["ide_8000"];

            if (AsignarPropiedades)
            {
                eje_8000 = (Int16)dr["eje_8000"];
                ide_1005 = DBNull.Value.Equals(dr["ide_1005"]) ? (int?)null : (int?)dr["ide_1005"];
                uar_8000 = (int)dr["uar_8000"];
                far_8000 = dr["far_8000"].ToString();
                dip_8000 = (string)dr["dip_8000"];
                uua_8000 = (int)dr["uua_8000"];
                fua_8000 = dr["fua_8000"].ToString();
            }
        }
        else
            ide_8000 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = "DELETE FROM " + _BaseDatos + ".dbo.relpag_8000 WHERE ide_8000 = " + _ide_8000
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = string.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.relpag_8000 SET");
        strSQL += string.Concat(" eje_8000 = @eje_8000");
        strSQL += string.Concat(", ide_1005 = @ide_1005");
        strSQL += string.Concat(", dip_8000 = @dip_8000");
        strSQL += string.Concat(", uua_8000 = @uua_8000");
        strSQL += string.Concat(", fua_8000 = @fua_8000");
        strSQL += string.Concat(" WHERE ide_8000 = ", _ide_8000);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_8000", _eje_8000);

        if (_ide_1005 == null)
            Command.Parameters.Add("@ide_1005", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ide_1005", _ide_1005);

        Command.Parameters.AddWithValue("@dip_8000", _dip_8000);
        Command.Parameters.AddWithValue("@uua_8000", _uua_8000);
        Command.Parameters.AddWithValue("@fua_8000", _fua_8000);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion

}
﻿using ClaseIntsa.Clases;
using ClaseIntsa.DataBase;
using ClaseIntsa.Formularios;
using ClaseIntsa.Funciones;
using ClaseIntsa.Propiedades;
using DatosGenerales.Formularios;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;


namespace Relaciones_Pagos
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            /// <summary>
            /// Propiedades seteadas 
            /// </summary>
            Variables_Globales.VersionAplicativo = 20220101;
            Variables_Globales.CodigoAplicativo = "168";
            Variables_Globales.NombreAplicativo = "RELACIONES DE PAGOS";
            Variables_Globales.PermitirFavoritos = true;

            string hashMD5;
            hashMD5 = Funciones_Globales.ObtenerMD5(Application.ExecutablePath);

            if ((args.Length != 0 && hashMD5 == args[0]) || Debugger.IsAttached)
            {
                bool llOk = true;
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                //LA INSTACIA SE LEE DEL ARCHIVO APP.CONFIG NUEVO "INI"
                //COJO LA INSTANCIA Y SE LO PASO A LAS VARIABLE GLOBALES
                try
                {
                    if (File.Exists(Path.Combine(Application.StartupPath, "Relaciones_Pagos.exe.config")))
                    {
                        AppConfig oCon = new AppConfig(Application.ExecutablePath);
                        Variables_Globales.InstanciaBD = oCon.Leer("Relaciones_Pagos", "servidor");
                    }
                    else
                    {
                        llOk = false;
                        frmMessageBox.Mostrar("NO SE ENCUENTRA EL ARCHIVO .CONFIG");
                    }


                }
                catch (Exception)
                {
                    llOk = false;
                    frmMessageBox.Mostrar("SE HA PRODUCIDO UN ERROR AL CARGAR LA CONFIGURACION DEL ARCHIVO .CONFIG");
                }

                if (llOk)
                {
                    Application.Run(new frm_Login());

                    if (Variables_Globales.LoginOK)
                    {
                        Funciones_Inicio.Bases_Datos_Aplicativo();

                        //SI EXISTE LA BASE DE DATOS
                        if (claseBaseDatos.existe(Variables_Globales.InstanciaBD, Variables_Globales.NombreBaseDatosAplicativo))
                        {
                            //SI NO ESTÁ ACTUALIZANDO LA BASE DE DATOS
                            if (!claseBaseDatos.solicitaBloqueoBD(Variables_Globales.InstanciaBD, Variables_Globales.NombreBaseDatosAplicativo))
                            {

                                long VersionBBDD = claseBaseDatos.solicitaVersionBD(Variables_Globales.InstanciaBD, Variables_Globales.NombreBaseDatosAplicativo);

                                // SI LA VERSION DE LA BASE DE DATOS ES MENOR QUE LA DEL APLICATIVO.. ACTUALIZO LA BASE DE DATOS
                                if (VersionBBDD < Variables_Globales.VersionAplicativo)
                                {
                                    //TODO: DE MOMENTO NO LO BLOQUEO POR SI AKA...
                                    claseBaseDatos.ActualizarPropiedadBD(Variables_Globales.InstanciaBD, Variables_Globales.NombreBaseDatosAplicativo, "bloqueado", "si");
                                    //ABRO CLASE DEL APLICATIVO DONDE SE ENCUENTRAN LAS ACTUALIZACIONES
                                    BaseDatos.Actualizar(Variables_Globales.InstanciaBD, Variables_Globales.NombreBaseDatosAplicativo, VersionBBDD);
                                    claseBaseDatos.ActualizarPropiedadBD(Variables_Globales.InstanciaBD, Variables_Globales.NombreBaseDatosAplicativo, "bloqueado", "no");
                                }

                                Variables_Globales.CadenaConexionAplicativo = "Data Source=" + Variables_Globales.InstanciaBD + "; Initial Catalog = " + Variables_Globales.NombreBaseDatosAplicativo + "; User ID = intsa; Password = +sire2011";

                                //Variables_Globales.CadenaConexionDatosGenerales = "Data Source=" + Variables_Globales.InstanciaBD + "; Initial Catalog = Generales; User ID = intsa; Password = +sire2011";
                                Variables_Globales.CadenaConexionDatosComunes = "Data Source=" + Variables_Globales.InstanciaBD + "; Initial Catalog = Comunes; User ID = intsa; Password = +sire2011";

                                Propiedades_Aplicativo.Ejercicio_Trabajo = Funciones_Aplicativo.Ejercicio_Relpag_8000(Variables_Globales.CadenaConexionAplicativo, 1);
                                Funciones_Inicio.Bases_Datos_Auxiliares();

                                Relpag_001 Actualiza = new Relpag_001();
                                Actualiza.ShowDialog();

                                Inicio Inicial = new Inicio
                                {
                                    Text = Variables_Globales.TextTituloFormMDI.Replace("Eje: 0", "Eje: " + Propiedades_Aplicativo.Ejercicio_Trabajo.ToString())
                                };

                                Application.Run(Inicial);
                            }
                            else
                            {
                                frmMessageBox.Mostrar("LA BASE DE DATOS '" + Variables_Globales.NombreBaseDatosAplicativo + "' ESTÁ SIENDO ACTUALIZA EN OTRO EQUIPO. PRUEBE DE NUEVO EN UNOS SEGUNDOS");
                            }
                        }
                        else
                        {
                            frmMessageBox.Mostrar("NO EXISTE LA BASE DE DATOS: " + Variables_Globales.NombreBaseDatosAplicativo);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No se puede ejecutar fuera del Sistema Integrado de Recursos Empresariales(SIRE).");
            }
        }
    }
}

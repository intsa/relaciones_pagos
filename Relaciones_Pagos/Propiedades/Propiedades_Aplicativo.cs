﻿namespace Relaciones_Pagos
{
    public static class Propiedades_Aplicativo
    {
        /// <summary>
        /// Ejercicio de trabajo
        /// </summary>
        public static short Ejercicio_Trabajo { get; set; } = 0;

        /// <summary>
        /// Nombre base de datos históricos contabilidad (Adm_Loc_Con_His_EEEEEE)
        /// </summary>
        public static string Bdd_Historico { get; set; } = string.Empty;

        /// <summary>
        /// Nombre base de datos comunes contabilidad (Adm_Loc_Con_Com_EEEEEE)
        /// </summary>
        public static string Bdd_Comunes { get; set; } = string.Empty;

        /// <summary>
        /// Nombre base de datos contabilidad (Adm_Loc_Con_EEEEEE_AAAA)
        /// </summary>
        public static string Bdd_Contabilidad { get; set; } = string.Empty;

        /// <summary>
        /// Versión base de datos comunes contabilidad
        /// </summary>
        public static long Version_Historico_Conta { get; set; } = 20210101;

        /// <summary>
        /// Versión base de datos comunes contabilidad
        /// </summary>
        public static long Version_Comunes_Conta { get; set; } = 20210101;

        /// <summary>
        /// Versión base de datos contabilidad
        /// </summary>
        public static long Version_Contabilidad { get; set; } = 20210101;

        /// <summary>
        /// Versión base de datos comunes
        /// </summary>
        public static long Version_Comunes { get; set; } = 20210101;

        /// <summary>
        /// Versión base de datos comunes
        /// </summary>
        public static long Version_Auxiliares { get; set; } = 20210101;

        /// <summary>
        /// Separador cuenta mayor y sufijo cuenta auxiliar
        /// </summary>
        public static string Separador { get; set; } = string.Empty;

    }
}